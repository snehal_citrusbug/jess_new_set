(function($) {

	"use strict"

	$('#back-to-top').on("click", function() {
		// When arrow is clicked
		$('body,html').animate({
			scrollTop : 0 // Scroll to top of body
		},800);
		return false;
	});

	
	
})(jQuery);
// Select all links with hashes
/*$('a.down-arrow[href*="#"]')
  // Remove links that don't actually link to anything
  .not('[href="#"]')
  .not('[href="#0"]')
  .click(function(event) {
    // On-page links
    if (
      location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
      && 
      location.hostname == this.hostname
    ) {
      // Figure out element to scroll to
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      // Does a scroll target exist?
      if (target.length) {
        // Only prevent default if animation is actually gonna happen
        event.preventDefault();
        $('html, body').animate({
          scrollTop: target.offset().top-130
        }, 1000, function() {
          // Callback after animation
          // Must change focus!
          var $target = $(target);
          $target.focus();
          if ($target.is(":focus")) { // Checking if the target was focused
            return false;
          } else {
            $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
            $target.focus(); // Set focus again
          };
        });
      }
    }
  });*/

  $(window).scroll(function(){
    if ($(window).scrollTop() >= 50) {
       $('.header-div').addClass('fixed-header');
       $('.logo-div').addClass('logo-div-100');
       $('.navbar').addClass('navbar-100');
       $('.right-div').addClass('right-div-100');
       $('.dropdown-menu').addClass('mt-22');
       $('.dropdown-menu-fullmenu').addClass('mt25');
    }
    else {
       $('.header-div').removeClass('fixed-header');
       $('.logo-div').removeClass('logo-div-100');
       $('.navbar').removeClass('navbar-100');
       $('.right-div').removeClass('right-div-100');
       $('.dropdown-menu').removeClass('mt-22');
       $('.dropdown-menu-fullmenu').removeClass('mt25');

    }

    
});

function openNav() {
  document.getElementById("mySidenav").style.width = "100%";   
}

/*
function closewrapper(){
document.getElementById("mySidenav").style.width = "0";
}
*/

/* Set the width of the side navigation to 0 */
function closeNav() {
document.getElementById("mySidenav").style.width = "0";
} 


$(document).ready(function() {
  /*"use strict";

  //PRE LOADING
  $('#status').fadeOut();
  $('#preloader').delay(350).fadeOut('slow');
  $('body').delay(350).css({
      'overflow': 'visible'
  });*/

});