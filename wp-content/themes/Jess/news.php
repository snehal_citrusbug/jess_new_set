<?php
/**
 * Template Name: News
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

$newsPost = get_post($post = 128);
//News list
$args = array(
    'order'    => 'DESC',
    'post_type'   => 'latest_news',
    'posts_per_page'   => 500,
	'offset'           => 0,
);
$news_posts_array = get_posts( $args );
 ?>

    <div class="middle-container">
    <div class="banner-div clearfix">
        <div class="fluid_container-all clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="top-head clearfix">
                            <h1> News </h1>
                            <div class="white-line-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-div clearfix">
                           <?php
                            echo do_shortcode( '[breadcrumb]' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .fluid_container -->

    </div><!-- End of banner-div  -->

    <div class="general-container-div clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                <?php foreach ($news_posts_array as $postkey => $post) {?>
                    <div class="dated">Posted: <?php echo date_format(date_create($post->post_date), "d-m-Y"); ?></div>
                        <h3 class="h3-title-div"><?php echo $post->post_title; ?>
                            <div class="black-line"></div>
                        </h3>
                    <p><?php echo apply_filters('the_content', substr($post->post_content, 0, 300)); ?>
                    <a href="<?php echo get_permalink($post->ID); ?>" class="read-link">Read more</a></p>
                <br><br>
                <?php }?>

                </div>
            </div>
        </div>
    </div><!-- End of general-container-div -->

    <div class="bottom-container-div1 clearfix">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 clearfix">

                    <div class="bottomimglist-div clearfix">
                        <div class="row">

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                            <?php
                                                echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Powerstroke</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                             <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Cummins</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                             <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Duramax</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                            <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">MERCHANDISE</h2>
                                    </div>

                                </a>
                            </div>

                        </div>
                    </div>

                </div>





            </div>
        </div>
    </div>

</div><!-- end of middle-container -->

<?php get_footer();
