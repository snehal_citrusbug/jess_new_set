<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$postId = get_the_ID();
$postType = get_post_type($postId);
$postTitle = get_the_title($postId);


?>
<div class="middle-container">

    <div class="banner-div clearfix">

        <div class="fluid_container-all clearfix">
             <?php if($postType != 'product')
            { ?>
                <div class="container">
                <div class="row">
            <?php }
            ?>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="top-head clearfix">
                            <h1> Product Details </h1>
                            <div class="white-line-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-div clearfix">
                            <a href="<?php echo site_url(); ?>"><p>Home</p></a>
                                <span class="gt-span">&gt;</span>
                            <a href="<?php echo site_url(); ?>/shop"><p>Product</p></a>
                                <span class="gt-span">&gt;</span>
                            <p>Product Details</p>
                        </div>
                    </div>
            <?php if($postType != 'product')
            { ?>
                </div>
            </div>
            <?php }
            ?>
        </div><!-- .fluid_container -->

    </div><!-- End of banner-div  -->
<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<?php
$layouts  = isset($_GET['layout']) ? $_GET['layout'] : jess_fnc_theme_options('woocommerce-single-content-layout','1');

if(!empty($layouts) && is_file(get_template_directory().'/woocommerce/single-product/layouts/layout_'.$layouts.'.php')){
	wc_get_template_part( 'woocommerce/single-product/layouts/layout_'.$layouts);
}else{
	wc_get_template_part( 'woocommerce/single-product/layouts/layout_1');
}
?>
<?php //do_action( 'woocommerce_after_single_product' ); ?>

</div><!-- end of content area -->