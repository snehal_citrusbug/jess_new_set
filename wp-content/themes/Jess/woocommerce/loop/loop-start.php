<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.3.1
 */
?>
<div class="middle-container">

    <div class="banner-div clearfix">

        <div class="fluid_container-all clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="top-head clearfix">
                            <h1> Shop </h1>
                            <div class="white-line-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-div clearfix">
                            <?php
                                //echo do_shortcode( '[breadcrumb]' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .fluid_container -->

    </div><!-- End of banner-div  -->

<?php

global $wp_registered_sidebars, $wp_registered_widgets;
 //echo '<pre>'; print_r($wp_registered_sidebars); exit;
// dynamic_sidebar('cs-1');
// dynamic_sidebar('cs-2');
// dynamic_sidebar('cs-3');

?>
 <div class="general-container-div clearfix">
    <div class="container">
        <div class="row">
        <div class="col-md-12 col-sm-12 clearfix">
            <div class="category-left box_1 sidebar-widget top-line">
                <h3 class="sidebar-title">BROWSE BY VEHICLE</h3>
                <div class="bottom-black-line"></div>
                <div class="filter-div clearfix">

                    <div class="row">
                        <div class="col-md-3 col-sm-4 p-0 clearfix">
                            <div class="form-group">
                            <?php dynamic_sidebar('cs-1'); ?>
                                <!-- <select class="form-control">
                                    <option>Make</option>
                                    <option>Chevrolet</option>
                                    <option>Dodge</option>
                                    <option>Ford</option>
                                    <option>GMC</option>
                                </select> -->
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-4 p-0 clearfix">
                            <div class="form-group">
                            <?php dynamic_sidebar('cs-2'); ?>
                                <!-- <select class="form-control">
                                    <option>Model</option>
                                    <option value="F-350 Super Duty">F-350 Super Duty</option>
                                    <option value="Ram 2500">Ram 2500</option>
                                    <option value="Ram 3500">Ram 3500</option>
                                    <option value="Sierra 2500 HD">Sierra 2500 HD</option>
                                    <option value="Sierra 3500 HD">Sierra 3500 HD</option>
                                    <option value="Silverado 3500 HD">Silverado 3500 HD</option>
                                </select> -->
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-4 p-0 clearfix">
                            <div class="form-group">
                            <?php dynamic_sidebar('cs-3'); ?>
                                <!-- <select class="form-control">
                                    <option>Year</option>
                                    <option>2016</option>
                                    <option>2017</option>
                                    <option>2018</option>
                                </select> -->
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-4 p-0 clearfix">
                            <div class="btngroup-div clearfix">
                                <button class="btn btn-custom-red"> Filter </button>
                                <button class="btn btn-custom-red"> Clear </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-4 clearfix">

            <div class="category-left sidebar-widget top-line c2">

                <div class="panel-group wrap" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    All Products (<?php echo do_shortcode('[product_count]'); ?>)
                                    <div class="bottom-black-line"></div>
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                <div class="category-list">
                                <?php //dynamic_sidebar('cs-4'); ?>
                                    <?php dynamic_sidebar('smartslider_area_1'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end of panel -->
                </div>
            </div>

        </div><!-- end of col-3 -->

        <div class="col-md-9 col-sm-8 clearfix">
            <div class="category-content clearfix">

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="clearfix">
                                <h3 class="h3title clearfix">All Products</h3>
                                <div class="bottom-black-line"></div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <div class="filter-group clearfix">
                                <div class="input-group pull-left ">
                                <?php ps_selectbox();?>
                                    <!-- <label class="input-group-addon" for="input-limit">Show:</label> -->
                                    <!-- <select class="form-control w60">
                                        <option selected="selected">12</option>
                                        <option >25</option>
                                        <option >50</option>
                                        <option >75</option>
                                        <option >100</option>
                                    </select> -->
                                </div>


                                <div class="input-group pull-right ">
                                    <label class="input-group-addon" for="input-limit">Sort By:</label>
                                     <?php woocommerce_catalog_ordering();?>
                                    <!-- <select class="form-control w120">
                                        <option selected="selected">Default  </option>
                                        <option >25</option>
                                        <option >50</option>
                                        <option >75</option>
                                        <option >100</option>
                                    </select> -->
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row">