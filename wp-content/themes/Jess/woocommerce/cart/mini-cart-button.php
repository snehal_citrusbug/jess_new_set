<?php   global $woocommerce; ?>
<div class="opal-topcart">
    <div id="cart" class="dropdown">
        <a class="dropdown-toggle mini-cart btn btn-primary" data-toggle="dropdown" aria-expanded="true" role="button" aria-haspopup="true" data-delay="0" href="#" title="<?php esc_html_e('View your shopping cart', 'burgerslap'); ?>">
            <i class="fa fa-shopping-basket gfg"></i>
            <span class="text-cart"><?php esc_html_e('Your Cart', 'burgerslap'); ?></span>
            <span class="mini-cart-items">                
        	   <?php echo sprintf( '%d', $woocommerce->cart->cart_contents_count );?>
            </span>
        </a>            
        <div class="dropdown-menu"><div class="widget_shopping_cart_content">
            <?php woocommerce_mini_cart(); ?>
        </div></div>
    </div>
</div> 
