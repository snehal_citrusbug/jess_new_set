<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

    // echo '<pre>'; print_r($product->slug); exit;
    $woocommerce_loop['loop'] = 0;

    $woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 3 );
    // Ensure visibility
    if ( empty( $product ) || ! $product->is_visible() ) {
        return;
    }

	// Extra post classes
	// $classes = array();
	// $classes[] = 'product-wrapper ';

	// $columns = 9/$woocommerce_loop['columns'];
	// $classes[] = 'col-lg-'.$columns.' col-md-'.$columns.' col-sm-6 grid';
    ?>

    <!-- <div <?php //wc_product_class( $classes ); ?>>
            <?php //wc_get_template_part( 'content', 'product-inner' ); ?>
    </div> -->
    <div class="border-box">
        <div class="col-md-4 col-sm-6 clearfix">
            <div class="intro intro-1 clearfix">

                <div class="clearfix">
                    <a href="<?php echo site_url().'/product/'.$product->slug; ?>">
                    <?php do_action('woocommerce_before_shop_loop_item_title');  ?>
                    </a>
                    <!-- <div class="image"> <img src="images/product-1.jpg" alt="product" class="img-responsive"> </div> -->
                </div>
                <div class="clearfix">
                    <div class="caption">
                        <h3 ><?php do_action( 'woocommerce_shop_loop_item_title' ); ?></h3>
                        <!-- <p>Item #: 74609-01A</p> -->
                        <h2><?php do_action( 'woocommerce_after_shop_loop_item_title' ); ?></h2>
                        <?php do_action( 'woocommerce_after_shop_loop_item' ); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>





