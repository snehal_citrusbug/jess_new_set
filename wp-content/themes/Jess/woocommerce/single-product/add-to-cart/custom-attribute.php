<ul class="custom-attribute" data-attribute="<?php echo esc_attr( $attribute_name ); ?>">
	<?php
	if ( $options ):

		foreach ( $options as $term ) :
			$label = $term->name;
		?>
			<li class="<?php if ( $term->slug == $selected ) echo 'selected'; ?>">
				<a data-value="<?php echo esc_attr( $term->slug ); ?>" href="javascript:void(0)" title="<?php echo esc_attr($label); ?>">
					<?php echo esc_html( $label ); ?>
				</a>
			</li>
		<?php
		endforeach;

	endif;
	?>
</ul>
