<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.3.1
 */

/**
* Layout Left Thumbnail
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $product;

?>

<div class="general-container-div clearfix">
    <div class="container">
        <div class="row">

        <div class="col-md-12 col-sm-12 clearfix">
            <div class="category-content clearfix">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="clearfix">
                            <h3 class="h3title clearfix">Products Details</h3>
                            <div class="bottom-black-line"></div>
                        </div>
                    </div>

                </div>

                <div class="row">

                    <div class="border-box detail-box">
                        <div class="col-md-12 col-sm-12 clearfix">
                            <div class="intro intro-1 clearfix">

                                <div class="col-md-4 col-sm-4 clearfix">
                                    <div class="image">
                                    <?php do_action( 'woocommerce_before_single_product_summary' ); ?>
                                    <!-- <img src="images/product-1.jpg" alt="product" class="img-responsive"> -->
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8 clearfix">
                                    <div class="caption">
                                        <h3 ><?php woocommerce_template_single_title(); ?></h3>
                                        <!-- <p >Item #: 74609-01A</p> -->

                                       <?php
                                        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
                                        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
                                        add_action( 'woocommerce_single_product_summary','woocommerce_template_single_price', 20 ); ?>

                                        <?php do_action('woocommerce_single_product_summary'); ?>

                                        <div class="social-icons">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="#" class="btn btn-primary btn-facebook"><i class="fas fa-thumbs-up"></i><span> Like 0 </span> </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn-primary btn-facebook"><span> Share </span> </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn-primary btn-twitter"><i class="fab fa-twitter"></i> <span> Tweet </span> </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn-primary btn-google"><i class="fab fa-google-plus-g"></i><span> Share </span> </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="btn btn-primary btn-linkedin"><i class="fab fa-linkedin-in"></i><span> Share </span> </a>
                                                </li>

                                            </ul>

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12 plr-0 clearfix">
                                    <div class="details-container">

                                        <div class="panel-group" id="accordion-2">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h4 class="panel-title">
                                                        <a data-toggle="collapse" data-parent="#accordion-2" href="#collapse-description" aria-expanded="true" class="collapsed">Product Description</a>
                                                        <div class="bottom-black-line line-bottom-1"></div>
                                                    </h4>
                                                </div>
                                                <div id="collapse-description" class="panel-collapse collapse in" aria-expanded="true">
                                                    <div class="panel-body" itemprop="description">

                                                        <div class="panel-container clearfix">
                                                            <!-- <ul class="clearfix">
                                                                <li class="c-40"><h3>BED EXTENDER & COMPONENTS COLOR:</h3></li>
                                                                <li class="c-60"><p>Black</p></li>
                                                            </ul> -->
                                                            <ul class="clearfix">
                                                                <li class="c-40"><h3>Extra Info:</h3></li>
                                                                <li class="c-60"> <?php echo
                                                                ($product->description != null) ?
                                                                 apply_filters('the_content',   $product->description ) : '<p>-</p>'; ?>  </li>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

    </div>

</div><!-- End of general-container-div -->

<div class="bottom-container-div1 clearfix">
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-sm-12 clearfix">

                <div class="bottomimglist-div clearfix">
                    <div class="row">

                        <div class="col-md-3 col-sm-3 clearfix">
                            <a href="#">
                                <div class="box-div clearfix">
                                    <div class="heading-div">
                                        <?php
                                            echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                        ?>
                                        <div class="overlay">
                                            <div class="content-div">
                                                <p class="title-p">Click Here</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="h2-bottom clearfix">Powerstroke</h2>
                                </div>

                            </a>
                        </div>

                        <div class="col-md-3 col-sm-3 clearfix">
                            <a href="#">
                                <div class="box-div clearfix">
                                    <div class="heading-div">
                                            <?php
                                            echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                        ?>
                                        <div class="overlay">
                                            <div class="content-div">
                                                <p class="title-p">Click Here</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="h2-bottom clearfix">Cummins</h2>
                                </div>

                            </a>
                        </div>

                        <div class="col-md-3 col-sm-3 clearfix">
                            <a href="#">
                                <div class="box-div clearfix">
                                    <div class="heading-div">
                                            <?php
                                            echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                        ?>
                                        <div class="overlay">
                                            <div class="content-div">
                                                <p class="title-p">Click Here</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="h2-bottom clearfix">Duramax</h2>
                                </div>

                            </a>
                        </div>

                        <div class="col-md-3 col-sm-3 clearfix">
                            <a href="#">
                                <div class="box-div clearfix">
                                    <div class="heading-div">
                                        <?php
                                            echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                        ?>
                                        <div class="overlay">
                                            <div class="content-div">
                                                <p class="title-p">Click Here</p>
                                            </div>
                                        </div>
                                    </div>
                                    <h2 class="h2-bottom clearfix">MERCHANDISE</h2>
                                </div>

                            </a>
                        </div>

                    </div>
                </div>

            </div>





        </div>
    </div>
</div>


<!-- <div  id="product-<?php //the_ID(); ?>" <?php //wc_product_class(); ?>>
	<div class="product-info clearfix single-layout1">
		<div class="row">

			<div class="col-md-6 col-sm-6 column">
				<div class="single-product-summary clearfix">
				<?php //woocommerce_template_single_title(); ?>
				<?php
					/**
					 * woocommerce_single_product_summary hook
					 *
					 * @hooked woocommerce_template_single_title - 5
					 * @hooked woocommerce_template_single_rating - 10
					 * @hooked woocommerce_template_single_price - 10
					 * @hooked woocommerce_template_single_excerpt - 20
					 * @hooked woocommerce_template_single_add_to_cart - 30
					 * @hooked woocommerce_template_single_meta - 40
					 * @hooked woocommerce_template_single_sharing - 50
					 */
					//remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_price', 10 );
					//remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_excerpt', 20 );
					//add_action( 'woocommerce_single_product_summary','woocommerce_template_single_price', 20 );
					//add_action( 'woocommerce_single_product_summary','woocommerce_template_single_excerpt', 12 );
					//do_action( 'woocommerce_single_product_summary' );
				?>
				</div>
			</div>


			<div class="col-md-6 col-sm-6">
			<?php
				/**
				 * woocommerce_before_single_product_summary hook
				 *
				 * @hooked woocommerce_show_product_sale_flash - 10
				 * @hooked woocommerce_show_product_images - 20
				 */
				//do_action( 'woocommerce_before_single_product_summary' );
			?>
			</div>
		</div>
	</div>
	<?php
		/**
		 * woocommerce_after_single_product_summary hook
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		//do_action( 'woocommerce_after_single_product_summary' );
	?>

</div> -->