<?php
/**
 * Description tab
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version       3.3.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$heading = esc_html( apply_filters( 'jess_woocommerce_product_features_heading', esc_html__( 'Nutritions', 'burgerslap' ) ) );
$features = get_post_meta( get_the_ID(), 'product_information', true);
?>

<?php if ( $heading ): ?>
  <h2><?php echo trim( $heading ); ?></h2>
<?php endif; ?>

<?php echo trim( $features ); ?>
