<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$aboutUsPost = get_post($post = 44);
?>

<?php
global $wp_registered_sidebars, $wp_registered_widgets;
// echo '<pre>'; print_r($wp_registered_sidebars['cs-1']); exit;

?>
<section class="part_finder_section clearfix">
    <div class="part_finder_div clearfix" >
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-4">
                    <h3>PARTS FINDER<span class="border-center"></span></h3>
                    <h2>SELECT YOUR VEHICLE</h2>
                </div>
                <div class="col-md-7 col-sm-7">
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group relative red-bg-select">
                        <?php echo dynamic_sidebar_html('pa_make');?>
                            <!-- <select class="form-control textbox-custom" placeholder="YEAR"><option>Year</option><option>2001</option><option>2000</option></select> -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group relative red-bg-select">
                        <?php echo dynamic_sidebar_html('pa_model'); ?>
                            <!-- <select class="form-control textbox-custom" placeholder="MAKE"><option>Make</option><option>Make name</option><option>Make name</option></select> -->
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="form-group relative red-bg-select">
                        <?php echo dynamic_sidebar_html('pa_years'); ?>
                            <!-- <select class="form-control textbox-custom" placeholder="Model"><option>Model</option><option>Model name</option><option>Model name</option></select> -->
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-1 ">
                    <button class="search-box attributeSearch"><i class="fas fa-search"></i></button>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="about_us_section" id="philosophy">
    <div class="about_us_div clearfix" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="h2-title">
                        <?php echo $aboutUsPost->post_title; ?>
                        <div class="black-line"></div>
                    </h2>

                    <div class="middle-center clearfix">

                        <div class="col-md-7 col-sm-6">
                            <?php echo apply_filters('the_content',$aboutUsPost->home_section_content); ?>
                        </div>

                        <div class="col-md-5 col-sm-6">
                            <?php
                                echo get_the_post_thumbnail( $post = $aboutUsPost->ID, array(),array('class' => 'img-responsive img-bottom-20','alt' => 'about us'));
                            ?>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div><!-- end of about_us_div -->
</section><!-- end of about_us_section -->

<section class="service_section clearfix">
    <div class="service_div clearfix" data-aos="fade-up" >
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="h2-title">
                        OUR SERVICES
                        <div class="black-line"></div>
                    </h2>
                    <div class="gallery_div clearfix">
                        <div class="col-md-4 col-sm-4 col-4">
                            <div class="our_service_div">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/gallery-1.png" class="img-responsive img-black">
                                <span class="overlay">
                                    <div class="text-middle">
                                        <h3>race tuner ford</h3>
                                        <a href="#">shop now</a>
                                    </div>
                                </span>
                            </div>

                                <div class="our_service_div">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/gallery-2.png" class="img-responsive img-black">
                                <span class="overlay">
                                    <div class="text-middle">
                                        <h3>angled cut smokers chrome</h3>
                                        <a href="#">shop now</a>
                                    </div>
                                </span>
                            </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-4 mobile-hidden">

                            <div class="our_service_div bg-red">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/gallery-3.png" class="img-responsive img-red">
                                <span class="overlay">
                                    <div class="text-middle">
                                        <h3>high performance motor mounts</h3>
                                        <a href="#">shop now</a>
                                    </div>
                                </span>
                            </div>

                        </div>

                        <div class="col-md-4 col-sm-4 col-4 desktop-hidden">

                            <div class="our_service_div bg-red">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/gallery-3-1.png" class="img-responsive img-red">
                                <span class="overlay">
                                    <div class="text-middle">
                                        <h3>high performance motor mounts</h3>
                                        <a href="#">shop now</a>
                                    </div>
                                </span>
                            </div>

                        </div>


                        <div class="col-md-4 col-sm-4 col-4">

                            <div class="our_service_div div-black">
                                <img src="<?php bloginfo('template_url'); ?>/assets/images/gallery-4.png" class="img-responsive img-red">
                                <span class="overlay2">
                                    <div class="text-middle">
                                        <h3>jess parts & accessories</h3>
                                        <a href="#">shop now</a>
                                    </div>
                                </span>
                            </div>

                        </div>

                    </div><!-- end of gallery -->

                </div>
            </div>
        </div>
    </div><!-- end of service_div -->
</section><!-- end of service_section -->

<?php $args = array(
        'posts_per_page'   => 20,
        'offset'           => 0,
        'orderby'          => 'date',
        'order'            => 'ASC',
        'post_type'        => 'partners',
        'post_status'      => 'publish',
        'suppress_filters' => true,
    );
    $partners_posts = get_posts( $args );
    if (count($partners_posts) > 0) {
?>

<section class="partner_section clearfix">
    <div class="partner_div clearfix" data-aos="fade-up">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="h2-title">
                        OUR PARTNERS
                        <div class="black-line"></div>
                    </h2>
                    <div class="slider-div">
                        <div class="owl-carousel owl-theme">
                        <?php foreach($partners_posts as $key => $partner) { ?>
                            <div class="item">
                                <div class="img-block-1">
                                <?php
                                if(get_the_post_thumbnail_url($partner->ID) != false ) {
                                    echo get_the_post_thumbnail($post = $partner->ID, array(), array('class' => 'img-responsive img-color'));
                                }
                                ?>
                                </div>
                            </div>
                        <?php
                            }
                        ?>
                        </div>
                    </div>
                    <!-- end of slider-div -->

                </div>
            </div>
        </div>
    </div><!-- end of partner_div -->
</section><!-- end of partner_section -->
    <?php } ?>
<?php get_footer();
