<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Jess
 * @since 1.0
 * @version 1.2
 */

?>

	</div><!-- end of content area -->

    <footer>
    	<div class="footer-div">
        	<div class="container">
                <div class="row">
                    <div class="footer-top clearfix">
                        <div class="col-md-3 col-sm-3">
                            <div class="img-box">
                                <?php //the_custom_logo();?>

                                <img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" class="img-responsive img-width-150">
                            </div>
                            <p><?php bloginfo('description'); ?></p>
                            <h3 class="h3-footer">Contact us</h3>
                            <ul>
                                <li><a href="tel:<?php echo get_option('phoneno');?>"> Phone: <?php echo get_option('phoneno');?></a></li>
                                <li><a href="mailto:<?php echo get_option( 'email' );  ?>"> Email:<?php echo get_option( 'email' );  ?></a></li>
                            </ul>
                        </div><!-- end of col -->
                        <div class="col-md-3 col-sm-3">
                            <?php
                            $defaults = array(
                                'theme_location' => 'primary',
                                'menu' => 'Footer Menu 1',
                                'container' => 'ul',
                                'menu_class' => '',
                                'echo' => true,
                                'fallback_cb' => 'wp_page_menu',
                                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'depth' => 0,
                                'id' => 3,
                                'walker' => new wp_bootstrap_navwalker,
                            );
                            wp_nav_menu($defaults);
                            ?>

                        </div><!-- end of col -->
                        <div class="col-md-3 col-sm-3">
                           <?php
                            $defaults = array(
                                'theme_location'  => 'primary',
                                'menu'            => 'Footer Menu 2',
                                'container'       => 'ul',
                                'menu_class'      => '',
                                'echo'            => true,
                                'fallback_cb'     => 'wp_page_menu',
                                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'depth'           => 0,
                                'id'               => 3,
                                'walker' => new wp_bootstrap_navwalker
                            );
                            wp_nav_menu( $defaults );
                        ?>
                        </div><!-- end of col -->

                        <div class="col-md-3 col-sm-3">
                            <h3>Newsletter</h3>
                            <p>Subscribe to our newsletter.</p>
                            <div class="form-group input-box-footer">
                               <?php echo do_shortcode('[contact-form-7 id="94" title="Subscribe Form"]'); ?>
                            </div>
                            <div class="social-icon">
                                <ul class="list-inline">
                                    <li><a href="<?php echo get_option( 'fb_url' );  ?>"><i class="fab fa-facebook-f"></i></a></li></span>
                                    <li><a href="<?php echo get_option( 'twitter_url' );  ?>"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="<?php echo get_option( 'linkedin_url' );  ?>"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="<?php echo get_option( 'pinterest' );  ?>"><i class="fab fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div><!-- end of col -->
                    </div><!-- end of footer-top -->
                </div><!-- end of row -->
            </div><!-- end of container -->

            <div class="footer-btm clearfix">
                <div class="container">
                    <div class="row">
                       <span class="copy-txt"><?php echo get_option( 'footer_content' );  ?></span>
                    </div><!-- end of row -->
                </div><!-- end of container -->
            </div><!-- end of footer-btm -->

        </div><!-- end of footer-div -->
    </footer>

</div><!-- end of wrapper -->


<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>
<link href="<?php bloginfo('template_url'); ?>/assets/css/hover-dropdown-menu.css" rel="stylesheet" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/hover-dropdown-menu.js"></script>
<!-- Menu jQuery Bootstrap Addon -->
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/jquery.hover-dropdown-menu-addon.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/home-roundbox-hover.js"></script>

<link rel='stylesheet' id='camera-css'  href='<?php bloginfo('template_url'); ?>/assets/css/camera.css' type='text/css' media='all'>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/assets/js/jquery.easing.1.3.js'></script>
<script type='text/javascript' src='<?php bloginfo('template_url'); ?>/assets/js/camera.min.js'></script>

<script>
	$(function(){
		$('#home-banner').camera({
			height: 'auto',
            pagination:false,
            navigation:true,
            thumbnails: false,
            loader:'none',
            playPause:false,
            fx:'simpleFade',
            time:2000,
            imagePath: '../images/'
		});

	});


    $(".attributeSearch").on("click", function(){
        make = $('.pa_make').val();
        model = $('.pa_model').val();
        year = $('.pa_years').val();
        window.location.href = window.location.href+'shop/?filter_model='+model+'&filter_years='+year+'&filter_make='+make;
    });

    $(".product_search_box").on("focusout", function(){
        if(window.location.href.indexOf('cate_id') > -1)
        {
            var url= window.location.href;
            var vars = url.split('?');
            var vars = vars[1].split('&');
            var newurl = '?';
            for (var i=0; i< vars.length; i++) {
                var pair = vars[i].split('=');
                if (pair[0] == 'cate_id') {
                    newurl += pair[0]+'='+cate_id+'&';
                }
                else
                {
                    newurl += pair[0]+'='+pair[1]+'&';
                }
            }
            console.log(newurl);
            window.location.href = newurl;
        }
        else if (window.location.href.indexOf('?') > -1) {
            window.location.href = window.location.href+'&cate_id='+cate_id;
        }
        else
        {
            window.location.href = '?cate_id='+cate_id;
        }
    });



    $(document).ready(function () {
        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            navText: [
                '<span aria-label="' + 'Previous' + '"><i class="fas fa-angle-left"></i></span>',
                '<span aria-label="' + 'Next' + '"><i class="fas fa-angle-right"></i></span>'
            ],
            dots: false,
            responsiveClass: true,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                300: {
                    items: 2,
                    nav: false
                },
                700: {
                    items: 3,
                    nav: true,
                    loop: false,
                    margin: 20
                },
                900: {
                    items: 4,
                    nav: true,
                    loop: false,
                    margin: 20
                }
            }
        })
    })

</script>

<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/owlcarousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/owlcarousel/assets/owl.theme.default.min.css">
<script src="<?php bloginfo('template_url'); ?>/assets/owlcarousel/owl.carousel.js"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/owlcarousel/owl.carousel.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/index.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/aos.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/highlight.min.js"></script>
<!--<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">-->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
<script>
    AOS.init({
        easing: 'ease-out-back',
        duration: 1000
    });
</script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/custom.js"></script>
<?php wp_footer(); ?>

</body>
</html>
