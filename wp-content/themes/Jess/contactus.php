<?php
/**
 * Template Name: Contact Us
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$Post = get_post($post = 46); ?>

<div class="middle-container">

    <div class="banner-div clearfix">

        <div class="fluid_container-all clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="top-head clearfix">
                            <h1> Contact Us </h1>
                            <div class="white-line-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-div clearfix">
                           <?php
                            echo do_shortcode( '[breadcrumb]' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .fluid_container -->

    </div><!-- End of banner-div  -->

    <div class="container-fluid">
       <div class="row">
            <div id="map" class="map"></div>
            <script>


                function initMap() {
                var lat = "<?php echo $Post->latitude; ?>";
                var lng = "<?php echo $Post->longitude; ?>";
                var myLatLng = {lat: parseFloat(lat), lng: parseFloat(lng) };

                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 16,
                    center: myLatLng
                });

                var marker = new google.maps.Marker({
                    position: myLatLng,
                    map: map,
                    title: 'map'
                });
                }
            </script>
            <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpiWWaPaKhBbPSs2vZBECcZcvH2LUtj2U&callback=initMap">
            </script>

        </div>
    </div>
    <div class="general-container-div clearfix">
        <div class="container">
            <div class="row">

                    <div class="col-md-5 col-sm-6">
                        <div class="address_div pt-zero">
                            <h3 class="h3-title-div h3-title02">
                                Jess Performance
                                <div class="black-line"></div>
                            </h3>
                            <h4>Address:</h4>
                            <p class="p-address">
                                <?php echo html_entity_decode(get_option( 'address' ));  ?>
                            </p>
                            <h4>Phone Number:</h4>
                            <p class="p-address"><a href="tel:<?php echo get_option('phoneno');?>">PH: <?php echo get_option('phoneno');?></a></p>
                        </div>
                        <div class="social-icons">
                            <ul class="list-inline">
                                <li>
                                    <a href="#" class="btn btn-primary btn-facebook"><i class="fas fa-thumbs-up"></i><span> Like 0 </span> </a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-primary btn-facebook"><span> Share </span> </a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-primary btn-twitter"><i class="fab fa-twitter"></i> <span> Tweet </span> </a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-primary btn-google"><i class="fab fa-google-plus-g"></i><span> Share </span> </a>
                                </li>
                                <li>
                                    <a href="#" class="btn btn-primary btn-linkedin"><i class="fab fa-linkedin-in"></i><span> Share </span> </a>
                                </li>

                            </ul>

                        </div>
                    </div>

                    <div class="col-md-7 col-sm-6">
                        <div class="contect-form-div pt-zero">
                            <?php echo do_shortcode('[contact-form-7 id="49" title="Contact form"]'); ?>
                        </div>
                    </div>

            </div>

        </div>
    </div><!-- End of general-container-div -->

    <div class="bottom-container-div1 clearfix">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 clearfix">

                    <div class="bottomimglist-div clearfix">
                        <div class="row">

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                            <?php
                                                echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Powerstroke</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                             <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Cummins</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                             <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Duramax</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                            <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">MERCHANDISE</h2>
                                    </div>

                                </a>
                            </div>

                        </div>
                    </div>

                </div>





            </div>
        </div>
    </div>

</div><!-- end of middle-container -->

<?php get_footer();
