<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$postId =  get_the_ID();
$postType = get_post_type($postId);
$postTitle = get_the_title($postId);


// if($postType != '' && $postType == 'new')
// {
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php //the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php twentyseventeen_edit_link( get_the_ID() ); ?>
	</header><!-- .entry-header -->
	<div class="entry-content">
        <?php if($postTitle != 'Shop')
        { ?>
            <div class="container">
            <div class="row">
        <?php }
        ?>

            <?php
                the_content();

                wp_link_pages( array(
                    'before' => '<div class="page-links">' . __( 'Pages:', 'twentyseventeen' ),
                    'after'  => '</div>',
                ) );
            ?>
            <?php if($postTitle != 'Shop' )
        { ?>
            </div>
        </div>
        <?php }
        ?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->
