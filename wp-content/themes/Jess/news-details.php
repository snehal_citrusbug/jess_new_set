<?php
/**
 * single-news
 */
$post = get_post($post = $postId);
get_header(); ?>

<div class="middle-container">
    <div class="banner-div clearfix">
        <div class="fluid_container-all clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="top-head clearfix">
                            <h1> News Details </h1>
                            <div class="white-line-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-div clearfix">
                           <?php
                            echo do_shortcode( '[breadcrumb]' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .fluid_container -->

    </div><!-- End of banner-div  -->
    <div class="middle-container clearfix">
        <div class="general-container-div clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h2 class="h2-title">
                            <?php echo $post->post_title; ?>
                            <div class="black-line"></div>
                        </h2>
                        <div class="privacy-policy-div clearfix">
                            <div class="col-md-5 col-sm-6">
                                <?php
                                    echo get_the_post_thumbnail( $post->ID, array(),array('class' => 'img-responsive img-bottom-20','alt' => 'about us'));
                                ?>
                            </div>
                            <div class="col-md-7 col-sm-6">
                                    <?php echo apply_filters('the_content',$post->post_content); ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- End of general-container-div -->

        <div class="bottom-container-div1 clearfix">
            <div class="container">
                <div class="row">

                    <div class="col-md-12 col-sm-12 clearfix">

                        <div class="bottomimglist-div clearfix">
                            <div class="row">

                                <div class="col-md-3 col-sm-3 clearfix">
                                    <a href="#">
                                        <div class="box-div clearfix">
                                            <div class="heading-div">
                                                <?php
                                                    echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                                ?>
                                                <div class="overlay">
                                                    <div class="content-div">
                                                        <p class="title-p">Click Here</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 class="h2-bottom clearfix">Powerstroke</h2>
                                        </div>

                                    </a>
                                </div>

                                <div class="col-md-3 col-sm-3 clearfix">
                                    <a href="#">
                                        <div class="box-div clearfix">
                                            <div class="heading-div">
                                                <?php
                                                echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                                ?>
                                                <div class="overlay">
                                                    <div class="content-div">
                                                        <p class="title-p">Click Here</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 class="h2-bottom clearfix">Cummins</h2>
                                        </div>

                                    </a>
                                </div>

                                <div class="col-md-3 col-sm-3 clearfix">
                                    <a href="#">
                                        <div class="box-div clearfix">
                                            <div class="heading-div">
                                                <?php
                                                echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                                ?>
                                                <div class="overlay">
                                                    <div class="content-div">
                                                        <p class="title-p">Click Here</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 class="h2-bottom clearfix">Duramax</h2>
                                        </div>

                                    </a>
                                </div>

                                <div class="col-md-3 col-sm-3 clearfix">
                                    <a href="#">
                                        <div class="box-div clearfix">
                                            <div class="heading-div">
                                                <?php
                                                echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                                ?>
                                                <div class="overlay">
                                                    <div class="content-div">
                                                        <p class="title-p">Click Here</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <h2 class="h2-bottom clearfix">MERCHANDISE</h2>
                                        </div>

                                    </a>
                                </div>

                            </div>
                        </div>

                    </div>





                </div>
            </div>
        </div>
    </div><!-- end of middle-container -->

<?php get_footer();