<?php
/**
 * Template Name: Video
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();
$post = get_post($post = 119); ?>

    <div class="middle-container">
    <div class="banner-div clearfix">
        <div class="fluid_container-all clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="top-head clearfix">
                            <h1> Video </h1>
                            <div class="white-line-1"></div>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="breadcrumb-div clearfix">
                           <?php
                            echo do_shortcode( '[breadcrumb]' );
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- .fluid_container -->

    </div><!-- End of banner-div  -->

    <div class="general-container-div clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="privacy-policy-div clearfix">
                        <h3 class="h3-title-div">
                            <?php echo $post->post_title; ?>
                            <div class="black-line"></div>
                        </h3>

                        <?php echo apply_filters('the_content',$post->post_content); ?>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                 <iframe width="100%" height="515" src="<?php echo $post->youtube_iframe_link; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>

            </div>
        </div>
    </div><!-- End of general-container-div -->

    <div class="bottom-container-div1 clearfix">
        <div class="container">
            <div class="row">

                <div class="col-md-12 col-sm-12 clearfix">

                    <div class="bottomimglist-div clearfix">
                        <div class="row">

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                            <?php
                                                echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Powerstroke</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                             <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Cummins</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                             <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">Duramax</h2>
                                    </div>

                                </a>
                            </div>

                            <div class="col-md-3 col-sm-3 clearfix">
                                <a href="#">
                                    <div class="box-div clearfix">
                                        <div class="heading-div">
                                            <?php
                                               echo wp_get_attachment_image( 113, array(),$icon = false ,array('class' => 'image'));
                                            ?>
                                            <div class="overlay">
                                                <div class="content-div">
                                                    <p class="title-p">Click Here</p>
                                                </div>
                                            </div>
                                        </div>
                                        <h2 class="h2-bottom clearfix">MERCHANDISE</h2>
                                    </div>

                                </a>
                            </div>

                        </div>
                    </div>

                </div>





            </div>
        </div>
    </div>

</div><!-- end of middle-container -->

<?php get_footer();
