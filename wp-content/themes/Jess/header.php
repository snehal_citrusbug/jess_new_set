<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link href='<?php bloginfo('template_url'); ?>/assets/images/favicon.png' rel='shortcut icon'>
<link href='<?php bloginfo('template_url'); ?>/assets/images/favicon.ico' rel='icon' type='image/ico'>

<link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Oswald:200,300,400,500,600,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<!-- <link href="css/style.css" media="all" rel="stylesheet" type="text/css" /> -->
<link href="<?php bloginfo('template_url'); ?>/assets/css/aos.css" media="all" rel="stylesheet" type="text/css" />
<!--<link href="css/style.css" media="all" rel="stylesheet" type="text/css" />-->


<link href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('template_url'); ?>/assets/css/animate.css" rel="stylesheet" />
<script src="<?php bloginfo('template_url'); ?>/assets/js/modernizr.js" type="text/javascript"></script>

<?php wp_head();
 global $woocommerce;
$total_item_cart = floatval( $woocommerce->cart->get_cart_contents_count() );
?>
</head>

<body <?php body_class(); ?>>
<div id="mySidenav" class="sidenav">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

    <nav class="right-menulinks" id="nav-res">
        <ul class="text-left logo-ul">
            <li>
            <?php //the_custom_logo(); ?>
            <a href="index.html"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" class="logo-img logo-big" alt="logo"></a>
            </li>
        </ul>
        <?php
            $defaults = array(
                'theme_location' => 'primary',
                'menu' => 'Top Menu',
                'container' => 'ul',
                'menu_class' => 'nav navbar-nav',
                'echo' => true,
                'fallback_cb' => 'wp_page_menu',
                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth' => 0,
                'id' => 3,
                'walker' => new wp_bootstrap_navwalker,
            );
            wp_nav_menu($defaults);
            ?>
    </nav>
</div>
<!-- end of sidenav -->

<div id="wrapper">
<header>

    <section class="top-section clearfix">
        <div class="top-header clearfix">
            <div class="container">
                <div class="row" data-aos="fade-up">
                    <div class="col-md-3 col-sm-4 sclearfix">
                        <div class="left-div-top clearfix">
                            <p> Call Us Today - <a href="tel:<?php echo get_option('phoneno');?>"><?php echo get_option('phoneno');?></a> </p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-4 clearfix">
                        <div class="middle-div-top clearfix">
                           <p>NO HANDLING FEE</p> <span class="dot"></span>
                           <p><a href="#" class="red_link">Free Shipping</a> on orders over $50.00*</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4 clearfix">

                        <div class="form-group search_div">
                        <?php dynamic_sidebar('cs-4'); ?>
                            <!-- <input type="text" class="form-control textbox-custom product_search_box" placeholder="search"> -->
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="header-div clearfix">
        <section class="container">
            <div class="col-md-2 col-sm-5 col-xs-5">
                <div class="logo-div"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" class="logo-img" alt="logo"></a></div>
            </div>
            <div class="col-md-7 col-sm-2 col-xs-2">
                <div class="nav-div">
                    <div class="navbar" role="navigation">
                        <div class="navbar-header">
                            <!-- Button For Responsive toggle -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <!-- Navbar Collapse -->

                        <div class="navbar-collapse collapse">
                            <!-- nav -->
                            <?php

                            // $Walker = new nav_custom_walker;
                            $defaults = array(
                                'theme_location' => 'primary',
                                'menu' => 'Top Menu',
                                'container' => 'ul',
                                'menu_class' => 'nav navbar-nav',
                                'echo' => true,
                                'fallback_cb' => 'wp_page_menu',
                                'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                'depth' => 0,
                                'id' => 3,
                                'walker' => new wp_bootstrap_navwalker,
                            );
                            wp_nav_menu($defaults);
                            ?>
                            <!-- end of navbar -->
                        </div><!-- end of nav bar collapse -->

                        <div class="last"><a href="#" onclick="openNav()" class="" data-placement="bottom" title="" data-original-title="Menu">
                            <i class="menu-bars"></i></a>
                        </div>

                    </div><!-- end of nav-bar -->
                </div><!-- end of nav-div -->
            </div>
            <div class="col-md-3 col-sm-5 col-xs-5">

                <div class="right-div">
                    <div class="dropdown dropdown-custom">
                        <a href="<?php echo site_url(); ?>/my-account" class="btn btn-default dropdown-toggle">
                        MY ACCOUNT
                        </a>
                        <!-- <ul class="dropdown-menu">
                          <li><a href="#">Update Profile</a></li>
                          <li><a href="#">Logout</a></li>
                        </ul> -->
                        <p class="p-right">My Shopping cart</p>
                    </div>
                    <div class="icon-div">
                        <i class="fas fa-shopping-cart"></i>
                        <span class="total-number"><?php echo $total_item_cart; ?></span>
                    </div>
                </div>

            </div>

        </section>
    </div><!-- end of header div -->

    <?php if (is_front_page()) { ?>
	<div class="banner-container clearfix">
        <div class="banner-div clearfix">
        <?php
        echo do_shortcode('[smartslider3 slider=3]');
        ?>
        </div><!-- end of banner-div -->
    </div><!-- end of banner container -->
    <?php } ?>

</header>

    <div class="content-area" id="content-div">
