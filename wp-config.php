<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'js_08');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3%&2^<zl<OPWGV@GR0;Z#n?2c}uU2}<E3B}M~A{1(tD/pXH&n#@W-h|ul`Do[@=p');
define('SECURE_AUTH_KEY',  'RVlnfawDUcdGP?zUnOJ ^mPkuS=mR8}/_W?Rh|i^ w?5mdHV`i[gz2Hz&GZ{;gA|');
define('LOGGED_IN_KEY',    'hSx%sKh=0nrcI2Xa6&4F3u/=4fkWYSY)aS6Pmg$!r:H=t9V hKav>1S^HA-i3R?R');
define('NONCE_KEY',        'lGa_Ett5y5&@} ^@k<S_)49-Bov-#aLLYcW+IHp=8S0Ax@z&o:K=fj<I~P.DBfw{');
define('AUTH_SALT',        'dNwE6bZBW}7$b*JlN[%svIX5^|gh%mp~HNz*fp;.5W_zf%7]Do;ZjIuxPP3qz*7*');
define('SECURE_AUTH_SALT', '5DxaF^OJAWR=7vqm-IkpYKe%]E57A?vP%l|SD>7[;MLA113V8mktX?Cfz#(*#Q<U');
define('LOGGED_IN_SALT',   'YzA-Nou::.0d+J#u-E42HnN+mOe_?)r9}v3~=Vhd}tx+zlw<{T^*An/e0r2v;#%@');
define('NONCE_SALT',       '&-9R7 %Yf[-n><]^%y%Z]eU;FPuMbs3:Wjt*VI6pyc-.J$ -b0QZc^v lvp!*as1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
