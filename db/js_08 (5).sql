-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2018 at 05:48 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `js_08`
--

-- --------------------------------------------------------

--
-- Table structure for table `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_comments`
--

INSERT INTO `wp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'A WordPress Commenter', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2018-09-28 05:00:55', '2018-09-28 05:00:55', 'Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(2, 37, 'WooCommerce', '', '', '', '2018-10-09 06:17:01', '2018-10-09 06:17:01', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(3, 38, 'WooCommerce', '', '', '', '2018-10-09 06:31:14', '2018-10-09 06:31:14', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(4, 39, 'WooCommerce', '', '', '', '2018-10-09 06:38:24', '2018-10-09 06:38:24', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(5, 40, 'WooCommerce', '', '', '', '2018-10-09 06:59:55', '2018-10-09 06:59:55', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(6, 41, 'WooCommerce', '', '', '', '2018-10-09 07:23:17', '2018-10-09 07:23:17', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(7, 42, 'WooCommerce', '', '', '', '2018-10-09 07:39:26', '2018-10-09 07:39:26', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(8, 43, 'WooCommerce', '', '', '', '2018-10-09 09:44:39', '2018-10-09 09:44:39', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(9, 48, 'WooCommerce', '', '', '', '2018-10-09 10:07:15', '2018-10-09 10:07:15', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(10, 50, 'WooCommerce', '', '', '', '2018-10-09 10:11:56', '2018-10-09 10:11:56', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(11, 114, 'WooCommerce', '', '', '', '2018-10-09 12:27:33', '2018-10-09 12:27:33', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(12, 115, 'WooCommerce', '', '', '', '2018-10-09 13:33:05', '2018-10-09 13:33:05', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(13, 118, 'WooCommerce', '', '', '', '2018-10-09 15:02:20', '2018-10-09 15:02:20', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(14, 143, 'WooCommerce', '', '', '', '2018-10-09 16:26:45', '2018-10-09 16:26:45', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(15, 144, 'WooCommerce', '', '', '', '2018-10-10 07:29:32', '2018-10-10 07:29:32', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0),
(16, 145, 'WooCommerce', '', '', '', '2018-10-10 08:58:53', '2018-10-10 08:58:53', 'Payment to be made upon delivery. Order status changed from Pending payment to Processing.', 0, '1', 'WooCommerce', 'order_note', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_image_storage`
--

CREATE TABLE `wp_nextend2_image_storage` (
  `id` int(11) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `image` text NOT NULL,
  `value` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_image_storage`
--

INSERT INTO `wp_nextend2_image_storage` (`id`, `hash`, `image`, `value`) VALUES
(1, '7c81ef9c68f7016ba9245584935f4451', '$upload$/slider2/banner-1.jpg', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ=='),
(2, '7148fa26ad6dd9ee953b6c3f5f30c99d', 'https://smartslider3.com/sample/programmer.jpg', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ=='),
(3, 'ab831bec2456ac5eb84e4eb873dad106', '$upload$/2018/10/banner-1.jpg', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ=='),
(4, '49af4b89814a2e066946c71b36020fb1', '$upload$/2018/10/banner-1-1.jpg', 'eyJkZXNrdG9wIjp7InNpemUiOiIwfCp8MCJ9LCJkZXNrdG9wLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJ0YWJsZXQiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwidGFibGV0LXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9LCJtb2JpbGUiOnsiaW1hZ2UiOiIiLCJzaXplIjoiMHwqfDAifSwibW9iaWxlLXJldGluYSI6eyJpbWFnZSI6IiIsInNpemUiOiIwfCp8MCJ9fQ==');

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_section_storage`
--

CREATE TABLE `wp_nextend2_section_storage` (
  `id` int(11) NOT NULL,
  `application` varchar(20) NOT NULL,
  `section` varchar(128) NOT NULL,
  `referencekey` varchar(128) NOT NULL,
  `value` mediumtext NOT NULL,
  `system` int(11) NOT NULL DEFAULT '0',
  `editable` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_section_storage`
--

INSERT INTO `wp_nextend2_section_storage` (`id`, `application`, `section`, `referencekey`, `value`, `system`, `editable`) VALUES
(10000, 'system', 'global', 'n2_ss3_version', '3.3.7r2543', 1, 1),
(10001, 'smartslider', 'tutorial', 'free', '1', 0, 1),
(10002, 'smartslider', 'sliderChanged', '2', '1', 0, 1),
(10003, 'smartslider', 'sliderChanged', '1', '1', 0, 1),
(10004, 'smartslider', 'sliderChanged', '3', '0', 0, 1),
(10017, 'cache', 'notweb/n2-ss-3', 'data.manifest', '{\"generator\":[]}', 0, 1),
(10018, 'cache', 'notweb/n2-ss-3', 'variations.manifest', '1', 0, 1),
(10019, 'cache', 'notweb/n2-ss-3', 'slideren_US1.manifest', '{\"hash\":\"\",\"nextCacheRefresh\":1762864054,\"currentPath\":\"4453624f1f432833d4076ab0fae766dc\",\"version\":\"3.3.7\"}', 0, 1),
(10020, 'cache', 'notweb/n2-ss-3', 'slideren_US1', '{\"html\":\"<style>div#n2-ss-3{width:1200px;float:left;margin:0px 0px 0px 0px;}html[dir=\\\"rtl\\\"] div#n2-ss-3{float:right;}div#n2-ss-3 .n2-ss-slider-1{position:relative;padding-top:0px;padding-right:0px;padding-bottom:0px;padding-left:0px;height:600px;border-style:solid;border-width:0px;border-color:#3e3e3e;border-color:RGBA(62,62,62,1);border-radius:0px;background-clip:padding-box;background-repeat:repeat;background-position:50% 50%;background-size:cover;background-attachment:scroll;}div#n2-ss-3 .n2-ss-slider-background-video-container{position:absolute;left:0;top:0;width:100%;height:100%;overflow:hidden;}div#n2-ss-3 .n2-ss-slider-2{position:relative;width:100%;height:100%;}.x-firefox div#n2-ss-3 .n2-ss-slider-2{opacity:0.99999;}div#n2-ss-3 .n2-ss-slider-3{position:relative;width:100%;height:100%;overflow:hidden;outline:1px solid rgba(0,0,0,0);z-index:10;}div#n2-ss-3 .n2-ss-slide-backgrounds,div#n2-ss-3 .n2-ss-slider-3 > .n-particles-js-canvas-el,div#n2-ss-3 .n2-ss-slider-3 > .n2-ss-divider{position:absolute;left:0;top:0;width:100%;height:100%;}div#n2-ss-3 .n2-ss-slide-backgrounds{z-index:10;}div#n2-ss-3 .n2-ss-slider-3 > .n-particles-js-canvas-el{z-index:12;}div#n2-ss-3 .n2-ss-slide-backgrounds > *{overflow:hidden;}div#n2-ss-3 .n2-ss-slide{position:absolute;top:0;left:0;width:100%;height:100%;z-index:20;display:block;-webkit-backface-visibility:hidden;}div#n2-ss-3 .n2-ss-layers-container{position:relative;width:1200px;height:600px;}div#n2-ss-3 .n2-ss-parallax-clip > .n2-ss-layers-container{position:absolute;right:0;}div#n2-ss-3 .n2-ss-slide{-webkit-perspective:1500px;perspective:1500px;}div#n2-ss-3 .n2-ss-slide-active{z-index:21;}div#n2-ss-3 .nextend-arrow{cursor:pointer;overflow:hidden;line-height:0 !important;z-index:20;}div#n2-ss-3 .nextend-arrow img{position:relative;min-height:0;min-width:0;vertical-align:top;width:auto;height:auto;max-width:100%;max-height:100%;display:inline;}div#n2-ss-3 .nextend-arrow img.n2-arrow-hover-img{display:none;}div#n2-ss-3 .nextend-arrow:HOVER img.n2-arrow-hover-img{display:inline;}div#n2-ss-3 .nextend-arrow:HOVER img.n2-arrow-normal-img{display:none;}div#n2-ss-3 .nextend-arrow-animated{overflow:hidden;}div#n2-ss-3 .nextend-arrow-animated > div{position:relative;}div#n2-ss-3 .nextend-arrow-animated .n2-active{position:absolute;}div#n2-ss-3 .nextend-arrow-animated-fade{transition:background 0.3s, opacity 0.4s;}div#n2-ss-3 .nextend-arrow-animated-horizontal > div{transition:all 0.4s;left:0;}div#n2-ss-3 .nextend-arrow-animated-horizontal .n2-active{top:0;}div#n2-ss-3 .nextend-arrow-previous.nextend-arrow-animated-horizontal:HOVER > div,div#n2-ss-3 .nextend-arrow-next.nextend-arrow-animated-horizontal .n2-active{left:-100%;}div#n2-ss-3 .nextend-arrow-previous.nextend-arrow-animated-horizontal .n2-active,div#n2-ss-3 .nextend-arrow-next.nextend-arrow-animated-horizontal:HOVER > div{left:100%;}div#n2-ss-3 .nextend-arrow.nextend-arrow-animated-horizontal:HOVER .n2-active{left:0;}div#n2-ss-3 .nextend-arrow-animated-vertical > div{transition:all 0.4s;top:0;}div#n2-ss-3 .nextend-arrow-animated-vertical .n2-active{left:0;}div#n2-ss-3 .nextend-arrow-animated-vertical .n2-active{top:-100%;}div#n2-ss-3 .nextend-arrow-animated-vertical:HOVER > div{top:100%;}div#n2-ss-3 .nextend-arrow-animated-vertical:HOVER .n2-active{top:0;}div#n2-ss-3 .n2-ss-control-bullet{visibility:hidden;text-align:center;justify-content:center;}div#n2-ss-3 .n2-ss-control-bullet-horizontal.n2-ss-control-bullet-fullsize{width:100%;}div#n2-ss-3 .n2-ss-control-bullet-vertical.n2-ss-control-bullet-fullsize{height:100%;flex-flow:column;}div#n2-ss-3 .nextend-bullet-bar{display:inline-flex;visibility:visible;align-items:center;flex-wrap:wrap;}div#n2-ss-3 .n2-bar-justify-content-left{justify-content:flex-start;}div#n2-ss-3 .n2-bar-justify-content-center{justify-content:center;}div#n2-ss-3 .n2-bar-justify-content-right{justify-content:flex-end;}div#n2-ss-3 .n2-ss-control-bullet-vertical > .nextend-bullet-bar{flex-flow:column;}div#n2-ss-3 .n2-ss-control-bullet-fullsize > .nextend-bullet-bar{display:flex;}div#n2-ss-3 .n2-ss-control-bullet-horizontal.n2-ss-control-bullet-fullsize > .nextend-bullet-bar{flex:1 1 auto;}div#n2-ss-3 .n2-ss-control-bullet-vertical.n2-ss-control-bullet-fullsize > .nextend-bullet-bar{height:100%;}div#n2-ss-3 .nextend-bullet-bar > div{display:inline-block;cursor:pointer;transition:background-color 0.4s;vertical-align:top;}div#n2-ss-3 .nextend-bullet-bar > div.n2-active{cursor:default;}div#n2-ss-3 div.n2-ss-bullet-thumbnail-container{position:absolute;opacity:0;z-index:10000000;}div#n2-ss-3 .n2-ss-bullet-thumbnail-container .n2-ss-bullet-thumbnail{background-size:cover;background-repeat:no-repeat;background-position:center;}div#n2-ss-3 .n2-ss-layer .n2-font-4089709583d98f5a6f47094916d232ef-hover{font-family: \'Oswald\',\'sans-serif\';color: #ffffff;font-size:350%;text-shadow: none;line-height: 3;font-weight: bold;font-style: normal;text-decoration: none;text-align: center;letter-spacing: 10px;word-spacing: normal;text-transform: uppercase;font-weight: bold;}div#n2-ss-3 .n2-style-aea1943254123158961ca7e8550fba2f-heading{background: #000000;background: RGBA(0,0,0,0.7);opacity:1;padding:0em 1em 0em 1em ;box-shadow: none;border-width: 0px;border-style: solid;border-color: #000000; border-color: RGBA(0,0,0,1);border-radius:5px;}div#n2-ss-3 .n2-style-05bdda2c726fc5112346a41f128f80fa-heading{background: #000000;opacity:1;padding:10px 30px 10px 30px ;box-shadow: none;border-width: 2px;border-style: solid;border-color: #000000; border-color: RGBA(0,0,0,1);border-radius:0px;}div#n2-ss-3 .n2-style-09efebcef1f2f45d29438e0cabcf79bc-dot{background: #000000;background: RGBA(0,0,0,0.67);opacity:1;padding:5px 5px 5px 5px ;box-shadow: none;border-width: 0px;border-style: solid;border-color: #000000; border-color: RGBA(0,0,0,1);border-radius:50px;margin: 4px;}div#n2-ss-3 .n2-style-09efebcef1f2f45d29438e0cabcf79bc-dot.n2-active, div#n2-ss-3 .n2-style-09efebcef1f2f45d29438e0cabcf79bc-dot:HOVER{background: #09b474;}div#n2-ss-3 .n2-style-42845aa02120076deb3a5681d1d750ac-simple{background: #000000;background: RGBA(0,0,0,0.5);opacity:1;padding:3px 3px 3px 3px ;box-shadow: none;border-width: 0px;border-style: solid;border-color: #000000; border-color: RGBA(0,0,0,1);border-radius:3px;margin: 5px;}<\\/style><div id=\\\"n2-ss-3-align\\\" class=\\\"n2-ss-align\\\"><div class=\\\"n2-padding\\\"><div id=\\\"n2-ss-3\\\" data-creator=\\\"Smart Slider 3\\\" class=\\\"n2-ss-slider n2-ow n2-has-hover n2notransition n2-ss-load-fade \\\" data-minFontSizedesktopPortrait=\\\"4\\\" data-minFontSizedesktopLandscape=\\\"4\\\" data-minFontSizetabletPortrait=\\\"4\\\" data-minFontSizetabletLandscape=\\\"4\\\" data-minFontSizemobilePortrait=\\\"4\\\" data-minFontSizemobileLandscape=\\\"4\\\" style=\\\"font-size: 16px;\\\" data-fontsize=\\\"16\\\">\\r\\n        <div class=\\\"n2-ss-slider-1 n2-ss-swipe-element n2-ow\\\" style=\\\"\\\">\\r\\n                        <div class=\\\"n2-ss-slider-2 n2-ow\\\">\\r\\n                                <div class=\\\"n2-ss-slider-3 n2-ow\\\" style=\\\"\\\">\\r\\n\\r\\n                    <div class=\\\"n2-ss-slide-backgrounds\\\"><\\/div><div data-first=\\\"1\\\" data-slide-duration=\\\"0\\\" data-id=\\\"6\\\" data-thumbnail=\\\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\\\" style=\\\"\\\" class=\\\"n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-6\\\"><div class=\\\"n2-ss-slide-background n2-ow\\\" data-mode=\\\"fit\\\"><img data-hash=\\\"ab831bec2456ac5eb84e4eb873dad106\\\" data-desktop=\\\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\\\" data-blur=\\\"0\\\" data-opacity=\\\"100\\\" data-x=\\\"50\\\" data-y=\\\"50\\\" src=\\\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\\\" alt=\\\"\\\" \\/><\\/div><div class=\\\"n2-ss-layers-container n2-ow\\\" data-csstextalign=\\\"center\\\" style=\\\"\\\"><div class=\\\"n2-ss-layer n2-ow\\\" style=\\\"overflow:visible;\\\" data-csstextalign=\\\"inherit\\\" data-has-maxwidth=\\\"0\\\" data-desktopportraitmaxwidth=\\\"0\\\" data-cssselfalign=\\\"inherit\\\" data-desktopportraitselfalign=\\\"inherit\\\" data-pm=\\\"content\\\" data-desktopportraitpadding=\\\"10|*|10|*|10|*|10|*|px+\\\" data-desktopportraitinneralign=\\\"inherit\\\" data-sstype=\\\"content\\\" data-hasbackground=\\\"0\\\" data-rotation=\\\"0\\\" data-desktopportrait=\\\"1\\\" data-desktoplandscape=\\\"1\\\" data-tabletportrait=\\\"1\\\" data-tabletlandscape=\\\"1\\\" data-mobileportrait=\\\"1\\\" data-mobilelandscape=\\\"1\\\" data-adaptivefont=\\\"1\\\" data-desktopportraitfontsize=\\\"100\\\" data-mobileportraitfontsize=\\\"60\\\" data-plugin=\\\"rendered\\\"><div class=\\\"n2-ss-section-main-content n2-ss-layer-content n2-ow\\\" style=\\\"padding:0.625em 0.625em 0.625em 0.625em ;\\\" data-verticalalign=\\\"center\\\"><div class=\\\"n2-ss-layer n2-ow\\\" style=\\\"margin:12.1875em 0em 0.625em 0em ;overflow:visible;\\\" data-pm=\\\"normal\\\" data-desktopportraitmargin=\\\"195|*|0|*|10|*|0|*|px+\\\" data-desktopportraitheight=\\\"0\\\" data-has-maxwidth=\\\"0\\\" data-desktopportraitmaxwidth=\\\"0\\\" data-cssselfalign=\\\"inherit\\\" data-desktopportraitselfalign=\\\"inherit\\\" data-sstype=\\\"layer\\\" data-rotation=\\\"0\\\" data-desktopportrait=\\\"1\\\" data-desktoplandscape=\\\"1\\\" data-tabletportrait=\\\"1\\\" data-tabletlandscape=\\\"1\\\" data-mobileportrait=\\\"1\\\" data-mobilelandscape=\\\"1\\\" data-adaptivefont=\\\"0\\\" data-desktopportraitfontsize=\\\"100\\\" data-plugin=\\\"rendered\\\"><h2 id=\\\"n2-ss-3item1\\\" class=\\\"n2-font-4089709583d98f5a6f47094916d232ef-hover n2-style-aea1943254123158961ca7e8550fba2f-heading   n2-ow\\\" style=\\\"display:inline-block;\\\">WHEN ORDINARY ISN\'T ENOUGH<\\/h2><\\/div><\\/div><\\/div><\\/div><\\/div><div data-slide-duration=\\\"0\\\" data-id=\\\"9\\\" data-thumbnail=\\\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\\\" style=\\\"\\\" class=\\\"n2-ss-slide n2-ss-canvas n2-ow  n2-ss-slide-9\\\"><div class=\\\"n2-ss-slide-background n2-ow\\\" data-mode=\\\"fit\\\"><img data-hash=\\\"ab831bec2456ac5eb84e4eb873dad106\\\" data-desktop=\\\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\\\" data-blur=\\\"0\\\" data-opacity=\\\"100\\\" data-x=\\\"50\\\" data-y=\\\"50\\\" src=\\\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\\\" alt=\\\"\\\" \\/><\\/div><div class=\\\"n2-ss-layers-container n2-ow\\\" data-csstextalign=\\\"center\\\" style=\\\"\\\"><div class=\\\"n2-ss-layer n2-ow\\\" style=\\\"overflow:visible;\\\" data-csstextalign=\\\"inherit\\\" data-has-maxwidth=\\\"0\\\" data-desktopportraitmaxwidth=\\\"0\\\" data-cssselfalign=\\\"inherit\\\" data-desktopportraitselfalign=\\\"inherit\\\" data-pm=\\\"content\\\" data-desktopportraitpadding=\\\"10|*|10|*|10|*|10|*|px+\\\" data-desktopportraitinneralign=\\\"inherit\\\" data-sstype=\\\"content\\\" data-hasbackground=\\\"0\\\" data-rotation=\\\"0\\\" data-desktopportrait=\\\"1\\\" data-desktoplandscape=\\\"1\\\" data-tabletportrait=\\\"1\\\" data-tabletlandscape=\\\"1\\\" data-mobileportrait=\\\"1\\\" data-mobilelandscape=\\\"1\\\" data-adaptivefont=\\\"1\\\" data-desktopportraitfontsize=\\\"100\\\" data-mobileportraitfontsize=\\\"60\\\" data-plugin=\\\"rendered\\\"><div class=\\\"n2-ss-section-main-content n2-ss-layer-content n2-ow\\\" style=\\\"padding:0.625em 0.625em 0.625em 0.625em ;\\\" data-verticalalign=\\\"center\\\"><div class=\\\"n2-ss-layer n2-ow\\\" style=\\\"margin:12.1875em 0em 0.625em 0em ;overflow:visible;\\\" data-pm=\\\"normal\\\" data-desktopportraitmargin=\\\"195|*|0|*|10|*|0|*|px+\\\" data-desktopportraitheight=\\\"0\\\" data-has-maxwidth=\\\"0\\\" data-desktopportraitmaxwidth=\\\"0\\\" data-cssselfalign=\\\"inherit\\\" data-desktopportraitselfalign=\\\"inherit\\\" data-sstype=\\\"layer\\\" data-rotation=\\\"0\\\" data-desktopportrait=\\\"1\\\" data-desktoplandscape=\\\"1\\\" data-tabletportrait=\\\"1\\\" data-tabletlandscape=\\\"1\\\" data-mobileportrait=\\\"1\\\" data-mobilelandscape=\\\"1\\\" data-adaptivefont=\\\"0\\\" data-desktopportraitfontsize=\\\"100\\\" data-plugin=\\\"rendered\\\"><h2 id=\\\"n2-ss-3item2\\\" class=\\\"n2-font-4089709583d98f5a6f47094916d232ef-hover n2-style-aea1943254123158961ca7e8550fba2f-heading   n2-ow\\\" style=\\\"display:inline-block;\\\">WHEN ORDINARY ISN\'T ENOUGH<\\/h2><\\/div><\\/div><\\/div><\\/div><\\/div>                <\\/div>\\r\\n            <\\/div>\\r\\n            <div data-ssleft=\\\"0+15\\\" data-sstop=\\\"height\\/2-previousheight\\/2\\\" id=\\\"n2-ss-3-arrow-previous\\\" class=\\\"n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile n2-style-05bdda2c726fc5112346a41f128f80fa-heading nextend-arrow n2-ow nextend-arrow-previous  nextend-arrow-animated-fade n2-ib\\\" style=\\\"position: absolute;\\\" role=\\\"button\\\" aria-label=\\\"Previous slide\\\" tabindex=\\\"0\\\"><img class=\\\"n2-ow\\\" data-no-lazy=\\\"1\\\" data-hack=\\\"data-lazy-src\\\" src=\\\"data:image\\/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTEuNDMzIDE1Ljk5MkwyMi42OSA1LjcxMmMuMzkzLS4zOS4zOTMtMS4wMyAwLTEuNDItLjM5My0uMzktMS4wMy0uMzktMS40MjMgMGwtMTEuOTggMTAuOTRjLS4yMS4yMS0uMy40OS0uMjg1Ljc2LS4wMTUuMjguMDc1LjU2LjI4NC43N2wxMS45OCAxMC45NGMuMzkzLjM5IDEuMDMuMzkgMS40MjQgMCAuMzkzLS40LjM5My0xLjAzIDAtMS40MmwtMTEuMjU3LTEwLjI5IiBmaWxsPSIjZmZmZmZmIiBvcGFjaXR5PSIwLjgiIGZpbGwtcnVsZT0iZXZlbm9kZCIvPjwvc3ZnPg==\\\" alt=\\\"previous arrow\\\" \\/><\\/div>\\n<div data-ssright=\\\"0+15\\\" data-sstop=\\\"height\\/2-nextheight\\/2\\\" id=\\\"n2-ss-3-arrow-next\\\" class=\\\"n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile n2-style-05bdda2c726fc5112346a41f128f80fa-heading nextend-arrow n2-ow nextend-arrow-next  nextend-arrow-animated-fade n2-ib\\\" style=\\\"position: absolute;\\\" role=\\\"button\\\" aria-label=\\\"Next slide\\\" tabindex=\\\"0\\\"><img class=\\\"n2-ow\\\" data-no-lazy=\\\"1\\\" data-hack=\\\"data-lazy-src\\\" src=\\\"data:image\\/svg+xml;base64,PHN2ZyB3aWR0aD0iMzIiIGhlaWdodD0iMzIiIHZpZXdCb3g9IjAgMCAzMiAzMiIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj48cGF0aCBkPSJNMTAuNzIyIDQuMjkzYy0uMzk0LS4zOS0xLjAzMi0uMzktMS40MjcgMC0uMzkzLjM5LS4zOTMgMS4wMyAwIDEuNDJsMTEuMjgzIDEwLjI4LTExLjI4MyAxMC4yOWMtLjM5My4zOS0uMzkzIDEuMDIgMCAxLjQyLjM5NS4zOSAxLjAzMy4zOSAxLjQyNyAwbDEyLjAwNy0xMC45NGMuMjEtLjIxLjMtLjQ5LjI4NC0uNzcuMDE0LS4yNy0uMDc2LS41NS0uMjg2LS43NkwxMC43MiA0LjI5M3oiIGZpbGw9IiNmZmZmZmYiIG9wYWNpdHk9IjAuOCIgZmlsbC1ydWxlPSJldmVub2RkIi8+PC9zdmc+\\\" alt=\\\"next arrow\\\" \\/><\\/div>\\n        <\\/div>\\r\\n        <div data-position=\\\"below\\\" data-offset=\\\"10\\\" class=\\\"n2-ss-widget n2-ss-widget-display-desktop n2-ss-widget-display-tablet n2-ss-widget-display-mobile  n2-flex n2-ss-control-bullet n2-ss-control-bullet-horizontal\\\" style=\\\"margin-top:10px;\\\"><div class=\\\" nextend-bullet-bar n2-ow n2-bar-justify-content-center\\\"><\\/div><\\/div>\\n<\\/div><div class=\\\"n2-clear\\\"><\\/div><div id=\\\"n2-ss-3-spinner\\\" style=\\\"display: none;\\\"><div><div class=\\\"n2-ss-spinner-simple-white-container\\\"><div class=\\\"n2-ss-spinner-simple-white\\\"><\\/div><\\/div><\\/div><\\/div><\\/div><\\/div><div id=\\\"n2-ss-3-placeholder\\\" style=\\\"position: relative;z-index:2;background-color:RGBA(0,0,0,0);max-height:3000px; background-color:RGBA(255,255,255,0);\\\"><img style=\\\"width: 100%; max-width:3000px; display: block;opacity:0;\\\" class=\\\"n2-ow\\\" src=\\\"data:image\\/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZlcnNpb249IjEuMCIgd2lkdGg9IjEyMDAiIGhlaWdodD0iNjAwIiA+PC9zdmc+\\\" alt=\\\"Slider\\\" \\/><\\/div>\",\"assets\":{\"css\":{\"staticGroup\":{\"smartslider\":\"E:\\\\xampp\\\\htdocs\\\\wordpress-project\\\\jess\\\\wp-content\\\\plugins\\\\smart-slider-3\\\\library\\\\media\\/smartslider.min.css\"},\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[\".n2-ss-spinner-simple-white-container {\\r\\n    position: absolute;\\r\\n    top: 50%;\\r\\n    left: 50%;\\r\\n    margin: -20px;\\r\\n    background: #fff;\\r\\n    width: 20px;\\r\\n    height: 20px;\\r\\n    padding: 10px;\\r\\n    border-radius: 50%;\\r\\n    z-index: 1000;\\r\\n}\\r\\n\\r\\n.n2-ss-spinner-simple-white {\\r\\n  outline: 1px solid RGBA(0,0,0,0);\\r\\n  width:100%;\\r\\n  height: 100%;\\r\\n}\\r\\n\\r\\n.n2-ss-spinner-simple-white:before {\\r\\n    position: absolute;\\r\\n    top: 50%;\\r\\n    left: 50%;\\r\\n    width: 20px;\\r\\n    height: 20px;\\r\\n    margin-top: -11px;\\r\\n    margin-left: -11px;\\r\\n}\\r\\n\\r\\n.n2-ss-spinner-simple-white:not(:required):before {\\r\\n    content: \'\';\\r\\n    border-radius: 50%;\\r\\n    border-top: 2px solid #333;\\r\\n    border-right: 2px solid transparent;\\r\\n    animation: n2SimpleWhite .6s linear infinite;\\r\\n    -webkit-animation: n2SimpleWhite .6s linear infinite;\\r\\n}\\r\\n@keyframes n2SimpleWhite {\\r\\n    to {transform: rotate(360deg);}\\r\\n}\\r\\n\\r\\n@-webkit-keyframes n2SimpleWhite {\\r\\n    to {-webkit-transform: rotate(360deg);}\\r\\n}\"],\"globalInline\":[]},\"less\":{\"staticGroup\":[],\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[],\"globalInline\":[]},\"js\":{\"staticGroup\":{\"smartslider-simple-type-frontend\":\"E:\\\\xampp\\\\htdocs\\\\wordpress-project\\\\jess\\\\wp-content\\\\plugins\\\\smart-slider-3\\\\library\\\\media\\/plugins\\\\type\\\\simple\\\\simple\\/dist\\/smartslider-simple-type-frontend.min.js\"},\"files\":[],\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[\"N2R([\\\"nextend-frontend\\\",\\\"smartslider-frontend\\\",\\\"smartslider-simple-type-frontend\\\"],function(){new N2Classes.SmartSliderSimple(\'#n2-ss-3\', {\\\"admin\\\":false,\\\"translate3d\\\":1,\\\"callbacks\\\":\\\"\\\",\\\"background.video.mobile\\\":1,\\\"align\\\":\\\"normal\\\",\\\"isDelayed\\\":0,\\\"load\\\":{\\\"fade\\\":1,\\\"scroll\\\":0},\\\"playWhenVisible\\\":1,\\\"playWhenVisibleAt\\\":0.5,\\\"responsive\\\":{\\\"desktop\\\":1,\\\"tablet\\\":1,\\\"mobile\\\":1,\\\"onResizeEnabled\\\":true,\\\"type\\\":\\\"auto\\\",\\\"downscale\\\":1,\\\"upscale\\\":1,\\\"minimumHeight\\\":0,\\\"maximumHeight\\\":3000,\\\"maximumSlideWidth\\\":3000,\\\"maximumSlideWidthLandscape\\\":3000,\\\"maximumSlideWidthTablet\\\":3000,\\\"maximumSlideWidthTabletLandscape\\\":3000,\\\"maximumSlideWidthMobile\\\":3000,\\\"maximumSlideWidthMobileLandscape\\\":3000,\\\"maximumSlideWidthConstrainHeight\\\":0,\\\"forceFull\\\":0,\\\"forceFullOverflowX\\\":\\\"body\\\",\\\"forceFullHorizontalSelector\\\":\\\"\\\",\\\"constrainRatio\\\":1,\\\"verticalOffsetSelectors\\\":\\\"\\\",\\\"decreaseSliderHeight\\\":0,\\\"focusUser\\\":0,\\\"focusAutoplay\\\":0,\\\"deviceModes\\\":{\\\"desktopPortrait\\\":1,\\\"desktopLandscape\\\":0,\\\"tabletPortrait\\\":1,\\\"tabletLandscape\\\":0,\\\"mobilePortrait\\\":1,\\\"mobileLandscape\\\":0},\\\"normalizedDeviceModes\\\":{\\\"unknownUnknown\\\":[\\\"unknown\\\",\\\"Unknown\\\"],\\\"desktopPortrait\\\":[\\\"desktop\\\",\\\"Portrait\\\"],\\\"desktopLandscape\\\":[\\\"desktop\\\",\\\"Portrait\\\"],\\\"tabletPortrait\\\":[\\\"tablet\\\",\\\"Portrait\\\"],\\\"tabletLandscape\\\":[\\\"tablet\\\",\\\"Portrait\\\"],\\\"mobilePortrait\\\":[\\\"mobile\\\",\\\"Portrait\\\"],\\\"mobileLandscape\\\":[\\\"mobile\\\",\\\"Portrait\\\"]},\\\"verticalRatioModifiers\\\":{\\\"unknownUnknown\\\":1,\\\"desktopPortrait\\\":1,\\\"desktopLandscape\\\":1,\\\"tabletPortrait\\\":1,\\\"tabletLandscape\\\":1,\\\"mobilePortrait\\\":1,\\\"mobileLandscape\\\":1},\\\"minimumFontSizes\\\":{\\\"desktopPortrait\\\":4,\\\"desktopLandscape\\\":4,\\\"tabletPortrait\\\":4,\\\"tabletLandscape\\\":4,\\\"mobilePortrait\\\":4,\\\"mobileLandscape\\\":4},\\\"ratioToDevice\\\":{\\\"Portrait\\\":{\\\"tablet\\\":0.7,\\\"mobile\\\":0.5},\\\"Landscape\\\":{\\\"tablet\\\":0,\\\"mobile\\\":0}},\\\"sliderWidthToDevice\\\":{\\\"desktopPortrait\\\":1200,\\\"desktopLandscape\\\":1200,\\\"tabletPortrait\\\":840,\\\"tabletLandscape\\\":0,\\\"mobilePortrait\\\":600,\\\"mobileLandscape\\\":0},\\\"basedOn\\\":\\\"combined\\\",\\\"orientationMode\\\":\\\"width_and_height\\\",\\\"scrollFix\\\":0,\\\"overflowHiddenPage\\\":0,\\\"desktopPortraitScreenWidth\\\":1200,\\\"tabletPortraitScreenWidth\\\":800,\\\"mobilePortraitScreenWidth\\\":440,\\\"tabletLandscapeScreenWidth\\\":800,\\\"mobileLandscapeScreenWidth\\\":440},\\\"controls\\\":{\\\"scroll\\\":0,\\\"drag\\\":1,\\\"touch\\\":\\\"horizontal\\\",\\\"keyboard\\\":1,\\\"tilt\\\":0},\\\"lazyLoad\\\":0,\\\"lazyLoadNeighbor\\\":0,\\\"blockrightclick\\\":0,\\\"maintainSession\\\":0,\\\"autoplay\\\":{\\\"enabled\\\":1,\\\"start\\\":1,\\\"duration\\\":8000,\\\"autoplayToSlide\\\":-1,\\\"autoplayToSlideIndex\\\":-1,\\\"allowReStart\\\":0,\\\"pause\\\":{\\\"click\\\":1,\\\"mouse\\\":\\\"0\\\",\\\"mediaStarted\\\":1},\\\"resume\\\":{\\\"click\\\":0,\\\"mouse\\\":0,\\\"mediaEnded\\\":1,\\\"slidechanged\\\":0}},\\\"perspective\\\":1500,\\\"layerMode\\\":{\\\"playOnce\\\":0,\\\"playFirstLayer\\\":1,\\\"mode\\\":\\\"skippable\\\",\\\"inAnimation\\\":\\\"mainInEnd\\\"},\\\"background.parallax.tablet\\\":0,\\\"background.parallax.mobile\\\":0,\\\"initCallbacks\\\":[\\\"N2D(\\\\\\\"SmartSliderWidgetArrowImage\\\\\\\",function(i,e){function t(e,t,s,h){this.slider=e,this.slider.started(i.proxy(this.start,this,t,s,h))}return t.prototype.start=function(e,t,s){return this.slider.sliderElement.data(\\\\\\\"arrow\\\\\\\")?!1:(this.slider.sliderElement.data(\\\\\\\"arrow\\\\\\\",this),this.deferred=i.Deferred(),this.slider.sliderElement.on(\\\\\\\"SliderDevice\\\\\\\",i.proxy(this.onDevice,this)).trigger(\\\\\\\"addWidget\\\\\\\",this.deferred),this.previous=i(\\\\\\\"#\\\\\\\"+this.slider.elementID+\\\\\\\"-arrow-previous\\\\\\\").on(\\\\\\\"click\\\\\\\",i.proxy(function(i){i.stopPropagation(),this.slider[n2const.rtl.previous]()},this)),this.previousResize=this.previous.find(\\\\\\\".n2-resize\\\\\\\"),0===this.previousResize.length&&(this.previousResize=this.previous),this.next=i(\\\\\\\"#\\\\\\\"+this.slider.elementID+\\\\\\\"-arrow-next\\\\\\\").on(\\\\\\\"click\\\\\\\",i.proxy(function(i){i.stopPropagation(),this.slider[n2const.rtl.next]()},this)),this.nextResize=this.next.find(\\\\\\\".n2-resize\\\\\\\"),0===this.nextResize.length&&(this.nextResize=this.next),this.desktopRatio=e,this.tabletRatio=t,this.mobileRatio=s,void i.when(this.previous.n2imagesLoaded(),this.next.n2imagesLoaded()).always(i.proxy(this.loaded,this)))},t.prototype.loaded=function(){this.previousResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"inline-block\\\\\\\"),this.previousWidth=this.previousResize.width(),this.previousHeight=this.previousResize.height(),this.previousResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"\\\\\\\"),this.nextResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"inline-block\\\\\\\"),this.nextWidth=this.nextResize.width(),this.nextHeight=this.nextResize.height(),this.nextResize.css(\\\\\\\"display\\\\\\\",\\\\\\\"\\\\\\\"),this.previousResize.find(\\\\\\\"img\\\\\\\").css(\\\\\\\"width\\\\\\\",\\\\\\\"100%\\\\\\\"),this.nextResize.find(\\\\\\\"img\\\\\\\").css(\\\\\\\"width\\\\\\\",\\\\\\\"100%\\\\\\\"),this.onDevice(null,{device:this.slider.responsive.getDeviceMode()}),this.deferred.resolve()},t.prototype.onDevice=function(i,e){var t=1;switch(e.device){case\\\\\\\"tablet\\\\\\\":t=this.tabletRatio;break;case\\\\\\\"mobile\\\\\\\":t=this.mobileRatio;break;default:t=this.desktopRatio}this.previousResize.width(this.previousWidth*t),this.previousResize.height(this.previousHeight*t),this.nextResize.width(this.nextWidth*t),this.nextResize.height(this.nextHeight*t)},t});\\\",\\\"new N2Classes.SmartSliderWidgetArrowImage(this, 1, 0.7, 0.5);\\\",\\\"N2D(\\\\\\\"SmartSliderWidgetBulletTransition\\\\\\\",function(t,e){function i(e,i){this.slider=e,this.slider.started(t.proxy(this.start,this,i))}return i.prototype.start=function(e){if(this.slider.sliderElement.data(\\\\\\\"bullet\\\\\\\"))return!1;if(this.slider.sliderElement.data(\\\\\\\"bullet\\\\\\\",this),this.axis=\\\\\\\"horizontal\\\\\\\",this.offset=0,this.parameters=e,this.bar=this.slider.sliderElement.find(\\\\\\\".nextend-bullet-bar\\\\\\\"),this.event=\\\\\\\"universalclick\\\\\\\",\\\\\\\"mouseenter\\\\\\\"===this.parameters.action&&(this.event=\\\\\\\"mouseenter\\\\\\\"),this.slider.sliderElement.on({slideCountChanged:t.proxy(this.onSlideCountChanged,this),sliderSwitchTo:t.proxy(this.onSlideSwitch,this)}),this.slider.firstSlideReady.done(t.proxy(this.onFirstSlideSet,this)),0===e.overlay){var i=!1;switch(e.area){case 1:i=\\\\\\\"Top\\\\\\\";break;case 12:i=\\\\\\\"Bottom\\\\\\\";break;case 5:i=\\\\\\\"Left\\\\\\\",this.axis=\\\\\\\"vertical\\\\\\\";break;case 8:i=\\\\\\\"Right\\\\\\\",this.axis=\\\\\\\"vertical\\\\\\\"}i&&(this.offset=parseFloat(this.bar.data(\\\\\\\"offset\\\\\\\")),this.slider.responsive.addStaticMargin(i,this))}},i.prototype.onFirstSlideSet=function(t){this.onSlideCountChanged(),this.$dots.eq(t.index).addClass(\\\\\\\"n2-active\\\\\\\")},i.prototype.onDotClick=function(e,i){this.slider.directionalChangeTo(e),t(i.target).blur()},i.prototype.onSlideSwitch=function(t,e){this.$dots.filter(\\\\\\\".n2-active\\\\\\\").removeClass(\\\\\\\"n2-active\\\\\\\"),this.$dots.eq(e).addClass(\\\\\\\"n2-active\\\\\\\")},i.prototype.isVisible=function(){return this.bar.is(\\\\\\\":visible\\\\\\\")},i.prototype.getSize=function(){return\\\\\\\"horizontal\\\\\\\"===this.axis?this.bar.height()+this.offset:this.bar.width()+this.offset},i.prototype.showThumbnail=function(e,i){var s=this.getThumbnail(e);NextendTween.to(s,.3,{opacity:1}),this.$dots.eq(e).one(\\\\\\\"universalleave.thumbnailleave\\\\\\\",t.proxy(this.hideThumbnail,this,e,s))},i.prototype.hideThumbnail=function(t,e,i){i.stopPropagation(),NextendTween.to(e,.3,{opacity:0,onComplete:function(){e.remove()}})},i.prototype.getThumbnail=function(e){var i=this.$dots.eq(e),s=this.slider.sliderElement.offset(),a=i.offset(),o=i.outerWidth(),r=i.outerHeight(),n=t(\\\\\\\"<div\\\\\\/>\\\\\\\").append(t(\\\\\\\"<div\\\\\\/>\\\\\\\").css({width:this.parameters.thumbnailWidth,height:this.parameters.thumbnailHeight,backgroundImage:\'url(\\\\\\\"\'+this.slider.slides[e].getThumbnail()+\'\\\\\\\")\'}).addClass(\\\\\\\"n2-ss-bullet-thumbnail\\\\\\\")).addClass(this.parameters.thumbnailStyle).addClass(\\\\\\\"n2-ss-bullet-thumbnail-container\\\\\\\").appendTo(this.slider.sliderElement);switch(this.parameters.thumbnailPosition){case\\\\\\\"right\\\\\\\":n.css({left:a.left-s.left+o,top:a.top-s.top+r\\\\\\/2-n.outerHeight(!0)\\\\\\/2});break;case\\\\\\\"left\\\\\\\":n.css({left:a.left-s.left-n.outerWidth(!0),top:a.top-s.top+r\\\\\\/2-n.outerHeight(!0)\\\\\\/2});break;case\\\\\\\"top\\\\\\\":n.css({left:a.left-s.left+o\\\\\\/2-n.outerWidth(!0)\\\\\\/2,top:a.top-s.top-n.outerHeight(!0)});break;case\\\\\\\"bottom\\\\\\\":n.css({left:a.left-s.left+o\\\\\\/2-n.outerWidth(!0)\\\\\\/2,top:a.top-s.top+r})}return i.data(\\\\\\\"thumbnail\\\\\\\",n),n},i.prototype.onSlideCountChanged=function(){this.bar.html(\\\\\\\"\\\\\\\");for(var e=0;e<this.slider.slides.length;e++){var i=this.slider.slides[e],s=t(\'<div class=\\\\\\\"n2-ow \'+this.parameters.dotClasses+\'\\\\\\\" tabindex=\\\\\\\"0\\\\\\\"><\\\\\\/div>\').on(this.event,t.proxy(this.onDotClick,this,e)).appendTo(this.bar);switch(this.parameters.mode){case\\\\\\\"numeric\\\\\\\":s.html(e+1);break;case\\\\\\\"title\\\\\\\":s.html(i.getTitle())}if(1===this.parameters.thumbnail){var a=i.getThumbnail();a&&s.on({universalenter:t.proxy(this.showThumbnail,this,e)},{leaveOnSecond:!0})}}this.$dots=this.bar.find(\\\\\\\">*\\\\\\\")},i});\\\",\\\"new N2Classes.SmartSliderWidgetBulletTransition(this, {\\\\\\\"overlay\\\\\\\":1,\\\\\\\"area\\\\\\\":12,\\\\\\\"dotClasses\\\\\\\":\\\\\\\"n2-style-09efebcef1f2f45d29438e0cabcf79bc-dot \\\\\\\",\\\\\\\"mode\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"action\\\\\\\":\\\\\\\"click\\\\\\\",\\\\\\\"thumbnail\\\\\\\":1,\\\\\\\"thumbnailWidth\\\\\\\":120,\\\\\\\"thumbnailHeight\\\\\\\":81,\\\\\\\"thumbnailStyle\\\\\\\":\\\\\\\"n2-style-42845aa02120076deb3a5681d1d750ac-simple \\\\\\\",\\\\\\\"thumbnailPosition\\\\\\\":\\\\\\\"top\\\\\\\"});\\\"],\\\"allowBGImageAttachmentFixed\\\":false,\\\"bgAnimationsColor\\\":\\\"RGBA(51,51,51,1)\\\",\\\"bgAnimations\\\":0,\\\"mainanimation\\\":{\\\"type\\\":\\\"horizontal\\\",\\\"duration\\\":600,\\\"delay\\\":0,\\\"ease\\\":\\\"easeOutQuad\\\",\\\"parallax\\\":0,\\\"shiftedBackgroundAnimation\\\":0},\\\"carousel\\\":1,\\\"dynamicHeight\\\":0});});\"],\"globalInline\":[]},\"googleFonts\":{\"staticGroup\":[],\"files\":{\"Oswald\":[\"300\",\"400\"]},\"urls\":[],\"codes\":[],\"firstCodes\":[],\"inline\":[],\"globalInline\":[]},\"image\":{\"images\":[\"\\/\\/localhost\\/wordpress-project\\/jess\\/wp-content\\/uploads\\/2018\\/10\\/banner-1.jpg\"]}}}', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_generators`
--

CREATE TABLE `wp_nextend2_smartslider3_generators` (
  `id` int(11) NOT NULL,
  `group` varchar(254) NOT NULL,
  `type` varchar(254) NOT NULL,
  `params` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_sliders`
--

CREATE TABLE `wp_nextend2_smartslider3_sliders` (
  `id` int(11) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `title` varchar(100) NOT NULL,
  `type` varchar(30) NOT NULL,
  `params` mediumtext NOT NULL,
  `time` datetime NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_sliders`
--

INSERT INTO `wp_nextend2_smartslider3_sliders` (`id`, `alias`, `title`, `type`, `params`, `time`, `thumbnail`, `ordering`) VALUES
(1, NULL, 'Sample Slider', 'simple', '{\"controlsScroll\":\"0\",\"controlsDrag\":\"1\",\"controlsTouch\":\"horizontal\",\"controlsKeyboard\":\"1\",\"controlsTilt\":\"0\",\"thumbnail\":\"\",\"align\":\"normal\",\"backgroundMode\":\"fill\",\"animation\":\"horizontal\",\"animation-duration\":\"600\",\"animation-delay\":\"0\",\"animation-easing\":\"easeOutQuad\",\"animation-parallax-overlap\":\"0\",\"background-animation\":\"\",\"background-animation-speed\":\"normal\",\"animation-shifted-background-animation\":\"auto\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"carousel\":\"1\",\"background\":\"\",\"background-fixed\":\"0\",\"background-size\":\"cover\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"backgroundVideoMode\":\"fill\",\"dynamic-height\":\"0\",\"loop-single-slide\":\"0\",\"padding\":\"0|*|0|*|0|*|0\",\"border-width\":\"0\",\"border-color\":\"3E3E3Eff\",\"border-radius\":\"0\",\"slider-preset\":\"\",\"slider-css\":\"\",\"slide-css\":\"\",\"width\":\"1200\",\"height\":\"600\",\"desktop-portrait-minimum-font-size\":\"1\",\"desktop-landscape\":\"0\",\"desktop-landscape-width\":\"1440\",\"desktop-landscape-height\":\"0\",\"desktop-landscape-minimum-font-size\":\"1\",\"fontsize\":\"16\",\"desktop\":\"1\",\"tablet\":\"1\",\"mobile\":\"1\",\"margin\":\"0|*|0|*|0|*|0\",\"tablet-portrait\":\"0\",\"tablet-portrait-width\":\"800\",\"tablet-portrait-height\":\"0\",\"tablet-portrait-minimum-font-size\":\"1\",\"tablet-landscape\":\"0\",\"tablet-landscape-width\":\"1024\",\"tablet-landscape-height\":\"0\",\"tablet-landscape-minimum-font-size\":\"1\",\"mobile-portrait\":\"0\",\"mobile-portrait-width\":\"440\",\"mobile-portrait-height\":\"0\",\"mobile-portrait-minimum-font-size\":\"1\",\"mobile-landscape\":\"0\",\"mobile-landscape-width\":\"740\",\"mobile-landscape-height\":\"0\",\"mobile-landscape-minimum-font-size\":\"1\",\"responsive-mode\":\"auto\",\"responsiveScaleDown\":\"1\",\"responsiveScaleUp\":\"1\",\"responsiveSliderHeightMin\":\"0\",\"responsiveSliderHeightMax\":\"3000\",\"responsiveSlideWidthMax\":\"3000\",\"autoplay\":\"1\",\"autoplayDuration\":\"8000\",\"autoplayStart\":\"1\",\"autoplayfinish\":\"0|*|loop|*|current\",\"autoplayAllowReStart\":\"0\",\"autoplayStopClick\":\"1\",\"autoplayStopMouse\":\"0\",\"autoplayStopMedia\":\"1\",\"autoplayResumeClick\":\"0\",\"autoplayResumeMouse\":\"0\",\"autoplayResumeMedia\":\"1\",\"playfirstlayer\":\"1\",\"playonce\":\"0\",\"layer-animation-play-in\":\"end\",\"layer-animation-play-mode\":\"skippable\",\"parallax-enabled\":\"1\",\"parallax-enabled-mobile\":\"0\",\"parallax-3d\":\"0\",\"parallax-animate\":\"1\",\"parallax-horizontal\":\"mouse\",\"parallax-vertical\":\"mouse\",\"parallax-mouse-origin\":\"slider\",\"parallax-scroll-move\":\"both\",\"perspective\":\"1000\",\"imageload\":\"0\",\"imageloadNeighborSlides\":\"0\",\"optimize\":\"0\",\"optimize-quality\":\"70\",\"optimize-background-image-custom\":\"0\",\"optimize-background-image-width\":\"800\",\"optimize-background-image-height\":\"600\",\"optimizeThumbnailWidth\":\"100\",\"optimizeThumbnailHeight\":\"60\",\"layer-image-optimize\":\"0\",\"layer-image-tablet\":\"50\",\"layer-image-mobile\":\"30\",\"layer-image-base64\":\"0\",\"layer-image-base64-size\":\"5\",\"playWhenVisible\":\"1\",\"fadeOnLoad\":\"1\",\"fadeOnScroll\":\"0\",\"spinner\":\"simpleWhite\",\"custom-spinner\":\"\",\"custom-spinner-width\":\"100\",\"custom-spinner-height\":\"100\",\"custom-display\":\"1\",\"dependency\":\"\",\"delay\":\"0\",\"is-delayed\":\"0\",\"randomize\":\"0\",\"randomizeFirst\":\"0\",\"randomize-cache\":\"1\",\"variations\":\"5\",\"maximumslidecount\":\"1000\",\"global-lightbox\":\"0\",\"global-lightbox-label\":\"0\",\"maintain-session\":\"0\",\"blockrightclick\":\"0\",\"overflow-hidden-page\":\"0\",\"scroll-fix\":\"0\",\"bg-parallax-tablet\":\"1\",\"bg-parallax-mobile\":\"1\",\"callbacks\":\"\",\"widgetarrow\":\"imageEmpty\",\"widget-arrow-display-desktop\":\"1\",\"widget-arrow-display-tablet\":\"1\",\"widget-arrow-display-mobile\":\"1\",\"widget-arrow-exclude-slides\":\"\",\"widget-arrow-display-hover\":\"0\",\"widget-arrow-responsive-desktop\":\"1\",\"widget-arrow-responsive-tablet\":\"0.7\",\"widget-arrow-responsive-mobile\":\"0.5\",\"widget-arrow-previous-image\":\"\",\"widget-arrow-previous\":\"$ss$/plugins/widgetarrow/image/image/previous/thin-horizontal.svg\",\"widget-arrow-previous-color\":\"ffffffcc\",\"widget-arrow-previous-hover\":\"0\",\"widget-arrow-previous-hover-color\":\"ffffffcc\",\"widget-arrow-style\":\"\",\"widget-arrow-previous-position-mode\":\"simple\",\"widget-arrow-previous-position-area\":\"6\",\"widget-arrow-previous-position-stack\":\"1\",\"widget-arrow-previous-position-offset\":\"15\",\"widget-arrow-previous-position-horizontal\":\"left\",\"widget-arrow-previous-position-horizontal-position\":\"0\",\"widget-arrow-previous-position-horizontal-unit\":\"px\",\"widget-arrow-previous-position-vertical\":\"top\",\"widget-arrow-previous-position-vertical-position\":\"0\",\"widget-arrow-previous-position-vertical-unit\":\"px\",\"widget-arrow-next-position-mode\":\"simple\",\"widget-arrow-next-position-area\":\"7\",\"widget-arrow-next-position-stack\":\"1\",\"widget-arrow-next-position-offset\":\"15\",\"widget-arrow-next-position-horizontal\":\"left\",\"widget-arrow-next-position-horizontal-position\":\"0\",\"widget-arrow-next-position-horizontal-unit\":\"px\",\"widget-arrow-next-position-vertical\":\"top\",\"widget-arrow-next-position-vertical-position\":\"0\",\"widget-arrow-next-position-vertical-unit\":\"px\",\"widget-arrow-animation\":\"fade\",\"widget-arrow-mirror\":\"1\",\"widget-arrow-next-image\":\"\",\"widget-arrow-next\":\"$ss$/plugins/widgetarrow/image/image/next/thin-horizontal.svg\",\"widget-arrow-next-color\":\"ffffffcc\",\"widget-arrow-next-hover\":\"0\",\"widget-arrow-next-hover-color\":\"ffffffcc\",\"widgetbullet\":\"transition\",\"widget-bullet-display-desktop\":\"1\",\"widget-bullet-display-tablet\":\"1\",\"widget-bullet-display-mobile\":\"1\",\"widget-bullet-exclude-slides\":\"\",\"widget-bullet-display-hover\":\"0\",\"widget-bullet-thumbnail-show-image\":\"1\",\"widget-bullet-thumbnail-width\":\"120\",\"widget-bullet-thumbnail-height\":\"81\",\"widget-bullet-thumbnail-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwODAiLCJwYWRkaW5nIjoiM3wqfDN8KnwzfCp8M3wqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiIzIiwiZXh0cmEiOiJtYXJnaW46IDVweDsifV19\",\"widget-bullet-thumbnail-side\":\"before\",\"widget-bullet-position-mode\":\"simple\",\"widget-bullet-position-area\":\"12\",\"widget-bullet-position-stack\":\"1\",\"widget-bullet-position-offset\":\"10\",\"widget-bullet-position-horizontal\":\"left\",\"widget-bullet-position-horizontal-position\":\"0\",\"widget-bullet-position-horizontal-unit\":\"px\",\"widget-bullet-position-vertical\":\"top\",\"widget-bullet-position-vertical-position\":\"0\",\"widget-bullet-position-vertical-unit\":\"px\",\"widget-bullet-action\":\"click\",\"widget-bullet-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwYWIiLCJwYWRkaW5nIjoiNXwqfDV8Knw1fCp8NXwqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiI1MCIsImV4dHJhIjoibWFyZ2luOiA0cHg7In0seyJleHRyYSI6IiIsImJhY2tncm91bmRjb2xvciI6IjA5YjQ3NGZmIn1dfQ==\",\"widget-bullet-bar\":\"\",\"widget-bullet-bar-full-size\":\"0\",\"widget-bullet-align\":\"center\",\"widget-bullet-orientation\":\"auto\",\"widget-bullet-overlay\":\"0\",\"widgetautoplay\":\"disabled\",\"widget-autoplay-display-desktop\":\"1\",\"widget-autoplay-display-tablet\":\"1\",\"widget-autoplay-display-mobile\":\"1\",\"widget-autoplay-exclude-slides\":\"\",\"widget-autoplay-display-hover\":\"0\",\"widgetindicator\":\"disabled\",\"widget-indicator-display-desktop\":\"1\",\"widget-indicator-display-tablet\":\"1\",\"widget-indicator-display-mobile\":\"1\",\"widget-indicator-exclude-slides\":\"\",\"widget-indicator-display-hover\":\"0\",\"widgetbar\":\"disabled\",\"widget-bar-display-desktop\":\"1\",\"widget-bar-display-tablet\":\"1\",\"widget-bar-display-mobile\":\"1\",\"widget-bar-exclude-slides\":\"\",\"widget-bar-display-hover\":\"0\",\"widgetthumbnail\":\"disabled\",\"widget-thumbnail-display-desktop\":\"1\",\"widget-thumbnail-display-tablet\":\"1\",\"widget-thumbnail-display-mobile\":\"1\",\"widget-thumbnail-exclude-slides\":\"\",\"widget-thumbnail-display-hover\":\"0\",\"widget-thumbnail-show-image\":\"1\",\"widget-thumbnail-width\":\"100\",\"widget-thumbnail-height\":\"60\",\"widgetshadow\":\"disabled\",\"widget-shadow-display-desktop\":\"1\",\"widget-shadow-display-tablet\":\"1\",\"widget-shadow-display-mobile\":\"1\",\"widget-shadow-exclude-slides\":\"\",\"widgetfullscreen\":\"disabled\",\"widget-fullscreen-display-desktop\":\"1\",\"widget-fullscreen-display-tablet\":\"1\",\"widget-fullscreen-display-mobile\":\"1\",\"widget-fullscreen-exclude-slides\":\"\",\"widget-fullscreen-display-hover\":\"0\",\"widgethtml\":\"disabled\",\"widget-html-display-desktop\":\"1\",\"widget-html-display-tablet\":\"1\",\"widget-html-display-mobile\":\"1\",\"widget-html-exclude-slides\":\"\",\"widget-html-display-hover\":\"0\",\"widgets\":\"arrow\"}', '2015-11-01 14:14:20', '', 0),
(3, NULL, 'Home Slider', 'simple', '{\"alias-id\":\"\",\"alias-smoothscroll\":\"\",\"controlsScroll\":\"0\",\"controlsDrag\":\"1\",\"controlsTouch\":\"horizontal\",\"controlsKeyboard\":\"1\",\"align\":\"normal\",\"backgroundMode\":\"fill\",\"animation\":\"horizontal\",\"animation-duration\":\"600\",\"background-animation\":\"\",\"background-animation-color\":\"333333ff\",\"background-animation-speed\":\"normal\",\"width\":\"1200\",\"height\":\"600\",\"margin\":\"0|*|0|*|0|*|0\",\"responsive-mode\":\"auto\",\"responsiveScaleDown\":\"1\",\"responsiveScaleUp\":\"1\",\"responsiveSliderHeightMin\":\"0\",\"responsiveSliderHeightMax\":\"3000\",\"responsiveSlideWidthMax\":\"3000\",\"autoplay\":\"1\",\"autoplayDuration\":\"8000\",\"autoplayStopClick\":\"1\",\"autoplayStopMouse\":\"0\",\"autoplayStopMedia\":\"1\",\"optimize\":\"0\",\"optimize-quality\":\"70\",\"optimize-background-image-custom\":\"0\",\"optimize-background-image-width\":\"800\",\"optimize-background-image-height\":\"600\",\"optimizeThumbnailWidth\":\"100\",\"optimizeThumbnailHeight\":\"60\",\"playWhenVisible\":\"1\",\"playWhenVisibleAt\":\"50\",\"dependency\":\"\",\"delay\":\"0\",\"is-delayed\":\"0\",\"overflow-hidden-page\":\"0\",\"clear-both\":\"0\",\"clear-both-after\":\"1\",\"custom-css-codes\":\"\",\"callbacks\":\"\",\"related-posts\":\"\",\"widgetarrow\":\"imageEmpty\",\"widget-arrow-display-hover\":\"0\",\"widget-arrow-previous\":\"$ss$\\/plugins\\/widgetarrow\\/image\\/image\\/previous\\/thin-horizontal.svg\",\"widget-arrow-previous-color\":\"ffffffcc\",\"widget-arrow-previous-hover\":\"0\",\"widget-arrow-previous-hover-color\":\"ffffffcc\",\"widget-arrow-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwZmYiLCJvcGFjaXR5IjoxMDAsInBhZGRpbmciOiIxMHwqfDMwfCp8MTB8KnwzMHwqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMnwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiIwIiwiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"widget-arrow-previous-position-mode\":\"simple\",\"widget-arrow-previous-position-area\":\"6\",\"widget-arrow-previous-position-stack\":\"1\",\"widget-arrow-previous-position-offset\":\"15\",\"widget-arrow-previous-position-horizontal\":\"left\",\"widget-arrow-previous-position-horizontal-position\":\"0\",\"widget-arrow-previous-position-horizontal-unit\":\"px\",\"widget-arrow-previous-position-vertical\":\"top\",\"widget-arrow-previous-position-vertical-position\":\"0\",\"widget-arrow-previous-position-vertical-unit\":\"px\",\"widget-arrow-next-position-mode\":\"simple\",\"widget-arrow-next-position-area\":\"7\",\"widget-arrow-next-position-stack\":\"1\",\"widget-arrow-next-position-offset\":\"15\",\"widget-arrow-next-position-horizontal\":\"left\",\"widget-arrow-next-position-horizontal-position\":\"0\",\"widget-arrow-next-position-horizontal-unit\":\"px\",\"widget-arrow-next-position-vertical\":\"top\",\"widget-arrow-next-position-vertical-position\":\"0\",\"widget-arrow-next-position-vertical-unit\":\"px\",\"widget-arrow-previous-alt\":\"previous arrow\",\"widget-arrow-next-alt\":\"next arrow\",\"widgetbullet\":\"transition\",\"widget-bullet-display-hover\":\"0\",\"widget-bullet-thumbnail-show-image\":\"1\",\"widget-bullet-thumbnail-width\":\"120\",\"widget-bullet-thumbnail-height\":\"81\",\"widget-bullet-thumbnail-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwODAiLCJwYWRkaW5nIjoiM3wqfDN8KnwzfCp8M3wqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiIzIiwiZXh0cmEiOiJtYXJnaW46IDVweDsifV19\",\"widget-bullet-thumbnail-side\":\"before\",\"widget-bullet-position-mode\":\"simple\",\"widget-bullet-position-area\":\"12\",\"widget-bullet-position-stack\":\"1\",\"widget-bullet-position-offset\":\"10\",\"widget-bullet-position-horizontal\":\"left\",\"widget-bullet-position-horizontal-position\":\"0\",\"widget-bullet-position-horizontal-unit\":\"px\",\"widget-bullet-position-vertical\":\"top\",\"widget-bullet-position-vertical-position\":\"0\",\"widget-bullet-position-vertical-unit\":\"px\",\"widget-bullet-style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwYWIiLCJwYWRkaW5nIjoiNXwqfDV8Knw1fCp8NXwqfHB4IiwiYm94c2hhZG93IjoiMHwqfDB8KnwwfCp8MHwqfDAwMDAwMGZmIiwiYm9yZGVyIjoiMHwqfHNvbGlkfCp8MDAwMDAwZmYiLCJib3JkZXJyYWRpdXMiOiI1MCIsImV4dHJhIjoibWFyZ2luOiA0cHg7In0seyJleHRyYSI6IiIsImJhY2tncm91bmRjb2xvciI6IjA5YjQ3NGZmIn1dfQ==\",\"widget-bullet-bar\":\"\",\"widgetautoplay\":\"disabled\",\"widget-autoplay-display-hover\":\"0\",\"widgetbar\":\"disabled\",\"widget-bar-display-hover\":\"0\",\"widgetthumbnail\":\"disabled\",\"widget-thumbnail-display-hover\":\"0\",\"widget-thumbnail-width\":\"100\",\"widget-thumbnail-height\":\"60\",\"widgetshadow\":\"disabled\",\"widgets\":\"arrow\"}', '2018-10-09 11:54:58', '$upload$/2018/10/banner-1.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_sliders_xref`
--

CREATE TABLE `wp_nextend2_smartslider3_sliders_xref` (
  `group_id` int(11) NOT NULL,
  `slider_id` int(11) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `wp_nextend2_smartslider3_slides`
--

CREATE TABLE `wp_nextend2_smartslider3_slides` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `slider` int(11) NOT NULL,
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `published` tinyint(1) NOT NULL,
  `first` int(11) NOT NULL,
  `slide` longtext,
  `description` text NOT NULL,
  `thumbnail` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `ordering` int(11) NOT NULL,
  `generator_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `wp_nextend2_smartslider3_slides`
--

INSERT INTO `wp_nextend2_smartslider3_slides` (`id`, `title`, `slider`, `publish_up`, `publish_down`, `published`, `first`, `slide`, `description`, `thumbnail`, `params`, `ordering`, `generator_id`) VALUES
(1, 'Slide One', 1, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":null,\"class\":\"\",\"crop\":\"\",\"parallax\":0,\"adaptivefont\":1,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"10|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Martin Dwyer\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Martin Dwyer\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6IjBiMGIwYmZmIiwic2l6ZSI6IjM2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxLjUiLCJib2xkIjowLCJpdGFsaWMiOjAsInVuZGVybGluZSI6MCwiYWxpZ24iOiJjZW50ZXIiLCJsZXR0ZXJzcGFjaW5nIjoiMTBweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6InVwcGVyY2FzZSJ9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiJmZmZmZmZjYyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjAuNHwqfDF8KnwwLjR8KnwxfCp8ZW0iLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAifSx7ImV4dHJhIjoiIn1dfQ==\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}},{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"0|*|0|*|0|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Application Developer\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Application Developer\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"1\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjIyfHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxIiwiYm9sZCI6MCwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjJweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6Im5vbmUifSx7ImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwY2MiLCJwYWRkaW5nIjoiMC44fCp8MXwqfDAuOHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiMCIsImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}}]}]', '', 'https://smartslider3.com/sample/developerthumbnail.jpg', '{\"background-type\":\"image\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"preload\":\"auto\",\"backgroundVideoMode\":\"fill\",\"backgroundImage\":\"https://smartslider3.com/sample/programmer.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"default\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.2.0\"}', 0, 0),
(2, 'Slide Two', 1, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":null,\"class\":\"\",\"crop\":\"\",\"parallax\":0,\"adaptivefont\":1,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"10|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Rachel Wright\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Rachel Wright\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6IjBiMGIwYmZmIiwic2l6ZSI6IjM2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxLjUiLCJib2xkIjowLCJpdGFsaWMiOjAsInVuZGVybGluZSI6MCwiYWxpZ24iOiJjZW50ZXIiLCJsZXR0ZXJzcGFjaW5nIjoiMTBweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6InVwcGVyY2FzZSJ9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiJmZmZmZmZjYyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjAuNHwqfDF8KnwwLjR8KnwxfCp8ZW0iLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAifSx7ImV4dHJhIjoiIn1dfQ==\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}},{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"0|*|0|*|0|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Art Director & Photographer\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Art Director & Photographer\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"1\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjIyfHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxIiwiYm9sZCI6MCwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjJweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6Im5vbmUifSx7ImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwY2MiLCJwYWRkaW5nIjoiMC44fCp8MXwqfDAuOHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiMCIsImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}}]}]', '', 'https://smartslider3.com/sample/artdirectorthumbnail.jpg', '{\"background-type\":\"image\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"preload\":\"auto\",\"backgroundVideoMode\":\"fill\",\"backgroundImage\":\"https://smartslider3.com/sample/free1.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"default\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.2.0\"}', 1, 0),
(3, 'Slide Three', 1, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":null,\"class\":\"\",\"crop\":\"\",\"parallax\":0,\"adaptivefont\":1,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"10|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Andrew Butler\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Andrew Butler\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6IjBiMGIwYmZmIiwic2l6ZSI6IjM2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxLjUiLCJib2xkIjowLCJpdGFsaWMiOjAsInVuZGVybGluZSI6MCwiYWxpZ24iOiJjZW50ZXIiLCJsZXR0ZXJzcGFjaW5nIjoiMTBweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6InVwcGVyY2FzZSJ9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiJmZmZmZmZjYyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjAuNHwqfDF8KnwwLjR8KnwxfCp8ZW0iLCJib3hzaGFkb3ciOiIwfCp8MHwqfDB8KnwwfCp8MDAwMDAwZmYiLCJib3JkZXIiOiIwfCp8c29saWR8KnwwMDAwMDBmZiIsImJvcmRlcnJhZGl1cyI6IjAifSx7ImV4dHJhIjoiIn1dfQ==\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}},{\"type\":\"layer\",\"animations\":\"\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"0|*|0|*|0|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":null,\"class\":\"\",\"crop\":\"visible\",\"parallax\":0,\"adaptivefont\":0,\"mouseenter\":\"\",\"click\":\"\",\"mouseleave\":\"\",\"play\":\"\",\"pause\":\"\",\"stop\":\"\",\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Photographer & Illustrator\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"heading\":\"Photographer & Illustrator\",\"link\":\"#|*|_self\",\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"title\":\"\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjIyfHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiUmFsZXdheSxBcmlhbCIsImxpbmVoZWlnaHQiOiIxIiwiYm9sZCI6MCwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjJweCIsIndvcmRzcGFjaW5nIjoibm9ybWFsIiwidGV4dHRyYW5zZm9ybSI6Im5vbmUifSx7ImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siYmFja2dyb3VuZGNvbG9yIjoiMDAwMDAwY2MiLCJwYWRkaW5nIjoiMC44fCp8MXwqfDAuOHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiMCIsImV4dHJhIjoiIn0seyJleHRyYSI6IiJ9XX0=\",\"split-text-animation-in\":\"\",\"split-text-delay-in\":\"0\",\"split-text-animation-out\":\"\",\"split-text-delay-out\":\"0\",\"split-text-backface-visibility\":\"1\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"class\":\"\"}}}]}]', '', 'https://smartslider3.com/sample/photographerthumbnail.jpg', '{\"background-type\":\"image\",\"backgroundVideoMp4\":\"\",\"backgroundVideoMuted\":\"1\",\"backgroundVideoLoop\":\"1\",\"preload\":\"auto\",\"backgroundVideoMode\":\"fill\",\"backgroundImage\":\"https://smartslider3.com/sample/photographer.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"default\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"kenburns-animation\":\"50|*|50|*|\",\"kenburns-animation-speed\":\"default\",\"kenburns-animation-strength\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.2.0\"}', 2, 0),
(6, 'Slide One', 3, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"lastplacement\":\"content\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":\"\",\"uniqueclass\":\"\",\"class\":\"\",\"crop\":\"visible\",\"rotation\":0,\"parallax\":0,\"adaptivefont\":1,\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"lastplacement\":\"normal\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"195|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":\"\",\"uniqueclass\":\"\",\"class\":\"\",\"crop\":\"visible\",\"rotation\":0,\"parallax\":0,\"adaptivefont\":0,\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"WHEN ORDINARY ISN\'T ENOUGH\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"heading\":\"WHEN ORDINARY ISN\'T ENOUGH\",\"title\":\"\",\"link\":\"#|*|_self\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjU2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiJ09zd2FsZCcsIHNhbnMtc2VyaWYiLCJsaW5laGVpZ2h0IjoiMyIsImJvbGQiOiIxIiwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjEwcHgiLCJ3b3Jkc3BhY2luZyI6Im5vcm1hbCIsInRleHR0cmFuc2Zvcm0iOiJ1cHBlcmNhc2UiLCJ3ZWlnaHQiOjF9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiIwMDAwMDBiMyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjB8KnwxfCp8MHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiNSJ9LHsiZXh0cmEiOiIifV19\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"split-text-backface-visibility\":1,\"split-text-animation-in\":\"\",\"split-text-delay-in\":0,\"split-text-animation-out\":\"\",\"split-text-delay-out\":0,\"class\":\"\"}}}]}]', '', '$upload$/2018/10/banner-1.jpg', '{\"background-type\":\"image\",\"backgroundImage\":\"$upload$\\/2018\\/10\\/banner-1.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"fit\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.3.7\"}', 0, 0),
(9, 'Slide two', 3, '2015-11-01 12:27:34', '2025-11-11 12:27:34', 1, 0, '[{\"type\":\"content\",\"lastplacement\":\"content\",\"desktopportraitfontsize\":100,\"desktopportraitmaxwidth\":0,\"desktopportraitinneralign\":\"inherit\",\"desktopportraitpadding\":\"10|*|10|*|10|*|10|*|px+\",\"desktopportraitselfalign\":\"inherit\",\"mobileportraitfontsize\":60,\"opened\":1,\"id\":\"\",\"uniqueclass\":\"\",\"class\":\"\",\"crop\":\"visible\",\"rotation\":0,\"parallax\":0,\"adaptivefont\":1,\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"Content\",\"namesynced\":1,\"bgimage\":\"\",\"bgimagex\":50,\"bgimagey\":50,\"bgimageparallax\":0,\"bgcolor\":\"00000000\",\"bgcolorgradient\":\"off\",\"bgcolorgradientend\":\"00000000\",\"verticalalign\":\"center\",\"layers\":[{\"type\":\"layer\",\"lastplacement\":\"normal\",\"desktopportraitfontsize\":100,\"desktopportraitmargin\":\"195|*|0|*|10|*|0|*|px+\",\"desktopportraitheight\":0,\"desktopportraitmaxwidth\":0,\"desktopportraitselfalign\":\"inherit\",\"id\":\"\",\"uniqueclass\":\"\",\"class\":\"\",\"crop\":\"visible\",\"rotation\":0,\"parallax\":0,\"adaptivefont\":0,\"generatorvisible\":\"\",\"desktopportrait\":1,\"desktoplandscape\":1,\"tabletportrait\":1,\"tabletlandscape\":1,\"mobileportrait\":1,\"mobilelandscape\":1,\"name\":\"WHEN ORDINARY ISN\'T ENOUGH\",\"namesynced\":1,\"item\":{\"type\":\"heading\",\"values\":{\"priority\":\"2\",\"fullwidth\":\"0\",\"nowrap\":\"0\",\"heading\":\"WHEN ORDINARY ISN\'T ENOUGH\",\"title\":\"\",\"link\":\"#|*|_self\",\"font\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJjb2xvciI6ImZmZmZmZmZmIiwic2l6ZSI6IjU2fHxweCIsInRzaGFkb3ciOiIwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImFmb250IjoiJ09zd2FsZCcsIHNhbnMtc2VyaWYiLCJsaW5laGVpZ2h0IjoiMyIsImJvbGQiOiIxIiwiaXRhbGljIjowLCJ1bmRlcmxpbmUiOjAsImFsaWduIjoiY2VudGVyIiwibGV0dGVyc3BhY2luZyI6IjEwcHgiLCJ3b3Jkc3BhY2luZyI6Im5vcm1hbCIsInRleHR0cmFuc2Zvcm0iOiJ1cHBlcmNhc2UiLCJ3ZWlnaHQiOjF9LHsiZXh0cmEiOiIifSx7ImV4dHJhIjoiIn1dfQ==\",\"style\":\"eyJuYW1lIjoiU3RhdGljIiwiZGF0YSI6W3siZXh0cmEiOiIiLCJiYWNrZ3JvdW5kY29sb3IiOiIwMDAwMDBiMyIsIm9wYWNpdHkiOjEwMCwicGFkZGluZyI6IjB8KnwxfCp8MHwqfDF8KnxlbSIsImJveHNoYWRvdyI6IjB8KnwwfCp8MHwqfDB8KnwwMDAwMDBmZiIsImJvcmRlciI6IjB8Knxzb2xpZHwqfDAwMDAwMGZmIiwiYm9yZGVycmFkaXVzIjoiNSJ9LHsiZXh0cmEiOiIifV19\",\"split-text-transform-origin\":\"50|*|50|*|0\",\"split-text-backface-visibility\":1,\"split-text-animation-in\":\"\",\"split-text-delay-in\":0,\"split-text-animation-out\":\"\",\"split-text-delay-out\":0,\"class\":\"\"}}}]}]', '', '$upload$/2018/10/banner-1.jpg', '{\"background-type\":\"image\",\"backgroundImage\":\"$upload$\\/2018\\/10\\/banner-1.jpg\",\"backgroundFocusX\":\"50\",\"backgroundFocusY\":\"50\",\"backgroundImageOpacity\":\"100\",\"backgroundImageBlur\":\"0\",\"backgroundAlt\":\"\",\"backgroundTitle\":\"\",\"backgroundColor\":\"ffffff00\",\"backgroundGradient\":\"off\",\"backgroundColorEnd\":\"ffffff00\",\"backgroundMode\":\"fit\",\"background-animation\":\"\",\"background-animation-speed\":\"default\",\"thumbnailType\":\"default\",\"link\":\"|*|_self\",\"guides\":\"eyJob3Jpem9udGFsIjpbXSwidmVydGljYWwiOltdfQ==\",\"first\":\"0\",\"static-slide\":\"0\",\"slide-duration\":\"0\",\"version\":\"3.3.7\"}', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/wordpress-project/jess', 'yes'),
(2, 'home', 'http://localhost/wordpress-project/jess', 'yes'),
(3, 'blogname', 'Jess', 'yes'),
(4, 'blogdescription', 'Diesel Truck Mechanic, Diesel Engine Repair, Diesel Performance, Diesel Maintenance', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'user.citrusbug@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '1', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:225:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:45:\"make/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?pa_make=$matches[1]&feed=$matches[2]\";s:40:\"make/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?pa_make=$matches[1]&feed=$matches[2]\";s:21:\"make/([^/]+)/embed/?$\";s:40:\"index.php?pa_make=$matches[1]&embed=true\";s:33:\"make/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?pa_make=$matches[1]&paged=$matches[2]\";s:15:\"make/([^/]+)/?$\";s:29:\"index.php?pa_make=$matches[1]\";s:46:\"model/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pa_model=$matches[1]&feed=$matches[2]\";s:41:\"model/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pa_model=$matches[1]&feed=$matches[2]\";s:22:\"model/([^/]+)/embed/?$\";s:41:\"index.php?pa_model=$matches[1]&embed=true\";s:34:\"model/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pa_model=$matches[1]&paged=$matches[2]\";s:16:\"model/([^/]+)/?$\";s:30:\"index.php?pa_model=$matches[1]\";s:46:\"years/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pa_years=$matches[1]&feed=$matches[2]\";s:41:\"years/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pa_years=$matches[1]&feed=$matches[2]\";s:22:\"years/([^/]+)/embed/?$\";s:41:\"index.php?pa_years=$matches[1]&embed=true\";s:34:\"years/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pa_years=$matches[1]&paged=$matches[2]\";s:16:\"years/([^/]+)/?$\";s:30:\"index.php?pa_years=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"partners/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"partners/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"partners/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"partners/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"partners/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"partners/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"partners/([^/]+)/embed/?$\";s:41:\"index.php?partners=$matches[1]&embed=true\";s:29:\"partners/([^/]+)/trackback/?$\";s:35:\"index.php?partners=$matches[1]&tb=1\";s:37:\"partners/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?partners=$matches[1]&paged=$matches[2]\";s:44:\"partners/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?partners=$matches[1]&cpage=$matches[2]\";s:34:\"partners/([^/]+)/wc-api(/(.*))?/?$\";s:49:\"index.php?partners=$matches[1]&wc-api=$matches[3]\";s:40:\"partners/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\"partners/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:33:\"partners/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?partners=$matches[1]&page=$matches[2]\";s:25:\"partners/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"partners/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"partners/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"partners/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"partners/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"partners/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:39:\"latest_news/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:49:\"latest_news/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:69:\"latest_news/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"latest_news/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:64:\"latest_news/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:45:\"latest_news/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:28:\"latest_news/([^/]+)/embed/?$\";s:44:\"index.php?latest_news=$matches[1]&embed=true\";s:32:\"latest_news/([^/]+)/trackback/?$\";s:38:\"index.php?latest_news=$matches[1]&tb=1\";s:40:\"latest_news/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?latest_news=$matches[1]&paged=$matches[2]\";s:47:\"latest_news/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?latest_news=$matches[1]&cpage=$matches[2]\";s:37:\"latest_news/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?latest_news=$matches[1]&wc-api=$matches[3]\";s:43:\"latest_news/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:54:\"latest_news/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:36:\"latest_news/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?latest_news=$matches[1]&page=$matches[2]\";s:28:\"latest_news/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:38:\"latest_news/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:58:\"latest_news/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"latest_news/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:53:\"latest_news/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:34:\"latest_news/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"image_gallery/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"image_gallery/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"image_gallery/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"image_gallery/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"image_gallery/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"image_gallery/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"image_gallery/([^/]+)/embed/?$\";s:46:\"index.php?image_gallery=$matches[1]&embed=true\";s:34:\"image_gallery/([^/]+)/trackback/?$\";s:40:\"index.php?image_gallery=$matches[1]&tb=1\";s:42:\"image_gallery/([^/]+)/page/?([0-9]{1,})/?$\";s:53:\"index.php?image_gallery=$matches[1]&paged=$matches[2]\";s:49:\"image_gallery/([^/]+)/comment-page-([0-9]{1,})/?$\";s:53:\"index.php?image_gallery=$matches[1]&cpage=$matches[2]\";s:39:\"image_gallery/([^/]+)/wc-api(/(.*))?/?$\";s:54:\"index.php?image_gallery=$matches[1]&wc-api=$matches[3]\";s:45:\"image_gallery/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:56:\"image_gallery/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:38:\"image_gallery/([^/]+)(?:/([0-9]+))?/?$\";s:52:\"index.php?image_gallery=$matches[1]&page=$matches[2]\";s:30:\"image_gallery/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"image_gallery/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"image_gallery/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"image_gallery/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"image_gallery/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"image_gallery/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:10:{i:0;s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";i:1;s:30:\"advanced-custom-fields/acf.php\";i:2;s:25:\"breadcrumb/breadcrumb.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:5;s:34:\"custom-sidebars/customsidebars.php\";i:6;s:33:\"smart-slider-3/smart-slider-3.php\";i:7;s:27:\"woocommerce/woocommerce.php\";i:8;s:20:\"wp-fancybox/main.php\";i:9;s:31:\"wp-migrate-db/wp-migrate-db.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'Jess', 'yes'),
(41, 'stylesheet', 'Jess', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '38590', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '1', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'posts', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'file', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:5:{i:2;a:4:{s:5:\"title\";s:7:\"Find Us\";s:4:\"text\";s:168:\"<strong>Address</strong>\n123 Main Street\nNew York, NY 10001\n\n<strong>Hours</strong>\nMonday&mdash;Friday: 9:00AM&ndash;5:00PM\nSaturday &amp; Sunday: 11:00AM&ndash;3:00PM\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:3;a:4:{s:5:\"title\";s:15:\"About This Site\";s:4:\"text\";s:85:\"This may be a good place to introduce yourself and your site or include some credits.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:4;a:4:{s:5:\"title\";s:7:\"Find Us\";s:4:\"text\";s:168:\"<strong>Address</strong>\n123 Main Street\nNew York, NY 10001\n\n<strong>Hours</strong>\nMonday&mdash;Friday: 9:00AM&ndash;5:00PM\nSaturday &amp; Sunday: 11:00AM&ndash;3:00PM\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}i:5;a:4:{s:5:\"title\";s:15:\"About This Site\";s:4:\"text\";s:85:\"This may be a good place to introduce yourself and your site or include some credits.\";s:6:\"filter\";b:1;s:6:\"visual\";b:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:1:{s:48:\"woocommerce-ajax-filters/woocommerce-filters.php\";a:2:{i:0;s:13:\"BeRocket_AAPF\";i:1;s:24:\"br_delete_plugin_options\";}}', 'no'),
(82, 'timezone_string', '', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '0', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '0', 'yes'),
(93, 'initial_db_version', '38590', 'yes'),
(94, 'wp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:122:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:7:\"nextend\";b:1;s:14:\"nextend_config\";b:1;s:19:\"nextend_visual_edit\";b:1;s:21:\"nextend_visual_delete\";b:1;s:11:\"smartslider\";b:1;s:18:\"smartslider_config\";b:1;s:16:\"smartslider_edit\";b:1;s:18:\"smartslider_delete\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:42:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:7:\"nextend\";b:1;s:14:\"nextend_config\";b:1;s:19:\"nextend_visual_edit\";b:1;s:21:\"nextend_visual_delete\";b:1;s:11:\"smartslider\";b:1;s:18:\"smartslider_config\";b:1;s:16:\"smartslider_edit\";b:1;s:18:\"smartslider_delete\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(97, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'sidebars_widgets', 'a:10:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:25:\"woocommerce_layered_nav-4\";i:1;s:28:\"woocommerce_product_search-2\";i:2;s:6:\"text-2\";i:3;s:6:\"text-3\";i:4;s:6:\"text-4\";i:5;s:6:\"text-5\";}s:4:\"cs-1\";a:1:{i:0;s:25:\"woocommerce_layered_nav-2\";}s:4:\"cs-3\";a:1:{i:0;s:25:\"woocommerce_layered_nav-3\";}s:4:\"cs-2\";a:1:{i:0;s:25:\"woocommerce_layered_nav-5\";}s:4:\"cs-4\";a:1:{i:0;s:23:\"dgwt_wcas_ajax_search-2\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}s:18:\"smartslider_area_1\";a:1:{i:0;s:32:\"woocommerce_product_categories-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(102, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'cron', 'a:12:{i:1539185649;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1539187257;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1539190857;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1539214101;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1539216000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1539234103;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1539235701;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1539235711;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1539242649;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1539246501;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1541462400;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(112, 'theme_mods_twentyseventeen', 'a:3:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1538116612;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:4:{i:0;s:6:\"text-2\";i:1;s:6:\"text-3\";i:2;s:6:\"text-4\";i:3;s:6:\"text-5\";}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"nav_menu_locations\";a:0:{}}', 'yes'),
(116, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-4.9.8.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-4.9.8-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-4.9.8-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"4.9.8\";s:7:\"version\";s:5:\"4.9.8\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"4.7\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1539159846;s:15:\"version_checked\";s:5:\"4.9.8\";s:12:\"translations\";a:0:{}}', 'no'),
(120, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1539159849;s:7:\"checked\";a:4:{s:4:\"Jess\";s:0:\"\";s:13:\"twentyfifteen\";s:3:\"2.0\";s:15:\"twentyseventeen\";s:3:\"1.7\";s:13:\"twentysixteen\";s:3:\"1.5\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(124, 'can_compress_scripts', '1', 'no'),
(130, 'current_theme', 'Jess', 'yes'),
(131, 'theme_mods_Jess', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1538116578;s:4:\"data\";a:4:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:9:\"sidebar-2\";a:0:{}s:9:\"sidebar-3\";a:0:{}}}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(132, 'theme_switched', '', 'yes'),
(142, 'recently_activated', 'a:4:{s:19:\"akismet/akismet.php\";i:1539174651;s:50:\"ajax-product-filter-for-woocommerce/mainfilter.php\";i:1539159743;s:37:\"woocommerce-products-filter/index.php\";i:1539097095;s:48:\"woocommerce-ajax-filters/woocommerce-filters.php\";i:1539094123;}', 'yes'),
(146, 'cptui_new_install', 'false', 'yes'),
(147, 'wpmdb_settings', 'a:13:{s:3:\"key\";s:40:\"6y5cgxHBqz1r5tZKYq62HY5ufFU7S3+smCP3Ok9V\";s:10:\"allow_pull\";b:0;s:10:\"allow_push\";b:0;s:8:\"profiles\";a:0:{}s:7:\"licence\";s:0:\"\";s:10:\"verify_ssl\";b:0;s:17:\"whitelist_plugins\";a:0:{}s:11:\"max_request\";i:1048576;s:22:\"delay_between_requests\";i:0;s:18:\"prog_tables_hidden\";b:1;s:21:\"pause_before_finalize\";b:0;s:14:\"allow_tracking\";N;s:28:\"compatibility_plugin_version\";s:3:\"1.1\";}', 'no'),
(148, 'wpmdb_schema_version', '2', 'no'),
(165, 'widget_akismet_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(173, '_site_transient_timeout_browser_8651940b33fd1e958c905441aa40a03d', '1539615299', 'no'),
(174, '_site_transient_browser_8651940b33fd1e958c905441aa40a03d', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"69.0.3497.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(179, '_transient_timeout_plugin_slugs', '1539261492', 'no'),
(180, '_transient_plugin_slugs', 'a:12:{i:0;s:30:\"advanced-custom-fields/acf.php\";i:1;s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";i:2;s:19:\"akismet/akismet.php\";i:3;s:25:\"breadcrumb/breadcrumb.php\";i:4;s:36:\"contact-form-7/wp-contact-form-7.php\";i:5;s:43:\"custom-post-type-ui/custom-post-type-ui.php\";i:6;s:34:\"custom-sidebars/customsidebars.php\";i:7;s:9:\"hello.php\";i:8;s:33:\"smart-slider-3/smart-slider-3.php\";i:9;s:27:\"woocommerce/woocommerce.php\";i:10;s:20:\"wp-fancybox/main.php\";i:11;s:31:\"wp-migrate-db/wp-migrate-db.php\";}', 'no'),
(189, 'wpmdb_usage', 'a:2:{s:6:\"action\";s:8:\"savefile\";s:4:\"time\";i:1539173689;}', 'no'),
(198, 'woocommerce_store_address', '', 'yes'),
(199, 'woocommerce_store_address_2', '', 'yes'),
(200, 'woocommerce_store_city', '', 'yes'),
(201, 'woocommerce_default_country', 'GB', 'yes'),
(202, 'woocommerce_store_postcode', '', 'yes'),
(203, 'woocommerce_allowed_countries', 'all', 'yes'),
(204, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(205, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(206, 'woocommerce_ship_to_countries', '', 'yes'),
(207, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(208, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(209, 'woocommerce_calc_taxes', 'no', 'yes'),
(210, 'woocommerce_enable_coupons', 'no', 'yes'),
(211, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(212, 'woocommerce_currency', 'GBP', 'yes'),
(213, 'woocommerce_currency_pos', 'left', 'yes'),
(214, 'woocommerce_price_thousand_sep', ',', 'yes'),
(215, 'woocommerce_price_decimal_sep', '.', 'yes'),
(216, 'woocommerce_price_num_decimals', '2', 'yes'),
(217, 'woocommerce_shop_page_id', '19', 'yes'),
(218, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(219, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(220, 'woocommerce_weight_unit', 'kg', 'yes'),
(221, 'woocommerce_dimension_unit', 'cm', 'yes'),
(222, 'woocommerce_enable_reviews', 'no', 'yes'),
(223, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(224, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(225, 'woocommerce_enable_review_rating', 'no', 'yes'),
(226, 'woocommerce_review_rating_required', 'yes', 'no'),
(227, 'woocommerce_manage_stock', 'yes', 'yes'),
(228, 'woocommerce_hold_stock_minutes', '60', 'no'),
(229, 'woocommerce_notify_low_stock', 'yes', 'no'),
(230, 'woocommerce_notify_no_stock', 'yes', 'no'),
(231, 'woocommerce_stock_email_recipient', 'user.citrusbug@gmail.com', 'no'),
(232, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(233, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(234, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(235, 'woocommerce_stock_format', '', 'yes'),
(236, 'woocommerce_file_download_method', 'force', 'no'),
(237, 'woocommerce_downloads_require_login', 'no', 'no'),
(238, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(239, 'woocommerce_prices_include_tax', 'no', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(240, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(241, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(242, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(243, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes'),
(244, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(245, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(246, 'woocommerce_price_display_suffix', '', 'yes'),
(247, 'woocommerce_tax_total_display', 'itemized', 'no'),
(248, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(249, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(250, 'woocommerce_ship_to_destination', 'billing', 'no'),
(251, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(252, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(253, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(254, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(255, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(256, 'woocommerce_registration_generate_username', 'yes', 'no'),
(257, 'woocommerce_registration_generate_password', 'yes', 'no'),
(258, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(259, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(260, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(261, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(262, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(263, 'woocommerce_trash_pending_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(264, 'woocommerce_trash_failed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(265, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(266, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(267, 'woocommerce_email_from_name', 'Jess', 'no'),
(268, 'woocommerce_email_from_address', 'user.citrusbug@gmail.com', 'no'),
(269, 'woocommerce_email_header_image', '', 'no'),
(270, 'woocommerce_email_footer_text', '{site_title}', 'no'),
(271, 'woocommerce_email_base_color', '#96588a', 'no'),
(272, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(273, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(274, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(275, 'woocommerce_cart_page_id', '21', 'yes'),
(276, 'woocommerce_checkout_page_id', '23', 'yes'),
(277, 'woocommerce_myaccount_page_id', '25', 'yes'),
(278, 'woocommerce_terms_page_id', '', 'no'),
(279, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(280, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(281, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(282, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(283, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(284, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(285, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(286, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(287, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(288, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(289, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(290, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(291, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(292, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(293, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(294, 'woocommerce_api_enabled', 'no', 'yes'),
(295, 'woocommerce_single_image_width', '600', 'yes'),
(296, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(297, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(298, 'woocommerce_demo_store', 'no', 'no'),
(299, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:7:\"product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(300, 'current_theme_supports_woocommerce', 'no', 'yes'),
(301, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(304, 'default_product_cat', '15', 'yes'),
(307, 'woocommerce_version', '3.4.5', 'yes'),
(308, 'woocommerce_db_version', '3.4.5', 'yes'),
(309, 'woocommerce_admin_notices', 'a:1:{i:1;s:20:\"no_secure_connection\";}', 'yes'),
(310, '_transient_woocommerce_webhook_ids', 'a:0:{}', 'yes'),
(311, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(312, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(313, 'widget_woocommerce_layered_nav', 'a:5:{i:2;a:6:{s:5:\"title\";s:9:\"Filter by\";s:9:\"attribute\";s:4:\"make\";s:12:\"display_type\";s:8:\"dropdown\";s:10:\"query_type\";s:3:\"and\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:18:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:2:{i:0;s:9:\"frontpage\";i:1;s:6:\"single\";}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}}s:6:\"always\";b:0;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"1\";s:5:\"state\";s:2:\"ok\";}}i:3;a:6:{s:5:\"title\";s:9:\"Filter by\";s:9:\"attribute\";s:5:\"model\";s:12:\"display_type\";s:8:\"dropdown\";s:10:\"query_type\";s:3:\"and\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:18:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"2\";s:5:\"state\";s:2:\"ok\";}}i:4;a:6:{s:5:\"title\";s:9:\"Filter by\";s:9:\"attribute\";s:5:\"years\";s:12:\"display_type\";s:8:\"dropdown\";s:10:\"query_type\";s:3:\"and\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:18:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"3\";s:5:\"state\";s:2:\"ok\";}}i:5;a:6:{s:5:\"title\";s:9:\"Filter by\";s:9:\"attribute\";s:5:\"years\";s:12:\"display_type\";s:8:\"dropdown\";s:10:\"query_type\";s:3:\"and\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:23:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:14:\"pt-latest_news\";a:0:{}s:16:\"pt-image_gallery\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"5\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(314, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(315, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:10:{s:5:\"title\";s:18:\"Product categories\";s:7:\"orderby\";s:4:\"name\";s:8:\"dropdown\";i:0;s:5:\"count\";i:1;s:12:\"hierarchical\";i:1;s:18:\"show_children_only\";i:0;s:10:\"hide_empty\";i:0;s:9:\"max_depth\";s:0:\"\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:21:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"4\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(316, 'widget_woocommerce_product_search', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:23:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:14:\"pt-latest_news\";a:0:{}s:16:\"pt-image_gallery\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"6\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(317, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(318, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(319, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(320, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(321, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(322, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(326, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(327, '_transient_timeout_external_ip_address_::1', '1539667705', 'no'),
(328, '_transient_external_ip_address_::1', '182.70.122.97', 'no'),
(342, '_transient_product_query-transient-version', '1539088238', 'yes'),
(345, '_transient_shipping-transient-version', '1539063598', 'yes'),
(348, 'woocommerce_cod_settings', 'a:6:{s:7:\"enabled\";s:3:\"yes\";s:5:\"title\";s:16:\"Cash on delivery\";s:11:\"description\";s:28:\"Pay with cash upon delivery.\";s:12:\"instructions\";s:28:\"Pay with cash upon delivery.\";s:18:\"enable_for_methods\";a:0:{}s:18:\"enable_for_virtual\";s:3:\"yes\";}', 'yes'),
(350, '_transient_timeout_wc_product_loop0cc41539063580', '1541655625', 'no'),
(351, '_transient_wc_product_loop0cc41539063580', 'O:8:\"stdClass\":5:{s:3:\"ids\";a:0:{}s:5:\"total\";i:0;s:11:\"total_pages\";i:1;s:8:\"per_page\";i:-1;s:12:\"current_page\";i:1;}', 'no'),
(356, '_transient_product-transient-version', '1539088238', 'yes'),
(384, 'n2_ss3_version', '3.3.7r2543', 'yes'),
(385, 'widget_smartslider3', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(388, '_transient_timeout_wc_shipping_method_count_1_1539063598', '1541657680', 'no'),
(389, '_transient_wc_shipping_method_count_1_1539063598', '0', 'no'),
(393, '_transient_orders-transient-version', '1539161924', 'yes'),
(394, '_transient_timeout_wc_order_37_needs_processing', '1539152218', 'no'),
(395, '_transient_wc_order_37_needs_processing', '1', 'no'),
(400, '_transient_timeout_wc_shipping_method_count_0_1539063598', '1541658548', 'no'),
(401, '_transient_wc_shipping_method_count_0_1539063598', '0', 'no'),
(402, '_transient_timeout_wc_order_38_needs_processing', '1539153071', 'no'),
(403, '_transient_wc_order_38_needs_processing', '1', 'no'),
(410, '_transient_timeout_wc_order_39_needs_processing', '1539153501', 'no'),
(411, '_transient_wc_order_39_needs_processing', '1', 'no'),
(415, '_transient_timeout_wc_order_40_needs_processing', '1539154793', 'no'),
(416, '_transient_wc_order_40_needs_processing', '1', 'no'),
(421, '_transient_timeout_wc_order_41_needs_processing', '1539156188', 'no'),
(422, '_transient_wc_order_41_needs_processing', '1', 'no'),
(437, 'wdev-frash', 'a:3:{s:7:\"plugins\";a:1:{s:34:\"custom-sidebars/customsidebars.php\";i:1539070350;}s:5:\"queue\";a:2:{s:32:\"6a9b139509f3226afafc03dc81d90bd2\";a:3:{s:6:\"plugin\";s:34:\"custom-sidebars/customsidebars.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1539070350;}s:32:\"f21a0d5a84b747557fce042d7049df2b\";a:3:{s:6:\"plugin\";s:34:\"custom-sidebars/customsidebars.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1539675150;}}s:4:\"done\";a:0:{}}', 'no'),
(438, 'cs_modifiable', 'a:15:{s:10:\"modifiable\";a:4:{i:0;s:9:\"sidebar-1\";i:1;s:9:\"sidebar-2\";i:2;s:9:\"sidebar-3\";i:3;s:18:\"smartslider_area_1\";}s:7:\"authors\";a:0:{}s:4:\"blog\";a:0:{}s:16:\"category_archive\";a:0:{}s:14:\"category_pages\";N;s:14:\"category_posts\";N;s:15:\"category_single\";a:0:{}s:4:\"date\";a:0:{}s:8:\"defaults\";N;s:17:\"post_type_archive\";a:0:{}s:15:\"post_type_pages\";N;s:16:\"post_type_single\";a:0:{}s:6:\"search\";a:0:{}s:4:\"tags\";a:0:{}s:6:\"screen\";N;}', 'yes'),
(439, 'cs_sidebars', 'a:4:{i:0;a:7:{s:2:\"id\";s:4:\"cs-1\";s:4:\"name\";s:14:\"Filter by Make\";s:11:\"description\";s:0:\"\";s:13:\"before_widget\";s:0:\"\";s:12:\"before_title\";s:0:\"\";s:12:\"after_widget\";s:0:\"\";s:11:\"after_title\";s:0:\"\";}i:1;a:7:{s:2:\"id\";s:4:\"cs-2\";s:4:\"name\";s:14:\"Filter by Year\";s:11:\"description\";s:0:\"\";s:13:\"before_widget\";s:0:\"\";s:12:\"before_title\";s:0:\"\";s:12:\"after_widget\";s:0:\"\";s:11:\"after_title\";s:0:\"\";}i:2;a:7:{s:2:\"id\";s:4:\"cs-3\";s:4:\"name\";s:15:\"Filter by Model\";s:11:\"description\";s:0:\"\";s:13:\"before_widget\";s:0:\"\";s:12:\"before_title\";s:0:\"\";s:12:\"after_widget\";s:0:\"\";s:11:\"after_title\";s:0:\"\";}i:3;a:7:{s:2:\"id\";s:4:\"cs-4\";s:4:\"name\";s:14:\"Search Product\";s:11:\"description\";s:0:\"\";s:13:\"before_widget\";s:0:\"\";s:12:\"before_title\";s:0:\"\";s:12:\"after_widget\";s:0:\"\";s:11:\"after_title\";s:0:\"\";}}', 'yes'),
(456, '_transient_timeout_wc_order_42_needs_processing', '1539157157', 'no'),
(457, '_transient_wc_order_42_needs_processing', '1', 'no'),
(468, '_transient_timeout_wc_order_43_needs_processing', '1539164670', 'no'),
(469, '_transient_wc_order_43_needs_processing', '1', 'no'),
(491, '_transient_timeout_wc_order_48_needs_processing', '1539166026', 'no'),
(492, '_transient_wc_order_48_needs_processing', '1', 'no'),
(498, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.0.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1539079877;s:7:\"version\";s:5:\"5.0.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(500, '_transient_timeout_wc_order_50_needs_processing', '1539166307', 'no'),
(501, '_transient_wc_order_50_needs_processing', '1', 'no'),
(527, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(534, 'WPLANG', '', 'yes'),
(535, 'new_admin_email', 'user.citrusbug@gmail.com', 'yes'),
(536, 'address', '280 North Village Drive&lt;br&gt;\r\nShipshewana, Indiana 46565', 'yes'),
(537, 'footer_content', 'Copyright© 2016 periodicitems', 'yes'),
(538, 'phoneno', '(574) 349-4354', 'yes'),
(539, 'email', 'sales@jessperformance.com', 'yes'),
(540, 'pinterest', '#', 'yes'),
(541, 'fb_url', 'https://www.facebook.com/jess.performace/', 'yes'),
(542, 'twitter_url', 'https://twitter.com/JessPerformance', 'yes'),
(543, 'linkedin_url', '#', 'yes'),
(554, 'acf_version', '5.7.7', 'yes'),
(561, 'cptui_post_types', 'a:3:{s:8:\"partners\";a:28:{s:4:\"name\";s:8:\"partners\";s:5:\"label\";s:8:\"Partners\";s:14:\"singular_label\";s:8:\"Partners\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:5:\"false\";s:9:\"rest_base\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:23:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:11:\"latest_news\";a:28:{s:4:\"name\";s:11:\"latest_news\";s:5:\"label\";s:11:\"Latest News\";s:14:\"singular_label\";s:11:\"Latest News\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:5:\"false\";s:9:\"rest_base\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:23:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}s:13:\"image_gallery\";a:28:{s:4:\"name\";s:13:\"image_gallery\";s:5:\"label\";s:13:\"Image Gallery\";s:14:\"singular_label\";s:13:\"Image Gallery\";s:11:\"description\";s:0:\"\";s:6:\"public\";s:4:\"true\";s:18:\"publicly_queryable\";s:4:\"true\";s:7:\"show_ui\";s:4:\"true\";s:17:\"show_in_nav_menus\";s:4:\"true\";s:12:\"show_in_rest\";s:5:\"false\";s:9:\"rest_base\";s:0:\"\";s:11:\"has_archive\";s:5:\"false\";s:18:\"has_archive_string\";s:0:\"\";s:19:\"exclude_from_search\";s:5:\"false\";s:15:\"capability_type\";s:4:\"post\";s:12:\"hierarchical\";s:5:\"false\";s:7:\"rewrite\";s:4:\"true\";s:12:\"rewrite_slug\";s:0:\"\";s:17:\"rewrite_withfront\";s:4:\"true\";s:9:\"query_var\";s:4:\"true\";s:14:\"query_var_slug\";s:0:\"\";s:13:\"menu_position\";s:0:\"\";s:12:\"show_in_menu\";s:4:\"true\";s:19:\"show_in_menu_string\";s:0:\"\";s:9:\"menu_icon\";s:0:\"\";s:8:\"supports\";a:3:{i:0;s:5:\"title\";i:1;s:6:\"editor\";i:2;s:9:\"thumbnail\";}s:10:\"taxonomies\";a:0:{}s:6:\"labels\";a:23:{s:9:\"menu_name\";s:0:\"\";s:9:\"all_items\";s:0:\"\";s:7:\"add_new\";s:0:\"\";s:12:\"add_new_item\";s:0:\"\";s:9:\"edit_item\";s:0:\"\";s:8:\"new_item\";s:0:\"\";s:9:\"view_item\";s:0:\"\";s:10:\"view_items\";s:0:\"\";s:12:\"search_items\";s:0:\"\";s:9:\"not_found\";s:0:\"\";s:18:\"not_found_in_trash\";s:0:\"\";s:17:\"parent_item_colon\";s:0:\"\";s:14:\"featured_image\";s:0:\"\";s:18:\"set_featured_image\";s:0:\"\";s:21:\"remove_featured_image\";s:0:\"\";s:18:\"use_featured_image\";s:0:\"\";s:8:\"archives\";s:0:\"\";s:16:\"insert_into_item\";s:0:\"\";s:21:\"uploaded_to_this_item\";s:0:\"\";s:17:\"filter_items_list\";s:0:\"\";s:21:\"items_list_navigation\";s:0:\"\";s:10:\"items_list\";s:0:\"\";s:10:\"attributes\";s:0:\"\";}s:15:\"custom_supports\";s:0:\"\";}}', 'yes'),
(565, '_transient_timeout_wc_order_114_needs_processing', '1539174445', 'no'),
(566, '_transient_wc_order_114_needs_processing', '1', 'no'),
(585, '_transient_timeout_wc_term_counts', '1541761696', 'no'),
(586, '_transient_wc_term_counts', 'a:28:{i:17;s:1:\"2\";i:44;s:0:\"\";i:37;s:0:\"\";i:43;s:0:\"\";i:36;s:0:\"\";i:52;s:0:\"\";i:41;s:0:\"\";i:42;s:0:\"\";i:35;s:0:\"\";i:51;s:0:\"\";i:50;s:0:\"\";i:49;s:0:\"\";i:40;s:0:\"\";i:34;s:0:\"\";i:48;s:0:\"\";i:33;s:0:\"\";i:47;s:0:\"\";i:46;s:0:\"\";i:38;s:0:\"\";i:39;s:0:\"\";i:45;s:0:\"\";i:32;s:0:\"\";i:31;s:0:\"\";i:16;s:1:\"2\";i:15;s:0:\"\";i:56;s:0:\"\";i:57;s:0:\"\";i:58;s:0:\"\";}', 'no'),
(587, '_transient_timeout_wc_layered_nav_counts_pa_make', '1539246019', 'no'),
(588, '_transient_wc_layered_nav_counts_pa_make', 'a:10:{i:0;b:0;s:32:\"e0044ba5a7055e2b2fcde8af78e084dc\";a:4:{i:18;i:1;i:19;i:1;i:20;i:1;i:21;i:1;}s:32:\"6b0c213ae0b5e199f948821f6cc7e741\";a:1:{i:18;i:1;}s:32:\"59615230eed52421457782d974a25d10\";a:1:{i:18;i:1;}s:32:\"f1f2645e55087335b8b55cb886883913\";a:1:{i:18;i:1;}s:32:\"2f57199c70ef27c60f4fa93cef1e37e0\";a:2:{i:18;i:1;i:21;i:1;}s:32:\"65c4e28bdfb899d2a20546411c110cbb\";a:2:{i:18;i:1;i:19;i:1;}s:32:\"02c48154a7815ce687442cd8a98aa535\";a:1:{i:18;i:1;}s:32:\"5f7d91339c5859a0374e354c6e5a5d2b\";a:2:{i:20;i:1;i:21;i:1;}s:32:\"8d29429b5b2447b5fe3604ce78b65a8e\";a:1:{i:20;i:1;}}', 'no'),
(589, '_transient_timeout_wc_layered_nav_counts_pa_years', '1539246019', 'no'),
(590, '_transient_wc_layered_nav_counts_pa_years', 'a:10:{i:0;b:0;s:32:\"4a1b1cf45fecb6bcad75c28737f74696\";a:3:{i:28;i:2;i:29;i:2;i:30;i:2;}s:32:\"6c4673c5fd377c025700d29fa7c768ab\";a:1:{i:28;i:1;}s:32:\"af5386a4f984e18a4ce28b189d7f2014\";a:1:{i:28;i:1;}s:32:\"4be00745bd91be1ec01b16eadb9425a7\";a:1:{i:28;i:1;}s:32:\"98e463f19b97b86498d70c7f4c1fbe28\";a:3:{i:28;i:2;i:29;i:1;i:30;i:1;}s:32:\"7fb6d18c4604277bf91ee7b46e30cdcb\";a:2:{i:28;i:1;i:29;i:1;}s:32:\"c3f161fcc7ae482434b6c8022b490780\";a:1:{i:28;i:1;}s:32:\"c17051eb5526f31c4e55369caa0918b4\";a:3:{i:28;i:1;i:29;i:1;i:30;i:2;}s:32:\"53bbefb22bef5568088020fc35d112a6\";a:1:{i:30;i:1;}}', 'no'),
(591, '_transient_timeout_wc_layered_nav_counts_pa_model', '1539246020', 'no'),
(592, '_transient_wc_layered_nav_counts_pa_model', 'a:10:{i:0;b:0;s:32:\"bbaa91fd51b49a9ff0cf8de3d243adb3\";a:4:{i:22;i:1;i:23;i:1;i:24;i:1;i:25;i:1;}s:32:\"ec5b20af4217c2a03f52c37e16246282\";a:1:{i:22;i:1;}s:32:\"a68c493932d0575dfd6ee20a1252d3bf\";a:1:{i:22;i:1;}s:32:\"286b537387bdc6666e2ae27ee82fff70\";a:1:{i:22;i:1;}s:32:\"99b1bc49d1055fdbfd76711a55628ca0\";a:2:{i:22;i:1;i:25;i:1;}s:32:\"23ebce0fcf3514502be71d1ea1fba4af\";a:2:{i:22;i:1;i:23;i:1;}s:32:\"508f143f8330df3b6316008780edd501\";a:1:{i:22;i:1;}s:32:\"0cd3bab576b7bd97678094a7a1d80f79\";a:2:{i:24;i:1;i:25;i:1;}s:32:\"c7ac8f003cd1783609edec2de8d0eaf0\";a:1:{i:24;i:1;}}', 'no');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(603, 'wp_installer_settings', 'eJzs/etyG1mWJgr+7jKrd/CDPpmipgjwTkqMCNVQFKVgli5MkQplWE8b2gE4CQ8BcKQ7IIpRFmP1DvOn22zGrF5kbMzqUeoF5hVmr9vea18cgCIj89TMVJt1pYIAtm/fl3X91rfy04PTf25O9/ZPO3Uxr5pyUdVl0XS+yU/34YPD0879fDqB/97j/x7lixz/G39pfliOOt80pwdP6Kvdxbjo3lf1aF4XTdOdLieLclLO7pb5pDufLO/KGXzbjDPLpwX+c/e08/Hqzeusm92Mi+yj+ekV/DR7o36aXdmfwlzndTVaDhddOwYNgfM47SzrCX7x+LQzXizmzenODsysV9V38Pd9823zokX/U/HQ9Kf5LL8rpsVs0effHezFv9vJh8NqOVvswA+bHZlHs5zPq3rRX+R3Da9RebqrFm/lBA+eJh50W9XLabOzqOblsGvGxU/MA3/5pTk1SzzPh5/MfPFpB/C0/cOD4+PjA/OfJ95+2J1rm8eeec1R0Qzrcr4oK1raXfNz2IRhNZ1PikWR8eO2s7uqGmVmblk+e8j0pmawIttZORtOliPzpyyfNJX/jY9VdV5Np0U9LDJesR48zrx+OTWjy8Ifpxb+ft4dVrOF2Z+d5XxS5aNmZ39373Bn9wl+pzup7qq97u5ebz7D3T067ZgTVNSdb2AnzKCwZrX5ufzhiT0+sojmxc1UvJNqZjUbPuCWhlsJ3/WO5hl/N7mk5nDzrOZ1OSxgEntHT+G7J3B8BvbL/cXDnD4+2dt7Asd0P/GF/qL4slg3i3DkcnZbyW9+LPJ68pDpz/FOPEk9rPjzsvycT3DxcA2OT/b3/DN8lNqzf8xHo+6i6g7zevEdHdC93+fT+TeD5UN/Vt1/h4M8AZkzK+7NgYGF3j39Zzriy/ldnY8K9UfzQiQ66JId0i0zsz6mm5wQNcNpg4sJ3zpgydQsavNpd1Hns2aS06ubCwRfORHh5T7rOskAXzugXYR1wZEP6Q3M8fxcFvf4pyPeNRxpWozKPHzWMYuf+6oa8o3wpg3fOYHvmCeZRfhcLh7MnZs20Zee8HsNlqPRQ/z68BU5ZPzq5fDTQ9d8/InWZZclGH5qFsuI0s/4wR69Zz68neIwe7A+e0f8zXwy6ZZTuMH4GSzK/p68cV5OhmPzadfMuXs/NzIL7/ioup/hxZUNNX/eexJcOJjCfvq+HQcn/fzN9eaX7WTlXTs+2Xu6/qq1TeCvdM/4Lb7+ju3+5x37zzum7lio1AZGV7ZdspPgjD833938lu2vu2UbKLTWGfwHu2ZHT3+7a/b0626Zvhv78d04WHs3Dje4G0frj/1xcH5P2o/vk193en/5xUz2aG/XHJzQsAVxHC/Qr1UZ//9o8uIK8/oex+t7khAb4QJ/hbg44gUe5E05zNDT+1zUjflULbG3ePDA5j/YciUOKjrEsdpcf5kT8heuCC5N6tiCv2qe0dlATiR34MnuMY9uDvmgnBWNv968elm+XIwrUN/ZfbkYZ/Pqvqhvl5NMCZHMKexedrmg7zRmhNoc8dzcHvSPzY0ZZWVj/gPuVFbMquXdGPcZtvbO/KL393/37Wjy7O//Lvsv344Wz26W9axRrr+e3rc75gv0xdGzC3N0HrJigjMwN1D9ZlCYtwterZfdjM086KbC1Tae87aZdLMw/wNO+3Y2NLO+w6CH+feyWVTTDGUsvkPxuZhlKClni6ZnZjKyU/6hrBeweJMcnnRXZKOyLoYUPsHfygfwePPh7W0BZywbVUYKzhrvrX6slmYe8K1mPskf9ILjrz+8f20WczkcZ3mTfVtMnxVfcljZnpnaTtHsfLtj/rYdfXJb0yeZWfhyZiWc0ZfRdLbhO0Zjdu1/z4vavoL/5ufVzCxLCb/WJwNOUV1N/Df7ODYL+GBeD5RUtjBiYFKaxc+qW9oMXCneD/O1RxMjXIsiux/ni0fmw0ld5CO3HsUIvw+fZrPKnEBZOKOk/UW7rWEf+XGoIeEF78clHMdRuYBDzsdeXs78z+QZC53h2Lx5wTLt4PjgxMiwb8eHzw57u72Tb3fMv74dHzx7WeSLpTl75g8HZgAzxBKPtPl/307KZy/LLwVcg2YJL2TWIW+a8m4Gj7aHBmyZekoLuKhoJbKheWt418W4xotTzmZmM+Az88+mqOHbZtbmETBrfKb5B0zIPHHFbPAEmGcNzPVYPNDMGrrrH6+y6yVs+Xk+HBcyuPz2fdFUk89mRrf5whz5oq5hMeF3N5XZ8WKR/QA2CBwyWHOz+LNFtzA7Nc7n82JmJQrKIFjD4/AJtFZmJ2W5SnPYG3OfFyXcaSPuPsMV9A6cOUNnn/NRbg7K3aQamIkZ2TNt4slPis+5OazmxYaf4FzDksPyVln+uSpHcJSMHZuekvfGsIlmpZ7nn0AKsbS4q80Q8GSQeGaoLM+MkQnDLo3+Nyrv8kXrep7HG0IrlXoxev5yPsrx+JqVntI5z7NmYe5FXo/wUoVPOxuNzKPYCoG3ttdJhjejz+G/QZDP4Haw3LkgOVvVLdsVr05uxqqnZt8bPAmNUW8Z6C5fBjrRCNcZf3H5ApbPfWVhbqCR0F+qWTV9kK+k32xq1g0WxFj3w0/ofqGqeQMGsHdgzEGFlbPn9Fr+O7VqwYnkE8gL7w1rVnQAr4oXxDzYnBA2cIywNsf9xBz4YPDvzXtPCpR8TTmYoPU5Xc7KIY2ICwoabXFf4JEz1wZW6uzmIhzpLZ5ds5A1GZqgCs3Ag4es9s59dC3o/cz3bssZGrGwCHT4jA4ZLmjb4Km8bOYQgvoZFEYMGYk1HKdHhNXBCZufLed8t/hC3+eN6Dheq1k16+ajaTkzYtrYD+atPcEmMve4TeayjPsv7jTk2QyEPRo/RoFMqnuY+7IBM8Xs1Kgw72vk7m1W5/fZ9zcw0XG1nIxwD+HkwbTU9tq3xGdcgo4zliG/6Ldm1tXs7hnYmf1J05eL1De3emzmyB+bNZ6YI5yNq+qTP56ZM5wxni5IJJhxKObsVM3BxW0aLMvJqHCLpRfAF/K4oTBVe5fhfILTbT4a3RnRPTXvMylSI8F0G1wPEnXlz3L8B8U4/1yaD8zk8L/r6r4Bg6G6K4epoXz9kpHJbGUSPKIu7swZKGrSzNUnMKPo1ICCH8AVmd2Wd2bvR3zjhjDUKPU0PS5KUhF0bDg2gfYMdac+VZFmMvIpEgFmGfLs1dKYE4OiviNVjU5hjvPBTXAfl83skRGCRlDRu8Boxm40K2KmCL/138lqi+LLsKCDYi4AWLkzlKtHu7taBKPtIhtlFMNi2dD8PJ90XM19sfdf/Cus3w6UGl9bGLcKtTw+9sJYMpORdzwfkWQFwa4sONJvIyOa4BbdqOdcjEqta/yXN65EXcEJMsehAkk5wRX0JopD/1jlZvWvL96tOxlaf+tRzJL+UDbof5mrVJlz3TIlWIXOubmxsInLGYkWI9eWsyEOZG5XHwRPf/Cw9bgTG0/nlTlKMyPkq/oTWHbm40lq0pGkoZn3b8tiMmr6zcNs2CcJ0kfroBhZyfNv/5rTXOCFrenAb06/z+D3oN9Q8xpd4k8BXHfyJsXCn6SUoHnNz6iIwOBmkQqn3NxTcJgLPiULMN3xK2wsmUeakxJe4xcVfbiko4Zm8SPvefSiJHBiw4huoHUC3FHdKnp3vW36wXxZm7uCpkOescPPIodGZxtmUQ0/PQ7VADqUIy+cQaMWsz8vi2XhWfkkz3gZg/W9xoA5m+FomdbeZlMgz6kSFN3kpfE0QYb827/Kb2x0wP5krTShGzlHyYwGmTpvvfEiHw71cKDMikDU2Ddl2WDu6TTHQHirkNESFTTBYFqSmZHjKtDmGVdzAbbISwjQZycZaB3zFVx1+0zwP4xhkVsP/KHljc0j1UvjmWn07Bt0etkDxpVAT4wPCLlrdCQKsJNz4wPIT3dkR3DA2G7Wp9p6+cEVKlD8wUmczhdsTpvL+9mbAYtWnF1uf1M6X7kxOm64CC9UymDnvSLsxe/vFt9kr91CtO+aOX+18TgmeQOeaXY7yc3ppcBbBiqgYD3EK6LjCA1OlN4pnGF6l5TZTSYcHgu1LZR0oftOtxpdaDSH6VyAiSnLnA9CS0cWxqy6+WxKdwHexdtHsR/IZGraZm52BzUyuR4jsyhi0d+XP4N75ulteFFwA5omO/vD2Z9gCnDR/MGvaAEyQN0M8qYQ34DuXzUbLmv0l+rCiJ0GPeQHUEfGol6U5gr+TLLQqGK7CekFMCbcJzPkqw+XcjB4T51fkLs7N89r49otQtXoDomzR7RhB/tF/0C/ghwP30n7qRqg03DLS7fmASCt+ZCA6oXT0ZCQ8K6LzL8C/WBOcNLcNSoQXhD2xtjxxnh2sUFzoIbj8jPfjFCEj43JjmKaJ1AtwcZyi2UPJ11OlnLkyYBlA19Ct8OXCCvFJkiSCXtm2rKjawJyAfMWpOpBrBWLvJygJWjsVYh/byQlYd341OMjB6jZnBOXNgZJDIoOkRPQp4FaFFNCOjq5MawmyymaZuF1ptXFkK0/3uvSaBRa0gK0h7FJ7mcy386NDS2osGEHVDR43DY0ZCwQDF9VywUs3bSYRuol2Be0cNyJXLLIMo4AbTt7XZ/NYfl41T1/fZl0eI/I4eWFck4v/wHCexl5KS3GrRVd5ua78A5cu1Wi7BKMbDgE5v/UYNi2OJO3ZpUodFtXLtjBzqCTmnBXunwFo4Cl+ae8HrleLe/mhVFROqeCp3Quyd2CWJOISzyOnXxhzIgxun6d7L7QysEFsduUg3o0OznDnA64vq9gUbF3CccHA4HzeZFbPau+6G5LQpo4j4fis9UQZDzeuiV7x7HUZwsYss04sQrDrIctI5+/e3/tuYybmjlBYsMzl2hRk6f5cJPwzXMMdhsJkw8/4eNjLYppJTG9KRxlFBHeTLRiwKH6atdebe89HECzqGRIsi9o1nQ26hrpPxnVxSy1Y9E4MHv25zz1r2TjpZEOtfH9sosvRs3WZiH39laMasNnKJZ4i2/N+LhavF82hkAahSxIe/KdoMTUXLHgMIqT5bn20wPhzFKZlRY/QWxNCCMa7zc3tg0KHwwEgB3Wcv44Bg8RgGxQLRaTYgaheVKRpL39bE4gS5rUITtwhyyRBuEJTCtx3BdVZSyxOY4NqkHus/aNo4VVose5mahr8d6MQP5X3uvRJZZ1aD8ud2YvIalgplIvhtWIFLyemL+7NoitxIqzMy5f4IUEB1yiEuY8liOKXNlnJKaVx2GKzv28f2d2CPK7/eWyHB12QKa5zOuhy+fgSG9wkfH6PrIB327edLUI5CBJTpmPWcsFls3dT25ufJk9/8YKTlgTo5DhENUNgglckKYuFph8NpepKbZlC433ylmiD+9fm59hUN383Vh2+BfMgdbmCidt6cPdQ5lDnRAqkilRVm+cMOE/2F0Xx9y7xZ2Wy9tpkfsQtkejSa3TqdmpxbiiWEb/DILx/TfFbNm/NHbD6Wm/v6goRrH1OJuay8Er5mz5z/lkWYTrQB4FBoX1qyulg3ZEWZsBbfTYptKrglQz5hnQjs0/G+sVp66S0vE52dvwnATx3BvM+A9zjlix7DbfAzMNJkrlB413zXMMSRvltKB7csm/is6ES/N4XqbkrX5qxGOY5vOkaNvdRH8GLyYSSqcLzB+NQ1OzTrI6xR5EM5nIvDYKpC4Hy0XRuGuUezaTEQYgU4y8UWfY/ixxPEBqFPXnAm+SMWCM2MvruyXZTmbeiYPj/d7Lmp5mH2aDSWXUx5t3GJoiddG5k3B7R4fMfIV0zon23AtDv7EQl6yuJtHJ9k6PiG+UXsYhzgfVF+UnGY/SvOsYfnK7wIWjLL7L8fupAX92CEIYqeAaeG3kTgBO52cIjmHsGUQRRsTFiWRhSgkTequPZIKIGIFv7Lh4j3Egdkg7KvMO4rTmLUIPgaxcM7HOsJo/dI01WnTwyTfoZ8EqJNYMshR4GEkEgAlNhkNOs+UUGa3bkG4f4TU+XsGC3U6WzZjW9L4uzbmql5Oi8R508WVR8OuphFuGIXMbkZRQwAEsBp86WMNxbt7daL+l8XpqEJhLyezAeOakfrVhaeYNcrTJriHAbp6zJJhA6EIPiltIBOAqsKEIB4TROqNImsD7BE6WjXNQMg+l0xB8eArRFgKIyQf8jVTekAyfYmYsAzPBnVFB/8CtDRIigExggwVtcrQGh7mx4I1JuNo2PjdnBj1qUWyCPkOloJNkEMqI3p6CdcrAGpcL33BMR1VtAGgh2CO7ZpQGCU1VZyl2brxMXycb1dUckICR8wSZZ7jgiUn/4dqL3NlwAKlzP/vkpRLNTBPDJcbyLGZzAPwxmsQgy5kyyEAWm2dT9gziF4CZwtQ8+IGRDJat4MCMseDvzAJHWAmKh3GYSod0rE1vt0Eq21Yfn4EZ4JM5pAuMytkJX1+8y67hdhkNKvaVM9YbcffHpTnqqRchI43jfQ8STUC8FiOF/MkGVvcKxw0joKzHIIqt15wgcPheTTk19k2Ntl9qCRLQLcZUWTP8oPe09wUsThIorW8Zm6KY4IHZwSGAUSHcFcd6E0JDpIUDDinNb3aHSh9QEfW+GLGFuaOSA5CwrvZ3Rpw80HAohuFOJp6nlFM+KCYwFIfsKAFK6sc4lV+Sp5UWANFE6LWhQmBLic66yyqZdRiO0wFutbfkzcP/mQD49W7WRWseRdEUDFc745ZEOyZDCaB4yyEPSp8k3SZzlIsZGF0Enzlzca3V1+bsp/wLalXJuoIJT9oUMhLsYttIalt4oLxVJ3/1E2mpOQdYlQydqmy8SOeIzK2dY1KEZD7JQEmMQ0Iz8Sjnob2/uL6hkxun9DhElDi/CuoAx4EmWumjwCojEQWz/osFqJXNZnGUMw5Q2sClUYAMkaJoobirPoxDgitqBnCR0EG00KGOWTtfldwqt8nsr40XEQ7KG7GjjtYGeQELrLIGv+wrnRrZRoAUEyRcbQ5i3BVQfDbcgWhMs9MsZ3XZFL35eJ7YK4CDhreUY6QcGXW4szZYS4PoaJefzki9hRtcheEdEjL+qK+WIHUxU+Z5r/Z0GK8knxSCdEBfllN/5XDSX5mP81SliCI3MjpN/c+5MQEgcjCtEJMEiZ2ZihAkLZplgy94y+fK6OohSpar769cUKQwUsW4+4iSiTMA8NUTCMmkt0jjitkaQzw+54xZO6O9gYBTDKSskSZKBOJ1RTXJaholjVEe+XJRdY2CmdDRS2V4NMgRVc2I3NM10vPaJZl1FgxEN0srEhjwZuR5YAISv248nM6Z8SmsryWxGevhg94+XBm8TCLKnZCFpSSJTrPBbDIJe3Xrvr+5ubrGCxcjpEJc8gprw2UaD3oHvUP9BOdOmjP5IiiHcHYKmWPd5ZzkPFYdhMgNb3EONkXca+fUK6R5N3eBZMBNF16YZRP4vB7aCAwjbxYAnf3ijk4rbFy7uuY1zRgTQOfSdXxRfi7tvQuGogAA3qHzF29xm8n5hwoo9FnNPzDgzCAByRjN6CKiiG3d4yuIhUahUvNbuN5H7nqHeOjlrPgyp7A/GY5kHgdmrbv+1tHDsCbl4+BqdDD2xIZGRyfiik6Pn9sGMLaGOtVDFCMvIK+k+61xuR+16mYbDXHjpVS3dxj3N4IiE4o3cEWnYNsq5CHj71hjziCI5XByxYMFsZiTE9jRa/BUNeASjZ9tfWs4Nh3jvCwWYPNz3nPInrg3myT+8L7KCEbYUHqbQMqAGxwbAUSwYKNRkgNtEC7Rdwurt0BF+5csjVkxSq6Snac4Igb4a5ugNYYWAGEK3+dBf8scYcCYWFgxOc7r4CyM/cVgzhhBZwktDePPqqAaZR3Iwlwac0tidG+7KL7qXpnnvjW+BuOp10O+6oJ1aEXR/tfX/RsBB5yemvMWINj7VMkyNIo7Ohk/LZuFXMdqAu6DUZLm9ZvFw8SGn6PVgaouVJYxwsyCD+xFZriM+KlowGExSUPx5pEtkbDHZlXakYMmc0Hw46Dsq+WC+Ul5rOmF7bzUSZQPM3PoIH8NuYQL+tuZxBf3VAoJF/5iNup/AGgwbGH/cgThktsHyLaYRzeLejlcbD3utC9PkwO+62ezX+ViYsOdmMtEyS+nm0zQtmO9MvXNdhrksgTFTfJeotZQd1cPSiPhVAB//ZPgrN5REorVnmemhKWJYSHiZrcpfh1PxemsOMVgrNDFOLCXimuzzzliBqUNF++yZlYaB86FztQrYRDxRaL6CYuirJnUYV8uqXZWJbZsym2AYeuCwhrkVsLBlBRwdXsL6mQv+YDWHFOqkDBvF0sdtD4ROQhuSYdlU6DK7WH+cImn9pxUxxXcekzGbYv9Lvbjuaczwb5XmlXsOyj78gqoWf9dYy2SFwf1bAoOVpC/hqmMcV7ajQrnLggU1AIoG4tG6jjACUHAe+2FuWo6qisqMMsbtEsxWRA8kLTwbVGM0HtHDE51d8da99Z4V3AHFiC6FzawQoXqcvzA3FwUvrJT2TKyiQnRRZnOeL+sioYot+BA4gIrjLS6jYQtyZUvVqWLnIPoRAJwcev/YEQQq+JL6aKvPFdxQ1zKhdBJs+qeAWIgJQDTsOjP8s99+FqfvgaJ3yKHmwJfsB9Wg5/MJf1qn0G7axbJoV+CvGmGwb4nucEVOwghm3WxXB6tW1zgtE1ukwvwYg6zugMF7krb0sk4O38p5VcCxm6pfdS5+vqBw3eAXfRhCFhbBHPFvRCgUeR1cPRhNjPHGE4zxK8mxS340uNyNpJK1rtyYdPqbTIDwzxG+JupeDCSdW8Rm2qwAgIZzPk+Q1a1nFEuHE9Pclgl8QCiXVZLuj5vIddMwUQ4nxDBxFgA1i5NHtLxJr60b4oplsdx/k/K3ZLPpzJXASl1Xk7Kefa8+tKRDKDZFZcivKorTxpN808F4Od0Fr/N3dOaG/ccDpUzx0BJAomFjt0mCn2D0V9I9JElLokwLqQtFs6QaRHBNhEowAqLeTDa+LD3lFXRIv/SR7+gQx/1/wj/0fam6E2bjVpUxp5NAk7tuQGRv0jfRvU7zyxmg/hlVYGlZmXUtbOLzc7lkxXqKcAlqmi9OrMUdEIvDU1kt0NtcQAjPHwcywHWkddGP7EuwJgSRqch47pDSe15vCtU1YyvabH0W1zSdJPfPUbn2/iSAFsn1hDjDzvoYDDa9aIyzxxZjSqqbFzknx+yDhXC4SXpG/lRmKMP9y/1dyPKOTuU3npaUvolXI0+LGJfgKdgmCEwXYCoUQGduWujOr8lwRc+4wPXaKXQUARPQBwOl401UiQGl8HMm//c57+27WFg/XZ8yEPHqms/9xPAB8HWXXekGXCsUSkouCG42H9/8fLi/fuL9y2yPwg422Bz2fTzn/IvW4/bLiaJcL5AUoos4XAjpFFuo/ZpqXIPas28uAVvJY5BcluCQlHShQP5F7O7SdmsKH/3ctzwB87wYKSuQkMsDA2JcbeBFAazhSoaYKLT8os2KwiyNRvFOK6FNafTE1dP6E0rZRtGy+AC4HA1IY01xOSt2JwtGktH8wvk24NMrRtsWM3LgioPNEmFF5XhAT968hDX1do3BIKV4KPST7Ns53cS5/vdzu/ge/DB71Sx4obXi10RFe4WGwItE/+CqSLQUaENTO9Do1U3fDgJ+DYzglMhGnEQFphXszX+LsQJXQAS64yoVD33+Fc2Xa0rvDocbfkec3jmfAJhGO1aYgIOmjioi/wTnY4oDdWx0T0jHaksuQML+4diMd/sKuFZIdFNr4mSjQIfkEwiTo9qNuEnGtVTLwZGlsYO9JPNUzguIQ0qcDlb+OlB4xwNi3FFgE36HIEUo8HpqRGeQFXWtvLsT5aOpMJoV0KAIa6SWBuWSNtQNMN+8+fJ1mOfRevQvMlBculs4tCopVFlZopSG1wOXQDkRqJSUMxKHgJpUmo3CF7EFdBKrlDYufLEJEox/i5XcEtgQ4iSFH7/T8h2przd5ARmzJ4CA9zPwpV4Cstn4ZSJgo4Vcg4tMExwgBkjwgDoPl4xcI6j8ag+xU0AeWs8iFeAhKOvxaEAcwonVLZwQWgZZAeDO2WNyhuw0TmHDaQKQWFvi34GlQH6vYTAqlHUUJk/Gz7ArkIMqW6GgM7/qUkGNhJMRmvt42sKvWpaHou1KxC127jkKmXgjS3mRP0Y7E0CsQyX5giaafieemqzZa8JkMsoS9kfwqOnSnfQt3v39vyif332+ibDKG0+szQGfBI3FIkCQ6JQuxz6ubEv6djb8G0UFku+2ABy4Sq0T4W7eJ6wOFulnNocsreA4mWGLB9+kof1w+t2dWxEPJVYUFDaqUN6bfgIH/EIp2x27RFlxjAvTAVFKfm6ur6nPf3qAr+05ZYnAutgU6wrgZMJPDOETrLvGLlvbIsW95rbCuJjUbE7D/7frjm/9d+zHyDHYtORRi7nczYkEZvgwRBJwtWec71tfgMHBo0wBhzmHFB5dXEDsbWrd9c3tliwl53XCGoh89Qs+11FAcw/VMtPVXb1MDOiwAyzJVSbnyblp09l77Z8bMaHOw9lpi5RQy54AYVsZVwACHu375ezRvWeqT1E/99Mrx51fW9UDjfJIBI125hepxAoGz0YgkbJzQBrQONVC9isZEha2e9AKgLn5wsSabhiiZFR2sMFweBssrnGDcarlkFwSxK6zs2I5IKmVzJfLhxu3SMVmlsiNgtTs7Vm650P1A1sZ76t6vvirsxVDIyS7p1sCw+0kV8qHmOT8oP+23edFr8sJn+TYmlcdNj2XUJ+wH9SCZEXtN/vHUNaI+kte/6XC8s3BdYle4EzNBUhMSv1NAui/HS1weaH45zGJBFkTnfbXW9La4jX1R882DDTk3WisENZUizHi1huvdAlVtRh5kRuaeoa7X3VNVK8FPc5SWANRVmRWM5e5sYSNhMnti5jD03L5TR9bdp03Eeqc8f8JPsbQnWjD4FwJmIcet16qhw5DviCMYcex8haPI6qC0Rv7qE+eJ3HAWupmbsO42PEjWGWD4vfmPsHTV+NjUrjZ7T1UwCnhAXg+qGZyH3fzBJQwxdAtt0wpR2oAmfZupSAQOgaZtN1VyTQ9t4x3F1PTuCfxCtdEEtRfCr2CuDnUrWfFJdYJQu3UC2LcUSGVDfWB7YCtjgsjZ4xK8rbhzDUFbBGsFkAOtMma+E/uuD3dVY6EcI46F8lzAjifX9VVQALB1/EFickL5F7Q303XkrGz6/ZCcx2Zak7klV0up9TscI2IrcbzH+Z00n5JLNGsUhOzgrRgo8an8kxbz41LufHNPfwAmWN+LEf+FpsCQJrUVXgEYa6RFbQ7dzAuL+fEBSPfs2YmTLZE+ucSY77NbZZYAKwLc4qsVeYT+zt1pfSLBMi0ZvH7bG7m/vSXWoznf/rXu9g3ygyywjSnsGFqnftCpt1SL6tpumwNaNmicDknhWTbQWurmxQjZHS+G1AveS+1y0MoeUCo1HdODwTP9vREeeeWQB2ErGDbHuu3sQSqkgKF4IBzA2DQRLturTJ44iN1jnsYZSPKY7w+tE0ITlqxkYuzDnD+/OsrioyiraliM6s0We2qskckBIkNgW5VqwMpb7QHJSzz+Uit8R55uB06bATlY0nDDYiNPHl4YeGIl7DSZHPPJCeMIxY3qKQQcmrQuJoSHyYvWQFly5hRAKzdCLmEpSNLWYRk0PxLS2J2hB+Z8whRIptPba2w7PvEvGeFwWUyZnXni2ngwIDUJCYK8U0oAiL4BJXZj5VJB8nTa8oyT1W0YI3QJ4ppsXl6auCkLQSdYa7JffE7ABdBEohQNTRR97vr7FA7iDp9WVho10Q7MC3fubQEUoUd1JWvpCBiQfEHtFOe0eAHfPXnZ/UNyB+k+BF5FTKIiNBuua5K55nuYZ2ZDm6kjU1fyru8uFDV+ondyirBVNa/XxrjFkd5GESRgU27sgDvmjSVHyoXEFTXMCk7o0uhSd60bxG+QZITC/pJxH0GgMXfgUG+e2S/cE8x6cinXeWIpSOyIRzgSJ7FNOElwNRlDdl4REUd4JxL2d01SJ3UHFQgtdaIbpWkLJk9TGjQ1CPqv2S5EswUk8w2HQqfZoNRB0AmYOOYPDvjKLfIFifh4UkUqTDgLu2UcAcEf+HQlv5xFKY3s/7YGv0yRhuweHDweAKvz8vIXsXM0IxiwxTtzjOhNZQqzvHPhEIKma1a8YkwLB4e3btDViy6NYhNKOk3IVtZ1PO5ssFxqIcCB5hxTXmzlRW7z689bmzyi3QtoEEBKhVqffCFGEE+DB/QvpXrOAT8MsmcQRXX7TnVXXpmOzX+O6wwtw0wdVb+wLQyL/kiBJPGFSjBwvzA3i2mBuM8iCxQDqWpd/q4wwAt3nh0/cI2SVCRzXohFCiipP0+vpdWKgXnC4MHZq3quqd+3J653hR3Ysbp2FSwVHgqmt0EcBORztOMFqY/f1cTKo5QWWXNVT/r/M8xTYwqr5PjAu+uOaSL8TPaj4xVacdlWxGdrrLfkK8GccKxIOC5pLv1TqWrf7gL2rsrrIu01kxVw0T1RhQNmECK9iGPfUUjjE0Xlz+cKlS9a1TlqgjowVDk4WMFCKVKRsXzMf97/CPOxjjmy8HgGYItYh7VAq44vBfce1kSTIoAnLHkYSTDQNabRohJqVgckUq1BBLC4ODYkPDS90b2SuzcVaf+Tbija5/eEWSVNnjjwSsBDU6+H7TfAbPDst7Ui+5NlziEzl6PhqJK4xkOgrSttoKfrhHHiMTxF4LjlWVwkMysjkH4FLp379xRGliVIWHjFxvii2dGxHNtQ16FBfXoA5bBDSGYALk+jmfMSqINtcGbL0RqJKq4tQQSW2b1XJCEqdhBGhXcsITBwzW3SGADBJjjTvljP7RUW4NTk/DoCn4LG/s2EGxs1V5++BKVBLPAggBE6exH3FtS+D0vSfeHJI3KV8gdjTbXc1VxJkwIeIoKGe+5HVEjqDg0AHlWy9RBv16NLA0pKCwTDg0USEsF5q2GUIXrMk6wr8Xj+u4mNlLB3PoAYYi/DJQcr2mP3CAK3tBJygsYI9WwSaT/YJt84dXZmM/BT+DcKzxUD+zaCbmJoxEgWWbROC0PdgF5gJ7nBwLAv0Ws8+l2UVqUJF9JB1EIGVEpwgUfNv1xygxGAQYAxBs+JQ/UI0vJY8gJdgyJ+V4P5KopPB3AyzaXPfpoJpAJMfocD3I++I2H9ITVcF+5kx8sVaMciKzi0GJ9/edeDI2f2hB7JRFNl4OCLyHLoid7tQ4m1D/81PiIGr7cUU3Bkb8U3K8b3TZ1uMsHgzvZWmLaT0TL8YBO2bdimvQFFuFZm+xPSs8EK8KHrYVBeFmhiYIs11JDwIV4oRvv6Kmtkyab00+uhspbiTyRvTYl3eziksd0TFVFmQCEdix4TVrFzgbGbj8wBNAfGYmqUISHvhBUyziBcPy6BzpqsU6e+SjzxDQtJx8YvhuPITdjswZoXgFIW7jlhsszuUcjE7PTFED/V8gdo+Zk9NTarah+28QdxgCn81lJcr1EfpMiUz4saAY1lg8IaXC3QQPHHtbQj8VncfE5VBztynMHLOS/XJEAB6AjhNEEMEz8Pfyln2AXoZTtL7Qx3PbKeNzbt52kdYNtholyQvODh2zowHakCqgZtZpRQ6PeFgM5CAmz2KGgsZ6qXpcLE/HCB7HEQQPigT8xAJhf7X1fTGoi/vt7HwMjTKL7azX6z2Op4LbkswFaLtEVZrMcAqJtaIKTi7oVOb7K4ClzrKtUfHi4jFe7BfmO8ax2RrlL84eB0ATCMcZqZWQrznCtpkdwigvOAaI0cFP2EFWxrxP9hx2yHDOaPyoVMIe8Z7GERhqzHjqt14IiUPivyaERAYYY6lol5gCT/C5rD1PiRiQ+t7U+CirT41MAMrEc86q9IGvhZ9oeVuCb0Ntz61V2FZZp0Sb0wAonIV0C6DsUTdRTKrYb0RYbPe1LcKPsTpx8hevUuIAa60Z0edkW8ZXmmf7j1ESVAunpcl/nzHsGlqJtdupIuXOri5XWajKzGJSQSPqmeSYUXqruJBiw9vLkTCikfLNbEbg0UTmDTT4k0bJghB1IN/GTBNNLLFsKmnOQV33kaIJqwtfdUFHM/BTR2XDiym5S+lN6njPC11O1zJ4znhlgjLT/y2bkMUOc2XxdsWGIbVjc7YvqgXuolB6zHphv0asNMU4sZKoCpnINhgrGhwxcTQ1vZoD6z3Q+cQbRpoXbw8huuCvK6x+za3p1TFY1DUZCor/CYlbJey4YmicT8BKFQS8qCRWc3e5vETOTX4K3z1ou0orwBY4ORwEEv2NOWNtushZraXu9NKw+e8Zgg5KTvSW6M8ap8AFu9PPoI6I/Od+KkLxQpUoO00T0S8gTaSLHbDCF1qztnXy4MhtiyXxUmoR2MqU7HGbr6JIYtn76ObeTPchu6YeIMXsf6i4QtA1EIu+pXx9pfdoA1FRG6gZVg1HiRsIvd9ObFQ/PWpAXIiL+47uO2G/FOEgG28HN+fJBYnqCKHS+Kqutln/449/vL78+OMrzqG1bZ5AY/19sx8SBDAkYUp81dc7OmYyoeRv+lj8ftL8efANHEQAvHcPnu4d/L6GP2WkXvBoW/Hc5xvR5y0omXwJjCDLxVoXWNWhy9/ApGgbJOU4rIbQqum/ju6iNZZtFTwhrjCnCDJ0Wg3KScoqsxtqjmoC84ws5sK8Qvn8kYpeUz2Fc04h+QRRg5nRmnerVBCrNVlKnfuBvu3mWCYmkzsvxfq8ifA0OgJepHTCtIuxEMMC3sYSJvkdBilZo0e6hsmilU8xZ5TvloAR3BBUxSknxZe56fCu0ba5sSu3uVjqc6EJnYwpQbeULSaza5+Y7KRcWPPSNuxoJpVnU7xVseHGb4HrECRWOIL9AueI6EFh1VMxt1aAadBFp/ORsrOn2eXr1xevzl5n1zfvL9++yt69fHl9cZM9en329tWHs1cX/fN3Ly4edQLpQ5EopsEkkDAFI8SW0lygvqJOyjE1UYpdote5oN6njo8hH9bGJvbpTNMMNj4+VT/zgxwoFTkwQpM4tmqLw34ETF2PjECHcAlCeBvuwgdBWlHiL55/xRY41mQsxluORg+EDZVNgUC5pTSy0N4oftckaBLHBdKfOD5TnuDHqAnO+dhc1PagKQoBKyO5pQxdrbEj++68RdA99cLscEYsubM66z7n5HaO0YNqWGIliln1mnBtCh+Kfo5HHSBdDBAUc+G3lKSE4nCsfTMBNjHnP4XcMqCxD20YkfabJca8uKVm6KCM+wLfBKDEbMIrw9pY4sbrBaPXSI/WMJx2EuPTkP37v/zPyxkC7YTpyi7Gv//L//KMPXbaWiOpUuwgKsr1LxSRRvIcnFOv+K5F53G8BZbecm9QBziXuwza9HGthPws4du0uJ4JBOAHI41X2p4vRF7DuNdKmKc4qJUG96V7mahEPQ4TjqsOj5jAqbSI7fVEeLfG1XeoGskTrK4o78ZsjNr2XNNSGo1Ij15gfHEtyj5e9Yz1C40d5DENbzwZ3uQwKtbQhVFet54tfpYsP0+QGemWZfmQe5qz2xXR8CZDyIK8XVU0cdg7/gfhA+e8SCq7tUYmq/BQakpx65HW/HwYumMChztinXbhW2qKbIym7vnZY++R71RSg8wODLbhigRGJZBCUnrdeOqIhuOgkjBA+7zM3orAAZnFmRF38IIMieoEb8P2yYHjId+aJ33BZtWg2X12lw2H1XmhSSXVGhIlITphPGBGxBIpJX2UGMrDDEX5dagyk89cxzaWQYFTiu7FKLueVUj7iRUI9oQY7fU9cNt4NI6bnUmM1qBo3noEe7GsJ48et24Mo3dalw6pD5+bG1jMRv33FCiivASUkvc5dIRhg8Sv/8I0jCfvKFIF3tZQXHEM4TK3B75FzLkekYeYWw/qopcODdnScQ/uR906yJZBR0VAaO1nDasaFKJbkfwge8IQPCxuPDnQJtOKIUEas/FOJfegAKQentAUFOjExzDDUJNxpSmEZGtXmggpjNVXRYxyRvAPLP0DW2MEHEIqjhkxJBOqN/UKrUA/knhcdQMlMNdzYxtmN0AuTyRGaJhBkT3IgV7CnMNUxBJZVKZmNg2W5EqTi6RuVq2BWkxYBTRwRYlidmE6S3WuRvNWLEXAOlg2b+8pN0kPEWgROfhUfoHQ0yMQRcg4BU7iELvY3n7VQC/L23ggKMilemPqqCLlOdBicJDX3gMu2Ysf5uC3bAmcaBtC1uOq3s6wSgO1teWUE6jT4+0WV1jsSGRBA1g0crd64S922i3q1TcdhL6WSTzv5xQDNfpC+tvdSq5aahPCTHFi1y0jgZEniEIUmMVKruUoALbWerToLrRXKadheXex3dRLFIovdY1psABUeCXI6HwAgBqRBoo5EaePshGQ7Rrlp/SlJBDNT1pfal3o+n0xKqTQPbnnWzPbkejD5TaGPc+uLh/3YhP4qHcgHlToJ0W9Odl1vX4XcAQqSJT3rt7jOt80pyenHQZJwX8dnXagY98J/Pv4tLNAsiz7wdPeE/j34WkHnVXzz72np5393b0n3d2n3b39bO/wdHfv9HAfPjo47Rh1i983P5ZafIg+9qr6bucfxeD97nj3yZPf8xy+s0/fM7+HPs1d4//dz80vOt+Up7vmg/198wGTbXaT34CfQlF6bQ5OF6r9Ybzd0w78z5PTDpRoQg4d/nPfvGR7OUjnm1/MV8xoGNan4GZX1y58k5/u7Z/+My6JHdJ8v6VmnBcPSILkm60jm9fYM+tcUMUD/+nkyPzcxocBW+i3P7SkI6jWwJGgGjhFGyBNS0i9W/8EG2f2gnjzPJ/Rvfyug7HEUzBJv+k8Q8cHUFYzrtYTBu/sciEZS2otgk2svy2mz2hJfHbbN9edb3fMZxhiXuLTzRPx2SDWEzX3AvdmN63xwKasm+H1Rg9mN8y/cT16MOA1fUgLlGMIph51CT2B5aQwW0Duk1vdA86BKaiIbcclpcjmSFVbprFtBwNZrJw5jwYRSK+WJ6tLiE0Qm44ZpmV/4E1e6HfDBIe5Ow+LMQWUpEiuqLsQQMSm4JCmQeEGGKTcWIizgnev+IINZXrZOzwh3rLhK1dGe4IuWbozQ8kg6z9tTcpPNDpr2MdywPi89TBO57gYceeouZT3vG2K21LBPXLDFYu8iyYwHH0jaAgJYUwvvB37T/fN30Bi7veeQMXexvQqCSFKhbdMpaPaUiEDFROoGKU4gSqj6z++ljTxOr4+NB0X0vYcC+g85idCRZDBhrU5TaQO4OWON20fDORY6OvPwAyG+lam6vmy6KY6iIbz1b39ZoR5K1VdFd6Sf/tXqXNPNVvg5BGETCBj4IU4bqFgYOyQ7dJ+VcbX1anridgxUgmMP9CzdFHd3U2k8dG//euNi6FI69vXGgmt2fu9lT5azw2Q+Xj3gjKYXgdW46XVutk4iKggv9mwyOJqSojdVdDPBJa83QZpK8yN+lDA4qyb1iwhpzioVWK1qS0z8yq8dGdxOQletyQRaVRGiK+GTTeAleCBiS7DAZKb8Su7CnFMl0CaFRvA1yKvYOuBVd7WvSB3TX6XnMEGTdkZTyfpXJzH2sbkTOJFae4R0+SZ8zlcivkvUV4GDxDVn/SVJjAqR6qSE9+04bjdQG4uUc7iqGGjeFcskBLWFO9vY/OcFFMB6DLThlIHvNT0Nu1zjV29oobuGIe5giqg4UOfOfKJ5JYUlXN7oCE743SXQs1TZdyTOp7WRo2qYyYCW2KCK4kUkOrGtHdmhYZ3tpMZRWtc6W0MiUDaQIW9wT1BVBQVSSrmxa9u9qv5ZZHYBfPQbhr0YHNOJyNxMJEJBdwM18ow2Uo9ULiDamE0/AyZ2iiiOzdaF2PZlHdqmD0CMcKWpwoTxkx1LJVFzQ7VvXrNqDV1iN8XTTJegKZFSTgp8k8eEC7okuq4WPNsUN6p4v+CBO3qxiSOqMfckLeqHaTENr0OAretdJKU97nFtFsrpT4fzamT6rRNjZRnsDR2BK68e/J3de9VJUfq/bCniuLNym21A5YInF/dEHPkmNKnC48bdTW/unoMV3QjoStqGIvN5wba0kqHrMJReM0iWqfSBnLodPudkzgHoNtCQvEy232OUhgBpATzjiXIyYYsfxt3Y3NHNmIry+VqlkLrgekL9CD2U2uc5Qu3F4tyGrVNSKyZ18rJbjvZLlcEpHfXL1lSJJMVAihG3+OWSlYAnRbx6M5tA4jU+m7Uviw0hPROyvW6xV4rzQKIUbKtNx8eW3dlc9H5axt/+a3OEs28fUK4VJvuAIcHlYPWWk/0kOKyBuBm5HVgd4QDZMYYYhO1N+eTvt5RYKEZtHJXM9tG9KjwI6M0pWJ0Kt6fPPjPeAMcTG400BhSNimt5hQFVsLsOdmkzZBvK6ozjRXsmxzpxvpwEb96kHQPqNa9qX6FrSHEL6jwaekTXA3ZFqU9NNTpMTrxXCWyMyrwf32lmOY2Ex4GdN6s+LM3KdA6qpeaAjLlZpmnNiqx0CTeGrRgfSC0JCWDJZUUeVO438uS+2jn1nX/WiMouJhc6E+Xjnx3JgWsfCpE/ZnxnsHvpIibuOJi5dF3vMU7dw/5az0i5Tz7zVUa4y5BcIgaCChOSt3WUndZaZUJCjU+zK1jL/g9CMmHJLnaLGZTb8FUURgxa7Tmdy3rW9JtiQlGoZjGUeImr/fmBiwQUYrJQCkcG9rATKbTy6vndR3Dn23LEC9kABd8NNBWnLNM1DJBlhMtag1LwlgJybSblXXi6wwpzyjAOJ7ZgpoBs/dzrDaidYCqJAnz4PTaW+zWGsCwerV8uWSVO/ZhG5cTQdCkFJgahZPaKKjm8wke21vmDdWU3rRWFEXyg0hUTr/6KbAfZNeTLe8yj2TVryKtjIZkv8HW2E/yQTFpq2AiPbNpxabYgFAv+ZsT/PMd4rL42EsjebuNIuweyQnKSV5LFZ1xe23+7t7vf8urZ6s0k88VPIOtBIlo8kNvDCnjiZFD5V+Sg5udrZjEWaIkDBzytMcNiO+PVrR7aRDzXr3sctX0qMZxJPAJCqfJcFAOPCjIHIsodbSFKkJP2264euWEUvu+OIGLUUrLMiX/kssgxe9AeDmfFzMr9L034bYH9sYqLjkO2P7ehmtTAknzWcUSs6kShgZ7t41qMH7LtGcE/FrVEiS6zVdMJQ9pewpWOC+9nH22ROjs81yxnuLQU3J0FkM+tnNa9LECkqgpzTGqCaZASDekP6aaB/ry49S936TgYo2anNEUSNRPmTQddVCsdUqJJOAVFTeTW7G45EeubLV1eoaBiS8GcrboWUZClNhwFoINPk0ztfBLqQ8GVdpcjJFaVCb1dfLdJ6dD3V+MZNSm/NnipmxclYxyfHkkdZ/dwb8fp0S8QIaFTYsp7pJxeryeA2xEluotg7zzQRc07gs1TTnWmyK2W29GGMvSR8hD0uuCmhSplOvv/v2Vv+/WshSrijeftCLAP7jbM6mNMLoBPBC54scAE3WBLBGwLYRHhgyytaTfvHOSqr3FoLZLvPidFTgWBLVS17cNmjANNZ+qPI2JYc+9BqmImeJgVdSTYj9GWv8FTNhwHHmqV2y3B29GTEjruGBV4gOE55iFSQeLgoPqCMsgSKTQEXG07RM6Y2qn9ZTR11ALb6TP1IiW6xtnB5oB+HVXqMLWxSl9viTbNzr4PWPq4cHfgqP+jDlXv93B/+KGC8pHbjlD3wuOlCfPVO+2qixiLP4VFMCXCcugkZvzz/+sUC2//OKiFelseRIQKYSNmg2Gin2KHFqsPbcMwxiX4Ca0PhnlT9UgPISW0IsRao5sfbyYTiwF5QZcmBKaNUK24Sg9/QTq+L5INsKc96WyZZML4LGURTY9e314frtpst3QY+dWPKtES1jU7xcJignjQvd0cZyGl16poI1d8IXTIlR4lFx6CWVh/ycbg0jsa1MJd6NU7OUjv/xYZ+D4SAubN6JHkHQ+nMUNspUA5TgZDnEhprWnPcq9CiPmbQ2Mo+C8vu4i2Tz+goArNBbOR5L93oBcEEpAQdMs55wyTshVqryJ7geBAwMVMXHdS21Ym5JZhb7meDLi6LXoOX1AL5CpSY3vTr43gH/uiwVlrowCNcKq+/7qPDlCq7jhphSxtrSu+V35uZhZ6xQybK7TbGtckKJkSMXvUDHUGpAg4XAZHkGcjuOiuntywt3wdv1gnUrW6BMUXDq7YbdLioiA2mWY4BiReyhOUy7KEvl7CeIFl46djyJxsOwqCwE8ZIsl2K+cdF/0/ToWQGc3BeWSriegrsusjOFVmyM8SxRnumy1MRkhbX4rvGs3VXZm2xMH5JwtxS/xYlgIdVNRjTV01taXLqfDMcHcsghZ6686bzGBIUG9BRWe/U/FQ5+ANFuPM0KyKNbZbJ/jj/jl7Wy2NG9HR90Cpj1yK5saLevhcsrwsbaigDkpPpsmTXFum0ncAmEAqi7pHb+cuxfGUj3oapQDwywUjnnw7HleJpZcw9Nj8lS6lsBF/hxLETvbWYezjt9TcxRusSwke7TZnWxYTCaJt/UD28johiaG+bFNIlLTdwzqDZ2e/0LpE75kKMFspb132TfrKxacACrKIpWPjUYI6J6Qum/ymVlM1MijvBkPqqDslLOCNvcHmE3x61Xdsic1nbKXDGKqPkkwZreSFbS19yht6Ig3oaSYp+/EPBlG+WqA/qu6Ws7V6yj9Lg6k7N5OC+cv7NlqR1kXBOSlBAqo9QKSAU4Ud/t04ncDgkvRB9Rrn/OdYUMgLgbR9w2xyADCAZb+Kh0PTdj3rZxWwOBLMBtuvAi2Bd/e5QzNPgFh23MBL4JkmRL252/0NaAT7x5xj2Cqpk+j2e/6zUC8Jf+KguDVOSkbOci9zNQu2Gag+VcVrCJzHAJ1tUVh9tQL5rJZGcQgUuPZDqsqYtqaQbMOftDwk0rJHWjTgliKUQgPbAX9JZrfARnfyslTSNriWifQZSKGDrRmH19cvDz78PoGw29w3+S4V8lg1psHQFwf9U4SFUDmEMCeGBVTEQc5hQ7fF5TBB1/6xXPmAmK4Ip6Drnlf+JBbPyVm/OicRrh6hy/1yG94B+q9VKgVDFJMm7vh4suiIxqDetEmxmZUNnXyqgvARfNPpM5QOCYtKCvCBK/gCQrxyVjPbL5lIwgwaTxwcB2NXyX3NoFaTDxPxTlaRJ+NDtimy1Sy/KjBhrtQYNEEuQA2WCtXpqLw0X5Bng09wNiagothfdDcdjm1oK8b5ux2YPBHAS92hAYPrNM1iiVqCWVls7IL5R4LkNbTigmAyGFvP64Bu9IptGRrMGlonNDz4GrqSt4fjFGC82koPOm6EyZnQ3OZP/t2IBJ48OzbnfmzBAq95sZui8JWJ2MzNOryIXFX7MsB4Tk/XZwqKsilmyhWkFPVLxpiQdlvVQtmuRjtSCSDHRvZADlx/nOo/gVouD3YlrcEexZexP+N/5WsocOKlL+ghm7/9ODpV9bQPd23NXT26X+jGrpVZWxQQHfCn6sPulNrm6aL6E64iC5tz4aFdOuekCym2zvY3XXVdDftxrMYPNC5HGlE4kIO0oAu24kw3/8o5XTfjvh6jhbPblwA6n0FOQ3zN/ps9AwYCuhKUcQNYd7mlYxYMGLVvRzxSlHVnB2u6WVubPo9pu2x5a9tqyexAmv9BN2hpz55F+ohMzOZ/TkgTZgcQXucMGxdTby3OaMXQcSUjXFhQAO71EBxlJkEKAauTFEmNdqqMEXuiKyxAdyCm8rqqJQPws/u1bycstdUUK2WsCkOsb/QHfY2815Vn0Xj8mE/evV2F/lwrKOJYwwv4hcREMoB8Z+qQbAxI6ThgveizFf1iUvFqZHGqNoGwwb7/t7nJad2YGf86V2Xo6I7eOheY3AoImX35voxCIluZw/V0iwaYqnGvrMXkkzanRCYd/aj+e2Q+g5+NnYzwb8QPUaEqrMis5QaODSI9m3q6oBBXptb4BgCfIK1QnAugl2wHD3cTk1njtQbti/wfVWbu0Pl6RgXgXFcVKynWj76xXJ0yDiqYQZC/Y69l1BVxQE2f5ttIyTfIEGPG0xuZN6sqGq0mGABFr62+R+qC4/qLQ8P9g+l3vL4q+ottdUhmRoktcAAPLbCiG+9H7bHNnbiD2Nf3JJejRzjwSQ3u0P6J6G7jzeuoPT9lLPRZ7ByR4lYdhzn5+ZccpLN8isUMdZDGaUUgoZ9K0RySrrjF1brDmooVVnkdy50LXrry6S8ve3Oinv44r/9q1U5yJbzb/9qhoIbiudbslt/en358mUK5X0GLWILav1iK3WDrUFONmKbcxphaEH7dIHzu8iOE4ywK0XFyvRZQYAyB98JmaYJPrAejq73Iuh1SFSxhCoDC2M1uB1OFcAnkdcDdw2bTA29VQCHxhzqDVDyfOBB8YzMH0EiU/uiNb/FlB5IddgUcfhbLBTmJlTFKRuyTxFlcpM+hh16glYzzXJgbShoX4xCEWPNZNzfqzXxLuAGhbUx8B2I2SZQGC/4zoSq5j2W4vRIE/mvpgFxK2pjkZPlCyOe9HXJ3gLH260LIKHe5Vlh2lCC/hM9y23sbOzlW5l9Hyo35tV86QEa2y8oLssdRDJlSfRbXNOhtKuhkMCLfICEzcnz8xUFx1kyJq32Q3jWmUcvsS+QdqyLO3NgJyA7zKwE2M3Rj+QB+ppi4LjA31WwYtjMFt2mC/j/kH/Or6nTqBSH2Fo+KSieSz+Jy4tsby855Q2qh/1yUA6WAC8b94aWoFOaxDvPztuYzwhlv69J5iWwMgDANliKNtDUIlXoprMtwRvp1VGv5sxiRCSfZq5l5NoSTDdZqF8jttoKhZtc4fVlzjYcAUE2rcoAMeTReds8FcAbZuprZgrtuewEhu4/QFWzfZrrfOHBr6W07KdGaESn+TyJC9yoTOkC2HyH6gaDuFG9mzp6/ZRfzhVjFRe1deRwdbzT5UWCgh7KTuW/WpoFGxT1nfHoXJ/DTXTC9X2OWAdNTQGAfXDOikU0jGphFM0QPU2jX4UljSivBQ88wPYvWPVIpnyeMil8VWw75AC5KCgLy4G9YcJPUv1Bqd2nQpdrdV6g0yImX0LN2vYZ6RXtWJr9wYMt04LQHJD7l4tQj2k1nMdEFfgatU+UyxN3fR21ndiI4utQMhQKm+EIdVTckLY3h2IfAAtKN0QukWmfX11gLw3O9mDUUlpjMwdrQgWH2GZi+VaEDl7TxrZNfBFkbbOIPa2UiVrb/PI8nzGMuaAsxaOGqSWhq6TsO2qElO2QKstiogKI1NbFtJyhyfgTuaVSoTwDf8cIG2ZLJSPRrNnWn66vH2eflxNoJIl6qgweohc7UkdXAoO2CDmmIiAGJQkpE6EONl/k7zMJNqYUbOw3js94M7lxe5q4mB7/IZYU4MEQsNxnyNOIne5JPDmdHRCLX03NECqsy79IYemRZVkkvomuCfNlAtEDt+4egkqG02Pl+VpHyDqyVLbEJTE3V1mdk/UQkMWmXKFWUtfOtXEIP71bLv637ALbwF4hgQQ6JLq7kd2QRtssvuiXrrnD3Fh3sAbCngivmvIAVpd7oZsN4BgzY1cawqVAnPzAvpSpi/8HWP5oipYcl6IJfdcQUrhAdFeOGSMK99vnCUuDTvCOuMR8nEDLYJVXYYuY0CydS44NOCoketB2hVJhFMS0ovIQ2FmrJ+J6Zbm2B8MhxqAt0i7oVhT0oFhdTiIpDXXWx4JZ42bUHYz59hlhtgK285eSPKRgJZsTG7DJhFbjxdsfLt+/e/vm4u1NB9OfRs0tpPcQ1xtKjQOQ7S5n0kfZLKkvbasvD5zdXyjWZkxidQIdKS5eTjAk9OMspEbBofmoJ27T35JrQeJmnXdzbhmYMGKYxJ5xNGy9MdwVo3uUzUDLYf3jqEm1Q3BYB8hPnGCeH38ekbl7hCh8C2w0Ee1gQm9iiKxRnQY4gJWwCdZP22/AqjGvDmHO4bzUYA6rqYmHcS1iDVwlk99Hm7kd/nXgvDCSOzMrXCmlJPPuci7baRl1sdFuU0iBUSLG67PESjEbRz8rYlFkJDwWZCcdlkFhbMdR1/yy2wyLWdE4ejxqMlD4NhtNiWUd3HFO68o5TFkmpAssPxjnDHXfcbferbaXF4Ogqj0sKRoVb0r0UyUeAILFxX6lxTbWqnJDr0Aa3yQiQNTFlJomsHkc1M4lVjOo3GgAcIGS3Hgdly8ybO6bjzBkjtmTKtayf8QUWULNKhu9A5aVhTqi5aZY89IekG+ic+Q/9m1siA4Dciut/2iOerNgXLJ+Zl7vI7lgCX+qYQNW4DZQVw33xgzygJ2Ni+LTBEnMymbbnQECnPH504nNvPkU0k9FzGj+pjd2e77WCNaRLQgduXwGidWa42h+WECoD6SSgODhfi1BytTWV4s6FQkBDYa6ifSchK3yDhEP5o1hlvJTQMYhUkG3heXmV3jVpnTV+Jid21KtoLQPTo9Dys7Dka4fjEf4xY+fI1UWUUBwGFSa0Kruk5ulV1SLAj/Twjz78B2CcmAUE5sSgdf4Za3uiRVHdIRIk6RGancZni8n5hAb68acYWnPx77CZkYDgXWnfdh6DZPrD287CKvT5j7ij0KMZK7zbmuOBOW7C4xh3ep+a2YPibGe8tnmQzTTtuGvDBGhh5CxlRAwDWMEIuESw13jk6wByMR3gqKWONYlgy9FYOiZtJGy+EvMp8f2iCDgnldhQSG0WYwKt8lgWJBcapqaBeRXypkFV6TnEHgHztWht1N+HyIJ39j+YuBlcSlFwpw/3JAH7leQJ6T6aNTV8m78VXEkzaTAeVdXFeoz4yEwiftRl9OpUXZGsxk1QeKPeCRWVRrCi+h1dPckR5plssPazcMVvDaqu+cKyOJGe+CdhWlefwrqhMk5x0CLzbAu51iM7cV92vMWYc2gd7pb5CzXl6Rb76LLSsF7oBBJIhZ/PQdEqGCH1RxOwHJ2T7zAk4egMwSjn6hY1U+slOsSKH+/qneqlxY2qvx+rTd9+PWF7c681DN9WRQjgEhyMWmwk5K7GxWAtjI660EoK4JC9G04OGzqTqlYHcFza8vRjcL6TIAM7sxuC6sg00/4dOzwAl5pbqtg/R7ERP8e3Gm4UUTyTI3g6Znw/2zFHxI7WQZwCaGp1joYWiXcdo/n3I6h3ohQIlfVeU5mMLuC7WcDtVVtVdkyzqKqMiP3i9GD87Zcu3JX3SRsZ9SusjU6HIwO0v8t+d8Jf4AULN4OinfBAiLkG8Os0R5TqHRVpa2qi5MSv0A/No/IRd+EkD4qAZ7aJKEGkAwKTJpj1I8JCf6rUBF8eP96xdLY0ERtSW0EBRNyV4JFlxxJkCXsg6iQlU2o4cpUaNiSumaGE+xyCg+TprKr10TXM1ZYgoGGhAfWyEsLQXGqKRj2OUTYMHTqrBjCkXDIMVUVv9JytRAg8udhBvvofDSMUVoqoGBwCpPPicIHFPEMW0pSpMuow3q+gr/oe84pJZnN7ZD6cXOMLkqZUqoHOsnvgw3ZaLxAr56E5Va7wQ6ooHCIjMm17AoT6YL40kXwl22F7NtyaAruGCnxmUcUInm0bf4FQO9H2xYPtw0iwJyHIcH6hN2R4ZhSOW95/XJQoj/aZI+aFd+PiqAzIBHu6nw+TlaAaqyiaOp0E3PvjCQYBJTdpjJNJOTa6/T1nq7VyVFJDeo8Whm4SHSdyTqPAh+x6vmK8vIIjAFS7Ki3D0uKOMouVRdMu5jCx3qIetHFaXR5nXrz8dxrzB0lXSyFr1ZtzKvO7ekubv6ErenicYhoTlyF9SYVrfq+4HR/be2k4onzbnI5/LScg1mUqLBvjxvcYkTZo2v3g80qfkCpNOZZjx/SSOYO7LMGepW1h+b8o65Ln6NzpEJkhNlGAENc4q70VmqzIeY0yY1qgmQMhc0hbIUcIioAbWSHUCFLyrLAjG7nzHivliyzEx7uNZV46Vo8DYoLQnnTBG4k4VDsC4x603ul18PaNyD0muyuWrg99OQT1HX5oM7Wk75Jtvjs/CVy3Irnru+61ucYNyNhg/jKJkstwNFXXyfMx1WTwl3cRFzFs5FQxrCOnxmPiwOqKj/k4e6A/UUC14Qw8kZLZvhuwdtGem5EF+tqf6wY0eO7zpIyJFlbxiJbtMHJvTU7jAsZxXWCzfF3DsVt0ZzC6zw4lGxeL8QYht8Maqiz9iN4/jMT7fM4he2T9aDxz+kzEKpcloAgFlxHKFVBK3Mm+SLMSAPt6f24IoYyhwyrK+ymBfzqy7rhrsP0rGn+wE+Sc00gHEJp5yAmXJNM72UONq29vNCxaiNXivKzwDbiMwAuj2WmqcGDrdMJPQy4CwGtY1lhG4bwPp572TpI4pI2QaUFR7xf0jtwHVxiPCbbZLvrTT7M3l1nP5M4aYVPr6ryNP+DC+zL1PQ6vy+IqhUzdLYpCBBzlC7fTdG5iZDy67Sut7ubEHVuKuigmYpCxcdiUrUbioRfsrlnCDhufXzOFVpI8sgdmwPSXewxrseFKb+nKWNpgQKiQKwEImvcIQwjHMQOTrA9Cj9K/WtglkowiuiSkzFRbxqjkc/GUfTB/ewvsOpI03AAiDiuyIqfHCy8xdtajhpGZOMRwco0j/5Qh+sZkATGtbmtCRdP7e5b40q8AtI8yEBgJXy0Wd6+0n0g09LeitZagA0sdsZPBNapDT+Tg40JM5/rp7Ed8YKfoqjSU7bmDPeNbXxtR7CeVpUX7LoZ5sNlRMjM2GuAuhsb9yYfuDozYeVAjMMYtDo3PqzlWEjRKKxlGfQBt4gAu0rohT6DxMIzr3pPrZMOhRP4FMS5RgykhMqeiJWWHVIPWD9oS4n78V9Y4r775GtL3A9Vifvx37TEfV2B+S/46hjhwXr2g6CeHZ4ltsUNfcurYHc/Tpaq75vlAnQ8sPCwmLbdpo0d5ydhKeLAaFk4ml4nCUJjZxezn6oH1hdoFivLd5sGx6ockEHjco6DitC6B7ecpcG2VPFBbBrRLJZxwcJFp/knBv3Bee/9/d+l6ktPjp4cUXnpgWNaUDpX4qSDuroHI8z2cCArgjOd77mSSO4w0CINqi+R4m1tsEzrILB4ooj7rs7vO9ZEUtC2VP3LFGSaP4o5398t6mXRMa4foWqhFVhBUQ2zhphIxx/6IzLXZ1koxkIb+SaQSn9ZdgSkZ057Q9F84DUHqmkwdgqOhLLLKRlhPosvQnfPA3IZWdbZxRw+v4ojAIW3mNflzDXD1YvljfZjBfxuZ+aWPTTY5IUOisRHryGbVHRfG/G/TYKY/305xYa5H96/3qbo3fuC8TjcHhzOpBfNovvQ9irvX75irjTADREnl7SxJ+wPEohQ23BmBKNhsQltYBBBKtIu49lwiKwPkiWpVMMSuUFe1yLy3CqjtEjLm7tB7/SKfE9CcTVzW+ohB/sKTYZXHy69ka4V5gZekyUBWDyYubIlFYwL2GFggjeILOVLrpSyFTXBq7uKtUsmW1FtaBz+HthcSWLEX0vu0YOROsHf8S2oo+RQFgtXw7yjapCHeBwi9jevXjeLLGyLAz+w8DgEOXNmgQdNzAel5uABnJtxjuVN1CRKSUrqQYfhHe6xJDvPcicr/Yt1JlYEJlaokB8aFoG87cLhdTMimJ1+GMoAeJGd7Or9yxSvF91yz7GCyaiTpccnPGTK1w9i+vxOGEbXY0m3aZ3INy9zeXWDWElcb0RX59ldMTPO0TCbL2vga8xCfrEXhRDrawFKIheNe9WvTgnx3d6TUIxbA5EyQewbYODNnAXyNtDdeVlC9oLvW6VIxxMYXx1CWOEDiTigYCZYYakpn0S6jbaPcmwowvr05n2O/VPSTT1oJJnf5hMcGt9l02NW9RzOF5waLYzYarZge/gcznLSc18OjI2zZMEI5alkbTCqHxsRQAi8W86UUZLuH9XekwfP0hQ6wAGiyc9Ikl/pJLy7hcz7kigXjMauwZhjIJYVkXSBpXELGMKYV6FS84l7ZRIs5gdJ9LRtF84txKuZVPxpMaQanZKupxoilKapYenSy52GwBXOGEc2K1/Rzpn1Qb226Yow6W0zBxSBFWyOj4ehOeQrQBGLagOO2j81qEzyblJS/BFSG3L3fOPU8u+s3jLdOV3HSzWj5LaXosi9Ax6V8zEnGdwb1umqNFPxNkm7EyH2w56J4NhbHdA66cZKaPYD7RUovnCpBPHhYoPdNe9OwkCaJhgTkaOXcGAWcoJEMggAFB7KpgyQcwvPBpmYVsiZQ210P1A6iQ094k5cD2sm5duqwUsy2C9FaowiHXu/0Hf8x1jSWGaBrOW02MMMQTRpfjyqlA6nVWpyaJ3ycwG0yLIkVKmn2QS8d1JKhtdWvSafWBr841X//N3bm4u3N/0Xl+95+KScVK0q7Y3AK9N4clAyX0LxuBDG1kKKvGkkxSmTbJqb1C3HkW6JQdm8aOA9EN05rZiHLUnGAltOhS23YAktogShACxxyZDAlNq//ass7JvX/fPvL87/6fm7P/WvL15fnN9cvODZ/Nu/ShnTmodThl9VG/mHErozOKsMYgFmlZMrd5T2BzUgtnbGg5co8h8Jvp2CcKp2N9xddC09KkKllacHCsh/TR9Yssb38bl7vGOK2jQ4q8GtFs8xMbJqoSeWj8f4S7hgII+gXQKDhVgnJ8g0V0u17coV8WHUeB9INSLlMfmesLm+p5Pc48MWy2utCYK89l5MfaNKVbID5QCkZnSQPnVypWDPSb2h7gu9SWgI8udlOfyENmvsacAPkfwVnQyI0pCkRxvEyBl04G/hn9C0G7ZHhRzIvktsTQM9/ijaLTdfrAphep9JpUFQXe9GOcVHgkXOrpwE+ymwr7rnwReA9mxWdBdVF/tNpjS9tx+eDzXD6CTq5dRBa9++n/6IVOsvzLsC4KGoLcKsJcyUbZ0zwPBCbCYyMx57Dwbo4C2kOktsD0RVYIxnhqWA+DFHI0B/CyICHZriLh+uXQB7eQjDIEzE2XtjkVWSW5OSFpDSZhWkMYDtoceQJJoSutjbGXiHtupA/ojAe7F0zMJHcY4PTFLi4gAM7INFNNeZg7rJK7vfcmWTMBV1bciAhBozLDQD3GC8dBuUjPpeky8bQ5IjW+Gqa1s3HZqoUER3akVik7TmJS4BATwr4IDNJ+Zm14p7qP1JxL6cET+xPELDUMSi+/D+tU7xXl+/Tg1O+hsaJontQHFBc5vpIxIH8qFTZBaRZxVZ+/Dw1iDzgXD7Lxupzu+TA6zzt/16nHBXcGEHRu0VkA+q046RtetcJAMcaHtMt7MaryS68l6rc9o3IA7FgtZVzgdbNW3PWDMi20ZlnWZCSR2n2XKKwZyFNFNaYQ1FkeeNyqi98FMZRjG3nXvNF9BTjbBTioUt6XPo83E/X9DG93kF7AHgvO/KS2kuirGB6DCCwEq+zqoOZ2mrEo9EFIEN3Flcz7rC2DMuifQoaRuUqsWZ3QegK4TtkAYimI/e9FkU6wAVJ/9oe6y4blwCMWCgtDiK3It1OaM4LoV72qaBJwfB4UEoVgMtcPGYIQaeaLaKc8tUkBvSsIiyibNOZgNPs5erDmP4GkGYDe8LFYy74Fh0SE4zZlNgSAArlCvOar8vMGTCcO1SWnBTaIos7/6d+f/Mo9fHQbYek7nS8rn52FE1sRLHaAPy7iNjMcLxAEkNlj029YTnEzKN78YgDhmcZmfG1hw0QyiXBlQB4f+0RymiQjplid2IGyQbCV5ojk07O/SwjstqUu8jESm6P4JeUhWUgex/MVKtw7FujGXldH9qKQgl1Q8JgPxLOV1O2dQmp6xJMSicZtdsf9A7etJomCOBNl78STlFRmVJjGJE/b3+dotMPM2e19UnFBZKxmMwJ+EYAlcXbqgDUDfWdNOFv3gigQ+fiH6kd0ASprR33E53jxnbX4UFeGrsl9Oj3dO9owAL8GQlFmDv4MnR091jCwewE/iVcIC9NjiAeVW+QAlUgE7amy+WTRf8WnriL/jt+3kXSaXTmIAnDhNAbvHrkpZIAQO8QZLYgIO93dOO+z0mZIDtdj6hutiAPuIWmoF9m2fjurj9zq4xv2TPKKqdsdEJO/jELkG1uwXYzrNFl5V50zWnrPtQLesu4512Os9wBt/u5M84/a+EPibEsPwCoANmAlS0ljcP4NFBKowcLen96PFL9NTabBsj3twfgGRy9ZvkIkgc3FrkFzxA4hpQogbVbCSHSiUzUjiEg6P9XUdzfdjbx01tCQ5hoQDGmFg9KDEi7Am5383Gdr3c1qUGXyyNRpr8tLe3ahqedebaatILd6D7GUhPbH72sqpIlBbmgGABnDlOxMBv84VB+Qd1gmTDJz295OQSvTtIwzgyfsTFubN5X+fzeQj+N1oV+S9zPAhJQBXo9NdVNbdxCKi9kZ2ewAdEJYgni46Tg3RBpFr9bETEq0hpxM4ThjsUU02UB1eUJKwXabLUqp1sw9AgT7AsUV+Un4saoh0qiIL22dqoocQibnhns5jJTeq4EbwIN8bfe3ty1j4Lg6kTI0pFWfMG0tq6dMo9t8Ni7D1BnbHtQGP7Rm70MNekDnv/wVNsngFXWhI3g2JISOO6Xs6RDK+WEGJ+h1gVMgRpDIeTQYtiTRyypiq78+trvEt/uKZCrXDht3E/mWODbb9ocwouJEOGb94WVNHs2vjkB7lrnoN/o3kEY242+w6bfJYaE9+B5Silpthk2FbOGxQhoqdK+MFm2zpEIXL8r/R09o5BxMbzoHSl3z9BODrRhgQjL58t8QOoZagt0ouC3mvP4IDMLxtZBCAdNIunBt8cMsGThXT7ye5OrYO/pPpBWy1OmmOynM7wBplvYLOSkko7oHWSTQaJE0ls1HAqw1MBhYfQZQNms1C3l6vWNrl8H6/4urwk4eZeF75iLlPUk1u2mxDbyzszOdxtSejh5lBPB5o547AI7Iyiwv4oHbSNKb516i6mo8GEN67QDpQSVtbJKrl34qfiwYoUKo4QUUENN5m6cKMFI06Soc25UDgV105lqnwIBjoF0SGVnEwl/9LN3IUoa6M5YRGvEtXKkU4LMasbpNxDH+ptiTeM2E1MCSSXUkzvIZg+2rcOOiEq7hyo7NC7+QN3aolTE+YcYhtddP0Ca4wOsR12xykVFfMsSoZlWdntN/7D01MS3ZOmXkmnn9YnGihwQEpMtGnnAlBYwDrmGLTBXcS/QrPiAliMHdtp3qibM6skPOCDDRVrMIFobv3rCKVVfh84iOwY+5nulDTLkxQIphUJkDcK6n58EvykbeiMVG9RTrHLey4anqwHs3tpy6R1kGLWLOvCpTLEy7JnvkFz1gXxAsPZGsdJi0/xrUKt2kKbESUKxiQCFa4DRCJ4rYUkuzEG74ydnnD9Sw1NOTO60RzHtIMQuQdCvEedxRww0lyJgjWiXhnlP6reY2BKcQAFWOGGFvXNlPRsYVt73h9NBjJWOuvMycM2RRxzKfeyAyIztW8HwElTi5EjDeW0tNznJU84lT6KwxR7+1SzIB7drwlX7D/JjBd/eHR6cOiHK45Wly7sHx4dHO8eeNULeh6/dRHDyqhFEFHwAxd72LBvn6sdkO3J7+eX6NUH8QdqRwdfD1vzrRgrGc4w63TKjWcavxLKp53Eh3HnW5GD2EVLQ2zR04f8HzF7QVAPfpZtUb5r29iMxpjcxrg5/GexGPYe+3AbPYVtiwsfc1089pKlwV0RrfVsoHzqoxSRUlMnnoD1wPiQw8PxjbzKJXyhCmaC/qqxb7vuIWVIVtMg04/A4uCRsgRu1aB34B1Vh1GvQLRHFvndtlsrzgXSatqCVWyarh8GLxdPOZ80PFOUjHy/LWQs7PMFg1xSVqVpXXfkGWhU5zeK/XiHw1pO0J/PYkISdFypmM/x7hPb2uzgq1qbmVcjRIV5gDNfEnXrz7nQGxsaR6vGYEQCXzISQmLAEYMLgdewoyo7BSHUliRk0DzXI+P4df3QiJ2Rng+R8KCaPczZuIogJx6CJkie2qWBiWsrnUdDrpSaGNGJLryaGKPJUlrJ4gzzeVyI/IKQg/kC1PvCdrzWEyKuKOhwBl2/MFPJDBTZVtG76237fBWP/QfIjhbtbghMEZLsaHOr7UKpkNypzRgHAtZz39J0zHbiLsLsyxHB4BGjBAwJWE5jNyraAMwkLQB6BJsAnNXRfgLdhf875lXjXyZ/hG5FSBYrlim7+alCK4f+gKIFsuOtcLXYU+mbboTmJ3U1O9eA6IomIwzgyZ1QHah8TuqI+Z1zebBQDCa3EPFkDxVmBgsL0mNi3PBJ3J+j48bHymWO9ULKz0h0Y5v1jZk7zekj4eB3xGeSEAYcmOoY7738Br2s5JJCzk9qW+nO27b0CA4TfusGO7M6ie/rCntWk61Bzbe8ZHUaHErySsto8rnlMHD+kCbZYLmv7iiNVSxjwd8hAEdxseUWR4dVjStP6KJ+YHMBy4e9JK9aJ65tFxehKSDe5NW++KLZpqSn/HNupu4YrmKKQeRlZWK0Ta9VKKh4h9Q9S56Z9d256JlhP6xH52hsVFj5RWXhrusF1OnDwcaN6nPr763Hj1bQeq2iGPsP1azrcPcg67w1g5wtjZlZA9a+k13gYx1zmQ3k0FbAGR3SM/D5h3bcFPvWWkL9JFcQ0cqAr6xMcYm409FtaV1wPUbOSxSz4+V0MAPCpYgXir+A4jLJbBUockT0OHXPmCMQZcIGzj1TUPZ6jZ2toEzXIGjyPuAVwYkROmtBiUSiALex6W7edN34/qDCAg/pKZIrsa6TBQynY6NvSjxANmk0ooDYjEtoEnLVTkdWB7phWbFlBiH72Alev/R4LZe73fN2wjkKElFwLMV+//+d+0lrQmNcYz4iZmJTrMvxzgzHFSS+SPOyOUw0R0D0UFEf8cAU8k2C9fJqwAVtcelV1AYXeqNTSNgI+vu6NIbxzAWMJ1C1zQXfntyxgX2cO5WioVQLt8CSxiSC9S7gCsMJYQlEJRKG/4eZeaO78UJZso8uqbvkWX23hNW+kE9YEfBFsTUSfLDyZoXDEtkPfFws2ayuOX7A+BWh1UkdAg/pqFy04Ge+hhH8r/a+qYl9DU22phuyN+9Rw6I7Wh2sAQfmXUcvhkuaxBd9LWXzX49+eYM2uKnFaW9YRufodTnAqruzpimxtKkj5V50C1v6WmH/JZ8P0GNbduvhSKnyKVI8QYgfKrMzbsWqynIc1f1U7UgcbLDMRTIta5yiY5FZxwIEYQcBbRQGZGM2tdVfzwR6DgaYMXhZH4X7tCHZp9QhYAjaHhsM/JEWkB2cqLxZBWyFkgqEs9VFiw7xkqNRN3mp9nr7h38Z76bF1E6txkK39V19l89Ko0OmD7yFDR3mqbFBx1384kOR190BFtNze++OIsy3sBJt+wO1TBsxsCpvLht1lZOvvTFVXSq8hGj5LujAmXG67hxFiHKfAxd5c862s1E+ly5HZM9iMoHPAXuD7NO5bunEN25ZzMRf67+5eHF51n9+dnP+ff/15ZvLG9d2Dbwi4VDHJIVXNu0vl8he/k+NP1bV1b4NE7jJLtmOjKOdFzZAHHwxCBZ3MG1obEwoF5bJKVnGbxM8nI4jngvQdaK/hTja0Zv777lr3zN9I/DjxKVwUofbpUoppi9Rm8Jrku09e+8pDo55Akl+Wb5dlASW+Ww7QwjtEEo6GIHvWS7kHdExhON1XdDC21hyb0q1P5YCkImQKlJDIjoRgik98yzcXebrGC08KwqTKBhYx0wG8Q1Tnhtf239pimF/xLC604oHGMiR7xy3fmfffueo9Tt79juH3iHeIxHwwmlllDcc1hUvFTu/Q4B6yFjNIp800pt6hrBrW7Vm83vyhP2WSRnd0kJVdvAXUZUdxPDk1fm+k8MTTVV28DelKluRcIPcnnnV+6oack1qV3ODJbN78ANdxfzG+4Gf61s5dprY7OmB2S68nP/+L//zzgjKf/+X/yX3BnjDmoyIH20pbr2c4QV68GnNiq48llGG5FxEQGcgwJwjrhi2jBtZ7KhZ73SeqddFQDMcUG8gtevACzb5rjOrzCU37l4Hy9rhVz1w1g6f/ZNxkVTExUm5MGUAIa9UtXi2hVjuYtvokbqEsN02lU9gPSaficeeH3ORNw9+/sw5pdgUlIc28s48+a5C1lI0aKK2GjjePxXFvHH5QSuUNfwFLzlkRrlGyI8CGVEgjbvQD+bKVzKipiCJLRhLRvc9b1hb+NJnzBTh2w1tGR48dlAXuRRm0bqhWWQ1ub9A2DSosSkgwMsDCZm3AYTGJZEk3NXcmaMMWTJhp89jrLce7sJs1awhJwyPwdkEMfEQZH1IPBgLbVA/FfaXvaztJoJcpVsR2rDzar4Eh9+Nss11c+aXp/5ptId85x9BlffL0Xd7e3vHT4+fxsdcTeR5VcHSI/rfW+f0eCe7u7srx7vBxNR7uBXX4xIR4RsOffx072Tl0NfLgZU/m873+OnhwcpBr+jIAdCl+xXDPjk8XLcMAidTQ/KJe01rjveZiMTcBrvwGWk7KocLzgXIqX/MrAkjEJ7EXI+PT3af7kdTFQNGTzk1h9SDlYD8gB1HoTS8Xg4X6n68xMbtxRwilvi/pfoOGOviN4OpFnBcjqv5ttEiaOzdVSBy2uT3zqgaYkAFJeUOIwa7Ror8ZIxKTzN4+mxn5dZ5V/MNQhOoNAZ4W8B0PKO4ERXEm++BcWjLEeBtLe0UIDYoNUU1DATb8MK89BsQGowksU1rjaWeL3QAy6wM1AMMChVE3eYmauTvIwEjy7kHRF4o7k8hOMR3ILAJ2NLlbFkIyNKocb4PDahyFPGzB6VptqmtmK9v6LgwZsXHzSCb7riYmB1ds5XmLiynzc6impfDrnmIr9Y33i8V9F5O1Ul9weadHFC0WlR1VeuA98pSVd/5/d3iGyCJg7cFaAvDl5qKXvmrLQ6IYtwVuE82epHI/0ksoSVqTXZ6s63yw5p9v3Xx58YDHJv7Fi+0T6785ppvAqa8eWnfsM1vewkj1gusepgtT4mXvXWZuWOmI7CoJABvh2UKWlmDkpCj1YysEctHE6hFSsEEb2EO+v8tQ1CAkvRtOVf47n6Q8MPvtmwCfX/fF/kffewZ5kjHhZFwbMZsrziBRApLRUDOkE6CoU6eHB3tERrq0CKFcD3+ToddMRFbIyNr3TduEQZZF+TfCRoWwmb4Diqy8NInlnSwdM4kb0tnSYmKKuiEmfXv/+uT/f3db5gFzAox/PPeN+5hAne2kqyRxsvYcRtLyshcLIYEIKJwkHnEcTYyU+GmM2rmh7uHib4hhRJjWU4YXRcIUT+f1zbSBPqJ4fQET4GSS/qDsYPBlB0NjewZyBR2cPF5Q46CDYHEcQ00Kh4SZ+YMYeo7xNFtNSHbfSbwFaibE7I6xYYYkT64oa5q7IKnWGIy19MGSOt5w/6INEUQH+atglpkAjggE51sUymlPgUyRmBIMJx4a0Ipl5dGbgMgc0S5hY6TyveooexHgmp7mA2NVzNjtLv7+mtkvXbstrf4YnvH3ygd0x8aQdmHaj0kVsMvnHxD7YFhOXkpUqajsUtxYWVxrMrFQw8KJDpO4RKgTSTv77EhEIC7vFNpgcDTU+NGWAeUUxjBypBG6qD31PuNh6+ByQ6nE9dGiGr6iU+gHFI1ltl5UOzNp9IZNf4kjDVjTouOQrN6sTxIkJyUl5WSXoLLBN1SLesv98njQdFuZpHjnp7amudL6HMXngygGKnM7Zg2Apbp2v5eA/qFTC9M5qr3NM/LfrCnk+/beVseSadp7eDIwYJkXrzEzdCYc+ac+ps0rBroPzXU402Xi4m29aijgpIduuNGLD1ujdJe2PraBbR0qO/ca1NjFDL0fDc8EGiHsUCDYkg4gafZObETmDNWDCegofV9u5/3IfPYL2dIN+RPUDr86M5rA/GasLEu5tskgGC/g8TgiY2y8aUXD8AkO8xE9LmUqFQ1sLH7APKCeoQGZzOSaErJ6Yg75QftyYPpWfS23t2mXCwVpNa1Hf14ro3sITkb6FF9WvpD5HKgnSnOzBBEoOOvnTnTzdgfgFsiQok+4gshgAc2iD1fEImYEyOb7ZljdtUI3kkTzGUIyW3C3DUP00E1IXA4fdlVZLvuBlaBuIGSnSFZTdAiKtoM1hcFpkS34fVKYgdBv7rG2q1p7s/SJiOAvgS/IPhOMM1cDsmrHwt3XnVUD7om8xm19abSdBeKYSlaZdtHBodhitzFSm4g1T3guJxlse5Q2vokJBMhGIgSDE5vKiSHEC4o0IEr8XGjE6mtA8XQwmntJSb8A9GHDyDKuNhGqJvtnROKFH1Vz1NXFQExnmjtogfm9eiF/l0EV+WabEK6hWaKepmVOCk9F1jIhkMxwk8Yzn1RLXI667RnzNeirRpp/xFIlnhmuInudFGvw2Y5gLiGKxHBBT6oR+jkPfC3/JaIqsuaZ87ySUAyZ5wNoxkimw6f4eBmNdJ5w+VH/glc4Rz1ZTks1hxlsWWb0OHmGCTaz8SHllI1B0lVo58CSf5zG9Hk7CoyVoEKVefPhYJbrN9Q/7YodfM0FT+EN4Mj4lOnIRWn22xY3PjSvqdGUdivXOsQqmlQcX82deExSWUiTJDJ54nxZmbK9u2COf5nD8b0owhUMB6isD0AicfDJ68ve5v0nhhUbu1lKhH8SBWiePDAIVXoGQCXN1i1zDXIqwxd6rktL40EEnlj292Ji4oSmSxhiSiEg4ZmFvx+DHabwOzCFEJwQAUH5Y4omNND41gS7Muolvv8wadpp2uHaCEC/YWS4A1jeBgTVvkFCOouq4X3Z5W8NhaArgFozgUD0Q6BI882FTtFLdyFdGIE8tfRSOVw7BDm3eCe/H5ixLWxFzmQoo1Rd5+8Q84X09GLRWA4HOT6oz1YZLs3rj/kpJDe0JiGiBcn3LALAeV/PAeCa96N/jXDufqWmKFtdDGCfPaMt0bMklIgu9WX53CZx2S9UDmY+ApSv0uXEBgDIp2jUF4KqCdVgCFW225wL7UYu8FiMENPyI/DupgMpkFKmn1oLCjXh+TGkoyLKPlPJNLK2C5IoatyFXiYZdf5rTFr0r9LtY+lc2+3DzfcVVG1wAtVdMqWgMCD6l5ZZWdXl6FpkWPTQctdhcLJmfvaOsBeFlBcVkaE8wml9ANnk+3Pa6HhBylH184XLFa28nPxO76sfuMZ/GiyoxVJEzLux3AZTChw0KiMM/TBfFe8odDL3jdSKG4jXUSCACIRTztRZrhhbtQxtmYU6x7x2ABKgX4AtXj0R0Y7CrceYoHUiaOi4viicagimymnCkw1gUQ8Td7FPooavFhriRvi8udgnGaN/NaomnGFcbLEXvx1BEZoLNnDRxEb46pqi+QWibuQm5a+IG+bpFLlQMoth2ZhGVV8boW/0h62adjIt0Vk6jV11HNL6Mc1MB1CKzsKsGJxfY9j0WcUdyj7Wsy+xvG+nYDyiF1KT49Riyk3K9c3E/ufYoMPRqbqlae7zHVsAjOW73sFIdGPU6n908x3dLz8o0QGCOARz0IMSokchO3Z1fxSHmQoEf01BXWge2R7Lte4mBaJCZGFRxIAZfXC0d7jJMnlYQO3cV3yIg2t4qSRaywlliqUAuwmWHVS8pU11vRKrxkRu14WRb8fRAor7EAqKddT7N72gn13Rv3CrdRHyOa85Asi/dJP8TAT9IDr8m7WhWBOAayhItepcJecJF9PaSEMe2KUYwX9R43EpAHPycwjtjibssHQD5lwZD1JwcUqa/e6hLJzrMJ7Z+Y8LX8uanqIjzZ2j2HLTL09yj7l1Fp2gdhAzcVXd0ghOzLl1Vs2z/qYNLnnzsf0V3SVj08XgsGxGNIGFnPMQPx5mc8WYcDJyW3jIGP/4sZdvjcP0McQMwTmLEJKat4qZYPACr3CO1wHjGFkNWRswbQLzpYnMp8mJCbpnT8vHuwdsUVzqFNl6YnhM2yKEzjdbGZhNvkcYKsutOJGbdMqku9nOusJEfg04oJxiwpxwDi588JOKSFrJbszHFdNEFFnvWoreh1TcTqyUzgvA5raGdnH/Bk24AMxFb0DJZooYnZJi7MVkfn93pPIuXGPdrkhdsueJkcIfv9D2YB99xw6wj6w/CrqZHTEGnmVtaRs04JG2ZZakzULY9JgWrmxdk+8sZHpS9kL6Hyso+X2WbBOSqozExuTdvK+57MH2V0cjf+MDCw6L3ub5Z+NlsTHQwCbYb9YJRW8j4zHUsSDbsa28TmzdBW1NRVpo6RR9gbCnY0lzmy+NJoQ/nmw+01Pcplzne51NwAcdeaUjo/rq0k1gJVFdBwLq2KkZYPda5usQ3quVSbffyMcATrb/z1b90bvi4b+OpAWBlWIdIpktc3L2S7zVhBwWMHmZQomUBUtnInowCaTuquc5pUj68c98NLm4+26kDSO12GDzVTpffN/OLrl4surYtpvRNvBEacd39pX+ucxr2qsDK2eHHLpM8laoatHf6F0XVmJksxuAQW1lLnjiZKTlDCK0jsgpl2lKhTam19a7JXN4bF3kHxO8JSbBNCDQg5W5CcMOLm8UyFlt/4drR3EpBAqs5mH85Z4DiwkgnSOOdbIrlsvtL0o8gIjEep0CFQNnv9dhhWp/vBoF0AFH+0as1EgV/d0vniwmZqcSV2oPg6/u0X/Y66FdY7obFo/TE6JCwDBqnwORHHzFSKg/dKLOKGVVrYqX0CRtPbkeQ9VIqBsShUtKOrupLqDZLFYT23QA1u5UBddaonGzaOQFIvwmrgGeSOnSCPBAP/XMGAhQjaJca8OY3olLS2UJ6h8E0B5HNEAvdAg5pny53LC0UH1msy5qACepnYr8uJD9sPZjd0u+Hcj4fziC7I0oZlXLfFAr7KE9bDsR1D/ZJXcc1kHsnktCqts6FbtWByWCk+jE2nM89zI08BQkYmD5J5BUa+rQoYqJriYofOZ/ZiPq0rQaJzktRaBBakNCmxKC0eIezUJzlZpqGvw6xwMBR773IeekANqA1PAX2Ol9X3FbZEhZUY/Q6Jx9erWRwBc2AOk5VEf4Zp54UKE5EAR8If3r1MyNQQRtsDcvXoUs8E2wzSs5sxbrg7sIh8kJuubW6pF2HIwLRlSmP1UDQjbUqVyXE323z5eZX+o7Lz+u/K4qD2dsPYSrefDlJvAjasREemVM2pAykW3DVbPztzdk45hEDHZRKJJJWHclXtVSvAld3JvQ694wUmvBMUXpyBGPaPAk6lBt3pvxNVmmV1GK4gpVtgVa82ZFxA2U1556ULQKrSAnif5PSL3xD2jnrGzHB+HsUh1ds7IkJF00yuoaZ/53gFHi5wIUTYNFoaCnyeNaEfx15Fvg4IXWKA/lKMhlRfpjfBQvDdx6IzWURJ85ANwj3ZMGq+OkXLBipVjZxQoBqF9Z412uCJkCZDNIq21GBSCeB7zcjZQChBzfE/SX/o0Kln8WVnXWuHr7A5JpRBs2MxB1n4uZlBCih2RrRFkfmh0RkoAhaBZCpwIhHM4nfTH5YgBnOb8mP9LlIzi90grsMpGQOH79FT8vgpJ0S9Vsodif4w/vGKcPNpK2RakzQVQ4MhaHpvbTfFLFFXqmKjrlQzwN5SNX850ykwC/WiBaARIQDmB1GASv0B0FpoasbMpoRmKaf60bMQJZI9DYTCTKaSvcEav9C2E4lgj4KGD3Rj7DwTCV+lGDhhghEflPAgvKe0qdWITvskZizCQfeU3ROFQhJxbxbekIchEAQYpQWeTRzhXDPpI23R2TP1XQgmD7+3JI0Kl2UwQxA1b0sfUBoRa3Adt9by1xwKiC2phWbeAfkgEc7sUFVF2BZJOgRMwjsnuY5xEHPChYNabAmGj2MjP5aHk3Ze1Emkf8JA3Y+2Eqs5ogksWsl3dWQz2JcQ/AsTEFo+QmvHisBL0oCMOV6CaDoSdWrA8NqqL3td59qLACH20nm/yL7TiwLxJJmac1wAjMtBxch04OA22IVvHkgVMTUzQUTApH22UEpYhIJcAe/quqG0WapSwsQHaJli5DNkVbqIQZTOuI9GwY22BOvZu9Zaa6YxUBpfI0FDxuB9GONsZlT20YLGIKznyT94SXNUWNyYTBS1RFZaz8/nkge7pcp6ADanDJ90rixmTAo28C2a9OzFW8tggxYyQ46mk9CuHoGKryTGxtC9MOiVIQPwHcIg5oAOFh9buJW1MDoJTARY5AC9KbVWUo3ldgHDXJpSQlyHLieY3dsa04xHbyHS60HE3P4WbtBIWfocnPFs0N4UBpHoCINuxsa/PRO+Wvb+4vvGRHS+leWKKdMeGnYW9lAXarP3t7KHAmlzff72iw+kFA8QotrALhdpSE4IXiCcvxqZ2KriJC/Z55LdPBOjVAkQxO1nRnAvVmIknStLZ3gwqICJgR0kx63Wi5aP5TCo/gw7gBkvKYhECvBJqBb+/oqnyG4IZPBP3iywoV+7CzoacBWwrxYJHg3F5uBz2dTWYDSH5ZRCEViQHKYxmACFJZv0TNsmZZx9sEfsq0DEB3yrH9/vkCzymucewSdd6vRJ6W14jaIykjWKX8CW/wlf04FIkxCQZTAAER2FpJmjOvwrkyekwbz9y4QMMxXvON9BncvV1AM1qwxgw50YiAxNQWdFBg2vYNMA24oKFljYpK7XXKgKREqdULxXqEUreQek2IeILQlxSMZzrdSxBGWsXGtWrDnIVVAneKxAvoJdphiGChmMP0FfSsekGtpw+tCOOMLCJgoo1CDPSCdEYYj/amw5n4Jkh5U403RlHfKoAY9wKqqEoACEJ1JzQVXZBN2h1G/XJbt1vcnGqCYiYUfm5HFGYJAjEu9uoFoC1ps5r+1NvVz0iDjgfFQqIRDSYDFTjjujgbBXBogN8F9facAJcaNmTjxOlH79GrvoAq3I3lKbT3PjR3mA2SlLWmUZfYRc8ovBTFsO5EM9Q1IMJaIP7W35ZIWSVXNbFFbiocgeuoltv5IQGXiSqQb0gWp8Xpo9j87H3hQB3I4dgRJ3f0xf7VLIZdCT3mjpKY0DOxiImReMHrWiE4k20vjhTiznyIJLumUNpMG9wJp33e22s46IES5+bvfFi2rbXoZBtHalptQJhv1pph7XDrl1odvpVTQWrLI6YvIEmEsYTox9Z1Jf44cxoFJ4qZbRC/Vc+87nj4VAUs8+lEUy2LozC8b4t7FC1bjNEd1Kqz+iPbd5ybmvpjGjuNNPo+6A8AVv55WEpG5VTmEHhLn17amP5Zd0ClP9IWKGJ+5GdM1SqYOjOSEC+E6rb9heC4mDxPUXIjU2Z/dPFj3KkpZ2kbn5ib0aQqnf4NRQeKTMMvUFhOF3CWwIKoi4hfm2c2USWv0nHeRv/simAfIpaN2FGkY0kFje5KqAsXE8ey06xxWwAh1Di8NhGZQE7i5T3y3pipEJxi4VdyrxBI9NfMVoPNkKpCNzMVxYElkDKuWVpNKosJr33jL2AvPlsBM4quOc31RJt3shPfumnGh0BmcavOyWCxh+pB1cExdce1o58TLte2t4/MCvnL4UT5n66JHDeVpvUwXegjjLy72jC1jFVV/L62rUjjRwa2Rg5IgkWHrkcLVHEPPsp/5yTrgmSQ5LiVCiThR1O0+pJqyggv6z9oAvFG9mWQcTX2U85QJurTyVCEiYtedjAkDzlf3BDYz83qwpKcz97o+1H/6LZWNvUTIeTqKRNmLYcyiVFHJJVweEUQCx7CDCJptwavwCzohqWoyvCsHIKjqHtaS1hDqSmR+YlCYDkA7BL4thLGBX31IC8PqPj/bOcuiLUUg8nvR1fl6D4Ti4RCoZtOSZIPJksmdyLg4GouunoRF2vbSJbyp6y69BvbxhFqbhB0OQZNn2K7WFYsb+o+tDOREAG9AB/MfjX59X8gYwecOpGtdlo+RWmpGFBpQLMJb2D2pkEUAFllsOFS8e3MVXy9kt06M2+DjnEhX/lHzyYj7e9KjsU8JEHNxwaZTlbUOMyx+6QOHzWr96GiiAlmsUwwCZ3PjNvHlvjdryyBk2HjMnb3A/GS8Ra8aTQHKhH5sKF7E1DCbtIgZZRwyOW7eTK0cXUbMLtoZhrd9JbShgd9YILSbnindyR+KyViHymrcwfCJ1DKFtb4o9EDyEtkzHSkbpf6dpK8gVUmAyYMMKmOGzdIiH1x6u+UUGnp/wZOwyhpkhrIMfb78RHUKDlXtEh9yltET5io5m7dj7DPinzPpM7bD1umXrudbbwa0+X87uaLJvWECl0UGb8m1n1OKptzhsGN5RnuWLFho2U4GrNPata1Ltd1tQBSKMaMayDdthR7zCVLkuvOwH5TrMPdqlL868vp3Y+RjC1LHBq46D2DBj98IgBlhUo/WyXI/FK1QmBaojJyGPFuwoybYUFTnJLC2yp7Ozq63/6YJHPbosbbsNVO3SnSrXCbprf/WVn8eO5PXxcJmrFoocuw3i1JHaZey21saH7fDEbEz1M1OJBWxvSgslalbrnAaU9uY2BnhSAXO0hMSInqhFp9bWtBYT0FKWnFptEtZZGXPH7BNIdc6/g6GKjQN+BXz++Du2sZtQN45RbHBza5hNahyv0eOXQZ+cvMRm7hVzwq7+brOPZ6IdX2TUlRa+X9efiIf6RObmnOrZBnkfKLfWaSi6nkxxYjCReqRI+bPvomIKx9XrBI0G1Ch+ELf5OIkgDhYCaAkA51ERhLhhQOhWYnDZTqCwdWfhgBXuHUJd5OmKlYnNBMs0usOFZ21TfWjZh6VnyRVX5k7wjkA41cREoUZq+iCqNuNzGWqwgGYV3NXfdCNxz32lQtVO0ZnBXafBf4e5cqRQIjGrLWm4zQNp4dit8J3wQJhS8QKZ9x5R+D2NN+u3N5LaRxRWYNCL6pXgnFdEbGJhTa2HqOVsTjVrYtlpo4fCAdwLKI/U+jAttNVY2kzN28tYydhPnU6EyUwr7INxhuSOMMg9pxtldMSuo8y1ocG5SmNonn+guTxSW+HjtVSBtHPQ5J4FYGVuBsU1sWuK+WEkcFVHxfgTgXkiVQpV7+LiP/no7r1iqWvGpfhIiCrK3xmWEqtaBXXa3/X39HGQTVAgxOtip9XcTRlirJDuC7ArGcvPh2E+e+Y76uo2KLYTdyLd+Tp1ITrnewqG70A4TR9ZKV8epaQ3hJrRWklzqNoAiz0vH01XoE7ODjtcAIP9Bn7o12Sz7KFuW6BBbVL3I0bOqtuhEDhDdzTRFSOPY60EmwW1zSs92EQArLvHwHGfpHIHd3n5wbiEOxgdUyK7jNw2Oh5wELgyNIjfu8cnAKu0pwQVbCXxzyb5i8tuzR+XpQXLWPlWgvM528DhRI2lqzUMpYveuOBevGd1Z1Lf5EF1lip7ECJImMZsy7W1DZbwFOQ3EpFIwbzN/rF8A9z/bajntYos9TjyYdxUIUiwnlHp5aApS1J9jPiU/SdMaV3V8soCN9rFUoisC0gdPFqSpybxID2E4gbzxYQaYOvcQ4daH8W06Aclqjft4lHpcGHYQspnQYnM349YjieRMnBN6cSymD0k/1jYAaMsnNHvnrZtRXRrkECrv4omm3WO7zsSooVp0QSxZ3e+IKirguQzgdva0oebjgkT2txBKjHE2oC485Sg2oTCqWnRm+/M8lghlRfhtE63i10h4T/p4FGWpJWvP27rddC0CWrJ0jU1ZJFoCuJcUigUaPyDJldSp7eVt9bYjYPftfOXqx0Cb1HtQDHGTyHqjMqxlnIz2Qyy058wckDii2lNfNuJY51gCg33GpNGztF9RN7VQnWA2ncNybj4Lpd2+no6ctlYM548VcCZfX7yD8z4tl9PT7D3DaRtXzOlHXn1IX6pgMEljuWIWLZVfp9n3N29ekzo0amoOSKtKMOaNCr37YYYcisASsj4NlsOXoftqVQ1lA/j18QhtKX9LSDSYSSKlVtAT7c7zB4fz4tpOeBlV/4mYg0VTTG5pJfJsYI7mJ8pVhdf4oPc0RfRu46BK4JVIyd07JHnGskzhm+C5a61RHKGN5c8jTdG2+1aMd34sJEnOBVP1WlHhbYwbbMdZhcfHNknvvTUi4OzyTfbKnEtjgmXj3BygWSJVPazRVCHoBAOHWsxhxPsFZ8cISGN5ar/Ilj07utSweGTtlXhffK4mS/zt9aT0iM+kiwGVFELRBcbRnIdBFVSFqpqBo2wkTG7urM68TJp0EC9YZCW5SAlgWZv4ISmEe1k0f7nvkfB0/HBF6RKVNkSx+VOJdVfAzjoWEaAtW/m5NxNoVzVc/8vZ56ocblTzwmYhIadCbZBrfJltWEHEOkjqGv9A7QnhY6lNBjg1AdO2d/PTtmdQlCCpl3RGZFFZiIzdNM0TiDtge8RJ/GxY1BDEXHH9VfxEZ6uDOBXa1emM0ZTawqHrEtGOWxsnEkxkkLEsGxWz5AlsgWpY5Iu3yKHFbS/Ah8br2AOuhdRDIx01Lr71xCkg1e4pTovZct0tA+fBuo1MdJ2MoeVJAglyLLCqO3pOFLlGMVUuQjHlQsttrMl2SFfnyHg9rKZGn5FbB7JUbnFHuGAvMfLZWpCTdVWiwgJv0YFZagtgN1AhqKwECeUy9w3iEAX3ymUURCpsKaka5ttQOsQ7RXG+0L5NKk8aFIwTebHnJQqI6qh3HNEa4gNXyAalLRY2mKlL3yWa6cfH8mFtPP91gQKp1aLcgCs2tiqZ647oqRJIESExiin7vLuA0BAJXQTR8iCw5ynb1KYk3a3h6qwRJOy2BIbDVWFZsRgy4ZI6Q5dyvBcurq9wKUZfVsMSt0E7mDZ2sqGgcL5VAO3x28y5gnaIAUYOTPBbvplk4qGBj63fJKrTTsCX8vRwd60+scUSa0YyXlqFHHJ+Ta3ndeIVnFYczYOtsKy+lVeyOXtQzp5uB62+hC7hyoUHJgqz9piCrqOvJrflvkJwEyDKi6Gz8xCOgakD/vOp72H7uil1E6yt1EgRRJQ3NFZwSPiraptdYLLlojlMM8k4KFYxryKXzAVzUUMX90SyWIySrrod2alySBk6ShAMNqEsEKQQI+D4pPBygXWcik16UL88bWKzCQ7LA87/3OyO8nBWodJ9n7GRIbx71dDF0km/oBTKjuIJQ0lFiWcGdeTCNEDFMo8TgDRbz8YbzXRESP5W2AZWWyRYguF+pY3syke/Ikfp0yy182B+ccrInwz5FSEmwOuwuvptvCAejG9mhtU8bBN9PLg5D/XhCrG0ziNY2V42VEBPIn4dJJTU3ABcHdI4UhybqTWTJZhRypzLNY+cP6IHbEra5s5BxijrJOdwRaK+LSWcqEPMKsPYpu45iRChWlLXxiGChQo0RMfKS4xKMrk5KuolArwuFlalBbzl3lWlRs9FF2EKubRLqz4FmE2ZFMFZ2e6lH6FcY9t3ISD/1HFIR46m2CFW6KLUWeXKIYxA6y1Ol7QnqPUhL7Piung0E6DFr0vsX/RDIoBolywZ+l5HacZkvfUnwQh+kT54Yog3lKz3gogzImAxb5GYRwi5juMSVQWxROueinAnoId/pGIQF2K059NJlwLyvS/TiQ8jQavkFHJzDyIqHTG9vzrYO5aWwHdKmbpCGzPv8JWp2A8hSNYVptIRm99Lna/WvPWU+7jWxZ+XJRS9cMdB0fbJDgDJFkslKbGF9u44ciP0QyTOEk7LkyigQfpBW2xyhxJZ77CJk49g87QuGFmpMDjHY8hV1+MRGw0xkVL7KyezuJbr1ykJJta1DKTn9gxBKTfZQLwZTN7zaxW4zx8CZeJRJW009Gbv4OjM24qs0ToDubjZgK4xfXStU4z6oKQkM2yxJ2wPjepqDlkFsYkqVZPaSG4kBjlR5hew39s6JLGstaXdDhgIxHEbpi9Vr5TKEraHXf3hW6+pupy8dnDAbTTNlpiG4bSUZZHoF+RFCxeqq1AZQLiMzOWQGLTqoOqPZDWCW8mV+fzAs5F2dj6BAt3kmHOcHA6E7OgUcmISv6l4TPovFkoZcOgnjTUx9S0byZWqTW4pWQDp2hrJxGUUfqy43NaiRYceaXd7BkgZA60MO6X2HSRVH2EaEyw5TGxM2zUcF8olRVg6DJRK+Pm5ilaUbJAs9coLI5xh6n663wZgVS9yuCjG5jhhRxNODSjyPfuqQGgIEa/UgyIIonOd11laPusQjiMM0KtFricjvNY73u202QWxxCkGFMjRcLPTRhbl/HCjjRc7lhwKkprWgkYB1aIVSEuaBb5GuZlTj0NQgC9kMVAFfxPV+nMnFcEacujGOzDMNG4NDoAM1DPN9JMqqkrn9jZTlams2UFv9x/Wm/otZkLUIQL3HGrOtcTz8IwgHrspcqpELDVwik+t2LG3MWog48mwdFh747fDCljgI2Ghx0CBWBiVQVu3r3mIC5U4wW6hcYqKKJmaqu7uqNcvQ298qscNXLiAFy2s8l6kir/Wj7Ksl9P0b9vPKfzQ2YbWxA0p9ZxpsVxUXQfBpoCaX/CZQoS7nUNageWs/PMy5RE6+SihK2QRESOINmY5WCAmdZTt9nZVl6c3yFigns2UKo4vSR0AO1k0TagciSBuETOnw2HB20uhP1QTfH9lNRWIkgG2Z9bt4xPXI3SfqLS2ANrfbZZ/EwhdgJ12WxQTpiedKhQdilIoFscG31b24d8Tax/Yz+p8Yvpv7hKK2jWzbn2inf0ZNLnhCiVk4DFPGrJtpf1rdtRtK+lEvPwib8qiDuMGUL6gEw1T8wCQnzmU8IY4O+GQxFNdecHooD5fYGVeT+HgYMSsbNdQgXA3Xphv3wMBRlMsoBy4/Bn+A9Acy3Z0sT7jVKB/SdRY5p0tk31xa4ZeoL3PcF0WO9aY5QbV/plkxZYGZGFWB7hYOfOCCREE2kf4cYpXkM2GZDLQUgvgBooTiRuzrDeYdSNAvSjcIo81CYZT7kBJYcWWUqxvl+b7EIMYkEyQrC0dN50RUpfqpLcXhmnXVJbt946N664YhbDE3AifnBI3jWBOf60rn1L6WwEWJ3A3Y1czZSOrnhlpc9m3MZ1n4u9dQlvZR6SaP/8+n86/yc7maC1TmAdB002musWQftRxHwoqT73Yi30OwSYTZJTOfIltMd8U8w9BG6zPC5k+Pzu/tknLO4a1ac+K8CkeIWaqKC6FUWd3a0OrMEiQ+O/SAhlUvIUappgK350K+1KYodUZ+MBzYJP7jUfbFGV3bMsibSb40w/DhO5weR2NwwIK8y5QhybMlTGJZguqjBNw5hITpscxZWIUuJ3Cru1Gr96x07hZ7aJ+YE/HhR0TLBECCmRi8q9/MJHe2vUYY1MuyOzKWhr5FVE4mL/5vEbo7bn0c+xNR1rTHQQBC/DdaZxUx/I2guQGbrqX7Rff0bVP10BeDFApLWLF58/VjLWBHGTPwRy6xr+BpE8e0JbG2w6YZwmwbEWK9Kp1dZTMHetA7EfhTnODzkaVmFAmXS05w4RWIhtSqImkwPZ+hY3XIr2tomsibZlrIcSK+UvWCo9C/7MdF64oh+a2IS17H618aD7SQQveW2RYRysqtDJ87Albd8kXFyc59+pKnJb18DFMqnBNIKj1VZcpE8m2sAsgUPcFpIlWlDV6/hHB9YP0fsA3DG6AiAfv0vsdmKRjK/i2aNeugVVZWBsh3TAgva+77F5jxssfxc9/qVKLFVcm0Neo2qC5SK7L32Ne5hbQj48PooBQ8nQnegwXjhc3cLaTSUL2t7ZAu8FfkBulmgzyWsy8r7Em3xRTjKlhyNV1Xj0N8yBG9NfDMRQOJnl5jKxquVRCIotEwXIsA/IBgQc2qgerX6+UJSVSGTF7UR4ETsLlDCNuvEJWvK9OkUS1zCsNWK8h7zRnBrGoyn5bE0DQbSLViSuNdTj07hZAA/mrKXKbrILVerxm2wntGtWIOjPd4YQ0CNPmxlYWXhcBdVSIufXpJTGEELQpYWFnnn6mO5gGj/f6GUkpr28Pp/2vG+wanb3BJhojY9RAc46YAFbrYk3Yr2DWlTiomKBSFHeCjomq77mG2pZf8YG/mN0ZETJOP9wi9HVgyqto5NWCVHrPTEMoTrNmeQtVnTRPLz1E5+wrBMGr2kh986eXFdgNOjr7LgVZOkn1/tWFKPwG3iVnImpnZbQlOcOxKgzIUGNKBkYbQTxkImghY3f3kRhEBCAVuGHJtwkjZjrz4CxIMT9WWwS6S3rRAwpxqE1uelV9t+7oxqQ0e72n0GRIwHEVyqeEDtRmPelOUaW/NrgQtA3HB6u2YTbn6fqpwFf2k/i+v+TBeQTyN57A3R1a/6NlbTWJLU7hmtU1ibqXdj039ZCCedms3raG2JkVcr0qV8Hkvg6zwUeYC+SaU7+rMmtjcYp2hlx2hlcCYtTtpXJKR4g/S2xKs4cYroDmFVS7kIwvVEdV6UelOou2JNFbCARoztiFI5OSYDGJi4Wye6Xbls0eS8pSjN/KhS1zR0zW6iX7vFutuZzl5BNw5w0/qXATZZ9XaNLbSX4XEKKmwm3m5T5r1COtvPCggPIx953glObF2jsZeJYXFAkhnIeRFraPaZpZ1CkfC95o2SnVL2ux0kIxBhBIbRIdqoeXtDMTi73WXbtSCUAxLAg6ybjScYVxCYalMmovTf6bkvhhm+BA50Rlc74YQl+3XFgcDbzGSzNXvJbJ9JhfB6tjDyCaRhjmNzuGKWVAKq7+fStMKfyZ+k3cU0DawG3FzdODDSBQQtwILlPHWUDctuaxD7qyT39oHE9fGeEbMbSsfLzCs10VUTdNXwjpU9uaQFJjst5oQ0q/q2arPrQijFISE1TITYMohzVFzlwOJFEFqg1yYGcu2PPiSpI+B3QRg4qkJRP3BHOA9BQsa8P07ZojGtrCXmDKllwNvFCeKtljDgKUzy4K4nG9UELxoLf3Dwnc50l7yfyq8AGMGZCmHPT2s61EDV1K+Iqd4KjP0J1MEuywhnTvbDtVsz9jtObv4Pb+TjSAqgPBwLFmC9CzSLBi3lJzJeO8mHtJTjqmKFHsIf/wqMCWkhL2yyUQmG151KhwBk6zp8dP9mJbMDi42KQSK3Lzhyu4MBKcbwH/qOsjgEsNx8wVIJPPMEB+Vs/CtSJ3RXDchMjCdfK6zh8s0s0Dx+kVH2OnVgjKwHCpE7cuA6KCMX7iI9N9GfM4VG9LT1NPTeObvYxxDGBW0nnLr2l8bLnszbRvge+O4kKpuPjqR7nUbj7wKkG3WnIFv+GjzbqiuKd+d9WyxkKoJD3Ub/++3EEbW4p5zwxYSr/+0UgXE1eUUi9XN6u24aKipxhJs7mRjXzdqbgiXFakXteVP8wlg8U/ywEEMJJh7DgMl7BOxqVRwPVw/EDeLKLIHivTeDhDlbhJYTFpQqqjpW6wiA5xBX5ILMMbSiQzcWyMyF0oOJ1AXLZTmId8ZJxSJroGhjkiQzxUm+oVhs4fADwecuET2t+3SwxqEHHbZq/f0ltCQv02+aH5p7Qyk/39LfMNCk0f98PLscO2Oec5mC7r6Y22YlmKesd9A4CNKVVOjQjA2e1Xs751/QTX2fTLWd/2rtRsA4q+2poBRA8WNyr3BHmahi2k0RJHh5UXN+YYeV7ilnEDjDT1y1NWHYivLt1ycEQfhIhsy2ikU+owBNcngLlf/ewE/G8r7VnhhkOJMX00DGtKVplwKhGwriEfXRx1ByUar8wHHfYI6Fc8eZRAiTtaQhGcttYDCTjBmlvGTMW3S0Ro8o/SNSXWXrUMyV4QR3c70sRfeeT/0S7F7JJk2BA9tVyIllZ67uZhMMFbW6F7thWRVvJ4X3OxKgBjgDfvZPCI63jwSps/I6MYoofzh7mPe1O6ASsjYTGgBzXK/ygI5Mw6T8th38e0Agwa1/lZvZWwMJedTAZ4sc/0hupdjFPj/FZ0rq1T3WqNJia81viUuCIyZEflMj0PHqZgGXDqLJqQHEUzAuc+sq339FFjvcB/+Mtr0DXTaK6TSRrzBvAEuPzZVm2nkI61P/kNpoROGQkvoiRlBgK2Dchn81MGUA716+bGbpoNfquWlgk7QTe0VELN5Sq5RJn6KWFSk75q/B7zjyb9xVG1Kqi6eSmiqiljogBQwQ5hoXiJJdAMIrOsG0dem9/lVNKbgpVHIR14CanGgbFK1ytnAC3ChvVyOnACQQt6DJkCjTKaEtAEqBxSmxIUHbOIMi8GhqXKvkAAyQsbs2BB8UCbkQw9XUfAlU5yWDByIm7SmvFLAbWtTgUHKYZ3QNrb2LdkQY0BWe4VVa4BfXiJlZSlbR7cp/Bm76cGuaF11zXYNQgCs+Yi18cBUmZFMWrzW3I/6iPk5MwH0mo2tNFcYDrAVel6SsQbwP44SXRMdcTyReBCzFV63cZgpTIEHvOZyofxtkLXDkKJRG2yVrUDSdV5hVYBmTWAnjYrxriFPMNELJU8tWXqYi10HEO9nFW5UQyeMk4zP91JysXaH1sHvb3eU9c6xJ9AiMaxS3DuOT9+sCWkPiNklLEvLE8dm/nX4AUyYFBMfeJeSSz6lQMfpfircgfECRqZGeGTGc3hZE+pQl9UrDVPix2Z5ria60iaO2+u25rEvq1QjKPlECbGCsB/cFOlmcvhW9YT/EZiKuHOt+fDJeDKqgKmaCvxrDGgS5GZpAc904I0io4Q3qc0xY3GfiHXk6NBUTjD2BL/Cj0IE7wqRiMgF26JxjeLBwQf0q+k1Nk7mrr4MHb2QzRss6lEDoJeji9VIBxI3ZpYuWubRWyJQJEJwgmW5V0U6cq9tsObkuqnbncIUHFXLbjVaSwpk6p48l2deUtArNtdSOPrABt8yN5DYr1U+9o0UYpNyiu0WRQOxv1r5sDm77w0vrTTNujaYhyxs/1FzT4o+APNm4LVDLunTXxeXLpatruyh7vM6wQfaet5sD8j1QZUQcYtrRC/Yz5+cXF9njooUbUcVwlDF2tqttAGNw22ufcl24o2Z9sBRVLK3nVP0jxaCp0Iz0nW+UCuTqyLyQPjg8wyiiKEVztKPXKlXMVz9lAtbW4pF7HqhAAXqm6zkGXnKqrZF4+n9fCvnEjYmsTNgySDQ0vSPGO3fW5ZrugrXzsD+zzxLVAxFhH5uG5ysY61Y/3a59Fzda1JvAd2MnPHAaIqU1bORiwAhLX8cWETasSQJ+J7Yj4tbJlLkkC+HfN8HIUbnJFlJewm9BDOSYrVbi8renc98GSyH96+YHr2Gfz79OLDe78vApylg93t3d3d073UBmEgy/G7RBTWCXYDrXyt7aO7P+R3Ou3prU4ar9BC3Uz9ZyTJ6/P8qiy4S4Knnhjm2C84KBCKVns8fekXtTNM8tf64SiKDnLxZwBWFdxYVIFl7xSd+fsqFYtKTOOMGkUnfR4Mghjl4LbTgm/k66Q7tqPlCAV7Ql9D+HC1NamvzG1eTihOBuakijJwd2Jy/+SNLTVP2KbYmnRU8YMrDtEbFAHkG66iaQODbQVXm0TRZpUiYxMWCHtzU2bvck7Zt82wlaHBuYUXGr/WBrBKRMMIGKsoXkNO/JCo3S0DikDbLiLdM5EyQl4vQ4adBaWAEVuccvN8LHJMexHUtnEvimR+2Y76imL4Z8oiTIDoUoTC5gyuKy2IfpTovZjCA6WuJhwi812L4hCFs2CYpMWPhjwpCWoRxQJuS+U4rQtlKc5eXNVlWle1tN0OG32TisBIMUTNGJN8VJu1U/DXXtw8lYmi3Aebzc1yOgVtaVn2LI0FgLiyUsoRnGmJ8ruxAXpJeyHbs1mUdHpH19M6maSEGRpovLrAMAEvPNH97t3uIo0lWk6JB30AxdtehhNxrqlavxCglFPc1ZNpW5ZzSeiiH3vRAB3xLMacx9zoCvrbhv8kNgKvt8iK274udOClViNyniCWCRHEL2YjINyC9yBn7QsUU6TbwEJ3fRnZiiwpAwxxtiFG+eFhROgAh9lI0ipPxvJaGSWv8roppHkz9z3D/9rOlrPiy5xg5Df9q7OzN2c/Xr7pv734pw8v3t3Av6GVj98m57i3l3aiQg6ytvADdv0TKWDbWISQxZAHr9dq7aypxQ/UWmrlVvXuWG/3hdFPbtht9Bme2ZvozHrPbiWCO3OSEvPJ5PqscnwU/D+dNUwpw5D9RTjMWzhj042QN2ZkU+ox9ZzEIwTVObJnRhQWVbTN81mR0p83lL2583o/J4SYPC7SH0EkgzluWp6U7lnvs8wFKpyZa2yTBSnQ3AriB8nbBs9EiqZ8hP2My5EDd+cUIcFmGxKI9wqR4VbcD/vmYZZtDQ3XlufwDjQqRo0w5KjRGIe8VdrOmTvCv+Jd/NSd1pa6NRGM0TfHaof2hrErqw5SLpInzJt8ZoTWz0Wf0GxBtsGM/cJ8oRljcucV4G11TwPbrgAwmVNm17blkgsVHQd1p04DkP8TuZEb7YXgJOjgEtqYMvv3zFjClu42mBZ8xLbp8qcpjV00ol2I+mkd6/UGdaWEA1ZnSsgizUu6M7vPQBL77ZwSmmS1u18QUtm8qaWAbxLYjuMI26ELA8UL81zMEPtFZpaoCGn0yRCI/cSawMjUMYmWAoREo2g0iBht4tfErihJLb0SVIdRLEeFuDWSaoQNTW8Uhbatm44UviJ2CLArdZEovixZivdFeMpdNSt62RlQM6EV4oVmLCSLn1cXXcnzFGM7TnJ+mufeeyAd2zlwF9mND4l56PwSzykmISwXT9KvS01Amdmll6hJ0ZyqxGHuhfksWiz1CHKdYA6IY5ABSekyPUw7aByLihTCoUk9wkb7I/xuqxSEiAqbcavKGhwZgz2Yc3iTWENEiQ7iF8G0PxGvmLvTlkrxcjnWESTBkpTSAoO9GZeNiz41UmoDKlm3KYKzvpzln41lD8/qMQC2TZdYHB/gJgpfcomdocNTlpTVea9BRMxGt1fgSc6CzBAFgaD/Er/sruB26a+J1h3t0E/7lDeVsfLAYYBWI85J1iWHDEOMmDwj11K6JyR1CMsnD8dOAGvpsz5Z3qWEd2sbXYkPSf8oyUbaHugtxL8J0yxE+/+arFr7Yl85lkeP/f40uy3ugVd3kP15iWmqbeTGNWvYyz7M4dGHu78zahULPivgXqjvCqpYVPf+OqAoZGsLjpZl39L1OykKQ0gAaVjsO/vyqGG0SnKGKpYPV3V4xluiMGIlEIGlnw7nKZtrPSssrtOdHvsa1bxLKnSV+DtDwn8LacASFAvHWdePKxVvlt8KQWnokdqi29Rs+NUuZ1wIbN/Fy66KDwF4KKzD8tNRNraBpjq2wGizfNtgVvkEI5WEtkr9+JoYON8xySex+J0DJDEDgg8t3Sv/EL2j8OmK6Bv39zXTu7x6C+EhWwbMcTBnMzhv2cUDXUpedD51Y8Q6t9QzFbTV9gNWlDeuSnod+c1HnZ2JjLIgsM/G2bZ/G6S2jnA1jt5cvp6QeUdReocnVIwsqikPwsQexiyohYkyMnr3AK/02c03FIg3xrhOQw81M5Myga8LIyHgS9xjTGgvdHs24Uv7VDyINtMMm0MQlZNJMeqjAdcXI1kUni4kdqNeYKROytKpMovK1fGw0ZoINN3h2dsK1WnX//1f/uc5n7N//5f/xcgvbOK44tDDNkmcJx+A6SFVwfRMBgnjSeZ7KQIgJuL3M4RGO0xGHtTIOzctETw8N7lDU3JPlFg2okOZkoHBWsfn8Qry9DfICQvdIBrSXodt8PWjKGSmhpSO6IqsF08cH5UfzbTF7cN8uZwLrFUlBdy6L1R/BG70SPG6hXoxjUhpX4SEwBHhwq4d1udAmH7VInIZWo5pSMnXu/B7a37+KAoCrmGm2e8d9L4kZ7Jm7QVnCMs95/rgNSv+WsTlsJosp7N0XCsmaUuOJcFG7GBpRV66+i+ipbZxqPJnUcPJh7zkyi5jrCHRlS1PRFCuFRggKdFVWWdTaJbbm9Yj9G6G+B/AwS/GYIyHEt76K+10mfpEjpU571P9rITEYZ4u5m5rfUZkFvpc21tSnUD2hBQhuBoEKh81FmFVa/xPou6hpcElTASw50amghAD2QqCQbXnkm6XxH9gW9WAdErHEGFIa7c1ZrHQKPPRv2K6MmabbUOfOSyITTPHeeArpx8vRZcpaqPW+jy5HeaQxXFLGPUsUT6WO+XjxV8sgF7s0NYj0FLioNOdGjsi2VXdh9yrggh70HiCroVZFIWD9rMcRpd9K+5WmhhzbSWVZczUbmQqsQlOotpPLh1XhOSaR364gbLJtm5tX6AoaLAtrr5qiDjJ7x5HM1CurINDBBjNmcuoqb7GaK+kT6g4G3E8RLXhSMFzmUsEy6spjQzGM+C67TfVqXu85ulUHhrNQUk0yFKgV4Rf3XqM72r/CD0i+O+S/lDZRGbTzyqGdyfcLpjNR3W8iW0BH0LWHs/QhxvwJXDdJeUWUMjb6yqZuHG+hpJKsDvsFvBAf2WtURlhX5fmGMyE0UJU5KoCdSeClHv+T0Vh8zdypCC2bZ4Kwe1P5mPrdUOY0jUdnmHo14ZGtsMiGIrj0xleKQ+zn4wWpDKjMNEg4sbviaQCt+m2oE6PkZ7insbtaSK5wWu7q+lp/3YCnJiIpMYDc/0ausI0Y2G8y2V3zX8xJ5x5ASwNRoCSpVmMIG4tJ157kNVCcBJWEAXEgmvHqAuQtkE0JKr4TS+KctKdT0qFtZhX3/b1jdSSzORcF1/mWOzaEHOHHHE6X+bON/6p9PWDS6DUd8spu1F1fk99p5g3K1Y6h6v8Hwcnj2s7CaYqEUf3vu0Cw7iwyopRXmwQ0omMFoy2cwPS9Cm4tjfYLYPOMQGEUVSEpJ0IgsSUvgDGa3FD0GRjz5/ibVSIy8LT7wIT6piWhfi//8BsfGGGHqTvv//L/0OzDkbuA/dlM6p6wrFPa4wnPOYWtaVzjJS+L4e8FaptXmorWn0iW0xgs2+uAh4zlGUjbK2J2H14Ec1paeBcDx7gpFAw8q7izRxMKtmLFffRBY9pmdzhwJp4AZKJD1hKz56JPT1xxhHzY6lrFHF3DMGuqji/m8rtavZ/l4TyBm2lr28zoKspxRMBuWArCAHvgBsTd1MTGN3qLrekXcFCMo6n+d88YRGHEZ8zd7m0Cet5xm2st5GQi0EpqT5I0rWQ0Ce2vseTTA6loh7ywWJ8XTWOTtzw5Y5LpdrDmOSHRS/CvSQ1jUsD6Uhp/RmlK3VTMd8XTV/sy9amn/bMc7Wusvc2HspvzaOIOKKG5ety3BhsIhOCZR0rwYsviD2lZUGMtC4ndc1uxOiwJkcvu7Y/UcERGq5tBjHYyeeWFwvAEfW76Ad9lSNFm7zvzdgvem0Wtbm4QNEC/YTnbOQ6isQN6xXTcBh44Bvmsk2213VbxfycJDBy7+RlW3HR+mNA6xiFmH7mRaq/DDQIU0XH+KnRgh5hO6g94oFgNmBe0oDuXXYqka6IhRCZvQB+VLB92KPumpJVywD5+8nim2y/t9/7P6WeF9pOa6Od+zraeQawN2EBX90UhGJO6GBhu70YFXZlQ4RN/HOvjSCBUvNP0OYOoXVtJ6wnsGb1FCG0QH6DCN0Gae9t5b/fUVmudupdFBS5T80imUPAyQd0xSNOUKDIixGo1A9erO2V7OkQxhOAdiiQJT3C5jU1NYugUHP12mGShj2dtl6ap9lz+JEVoeyWS6DRh0I5ndlCyqqsLG7IBjnY0KmnKNe6zgcp59e/bNHD43MUQeidC5jostxOX5J4AGtSa/pno6pQTBzDav4QVCT5BUTeTU32TrLR1dfOXcTjYIG5GNZvfB2XhJW4oDVP26txSbXiVeXa0L2Q0S22ZiVXJVJBxIB//gYBlWfI5ooOn20e6tG+sXmr7zBS8kJ5B/YQxSfcquDcshEASxJ87Q0fn09Gk6H2FEytB27dhD7E355z2GrODgbR77Tv2BJUekE9U9tUitwYEXLf/f7OCH+FfwqB02cQcLI9n1XimHxuvpB9aqxhq5NC+YZqohViv+oxGOvog5Drgxywp9aI29XfNqcMTOJfPyV7BC2bRemRWShDlhN2ozonRQC2jmTuzC5YBj9a7WtwFFq4yiA35V8sp0y4yLhy8sG+Tvu98208CVG19qRPVwRwr12lbTcB9jQW9M+oQSjZZoQVQzkJZcVxQHNBHVYM5EICZx3TgOFIEr0i6y7RUTiWwudEa5G9pKB/nEGJ1dOZQ8GB/AC+Uuc4++2+VvsIntgOHeDrqFFFsHtBHwJdQWe5/yTZ04vtGmvJYM227MtaTlr+PrQu+3//v/6fJJHQlCg92OSGwJ14Xk0YUg9x4TJTiyiS0Lb9npc24jvpD2rRC7BOFqvL3aWKBiYubpGNEUW1Jjd1tYRmtOOqImWZ6F7J6XxlksOhfJduFe/hC6t5gnXZt7D3/sEhH9xvrSIBKvollExSSCTQJvgz9lncaTJ76+2IKrYYVxzOjKqW8cBo4EJ7waY+8cF5b4dsVrbFN5CooWHKCcYYkYllmRiZW0prxWFAl6BAURXCwD9Tr+xXHy7x+2/8PHgV0qvJ96JITQNlQOCDU8EOuMyREZ7OC0Z3TLKM0K3B5vGNeJx5dcctS0Z+0Pmb1/0bqBA6PbX6oS8P6OMqbuWfq3Kku1NQR0o162soFCP5rQsyuRc2RplaDd0XxRwIvCkVRNdoRTfMOmC53NO9B6GzhdJSjl4dXtUajw7gieUZqgslYKwQKkxgDyM2lOR5UVF/CbQ9nKGIvaJcLotIluK3FCo/Iw2HkyKHdF+GOCm24szfUc6Y/21gMVUjGPMC8CmYCeAbe2FoRjf4QuUNu26EcIM8431dQtd15+4AH9B8OukSJoT6pGLKt/gCurFUKvA1APPNBF7wDlrvAibFcWQyNtKmrO457xygqO6bmERtXXdBfnBTRO4lK7tQvAQxDddXG0LuQvhTJvuj+DCaj9gAZjw3iwKNyf68xE4JQL2UBo8wlAG/hf66KiHxA0JqekEgNew5lOHaYl91u6Re9A2x1glEOxEt5Jwtr6SoweaxR5XSx2LYeahuNQu1xOaMMZiYagO8iG+kxdGqFdk0NrpBYJk2md7mVlvfInuNhQ4OObuc+VwLbCSFruAbc89qWyXs+y/UdgFvvTlfxCLfalS6Ic2C8R+7Ihhhe9L0D+Fa9LxxUNzKbPBSV+wZ8mz4LsAe2aEJDgFhXHRqzGfK1uae5gSEtU2eGk53tqfCrYPPTQ6JEeT/XMweS/kaABNa0m3tFRV/XJbm/GE03a9Z5Q3BmGh4dc0bnYMYBUt5JslvXomP5/1ryv72v8+hyrs+PTUac+uxtwwv4/YgXrcB5HKgZIh5CMhgXjFvFBv8LkNAnqXtIiXoTro5leUEP6mLW6OWbKvbYOON4WGmcAanXs5ObptGFK5kFNgQ1YTEOFdZEm7HmLp2qhwB28ilg0pi7kSBWVXiiqBkFBSuG5n5T5XCunTaFV4ILhbGWj2Jd4rtBAimSeaJ2dXcayYh5VhmIQRGFQwBaEDzRfMUTDxPH2g/4alVsuSMvGzzuf0L1uZRIMnvmuNtPz7tylYFIOntGHEKwtrZAkXf0IvZfBvELBYldqrUGQQ5WWaMCigiCYJJ6yCip2hiQOGUhqPAauJbVz7UnAOjjJv2nAD8gA5ze9bZdTpmhgvGMJNHMTYugV9ea78tTchSLNBpBgQxn5wWSm+4cGvHyRrPA3uJfPn5YhxW33P7mmLK7lXvy+PwDNlXrhFaqMJAllTYo00J1X844KvQPKArBfCiRKXNafZioLNO3HB8DTDDL51zwj4YOpVZKBLJDM2a6UWVwqkWNmWnXCWEckisubxFAWs0G/ZMSNe83QriTLdBNrsN0DjzP73gsS6Va28WFWsqBEsxiqEdgH8hPJQXUnYOnV+NGDSWs3PbClz0+Az91ut8Ja35bNQpyonZPKuE963D5BIb5kctbYoiFRw4yKxTNUe+2ZdEDACuKPPgljG23Bn7CEhOKjHunZWY5DlSFnO07JbdPuDYjyxYv2phOK4auEm+y5J4gJwOqiSTQ2EzuLx9O479IvaTVPBjPwr3JX2ddOyNhy4XccugX5H/3e/t9vZ3E1Ncn/Bdzs3YibiUMtr9o8LwmFNbseDRAM81l9AGY0Rc4053uFHjEFHInIzJQC9Vm2259nzh9b0gOxzvo6pujULpbEUAQAk64JLjpqrY0kU3p+SMcRM/AniCR++Hetf3LyATLfDDKYEK4CbClSjGvGRIeYXg8iI3SPTtZyzFR9QBzKoOJ/jBZk2gEpnPuOVS1WJ4CHweYC/AwZmUVqkRd2t6hFBOUtlrXUB9YqMaNMI4lv2tUksbniF1OaJgvcJ9BXbaVmk8HwqEIR+CrO7j7PnFzVl8PFs77QE44NJY768hSAdRw615S+s7dWbfrYw36quAPOfKQ1lpv9qjhZh7xO1ju74mgLYXC5C2Y/DvSuYI4b6uKcl/mtKCrmDQXp7kT3Wx5R01WLH5EHJRvIxIwxx/D3ghsL9fw6We8wS2gZ5xvXhAQg05g8gxJFRQ8HJndT4oh+FvwSKDDpg6Ru7R25Fzn3xkzM4hKo10DxUNsleL3fD8IJRGGKphFex9YqluJAglH77AxoZIWRpfgd3oEhgVST2lVU+Tn/64hLSLbbc0MTp/K2IgxSl5Tp1lUMajPlofGm05CbCxnAI6V/z44IDY0uaqNYFK42koDsNEYXLSZlwSUpctUpkjME0BnJmPI7Qlj4gKpJlXM93ZWNmHAQtaBHXgsodJeTezOQBjcBHt0g601ADzi5M/ISXHWnwCLcSL6OT5x25V8YB39soY4kKhY0Xkks5sKjSP29lLCilTiMMyCnGqFFkYndf8YCZ/G7HPvC+aakIyZkUnaKMBG+OsDvLaQ1qg4ldazrIltS8v8sBBkGWIoMWtoPeuI1fGwxr9PH2BLiN+72xLmUQo27y+55yrxcohuHSPddpbfbFgCYk1nKrlI7+gkjAXrvDuFFoiIdQ+cqk2P3zv2UQJwM9k5m9JRDEXiAGAtu0DIA3ChiYi4h4Hiw4ihbtykKPqwT4wC7X1D8SC9thecoAkWsSk9GxTCzAZUoqppMpEhaE11xVwCON5hoX48oNLDH7cPogcMLP6NINkkNTBYekuRHXMO5VGly7NMbecVFCW38odkG1pOLpDh0PoDv5iHpNo7xP19qkFigmJT/g3pBhuJ9V92nYp8uYhWQXgvq5QKUF2P4YppFEA4bT3EwhVczOMAsHehxgA0/ipZDGpd6mQsGHuMGmNTk1MmPGCCBf8NGWhOTGE1IVpMZosd+fdL0uHn1I423chIGdoJyFZAAR7BuVH10CGrvCojbvbQNWGbPhypf03ZZ87/yn/YgcgbZV5tVTBRnuc9zW2ubM2YLY1Xizmpzs79/f3vfuq4kpOI1l35N7vDPA3O0qWXQKfY1BFoZXvRayacPIU38p1adiS+/bAeU3xuOPhUP4UVb+AgcDel5/uTp230EHGDciBoE8sNIYOaDJTZ9gqocE05OZywhBbI1BCKD0qIzIei1LAE4BnyiWzlOMqXcU8SIt0wvVgu5FFSKS58Esom6xnkHgCN9hnwvBvYIRh2k9gmBgEM0uEIICY9yhutLAfXeP3BXMqe123ZyqoSNkcofF1yB+8eT4jqF8WdObKQiFrOBKkC/s2zswhdnQ2EQLeknIBFoU6xNI6MAQuQTaYNjr2chDDhG6U7f+nScLxSOp0lsc2qJwl5dLniiMfsXGsLNuo9l8oQCET1LoIp6utCd7eg1m7wQB44mHhVXsPhlB6bQLyqQ48aGCBS0yRLIpR/mnnCeDhsAyz/LMtXym9ELCNivoyVHxkUUDnodRUZzUUAVdC4GBp9yV7KHklixFPKx0WxVMdXlHsbwvOfGI0hPDG5tsu4qamFl5FMMYcjkciZrbrLFsb3sJ7cphjLFwkilGqBAj3NdirV3nTGEVr23TH/rSDabFL7MIt+jqnLIbTbA6JaF2VwP07VCzJnHv79DvjcQKb6YKIUQHc16QHRukb1jjYDBZAxWpNn7CfOq+XDoZ1SqTpf7jeOb++JnmOY3l9NdWOhYYXrJMvfmFyPn1D+hTpkkb6IZp7YLq2qEK7H9CPAAv0cE+oS64+FCgXrJGAmBSZ2cfzzK/5gRMnFjd0VwzyVGyb+TdQnmCTtVym0KSecC54q6HLhdoF3YtYRkgwTKVSTK6TYp+NkTJKV97knxgtuECvlBjj+Zg1uLxxZz5ijYiKI9EBQjJDUolBD4VVxoI6YY3OEazV1HYFEtuX7unn/xLvOcMMKZbWELwMW2Aooma3eR4xUWB3gI/ApKcRlStdN6Mk/NNOhtBUE3FaoFAAT2Qt53KGFi2CddBm2moUgfA1xWdkiLZnDllrGf+rglpw3HrBMTSn0D7M52NC/nA0rsyFkApsVs2RFbUXVxcjLDHsKcyCDgGeQhxpSXy04PVek53pqDGLTZz5Zlbyt7xGZve6kmla/3OUMThEbe9/SCbNFX0IMXM4Eo92NaDSETvbGcHYTiWqF2cjuAwAfvpW4iJzPl0ZFzvXYt8QO7pwAs6qWXcFIAxedy55WQuBcOWukP4k82qMbaQCtgucTMiuIY9zt8sV1ebNfTGZhNeVCq2F9jPJ3d3akzMYC3Y2LiICL9qllrdupZMiyMXH8QCNblcN+JXlRJpG6+hz6zjvC0C7JnqCoQw7pUif+2PpoT7Czd8mwTcojOCCVFvelEUdigR72lUbETqhbESlRRED0khyiM++uURSwqClyQkikzVtQehrpNAooRqgQCOoAApxBrc3+X3mW6JaGS4gtzmvoKsr67427WNrdSg9ZXvXxyZN1PuLwnzQWyGvi5Z4CvtIIguiniX3FDMeUFmNDYmHedx4SyL2C1To42Iyv11OslE1REoa3gYMtzeh9p8aS4IMBt0nXL2GH6kMfz6P1mPFrukTWfgEjqUCEYcOHB+0eHqiXVUYuMWoNJ9E3Xe4+lc75TgaEggm4np7KT+mGIU51pajlvDA1RutCA0VkbGagk3RF/9w7WjyI5K98MvcxfkO+RLtN7dmle05Lh7ZY7qV4QAf5t3GiPpm5xxUGv1bgnVbQrM0maRM2Shp4PqY38eJBk2/WBdzhhbpOalNSrgubvz3yMmKKeiFMedk0A/vX4cTchpGeM5DehJx2xGlkczTaqxhTULcPEk2J5FQSw1gd+Yrfkq2ONYbe5SONBU1kP8CCiFpFTunY+MePuHK70bNul9i9sHovcI1jMevd75pTk9OO+xZw38dnXYOewe9Y/j38WlngazT9oOnvSfw78PTDigx+Of+3mlnf3fvSXf3aXdvP9s/3TucT7NXb27gw4PTzrKe4EPMPyH425zu7AjjQ9ODeACwSTW9qr7boWjDjirZ7eqQRw/n1fu5nMOAe2bA27oousbmup+bn3e+KU/3YEL75gNz2BHlFH1jl38KMel6lk+6wHcG4+2eduB/npx2YBMgk4JvZ960bT6db34xXzDLclcbC3HxAADpJvhGfrq3f/rPuGB2SLOqr+gX0GLXmIJvvJ/gl8FlxC+vHt68i1l+ZRfjoh+Y9zsjKkAuRqVGPxDG9J68rVqb+sGlyuiqISrxadP7+7/7+7+7oYEW2NcD92kbUJqPgJC+MIcfnvCoyb411mc1u3uWKp3mj1DCyvd0HJ9gfKDD3HfJHGx62TusZqPaLeiXdFdJPjU1lIVf2JF6QD5TF3bSgsL33nQ7A3Kah8hjgVV9etqhDPSkop053DV7w/ZYb4/aMn87PiCwt/mvg2d/jxfw7/8u+y++HHnxPGuMLJnmigFLN+bzTwdE0PHKykDW6UK2SYvXJ/YKLGt/RNwQHPunfIOMgffeTpuaesgSMt9yY1cNdp7fwfw/7RQLeGko2EE4YUjB8nxZEtdf3pDIujcOZX2HrwY9Y9NF9TwxeKD5p8yHlrJ1MqrQtL1p4PWy/lw8QNq5+26WXIQjfxGCh9IjeeWp5oNbWELHHWabDZPF/ANpb4VTHKnCx+2WRlZoec2KxnbEhXXtnmNFKsUKumKVhG9xuPItvPfQNiD2FIZiK5trLiNIdJM+PAd85vk/yQT+dv7s24E8fvDs25253jh8PNaw44XLjEZivN24NLZK1AMA7aJtPETDfBaQNBCCBpasc50zOZAtcH5tvlJ32NZ+7N8gyW/dhhhmSZSoWAzty4dZiQ08zIWtzVYbdZlekr1Nj5ID5lrEosuGUIhWR0Z6+lS9w5TQtJyZaQ+kliE5nV1vh556//XE/ld6uvhxYs6MfCGKBi4EbdioGnEOGnf3lAvn/WiQ9L/jH+j3Al4WRMQRtODLQjwl+yPjS00eYtcueG2SxdiCik0eEvOAm8CbaBeNklEtJeao97C1mm0HnMhYwjv+cVn+zHcTf3QtykUURm86ooCfoFwIYSKsqGLPwusRBBhuJemGnrwVi2p4z4mCJmFZL/aPxI4ydQ+wJCT/cG28lSFJ97GqPzVKbBPPBH/l0DskdMV1fh94LZhrBAYwSrEqjeJqOtkWtwElDwpUz2MZZr/luZArk7Pa+pU9HfT4IAQU4CdfkQD20AAsM/GX7Jrz42M6cV/R7hnr9ksPdo/7puARsYPstQyCwWgeiQZiOQofsgtqR9l8DP+HbQ9HAI73FhQF4T2QIkIm96LRjHvAi+lMEoeqI1TBI4iuf0JjieOQ/MN9+0Oos3JltFrjIocxWJFWWGZ42GsJTNnR0pu+D8SfpO8G5jL2YifFmNBkdW3qpew9FS/luLt7lO0dnR4cnh7u+z7K4bHzUeBaoVPyj+KtfPfkyZP93/MkvnOPTzoiu7+5I7LaEQBPxAw2WI5GD+hQbeCHmO8/h+9TIHmVE7Jq5KQLwu8Q2swHT588EZv5KGoox7GQZphTqwwUMlTK6Qf/w8iXvhD4Ri9qbKSEQjaRAEx0aXkjytTp0ug3aRgLGn7UZcn1a4GZQJTLYgLndQkyvsqONH+FmId46tEEYGCgcLGmKKv3El2pvIESXeTap6pBmlSpNTHKH2ODaAUR6hre509XdUVNb/EOp6YVJqk+xEmythn+UNSDqimYoIXoNgrsns4ZjnDTcaA/mf9/7rCc8cvoBFncQD5mRvN/MS1QvNFiYflWlRE5yrRslk0yEBz3U/LMJivkkuRrc17iIIYlb7OcS1tYrCA15shnyLcQFhItLyJoS3Vhj0+ddL4F4pTUeyQZA/FcYJaYsMiNjeTKmceL7R88Y8kWnOxBQjyNdKF5D+Z9o3PD35EIMHZeoxcnJKGzCQT8FIEoCGIxK1U5zqtBhTXYpJrX/ljzPiNVA/vS5rVramuB1Rqk/sKjqfphMn9AdXsLskCd8NSChwLGYsbspdFnzj7u+ZW7m/r0vsTVw1piKfIKkqirRlKYMTHdh5TRDhev/Tjz/fFD3iqCoDPnPDkwzavlYr6Makti5AabpQ3M20OA2A8cXgeqCEcV1BS7r9k68FP3t9fRe1jKDuE0U0ozvKfsScL6ObY3vqzsGbXms95qjgg6NosqmVK24BKbJlZbUlWLIpWVld5LpeN7Rx+uxQRI1eLqGXLKgjYMSAdA5Ni0ODIZPvcaC+vym2FdzUHVmcsUS6osgnk9t7ZHJqcTA+ctjS3e0KmjmDlTKIvmkzw7VjMo7QMrIz+g39Ntkec5QoRc0SH4YI+aMbMzvPCSC05AOWKMMBsw5hzv9Y5T/eBWH9GyYVuB0OdVNl0OFf3EH65pxf3aGjnO+UrM+3W5KO6h86xdQs7vIsE+wxiec3MDLvZeM1XYNE0MYekswi1shMNITxulsWp6zUQPCpAVFPLaiRsxVuTTRrqzs4FRQJ9TwOtCrKAsbEMg840hx5KMEK2qOyVdCxDGvexjQUEpXPZygeBZDufkZfQjiOJk0PZi0XrnzHSXU+7mR/UESBKE2YfZAh8o6elJVRFbOQJGMMIxpUgaNXUruJCRAFPmxSfEI1wIVXuZRBm1lrDGR/PMyyfu6AIgQkiiV2c0mBG9em+v6FM6Rf7eMle9jvG1CFsNhojyjN6cj/ScX0epRH8CTPvCR8CKV9tbnqWHre/PG0WMFICGp5BwBaolUIRQ/jcz3tud+OOW9kdjCNpWv6FXiWDxe704ycpIQt3eRLlHa434uH7TzcFtBcz1CZ60j1fZ22KBssSnnqF5xHfAy1ogIxCRNjvvjZtV0LVNAKxhki1bHk4ykRmNhO/zFqfPxSc2+m4y44od64cShexl6WlGNAu7iX19UXw2bvQcpCIryG2WpHTzpYkJp5ZLS75quXWLYkQSqpgAuul/W5sHRif9K/PAJ93dJ939J9nT06PjPJUH3t84D9wScujhtP7GaeBVEZBfzFBm7TAQ2xid/+mhixwRyagLfBMN5mv8ZvaavunFW1pGS0Zanjw1P7QpUOxyC3IHZT22KOQm5si1hjob9N42ONa3FeSIBToqECX0eR41TAYBP0ekXM/PJTXzHPomPUyK7zrDalLVp6DSv+lQ1B1sxxkHxDkM08suLYql4VITo7W+LabPaD08RXj+5rrz7Y75DCyuegnpD3giPhv1xzZxPGz7PL05w/AYBbfAJsiwHGg0iBltbMcG08EN2pENGXeSTq6oU/Xli1721hhS+YIIqfOFTdYK41tDTVHuKmIIvnxhe7vXxVSIi6DSbpu5rG4feLGnYpea96TsukurCwsdV2fpHvQvnssko1cxwgAR8rmeXq57ImyTiYO1+PQlGhtFgpQmW34TnNVHYcFDx4Fm4CXQHy3vJg+PrC1Cv8y+r+4BH8kEzFAPj+XPOMgDlKaCXNomloX7ajkZ2dbbnDZZzrvQFwTsLDeZH808wNoSNlGg59/OxmVRA8rpQTI5MQh2NlIr6W8QRsi9jM23eTaui9vvOjv/CGvYL0ffHT097DzTt5XI9u6WJai7/FkSHbD35OmBRDoPbU7FBwfEqfWukU75rGw4KI/3AmlUSuHTpDoUyIamukrQdxfF1DaitDle+A/sDJTIW0oLvFXoBZtv8rOmmuJ0TNf5bGC+uVwUr4kUGi4xktj5KdhLIDKbUhcGCxNtgsxLYqb7/8EAC77dvEHi928GCThkE2KzZLQul0RMHp53vEuqTlU1pFVy1nJnyCxw0DPjB8O9zzml73rhsYQrfT4BAMpN4FY6NnELu0DAnncHfY4+CS0KT7B6L6/F5/ub15aEyLLSKgq0gIcMN2rG5u19OSrCwace8zIeqWHF70sKVPjPiCcaTzqIP4EyJbP2fp5+74n/nye/QR6YcT+pRPBBAItJfcclbf1sMSNCXrirhqFXAfWz7z8Bq5fDyBT0A4olavBWzsqpcYFtASIfSZsE39skk7w+lSzIiMR3fLCEV2j6hU4p2gzM1jPrMtmnSnb6AAubTSbcv49RiJ+/13ti1IWX8k9/aV1Kf693Yr9Du0SShwpl5TvH9JsNARE+KuADF9P7NBJm5ShyhqmAI/ubto0DQeUl+Fk++iU5nn9MXph4VS2ui9G6vyo5fJztHp/u754ePgmSw0crk8PHu093VXKYn/63yQ23+Avgl+zzJ8Np053ln9MuyT67JMYazd7Cl3xvJBoj6Ygcw4RF15wjprTRIQ9Bem0z1OuB7TkIvuF5AGAJEkL8R3E1vh2x+TNaPHtuuxbCwYRuC+aP9OHoGUDlAQ2Bn3gGta7jMl+U0a6JLkgtjzfemUTHMP0kCwV1gjws0w3hizb4cM06ilEqhE9B/qAo78aDqra8nv5MXtTVHDvwcUgqeit63KJ4BKJv3p1AFELcC7Ixy1oTpPWoh7c3rESgGrRAu2PjLthjwNYsGhLUo0VQuUyMN4RaBpm0+Z/JsyQadxdulDUS0/bsfyCDe3/fzdDasMlJxhAHBDy+YCtGMccYGeeVvChI7bqVsIQfdQVdEV2PTmYEmnU9QOb51Q3d2XHe9MH9AriFhNTT77v3H9Ru39/YSP4bGu6eBXjoW4CHDmu+OXLzf/zvYKN3nyHzHHjj/wMFJ8Ldu8hBQKl2ZpUCn5zb8QlxdgRbrYYgb2waEQ8lmp/AytgRZlOkSFnOACCwzSQ0VFyKq2hkb/ft2Q/gGjTYxScbLUlSga/A7QbjxysoBcqcR67W23KYAgZtYeGl/uoer8OHEtSXst1/LUDnocWdf60l//cq9879mRoE7r15uLw+e2NcDjMVIc2z7M8gjoaTvlHifaN2+sN8CNVLFkLrr9DhOvfgUGz9Fe7BoYWEy3/u/XXdg8MVZr2b1NN278CO82S1d3DI5nvStj9cabYf2mesN9wPf2PD/fBXGO6HGxju6dOyDzBTz7RXoljKoK1lf6IAqVJeTQh3GxSS4uoWOCio/b/I4j/a+yo4qLH4jz2Lnx7/NzL5I6P8F3zZfHg7JbjnQWDlH5hPz6SpGZdBvySU0wropxsyafTvmWXroCB9dDdZFo9EDlIpc7ngAnqPTFUdETHz4KSWI1KSQnyKHgSDzNPzTsZdD45PyA7cVQUOuz0XNLGaBB8jhQXNw3CGNcENVsoCW9uSq0A05yaJU+EjMdZP1oHWrB3XDVzpml0F2eenXro6L23RDSfAY6JSst4oAgyLKzGmRBYDOBxVNm80AWlzxB2lBmwZzqlV9XvbTwz7ktzqZROoqEv9x7r6y6S8vSVVSXhDnKISVPFPpvkXlPKzgtoRkM+hqnR0P08qQi4YsRe/62H6Xb2oYI2lyFaboiGCvUi4gbOLjG6ZfeqSvV/XlEjgnzpetcfxG/Gq2Sp7FNDcId7h9RKb9l/SHsArblvZ8s4H4Tvn0Qhn5y8B2yQhef5/8F0Ozm49tvzwWKyzwNSitOyRLFk5QpiJLr/w+y3zBIi7Q1Ntrtu3/fAd3lB5ETCYYLCVDpZ9J+zjDjIlXVFKnrNNshnPmZqMgOXkjmhD/ZnA6OBsaBPwZm8zYR4+q29Wsf+n15cvX/avP1xdvXt/YxsJLwtquccN99CEgyckhER0uYXvUpdrq+NkCcO1rexxJbkbDH4oxOVPsyv41jvctW1ynyCOvp29L1ghj8v5dnbDBK3wd9ofPd9Y8RqFtUvK9eu07pPu3mG2++R09+R0//irtO7e7tOToydPreLlx/9ttK7Whk9I88yKCSX6Wd0OikUO/30E/x1pp0PRTU+7A9z2ZPASP+ZP+Bkw2f321TzYBRvG/OXoJFjNJ1+5mvjkX36B2OERmxhm9bqgaupFOnx4xOFDAIRd8vf8CGJqpKQ9cWI2n4aI69H5FluDtWfOqO0Nqh7OvFcwpWRq9ukBR4qgxckTZXb61aDo3OGAiO5DjL386kQqXUk03xLNg2vLJe0nocmblzNDfYKuGE11C5YlL7snT7Yz+efeY6iZlUdFJkSsDdwsLQCCELXuyb1YJ4GZ79EVKaEED9ZGgh8moiYnxJIn/9u/H04nfWsI9KvbvqNcgiZwn4qHgL80+dQWdY1wlno5X7DVUy/SLa2SBM6RBQQPinSkWxXqbERYbuiAYcQwnwEMYqaX8i3TZ6JWZ7qFn8FuZK/WKI3SdlIKIkofZuWfjcL4pwJLBvi/mAG8FBVJpHipV9lXZzh9eokLkwKuHmMPNrnACisqGRH17sjL+Bl7Lc+QsncJA/k3MR8AzZiMQb75FXkIi2qBdIt1YXvVIhO6WFp7vd2kbMRL+/UK52l370lmRNfR/ulhKCJPVorIEwgt71kJaZ//N/LzUqLzF0Lb4d+hpmk4Np90ze0yj0mK6AOzWG/MF8/hi4Hbv6rur/0haW8QXgy9wX//l/8J/uC//8v/+g08wraZsymXdAN390TTJq46Ffvd2qaMnqqh1EvCTrM2Kf/X3ioFbp76a6yifaPHn54eHp7uh9nHNXr88HB/f1/rcZ7A36g4tf2gkClhvu/R/PWnbCah2bS3C0+al+Qc8p/L0+OTvSfm33iYId4xMg5Xn6vN1FzwitBf+4zHsJ/S081vuWWMfcDu6T/zJ9O8/oTyvT+c+5+IVjFvhZ/s78pkKYtYjMqcDpu8vfzlK2It2qTciBnoTr5s/t3We4u/Cjp5hYXWZpYl0sGpHDBsejqdSheS+43Xq2GvNnU9EZwtfy1M3nhfxonuy5hpl0//YDHFbTnQG9VVmpAhx+CmpXcUTjDu0w6t2PBWvqs3EORsa5F28Ay1lQAtaCOZouepT9XvZX8P4o3cCf7bvll6z/cP/P0N1yOx/YdmjSAxHCOi4Vs79iMZIzwR+08SoIXguf7hODi0v4DDsWL//HNyIE9SP+hO7Tlp+S0cmV9wmcd5M8ZxzCqc7J8cFYX5v3tPBodHu8dPivzk9uD4yeD4yf7BKD8BkYNjLKpqYoxj7RpCXMmFZs1gJaqFwz377S7zrZP0FEx8d0TQfznUSrEfgsKl32bd7Ip7w/hqMvi1kmxdGebEjuJrHVBNonV4itCsQ8QP7HHfGPZGnNvV7PNPYbMSP91hQkM8H82OTIjdmT44ML6aEY4lWLgT0AoHR0/3jnfpv9Qq7j2x50uQpcYHCJZrX2R3NzsPv7aSxQDdP3m144P0q1G0B8yXHSrUbnaMXt/f2T2Et4aNBG7zvd58JqF1TFtiwcIeGQvUQRidfXO4jPqjNWBF5BRmy8smwv7wVbas1r/wHgiwG0TN/0N2hnPOwDawNJQ2BTWclJhYygCWLTVpUhvKjHIeCp9aBeI/pb0SZIO59ofWx2v12eM1Qm5Za5Oc+IZEHwaHD4+OgRIRrZro8z5QogTbH69GODJUxYuWuVYfCPIvQ4vlQURZ/FQITnzOJ3zt+Cy5u3XUcor+EXABiwrbRHx3cLR3fHSEAY3B8qE/q+6/2+OLYQ5KcZ9T8IlsFvPH5dyon1Gh/mhmz0KlQ7YULaRZWNwXqZaBS4h/6NJWoOwzB9+W6LgRwabFvj3egPtKimGRfFF3ERZkn3AUfQHts1/MWX+y9/TJUXSlzdQn+UO1XDTBNTYfvHYfpLBoZm1fFE15N8vAia4LLoUSnm6Mcc/gBGejOr/rmqPYBcgSxRlu82HRS178Fpm24uLzC6y4+Qeb3nxYfViso73D3Vj+HYMZQjvnrZX5+5n9e2Kp4Bie8y3EKhfXGg7KHBqCntH1rGaufiO9QH8Fybj79etz8PRptD5mSYxYGgWrY56EhnbL4pw8BUgjoAERa8ScdZixs9X9RooxUMU6r5iMwJrQLuQSBHaHAyWXbe+rlw3eZcWi7f+aRTuJFs2MCVU74Zkyf/5B/pxatUN7+xaFEbO245Yg72WdINs7Kb4QYgqyaSBaIbfD6UYIrKXP2f7XLNjejlkzfI/fQgHbFds93o+voZOr4YrdyJ9TeheS1h8JM1ZKuyZOlUKWqi6GBUDs+B5yMxq4iqidoQfV0rgGzbbqQU66FpYdv8Ot1py+xiS3UtqANwW6cukfCP7GcoZ18y7WvJz+VvtByqZ1P578iu3AaEGkQhbOsPVViLJ4U1tysgf0WHDrWYXcFwPSIni9ES5qbWwqJEkd34xK+8z2jKl3Gla4N7Q92xneo+3s/P3Fi222uLazN4SH4rb225lY+M+ragHnYg4YW6bFYkX4G4jjk53dY/kabkyv+Rzty+FRuDG78Z5g7gtyXTKamdNs2GKfHjoP5oy/tsoY1xbhvpHzK2zCk6P9J2ttwpbnt1mD4BH9WOQ1NApSn29qBZqHHZ0cBAHooxazwrME93afPDV20m9nCsJOpG1BdC66LPgp7ahNMeOCsS2NSgj1nFFzXWrSiockaVAaI51XVsc67GOOv6GQIw4qH+M2ymh8mKb5vBERsa//jreGHXucx1PeMPkGliFCSmXUWNMCIh/2mHLIrmtMewyMsF4AX3damuVcFF3RaF21Hqhu4UG3QEYCCIeuMFXXfzND2n9VNGSJvyd97Z66Y3+pv7vp3ds7XHf3TtbfvRWT+KtewKe/+gLu/ucF/M8L2HYBYUtsJM/YBdLwJXkDETnJh//K+/KmV/B43Q3cLCLSOom/0hWMIyEbX7+Tp7/d9Tv8i27ff4DDdoBGFxQKjKu66+9ESuKbdb+qq+wMv/8VRtZqQX90/HQDIyt+9t/scG0SZjvZO3yy+5+21X+K9pW21aY3DUJuX3nJ9lZ7MuaSrbem/Mf+Ne2nw91fd8f2nh78ujuGSY81Au9kI3kHrMP7VJumtmKTTcIX3ntyklzdQBr+RuIHHmMDOn3MNxQj8rl/+U+Z8//jMgehK+GDEAsT77ve8xRCV2WJb1R0UhX5LFZEJ4/2YLugr4vQjOmxskkBdGEQpoQK9YgSZjvoSan5uRwylgvbg4haLzvzcjXS1QmeSY+UBCJUjD0sxlhGAeE5qKdBqqueqoqdY1F/+Zn4iL7rAMnoyJzYaVF1hGEApGk3n5R3s9NsCPRE9TedZ9+WeBKyph5+Z68z0mrVPfw53mkArFU7eyfHh/tPjk72OgAlW4y/6xwbeZmNoQ5/8V3n4Nj8B442wKDadx3z3/fF4FO5wDpzqBglgutsWv0c/in4b/NONDPzD/Ne8Hrjw2fn795cvb64uchevDv/8Obi7c3ZzeW7t9vZ1buPF+9ffkAm6e3s+hK+lb36cJm9fPc+e/vubff83YuL99dSPTx/RjUSTI6JVcuQ9CxqsxlDhr2OhJVwW30VostzIHcuIQ1MpwQSrnUxLmYNBLITNFvH+x2oePiuM6uImK7zjE+MmS7ybPEGzp/ZGPZtifWhi7wEih1k5MIsVWL4w72T3b2T6BHceTJxatVx5XP6IKfUpnZgWirmfsozRKQhAA1T8zh6erh3EM3j3Nbp2owJchqZP00KPb0GnqnKgNsesbfpI8JXz7ief8MH7R63P4iHRro0ZljfdNSn6VELtRdFPU3OlTgPzDHBPnMbnwRoweIfMyXukFDZPHI4nkHzVSMslre3cByA9nn2j65f3Y0UfdsHp/Az4bNZoMLjpamYE3EDTEIQYGGhRaTKRkDmQaiuMRVhRaW8DsiFD9c3795kLy8vXr+4xkt//u7tjZEP29nN2Z/evX335sfs5uL9m+vs7O2L7MO1JwusrI9Evep2gwc025oWORX8K6wusTU64hD+sS3sgOEfb/vby53WIQ1NiSuhQXE9iAhcYjbCT1vb46Ze/+z8/OL6Gt/5/bvX+P60FO4lPyA+IMUBuHu4++Twv7LpE24f5W1IGDyk8DA6v25rAeM0O1hc1LnHGFGcObrPH7YtBQyAqImhQNcKahYHuNBFDVQGNC6RLmDbLDTocErwLaoQobW1J/6l+aD4ksNJY/ZGdwBzZvOHQjHH4Ij/sumv4LWJqVH9DCs56aigwgYtbbeYfs8ckGWtfyeyWL2q2tj3F68vz54bXcZFeOrQUlpTF8tw3m1OtA8/VZztXHVddzDx2exE+67kgU6RohRxiVZJ3sp3iPgcNgsNFfNwq0SB0LKYYHsMWPhRMSmhwccdiD6+7kDPV3LuzxYctV3y8Pra7KNekRyZ7RxLhb7VvUijSX9PVEwA2iyw45Syspxs5z8hPnizr3788fry44+vnDzdciLkh7IBgPEFuiWPEz8+5zYf6+eCzTyyc9sWZNUv3mMbEbxpq77miJSukRx81XdfQuUMZV5Xfe1yioTidimCPcyzuhoA5yvmfLtmjcqf+VzkwzFxDwIRSGqpXgBbhRtZjfmHa6h/LrrzcvipSC7zxRT5r9rn/Ra7oqz6xtXY3PxVX7j+ZN511RewTXT7x2fGHqtWfeEHsNVXfeECfAPsKQ4Q8FXfPAdOMlqule9cYQdmFPpDvfgESqPNvcK0erZjzmaJHSBdcW6TOZDt48jWYSPb08jUr7dwFAa+Ou5CDQYxwGyDZuEmBkPsIB5yGVlytgzUPNdQO3LEiVFSRHdP5A3UqlNe3TUKF4oDacobya2rd9c32c2PVxdkhLBdcnnRbolAPREEs4oFNEQK7Xhtv5eFMiMYowr989S3bf2gNT3YpuJajkYanIB6Y86ygnSZ0U5AORRJzJdgDYoFgP2a6OsBUxdUdjZNRS6Tcoxx9dt4VplSosBQxsh/in8WBHoFAt+ZHvGAfKCAZPHD5esXmdGt6D2evTYO482KbeCy+Lkc4GF0gM2JWNyDE5t6Pm6Mcx+9TQXOhUUFgSBieYZ/2D/4VwQ+JnNlXt0XtbF7SG2qg/bmw+uby9eXb199MO/0/uLsxY+RlmyYugsqeaPYhTotDGQGlj7bkgee7hcvd7GnNHL1wRYgyvmWLADcXM/0UOVdkbnxEfvpsG3h1kpZXK6izsVDNCbM9YbkVp3sM4RAJrHJuhbRCPVy6PUymeFcnZIbtKWvb86em3W9SS2nORVoWwB39BQ21vybQ1h6eojNhmezP7RtLaMRb3yJLF3UFy5at/v7npnhuLirmocGmfdSvta7mfHRXlXX9BVaz3dLEWoNHRy6Qke7u9u7u7sZRy+BkwFP0zYDidWyNdSmhVw0YHXPPmJ97D10ajHGn7Tz3Ia/fAJKDeR2NKKZeoVta0oUGF96iFE3F2ZAgnLdIYDLjM3Zw2quSs0cWqKPjZ1yNw6iLQsiUtnO/ngWjGdE90NTAtn/LdDSkWSSbhW0ywws88sr93ePmcoAeKA0g0VmO/nCuRnU1T24Y2KRMHMF91d5z+Th53zKQCEZeyyqvTxIPOH78m7c/fOS6J9/rHJzKa8v3iGA+85vmSz85UoS4T3B1aYy6C5zPGIxaFEDzeO//atHPHoLZ/8eY3inGeCvjfV/X3miyZdErnOcdAEQlkwbaEGWwlxiPK1SDRyxWujn/BFyqHnPkQEVAT9oq5gdtN8Q1LG3FOfiehurv5mV87lzkfw1u7LdS9xkoAudTy2mf6Hb9lHb+axD3dy+q/N7uIvQMaFAksF6ATNIDoAWbTDMsp58B7wqHaM/HJUMM2uT8cs/DIa8CZQpKvTFoi4HS3NXO8Cf2l+WSOEEMvQ2nzTcZskxHBGroO2lIAdYAuEvVLQlehk8CRDlNRvFL0P+B3AjwnvM6xJp8Emo6PXyh6NTfiZ3Vh11XAN2yV4b+bhNNZn8b/QjtoG2eptoYN6LFcotArGPD3leOqzS+jLvX74iatOsMbtI3IvmZWaPQKBVNe8JfUMwsTQulMCF9xLNJruUXHVkDS1eae8Mos3qD2NECNmiDegW1flQ2o0xHyRoPBE7VxitevXh0h9KN7uGN2XdD7WlHi43o6ySsXW4xNIbRdYTWOKRsskctplxi+7C1yeWU2wgIjkmIoeH/cG2sSDhUB9RzCb+WnqnHowUCD8gvwDuIBiLtGK4JOY9HXsmMHiOuH+Yef0aW4bxjJjZFn5gGyMaMTocC28vDZqaERpFg4dFoRtDVzOPbAxSUkZFgi6agXiwV411Q1YGd+wMBTd0twEOTKI9MaqKPBwUkkN3NGxnT34a9QQwr7KTXb1/mZqzbRag2lvDdHRvTfUAI36BHNSXoqwL/cYZ/FoQzfAGwwsG3KyLBTB0IuWzeZ3LqxtjWlK/NFIrOfGpGfk8X9ZzaCyqOOjoyS8K8HKaQKCSDEZzsJzGhFQHlhVHU1JJJ1LsG8M9aOHneN2IKwqN0JeYieGbx+9oyVy5c2tIRiM1WeRxktmjuOhEMlBFPLJ0JqZ8ElkItIHfwil6hvKsT2/eZ8Yx/EA/CEk20T6C6IOLgEWHwrj7cMTg4PhKnyhXhSgaPofznBqiWQ4sd3Sjwm5sl99iM2qj7LvlTHsceDPgymk65qQGxdfCwzTNR9jeULEFuuCsE/fuJoobyab+qrFroICgMignK+kSY1tFkvjE74uxRmhNJa9M0sX8IPUQOv9I3UYEPEK6mHuyCEILixJQw9IGGetnUKimhqV7L7faMiXiyGblK6b4rkjHbboipLIzY2oNlXCjJQItT2KVWPgGQGQKidSS2A3RFlgleu4m2AuW+Hbk7vl+KWgHbK+0esvwlHNfb00Yp+uNOO5vVABdS33Aow4+JBOInJb0O8o/pgJyiSomibJhRuzxDgELqwdaJ91YIc02tb0C3CxyxI2vC8gFrnl3EgakMtBkhFnhKZpWCzlBIhnYVmM/Dc0aMOO71EWYTU4r5CCpWtVgfIuvY7uGr5mUb7sGL4lnvxg5jm1L0UQiHXuZ0nfShoVk52o5LfYwQ8BfWPhHlVLktEpNDjSEP2OBvywJuUywl+usdl5b9Zp8Ymnwj1d9zj32X1y+5+GTchKD6fZ00Y3gXtxaDgpTLIYQOKohSCY+0TgSHWqUqKWORDnCq4RuOY50S0zCxosGzgTAbmTFbH9aeFyYWll1KqRnuUhoESXg4d+zxCVLAukwjbvKC/vmdf/8+4vzf3r+7k/964vXF+c3Fy94Nv/2r2bvQXYu1jycAjfORwsOpXmpxllmwCBkVjm5ci1Ms+Cv4tPwHlnjwUtq+o8U2lDaZEV7I3VnCYY7+xT8UWl0/xfl+Ak1pntNyksy41HKEdLWtRKfZiTvmKI2Dc5qcKvFkUyMTJx8lKtky8erB8XDjLFJV+GJVYTmQoP9UjMpW7FyRWAQxw+H94FUIyYFyBOFzfVdnuQeH7ZYXmtNEHAzfNJj/fNWe5DsQDkAqRm1cPsNbb5P1BvqvtCxzLrPss6fl+XwExqtHW8dz6RlJsBFyNOAJDY3s/6MJ7pBh/4W/gmNMjCg6WIQZOElNqcpQFgs1EStXTGmZ0JncfNJIUGRxCin+Eiwydmjg/aL8FKayngh1umiPYKVmGDgSM2Q2Aw1c+qotW/gT39cQpgRcpCUNrPNU1oCgtkWUBTM4IZdsNnUYVPjsfdoIGG8fRBykowwnsxKDIsxKwppkyuxZDnok+IuH65dAnuBiIPY9iGgJDH716xyUVJDdp24yegmWJtXpoS+9jZEXmfgcZfo/PEfsUuIWDsM3vAmJQSSLiTACDWEdy1ce4TEJdlvuba+jOCLoq4OGZEQzwQ9B1cosXRJ7ebL40S41MpHNhRHoskp0lKMPArlTYfGdkhWf2plIr3J4SWw/nBWwAmbT8zdrrO9vfVPaozdDWgjsALsI1yYEFoJklX34f1rWlASrdfXr1ODkw4fL6YTsR8oUmjuM31EAkE+dMrMPCjQ2e3Dw1uD3Ie87V82Up3fJwdY53NL78b0rlBfC6P6inyGTNrJELPYdi6agR1W5JhuZzVeSUp3OOUs53deYDpntQPClk3bM9aMyPZRWYfSuv04zZZTjOgYj3ZShDz+bTbEujCL9zgvBlWGIc1t52LzBfQTEWan5nXxuayWjbk1Sb9Dn4/7+YI2vs8rYA9AOfH84+SlJD5YOowgsJKvk1IykmdOW5Z4JFqisV6Yy/wTQ9G4JMaZEn84OSi+Sj7BVC/2hUCUHoU6OIQTN8BteZYlHLb/aHusuG/c/WowqdA6EGexJnNwOaOAru0/lJwGnhzs9xqEZCccqEA5SvAiRHfAE81WzUjO5QilMDNMKps4f2c28JSZllsOY/gaQagN70uONpeCEYSH5NQIdjzKjE1lhXJlbjQaQQWGTYz2XCwb8kaa5WRB4Smyvvt35v9zm+M+DrL1mAyWls/Nx4MH6RC3Lf2nObuIYoL6gANWDKx7cB/w+QRk5bsxiMMGp9mZsTYHzRDaWkPjH0oMaq9SRAVICUzHsOWIGyQbCZ4ogB/zrEMP6zjYLYalbFADiZ4SS6oCM7eEvYd2VZBNHVLDW5aV0/2p7hCNy6AafNB5JscMYiCJR12z/UHv6EmjYV6PSu7/Oymn5UIxs2BY3Uv5tsjE0+w5tbzzZDwGdBLOIWHOGRwh4l9MN68FIzaLyWfcRK3m1l6puxH6KueMIvC4secEHlMX4NTr7yZ9ZdAz0Ynk+Ie+AYnWCNxcW5IzmHBhgLfYUD7I6XLYSc4Aa9CC7yaRRBvBSTVf52KrlUPC8rEImaCHdgtLgGT2s+6oMM/wveW35rvvI0PvNEuuH1S54PXjSKQRa+2LsnqEsYRfPN6vGWUXPTmPgz03T8Toj/m/oNgE12GjVdFLQYZdLCRqSEJLQfC/QAyauwxlONpOo8OlBtWHw0vVuCRpr+37uT4cLrxxGiSSg0MlL6mBAhDFwBt75zEic9pLKxl/Nq8rEsfUDGyArX8LgeelHuT9+n1um3vCOgm7PDtu0hnmCBqVRcz6B3ETGiOAuS5VjqxzvcAUMd40jTjnpB+eavBKpTkTOY4YgiTHXI1nbR3bdVMXmVqwfjzPqIXMc5Rk1Il2bJ8x8nLHrsEpLMTowc6E4+6Yg6rAtNWTgHWPZiZsoIm5RaEYICOjF5XNIBvGVg98NhoFDyh8JBy4AIjLGPevxAyGYXOwhaHpEP6K42e0zvecTb2rONyjwpuKF9c/MsmUvGtD2svOOb5wP67MgXywcTp8BcL8VJnUbbpd/Qe4wkvsemD7Q2JyXedh+QaB/2AbcToUSpY3n0iXwnNzf2v0lspebjVVDdhUXA0wUh8nNmnfNsxtCeJ5eVgdjUwP5oUWbOuQ2gUkRMfOCWDEsiRAGZF3wKeiMSIjL2e4w8hcJyu4nJt7J0qsHRlqFb/XVdcFZ2iBgAN0LhEh6oO4b6P8W8rWlh2pbs3FLnFL+Wvo0z3c5w+PxdacFbmxFZYA/wtnwSG7DxJu7ZPjxyagQGcQ/gAQbkgmZf97CUGhu6U+t8F4L4B5G/uxOVebrwXrixOzTcmta+k0tyKa97zIIQTCzXjXReGLL+bkoGBqJnkzloYpFBX587JaEAibNA0lUtI3U04jpD4hzk7pUQpXtf1GtRI2B/DV63fP+8/fn51fJFciMsyC11nYYi5218WR9QQYb2UncoX9sHJqqSS3Aia9JLFs0hLzDwy2RgMa47MTzFJevU1HZP3IVRivQJrDBUSofTiTtyrSmDjVPNqLG9KGHkDfxLwGDkmXS2LfwgOcpR8WwVBST+IKXsnWVi56vsMxoB0vArR2Zdg6QaHEIHdOum9RSMIltg97T3r7aYm6l+g42RpetYlZqafg4xyHcuHgUrYHjlsi0SMhYfwpl3S5CQPFUw3XAa5WOmzvcrzxfuYBTBAyHByqv5Gi4vRqHLefG3Jldfjrs3HqRrnYPKTgJVE7Msd2ikDsuxnlX8ynVVoSrhJbKEPGxQlVtYQ/t93VZ4yI58Yi7OdxRILgZiKJBIZNoSCO0pGyBMgr13Oq7tr5LZQBZNiahAKVixzhJ1KykFhHvxdoaiXx8Qy+uZHqWLB3iybIJPomIBUpUlfetof/H/foOMno4me6oZU57fRk9NAdnXcyeOhaoFkUh1dd7jklLG1mBKflC4BhfZb7eGq92TPRrN8bCAHzzNfzdQ2snmZ7e9AIKOpTfNLCMeNaFR8d2dZA9vm/sjXQ3jdf2zKRuUV+CZho1hCV/KCIp4WoxPt9mrb3yXEwhLEqgLGi8dm50RwARxgDYY6bGltUQS2zOcpQDQH111R+Pa6qpuhBM/hvi+mzSOx9u2P+arP+FtVvg6bwI5yP/8UJhdtu2fFNFVvsPz04OZKOhce2IXfCRoPzjNgMDqmq0BuBc7UsRCl4QT0QKoj8w3+TzftFtE3qMsIM2qxET4/asTlO2fm+gG72HVyOzsuqosijJaMYIWW4AHnQPYDFG5BhCRCsOWDGOE+Qnlm6WSIk6a15QLFYW6tTYbda55Lc1zm5AN/fvHmdFfQORpyjDZbjkUq6oOB+vK6quc3ZQ92IuAAT+ID7AYLjQgfTTgZxXepnI6onQvXNaUaEBlR10+qpSiTERZBpshCLzoWDJUhdMQyEUs5U/YBAt+znogZkgAIcYCZjvekU3ArWCFbjgQM7nxvvGc8kReT9bbeHZjPzeGJElEhp3kBaW2fyUD83VEtWtyJzB0CmWSZs9DBKQvOT2AYTVB6utAQXB8UQkj9QnU8dK6VvNeaCR2TTmJNBYzhnEmPva1A7XAh0fn2N1+gP11RWFy78Nu7n4IEcUMqSRJtDBF/bZEnytmAwm5OA3vWDL+HEMfZQTujUhWNuNvsOJ0c6ci3xHTjISEBODq5vqzQnsD+gPT9YLhYVVA6K5pWqor/y09mLAMEaz4PAvTYN4kV5MNsCIUwJ/yCvlIAzGSK29gwOKFGhvQTIG3F0la10PFkN8Dno3Vs/+MslCkH24rdZX0yW01lDNFYwfw7U4Uo6i1zSrY7bJVKO2xRIpNpYd3vpYRtdvo9XfF1ecmzEvi5T3/hhCLXdmAFplnd3UARZzSz8FTcnr0E2sH1HFUzU6hxFhf1R2lNSyY8E0FVK4SsLM0J4OK7QDlRhVzYdiWQnizyDHrkiUqjYU0TFn5fU7n2y2GQzMWBUjVzQ2YLtce2UL+4XLKBlFB1SQTBW8i8OuOG3xYbaaE5o4StRrVLOaSFmdYO4n/pQb0tmfsQJ1ZRAcgDc9B66fr9kMo2NICxwtwKIZJ79XM71buJxTML4zDkEfDZF0QMbjA6xHXbHKRXlZhcl1zFZ2S1hPMp24ulhugap+G0Ha64P45FDS0pMtGnnAvIXTceZyx3gtqK/ZudACDrKjf3GNfD3yM0jN2dWSSLdT6yhWTuy6VWE0+jraP5+9oezP8E2lzM6uqMCO8Yw10JucxA4Bnh+VMM2WrYGpI/jJIQzTb31OCVyKVHuZDiYjUsbJa2DFLNmWRcO7yfuiD3uDRqxDukSmMuLMJTiGXsjF9zNmwboF5wFQaxPKbgyWtOQrudlZh1oZFI5m0Eu2/w9XPpS13CcMdNacoHjPBlqw8AuNcqUiAp0oWA+EdP3jwC5XeR3rFfp4EHBENkpDYEYdIVLjKMJYdk2e2/xaRyfkaB+mTK4WpwlJiCC9HAF0YWj3tE/bP5rX0G1/M6vddMHxtN3WPafT9ABR/ngXrR5MBLqy6ZGQLy/lHqkUTUNCZqEDYtjy36SgN54ByNyFO1uIT5KXPNASLKes/brRpmdlZqncz//3MWSUFjOjlovZyiUM6SoIscNNgGdsMQREkuBbeb1oXYzAh18ygrQcZ8KqIQFr8O9ii5jC4JLAqH2A38/Rjc6rc6g6Ddj6gGcRkExxioRqPUCZy3ncYlBT1bXfDh746oyJ/Kn/HNOoRdzRAdQ+ZRShrDLxogwznNuno0+NYfauFQMwCQzc5sW1JYvFAoRClUfr7TUSdw9zqApWExd/cTrAp9p8wpOXYSaIRhXKYcXsEYWfJAIB/yKKVhAq/VA7B1IzIYMTYlhQKDuPq9HTfBgjCmrGiVj6CF5Svv9+dXTVzalKqhXY5ZQTQb5pnjsCIy5UoVg2AyrSlFBzD1araC2qn0gjK20lR6xtA/tO0/TOOyJF3cgXbVGS0W/je/mRuMQmBAqwGERLC6N/FTWmGs2xxtPtA+2gYMMGsrCRsEZuThWfxkKccwhaMZ5jVjkwGy5+AKnmh8gICq68QhxtRuZWy1AOiW12fobDu1X1jH7yTYYyfSo6Ekk7EO6FBvL8UFqgKU0NgjRW/my/sqsXBnfF7xp2kl3q0/mPddLcDNc82xx/sQ2oWLTFag9zvGNqiHCAMjeIUMB42olgZBUMqBFvq8QeZSolXwIE+0m8FltKtdq3EE1evAV7ky2dtUJ2OghxFhvjKWuJOiCB5nPy7qQc8sOJNVCQQl+uWDnDhrwYerz60qZuPowWicP1qDLFYlMj5KOXOA7nOSI1jKu4KQclj5dadI+XbHYVHzrr8GifuD3p0AZJHk5fQvIWzxzlsvDC0IqZXvUmnaII2zXiBAnK97GOdloGhmFNfSyahhTgp+OKwr0K6iABFqJvEsMIzAI1bcoY51YJyQcmU8sok10FXvc7HpKFKSaDkobh3Zu0Ebr38Abk7Ek3osL80uVA2dGlVXroYvT7+A/y49SYgE0ETdq8LHUMjPBFLXu/rpzjRjqWV772fHVS+A2hEJurIVUROsOaoer+VLVazOExRc8yh0AqKGlSoidjViHGQ3DSZvGOruaKYnnW9ajLiFT2l0kb9wWm8hHZxD5UBBKDSKTSLZ09Tz/BCbnFTxcPsHm6tYh24IjgeGBwO9M4U2OYk/LjDJHlYx+McobS6oUUkedfc5HIpRsVopImRV9NX3uVhdktrZ62vTCDKrOAQUBwCtQNeBH8pk1Zj4mS9gQ6nC5GKR4Ox4SmhiQxJfrk+Lqwz/hu317+vrkOHRYta0I0CUOf5N09FPrTas93kPEtpD7aScB18h8aw9+OD54duPvAMt484UD+IJlpYXVxOH5L0CrWklRnHyNe74CF862I5KdkznikzvTnjHmTAAaWL7DuwrZRtzJ2AbFkxoHgAjHAhzvjSUm94+IPG5e1La/Cn0VSVe3jQRW+QOPAh5H4/VU35EKOki/C4MrNn9gGI956Dt+aFPQeeHg26BagMRsMbTT72iB/WTtwENf4lqWDSN2mSKR+/fKihNvqKerjFliKzUIfLKSadyz6HbqApknwXzu0lg7aFF0pV0MnIpuDqga+nyn8+wM/ku1Q/gtHmr76cxG3RxEBX0gLXPw76SZ7HY3XUavds2lhEC3mRn88jeeWWI5kBULSsthR+2yvOK/4j7/lZZnVH4uu0q0mse+MH/6jZ8miw7vaaRq8MBX9Ne//jJXQyMA7ud2gd/Bf3+8ihtxBIIPslqfivnCtRdo7kumj8MG5dR0RvipHiDSOYWbnmMhLmQYuK6hsnY0WbDkoVnhQ6G16MLjX6MbT+zV+DBrbdgpCwbd/IA8Myzp4+AfSXfr5XrWRCDlSVZIkaiVGRgj839n3g1jv5FMPrUSl1JjuADsZoThafOmQcxZS0+o3mQObCllSqgASwQCdKhMXu+ZN6Q7+GqLJjIDuu/DFXDocCH+pmUD5elw8xy65iorNiZ57V4sa6qItTUbyKRL6D1MxtbVwFgm4vWBUYCZxmWdMt1gUuaozWjxcYrUbgwaZhCgWId+0ZBe6FpuWF1OhzoaTymaYrQIlYSRPdKjhwipUiNJEFxvyFRAf5lyNge1zmS8jA3GHeaYi2RbKRxpLbFJCW1B/ryEuzWooWgdeq3cITICm2CJ02HmT7Amhnr3sud5Q2CFbVJvlLajpPPAHA90LuBu6JCPIm+FI4X2AARKxg+wJzCLMiohU+4B7TPxF7P+dB+K+B4pEhrXN8GycDOUTk6cdboApUX8lmGzsmt1pORnZh2MEc5RskU1Z7DvIEc8HN36eEJQAQJvRK8BK+CtYqE+lQ1owu2RQICkUeBL7P+iNAMbmU+JPjjkkWbXlXc8cf+p5P6f/9mGArAq4ZdfpOaeUElNFn8DL+P+s+dcWmdu3P4z32Z5U7LFXnxZMP1ZyXeH6OQaSTgtHJQNQY1NAyVcqnVBK/OvPON7CD6aY043WSyxWJL7v77G+tqEW4qBHSwHRkC/yqa5sAgKP4opcjAELzTTbHaQBrkZF6MOF2pvIxWndrmXRHLNLXzwygIRSJ3fYtcjrjavpA4oxQeQ5IMo7igTN8OWGo0H8KGroyKNTXpJrGhUbnUs6X0Ynq84tiW2bJ8tYAHANeKSYYdCK9DopkA+OQ28Rsi7ZPNleho9TvAgvNOg50ejtoiqI631ym5eFYDLybhro5/iSh+YIAX53gjXz8bDaUrtLG9jFm5cKDgph2KkZsmtQVwOpMtVctdEDm+Vn/ZZfzLwaCs4px1M8kaOJsxz/khJFZbngMsRvLpLfKsJ9bIIHd8E9tflV9S+uISrEIZw5hXBv/bDGXRbpA8cNBfbx1ECHyM+M6MI71SMO5HdwerCndfAj60ADDbAl7sWBISEsF9pGxCTrqKQJY40APWSY8Za4hUMftuhc4j/7pDBUZcNhT8xIrouPeYpADgPfMLk0fg3nI6cMxtYpR1XkOhEUOxPzHC6nHxyrCKxEAD+u+l80Y6mMr5lM25/jtOVaGZM8/rTcu6KXSXFKWeUF4chfRREah8cMDO3cEC8hFs6SOEOka3GYZbkkW56yJfOGEe1UVry1ijMIe7F5nH6bibgDJDeghJSqLvArJkggsn0wqW1yBu2HokvCu6dn1z6MMPGthxktKTAcaIhUYPFZBeaMUjAz7O75SQn5nDjhXAfDoiGteU+hGWyJZ8BihDOPgPMbYRbOhKAuLZpRYC/qxpMZBwtNktwfDw4z36gsjA8TbViYcD5jchB8M4Znz9VTkaRftp8wFCDkKZkZKTbzJhfhW62rQW4KRICZo36BJoSyDWR3dRYXwJpHTg/NUQjHjskMpEQE6XYBIYwvcvhRwI2Ml8BkF3Yt9hojqEhLWAMuQ/VF5e1+FQUc8IaWt3GnCeURlNYDkxyAK5+5SwcD8SsgpQ97h8rrM7Ld+/PL/rX16/7Zy/eXL61GG2uUMBmhrK9Yhsi5xqwMJqr8P3NzdVGS/C8qhYQKpsL8K6xBgeagOZOzucFBtBtZliAaEwjKAuPwAakmcjvwDR264JePQQNof8i2kgbbpB0Tp3Tb7TGEsReRNe/Cq33WzxMsMj6iq0d325WiK9XVgpx3DG0tsGUD5hsswQ722p7KBSDoyVkVK0xEes7BedVCmtbpeyIL5M223ed180ooZTUwsLali7CgrTF+DOoKLQJvDXgZXOT4AE8nl1qqyNklekZHjmes3axp0wNJ9x2XFWRAnNFlkNNXbBx7Y/mQaQbkbuZVdR6NC6nagXwTbAtExT8sg3p/oBmC7QCSlleHrRLW+AM9QArfyGfKye23Sg599G+pbZRIsHKMoUqDFFImnm9qo1Fa0wBh/KsksQucdErGYtOdqk2fex1Q/1XaGd5UjNhH/vuBj0jzrFr0Eb0CIgvWbSo1zx7BS2oZ97F2CAbl2wzHL2KEH+nssA1cQ4bUxuCF4o2vlinnLRW4gmNbkFKmLXlgoOhtLcuFxu8Dp82ipXY9dyGBUO0mf1QI5Sru2IxJjy3rSRthhqN0P5+gpqylqxx7BZ5rcJpFAHi0EUXvvPLL9s6LpQvF2amv/yCO0B/p2+1Bm/8VLly4BpjehWChAAcmTtD8NXXwNuGj3l3e4uhRc7ehQx6ypIm80wdY/BXx8Yv+AmaoYIdgFs5L3z4XGKijEWhmZazW1DlVKzp4XfUygCWyVsG54ZJ4UTQwbpyTZYTgRAbtRY593OXQDmWU3xf2X6JatIgSIZLQ8WjmCbKhsVksuGjQfPYkDKbmc9JfDlqK076+5kGVUYqwRI0FTBFszpu1zqb3/3u7dmbi9/9zvOMbRvCKNJhDRR7d3Ck3/3u/9Pet+64cWRp/h4DfoccYtuSxsUqslgXqdr2oKxbV7dkaVWS3b2DAZEkk1VpkZlsJlmlcsNAv8YCvYCfxY/ST7JxbhEnIiOTLFnSLBazl7aKTEZExuXEuXznOw9fvPnu9e9+t51HZc4+SGUxMK7MFSxBme0MXUbz0B4xX16UUCOR3J6ITDPbhSUMPQMUHPVH2uuhbaHrdM7RWzkzlyDfOYys4rpcNpjAmWGZpa2xlSWw3iWF3a7Zz6WNGThZQJwNsYkUToOxdC+3Ga8iO/uzsbm+X88KxYPnzhnaNc4CKbWQ11QMm3uUdYThosCBEIg2WyX11hGzbTnvnfCG73hmg3X6TSbOlxhEzBjaC9HUURoDl0R45z6pvIiNqDmRw7O4a9C6qrSfiR8SsRslrEJqv2P+/ABt9yH6qPUQI+DfviN8ACEgzjBNZ9dIHFFA0GesEHEULeYQFzcmUeUt0eEN3swiveJQxuiGgT9WkTDvXlJV+vHMSBlk+pGtuJVO7+G1hIEdnSyAB1f5m0gAr5ySgX4Uib6zPMAfepTJxDt7iwHxqcMMaEvNwqXVxka3qZhCSY0GVFra8IUwNtSMtO1SyKlXngVBy2eBXtrgmaRgE1h35CqRG+TPST4uCytB0KjfzjR6jH5FdM12FOw20rfIKALishu48TkkF0Yf6CSRNNMIdiJ5Vlc1YgPW8QcCnxZVOZOtRSYi+gxTwgfzXqEVQ3k6uYFqY5Rbb9HcG7u0daIIaIHMvDMKr9p0AKXi4Xf6RhbLCjcZ53xLeCtBStfGFHF/51CE0asyltoCT2E+HRVymZd+Hjqua5gKT3mqfgGs5lGkQiSAE+tSsMndVe04hYAuFJZgWBUbtliGfBYB93usu9LnKggTulhxy8D7IydU7m3Fi8A4HsYJNyUGYB73D8zlGKgUtLaQNWc+8RZWCBbdsyTUgY7LFYhFcqKG/AmO8l/mF5czqA2MwrFw9BzG9jqnHDm28tHwg5vJuiDgZ87jM4+nutXpOcn0a76rHP68AcbZYEiO04W6fhytt2crkfAnVhHOzCZrVVnl4LnCNOfGkszRxCx4EZuPwmWSW19kJ8BNWyj2wW4PfwKLE4ZU411nFNSXeppISiCxgJjg8687jPqXWxUGFtIgtxWVAIK6kF1kRzObNTDosWYkMadxdDS6kHS5uT089SwQ7AWPmHcabAAWKKT4lifvb5NxIp3ZVFaOMsWyriUVjfT7KJ9F+1Y01/kqEzPAVumSqAqlOkqQZYd982uJ90mYBd02xa1wKfVSb+xO9Wg+OIGJPA9Cy6mOQ10SbrEhOV0HxUR6zQU+3o2z5cJVMoDbiAjLwNoT3hBWPfkZBxsLpSJuBW5Sb4bY4GJ6hlUs8dOAME/HaXe2x5fHuv42HQt24CFoTbX+GyJam91lCwhZ0sRbWad9S45iApwBJTx8CakIsZaJ3DbCa58ul+YAU7QGkV24NMx+TOnU5skLlj8WeOqyw/3ePE5bjnGYi8oMAs/9v7nZsQrOh1+Z+CAkDc/oGOlknEpepSAIJY2QfWyojyhSgVv16cqHeXAiKBHFL66JWo8Eu3PD3JzXrsiNnaVxucwkNt7iN9zCyw9FD9K3lHjjQ2u88GUZSr/2frhxRBYgI1xAFYAutxFVHLjwiP1v17qzKLkxsatZoBBMkvaSCiWJX8FFkiKuz23HoAy3qccdjllBWMGtyDGXLBc2nnqqrVPA2vzVyrrkk0EvGPB/isHFmo/bXbBtWtrfIt3bmlklEhDy+XDSmr03Zbsq1DIGddFBWSnZg8wgA4ANnWCpzhalgF9mxISDhPboKCSvKwbCW7odZZfpVV6ithL6aGDDEFOOXgBCB8zgtWi2cjOGcr0MOVwd8sbPdopMLm3aOSWyW7QUPgxmg6u1sS7kkAIebjzOFsSyYzF9TDQHajysQOykTDJgwhWvccvUMHhLVoJ+B05BXA2KGhFPFAQoSWHSV4s6d7faCUxvfU6KmL1rXKephSjRSrW0pfMIzWpBwCCgYYtHhkdQI+tKzZonFKnGGAeMVst1ZcUcLiMuWo0jKTI6MyMLW5XTDhT3VSQnsxbUaBy6rXDEh5sYUvwMDkRII59MLKkiU/yPeNTjQRGu/O0VQdyjPWF2GLjq84pxs5nRv8QmJd0P8+qJXplwbBhvA1fwDyRMrHnPl0cpdGdwqPLa1Dqjl817Yx1Mvu78D44jDuH9O1rj10XdrODCWQ2Iplo971xmnu59y01H/iCIWDmIBNe4jgJOWKdxfW7p7q/dT0E9KIn1MUJP4Uls3BaLGjRNZWsC9iOpICIF0ZhrzUYaQA+F+kTQn33YGc6N1OybS+jhGCTNlPxG+RQm2kg+M4Inxpialu+AH5OIXauMklSIfZhogSoGPIG9Y2EVRF50y4E0cjtqojZWJ23SiufKj4EDtliFoGOHXLONI+aa5RVuCbKr4E8vfu7H1beM9nnMAawKKa5CAk/lVW7lY5TiVasvqUuoT7jEEIrGrA52TM4eNQzO6NoXBaoSwYY9e2RDTh/c3Gg6oRIF1lnh1SpdruQPuDEd7Fw+NcvmP+DgdNPcgv5cUTgxtbuo/BDdb1uBVCcnmS41M4JhZqfClQVIptZRZkt70jgucyPkLBkvGHbqUaRbtKUF2m4PL7EhtwU5lAq/EzBTSCDRMaF5rgy3p1pXiH/TeemMaViEcwSEVEb/IMWjo5l5TydXELmcSDJ3x+JBzA1A3FVSz9oRFTf8UqLgZqZoJtv3kxsEveWZ0xg6lI6UVrwwQjfMgG5iIZWbSVaFjuvKy4e36xfdMdelR0IAY2nKQgmSVdAhqOJyktxBiXDjbAmXUjLOl+P1nGPDW0yG2Owegs7z6DFumreNzXFA7BNuZkgOQ9L2cjpVGSmylyzcCnxYGoq93dF35KxeRoCotDv8HpH4ltV6FR7MJ9MiMgJXkFoUI+Hs3G6EqLeAx4+zRZ6XVBvstRers+4HuXAhknZbD34jLQtNsWwJJ14Qh+I4SPm4ClW1Vw6TveC4TATp4yKp7NNNXQIz0zs75ob2q06JSaqHmDx8dpbchQR06AgVaMglhRoe9/S2TwsCXtLgiUTJfHMxK0dpHPDUkOhV9+r7SGXctc5oFBEJr2y0HPg0TSzTq6QDarYsxH836x5WmcYN8goWxW4MWCyCjo9nWWrvIVtLfDzLkZwfRKscUJ3Hsc3713i/QoCVoh7FfuoTpj14CDIo59net+DG0fPQPgGmjXKcO14g3vhMHUh5GYU/rxIsXFlIBr8LcASIWEpXtxhEMDXo0+YjoMFPUQ/DGtzr5ITH+nSzeDsiVC3LAFpkrRp6bZeg+aRFp5n90u4OMAqWMF+eG9leEesFoCaA+gyB41vcAzqISakaCLCwShKrpjHvuc2oKbDQjUNf6MOCH16KXwnxmy0rVee8s4fnpQu+dPxbyyrSM4oCU+jdqsIYWd4ELPPhC3T1EwTHrD5oJ0IkBvwZV05rtLGmMJE8dFluWgqH20IJZGzBWoTy5SnLRoHV4LHFR3nWKZ+WPlpxcUyvcNEW7+5H20nJoKKC/R2/WpjLr6NLVVU3bOgPe7L7S737xvClLlAhkRRGEFBsjkAFsvvoK6zcUFB8ZjsJvTQNAJ+QnCffrdDxi72CALeimzRWEG444axmN/kivJ79K6nm5daehsr6kF34l1MNeQPS0viFLx3wIhKRjakWKIP8YYmRGssNiLBLrkqmPBa4P/l66dw6VgGWoWumvDR3X+vgfEsmoANXUGK3VyS8bvU/dJzlRfh2DKzYIbYF5NRrmKlAqfFi3TiybDlHyk/hHVzQBsHsMSByTR6dvn5Mq7a9p0TKcKD5TQyjJGGepwu8SbuWOR+sQBJNSIDHdeukyiF887QsL7CSgPnp6cuz7UfBqU8OaRtm9QtjpkcoKptB1gJK55iz4dRsvzRnIxY1wvSiY4DHcdBl7FeQjzXYPdrtNeM0JcMDnu2K6lAX541H6IsZkoH8/noxn3Vpw3+xpI/0EXcXKz3jF9LTVSRTZaBvUUBpy/4Vw4JX3EYbuu64clgg6vnedmDQxI/VEAtoI8Ofk69D4bz28pio1DYN0ozk4gKt81VZJubszmK85AFAPaa7UHPO4zwjdw3JuJYkKtULAg4ZbWfZeT0FZqu5cAsVzzCorxptC30M82ix0GiPzT5tV59XxZybIBctr+S8UCJ7aFYfQgFbgP6PbtirTEk/ZNVtPlFovMKFgpCNK1XhrxpfGhXN1rvJxm8ZKLi6dJXS2EN6kcNFjRDlrdDJiug/gsBHBdi+2LycmBdRjmTB71OY9f17CIM72BGDal2FHMrS11RMMTS6yHepNJK78tEBtFLvzJFZe4hPYi7W1x3InRta0H2nvkc3vqtm/xTXG4geuN6JIQvytaI3BGcr0F/q332/eip0TVjgrzuLfuebr6oFcFnSB5X5gIi2Wy4IIOo0v/mmPgg9BCol/UM2Q6pTIwOE57O3k5z+6TR5uLpczy7X8LAZ4b4aJMwN8TI5yklXJKIqZ2tHdke0/E4wca4IY+OIZA5jZVBiV2hr0OFIc2gHD2Owozd/yKydTmoVBlwpXQY8osZAp3pXfo4/rtdCNX9xPcv3qYa6fz/pD04ODk8O7gfVUB9sqoY66N231VDVCN6zHmrvdvVQvRKmP+MsMKNjvCLqoStn+kye82qi6gaiJVGPzUenwEVFxgoQlNjSf45tgu4mIhW0hSWc8kQB4Wh50t7h0ZGUJ43QAwXp0s/TiXN0CmcrHHWVyoy76jlwX0C3OOo5/IzAIukYMOhs0ywzsyDVpbMRR5hjXTGrZIBQbxsLq/vke6XKEBaKXgkU01x9/6H0peo/OffRcbBIMgT8+Y7SsTEPSZsiaAnmUxdIEHxJXvHrtY2asIbaTjKTscwRutL5d7Oxv/7+8avzsxffIaeSlGihUCzOLNRh3f2RSgUsSXeSSWWIlZgClPJAUeegPPO/sGgylvg7nklcHshbQR8U6PFIKEih+y7FG8GNAKGMSUIbz3xOJbV1AtresrwGRg+sxvfXdb4k59+PyKfX5aWwHhF5LbQ7k3q01x8p3TRs/bJCwOeHioKAKWw21qXRFr578+wZVT5NkIof/qECnPgoSVZBzJBHepVznPyZaljCoTLn8DfAWkdlke0+NxfizAXQ/3j+4rtWI6hW6PYJUSafJK+yLp/iaOdmMfDfTMol8TWxtNORoE4ZmINnD+5aI+0AbgLAEmoeHR+6AqLjvW7bvTT/EZSBY8ngaLrosONGHbY9QzHo+DFizSbWVpEDKduno841F4hKCMS+FOxNRu4jGwzKC6JJyFeZWy0Yf5UtJd0W4GtIGghPn9Nxfa1kAZWfoaiKhFwAYSHgglvNpTk3e2MsVwI+jLurFKJgkuyEQ5/cs74eTCUyWowvuVrKQjVM6CPKykrOjR46MtvXghlJUaLspFPzGctWTgqhzqGzWZUKIrDcpsNoabblas34sPdswt110ywzEn8veXV+3lCLL5AmagVIs3u6zCf4tiGMQKtgtXBaHobT8Ezua6p0nEAxlvSybTk8SkFjn2WG4LPKbVRjiuRX+YQNXonqWPOdrza5avNleOnBtQtpiyj7pukVkO9K7bCgkSBmBOxoRiRykhxGX7d8I0/xVKfW1QWmtCg5nbpOLhp+iPp7TDnZuGT6TrHHcIs7RVfTDX4WjIavx8oynNQyt95rcfOCHMt4e9RbEXfjAnL/9yAuIbFUlGK11LqgoxJN5WoOl/a4rMwmNnuYAQt5IS8KbxS+fXs6G/b1RiDS5ohyGRyzVkDHlE1EM3gLLOCYI4s0Y/n4rRWdDoXLUAv9Hqpt679zkGy87darsstZlkGpVfj6tdGcH7IQlUIH6nKnQ0nOj4Z+JW/B0mtYVkLYGEU2BrgwMRFzuhAKcT+VAK9hrCHc0Isj7gG7Z5UvEGJtphPQH5d5tgST78bFBWEub3O7oMDmK8y5mkTThXUgp0kcAupmrKVPBoQ7gqPXqJFgdBdAODm1yRvD7IMOaQQd2tz4tEyr1a5HN3at8MzLH/YBkIW3O2LhnYlHDkOr5nzd0NlyR6tUrPJwzLaUbLGyFlKkaZESpbATbBDWBWxVF+zc7XoAXl23kAw6YNiZ8DtDvZ5FtV17VDGGtCkS8HJ2uYoMRmZtWl/OO0RA9+uiy79K7TWBDix2P/OVse1FrAX7QzhPTgAPrANSLC88XEQmVoybN0PluRthqY0gWmCGJ+1A3AhwHlJQeHi7YrA4bjehWACRmmPCLJ/5KjnoLd5RCbeFttN4jUbpkrMUeLVG2SVIRzNlkIJAcmNqDlG2jHb7KkVNPnuXjdfsK8rLJWcdooHmpfiIBHJljWXfc813uLfFrMMVA9bt7Sbxzjm8oqJWp6bvUGhfbmsi10aAAG8RemzbUysxxjFk67AotwKJJHhLU1IxiJygXsqTIPacX5Qi4e9amvNIXOh1EAlij0oqRSlyQi1IPr2UisYuvvRvgWyJSF5ESsGFA6J4QuznADjTdYU9x6vYjZzmKjRbJWU5ZHuqaPNQDotAjSCu/vTNWbQ9KO2gb0iwmuj2Q2cADuyVBzuCoe0mZ6hGWsIgyNNzJWLrv4IXYm4XMzs4IumVlgPkryK18QfJFRyTWoEwMAawgIxk9DKGwyydpMFLkSLQEAQsCtoyZhpFnNKhytO8XYVxtbQcCBCNrmxRUPgLghpE6WxsPjgf6GopF1nh3ONCb8A+si21SB3XA6QZBxjO5liqrHxHshSzH5yh2Na2t93F+GRG/CozlmM5Exc20dNAIB7Yay6NlOt8AUGnzpZjF3NmFhgZOkr6CEj529rjgB1qO5aXgd2Jwrax26CCSYX77F0K2z2B3VWSKDOr9pdy/XoNpbdCphRfRbXHBDm9AoIpqEoBGwQLwaiyzvzKooZThUt6Y0X2rPPgpYoOpZXXzZtQpjwBxfLGPG3O/7vARUm6SlSB9JafqwR5uGgYwyUC1siBwHwOLnYpkAEN0YpEWj5wLbZokbVff4mUY6tXbZNv/rs6262qs8mUS3TCA73NrFjPiNNh5U2F8KHv/ne1tv+u1vbf1dp+Q7W26v/zam21SkfdJEqvmFpwxnI9k3zPC1fXVpAXeXHz/OFjuXnLIhBjdG1BL+IFIv/YYzMstk4tYSdE21ztMiFl3G0eYmA8MPizZHcipiIQrxolDSui1sy52cAw6DzCcBvQm1CnHd2rT2z+8vumajzOuWqBAQjK/tYvAV4Wzfd7U6+McX0NriaOKXiMm4A6UQ5Afm8MWpqPf/3FbSJGlPqVexv7fQ414rlHqvsFuD921fNAUJ/bepFgAAe9A7adpzoNc8lce3C2mtujlHaM1S3SIhPcOWPs8XNL8YghZYrHuBEaLSZo8tdfvL0uIGnUx885DdBue7vZ6s5zbxZ8gr9ZeXEhqrgrvWVrrAQeb2inVsDKGr8CXobJk8HQNGNaqpSj8IzyY29szgHM+Sf8xrxEEoNE0jsBFDoDnDC20xUTAqEzWrKh5Kd0qBv31fUCvM6LX3+563Ym+qEhQmQ2wuKy+vUXVrXuccRGvlCeLEoSC5cBOnUh+QDhAOR0QJKDUqD46zpbkyuVDgWQoOOC8xa/ZMtB517Q6mTjNbqMlJINFjU5qhh2AAJTUXGxyKQbC6QVOo7RU1bgHuDP7Vb6DZ1E3lqMHAJl+aR65v/Azfsyk4D2SfIC1ht+pyOoZ48wiXchNeGsGmoVVKEXgprwHOEyuweKbs3NBbC8EcoIcI4ugWs5G/l0CzyUwCzVewtSAvDGrQgYU8Of2MvHFV+HYVmPN053GJNjT37jOOT+SimTIB8nHchOMxrGZHXZcTOFl/1VmSOuZWrOhpQ7HNRwn3zs5tu9fUlMXCrjs8NIlU7y9OyJBnNQKAIoazH5ZZlxWC9iP/YidMviIMJDZJ1RcNhmnrjhAJrlTS0pug2nyVonVMmvLJKaiN+NOqXqfQZxPNrrei3DlQRgwvYdapcDNwCqE2xSsfXEAyxvr+4X1Cz2CCCRGDMrz1rLZdByRmuvSwaDleigIdHIyqUFFX64ptPqJnmUX+TAqCD0GVWsn6h3Q8+aUXnzeSoGtj8CyzULkcRr8/xNcp6/W5nR7Ni/UZzhJ2wR7FBJ3R0s/mn+ADORvoTlTPi6gLIJoMgblazVc8PyEryT7AemmCU4fwjBI+AqcxtUK+LCXF5RFTSRORSlF7ED2JIdPPL5ZEcLyXsY+2+DYoWjewbeMVbKyF+2MvfgMgegNMIgJAuUmJarCFF4zVXFO4Gj1J7+8KaYlHuvsklpSfLyUGUPNhm1UkmzU4D8RMPgNd2/qSFxE4pDkF5bnIZGYxnLZ/UqkmlxMy8D7u7Tl2emUYbdSM5jKQYYYtU491RVOV1jPJvJASJAA69RQGiCn87suVnmobJ0AVIQfDIDzoUeJJVtCAjptiVJCAMIWstXxcjYuYvM57w5MR5FG5s2zm2ggQ15TDIUymH0rQuG+qBmiF87U0elxN0eRqJ1D+YE50g6djhH0CD6OQsKBFeLkvzqj148F7CB5DzQjnv/yRgbc9tcZzeW4tGGGeVS9JF7CwjrLXAtRrMS89RhqDbJV+nrlF5hGdwkMXh2G/DLxSUYmhajKbtQmy3W0Y1ihd2LHMHUt3mo0tURgJhIa2GAEmdRKFbUuJA9wggyQOR7iKpVCW5aoBimUYMPZzmROKrIailrwx2KBkc6mOWMsCkuKUBRCRwDn1PLklbPlQqIxJkgEcBvR7EYf1623Q6Mk3YIK5Bd72wJNXONVo4H+zypCmACIQiGg3bjEgTqzSUSrQizMg0WLORqDR5riktDSLgV34s+ELwvSI4qzqJahy7IjGSIwBcN3iNOrmgTWyPKwX4W0QktnFVjYCxKHUtoE4R7kk/A80LFpleQoa6qGO1YrLjaXNdppesoETCLpeGm0eLaEQcPCRG0QgGr7ZP9efpxLWkmDkm3KPYwmgpjxyrmpB8RSwJiIxQJK2ViE+G0TKiRcOtZZhPV/QREgbST74MhVH6majBMuXZhPNfmlTGc/KaSqaD95uVHqc0C+/h1YOtxtIVfBWtQuuQo1KvQ/DBzcSm16uDmwktCfkuGbWW5G1KKlSul2tMn/Rd8+sbc0a+BqM7eMIFPMa3qMiNwclC1WL/h4DwpwjEvtsosYK4Ldti6K/z80twDFvcOmPPZjeUH8CV8tOfA++M4fxA7+U7+minFng6/G2Dlswr8+ostFxJATokOQA6xTZsCpkMQXHi3CCVQ06Ah2WNC84Fika9vBRmRcr+WNRh1CfsL+7Wib4eFo6fjvX6fLgnN76ul6OSOnejaeQ4VaDrQ4Olk5BaED3XZwdl6DtG8/CfUtHw0TluThN8sJhqi+Nd1Pn47Ey7VTMr2ot9e00057lMiLdCwnXif4rGYYMI0hb0tBpNLmUFEkMFXyOlqq85jJgtnVHu/QeHt4T2TJgVfS1qbc4FISfB8WQNk7Li6sfY36EgOK1rCzNPEz/LREoGiYzZcUk1q0Cb7Ysaxd7I8vRtjy+5vBqUywYNXKS9EuGsQu9yvunoenVDzyXVZdvkW66LPWvGhIIS72vw6DWaDBBQEDhJFsDOjRFsnji+e3TGMkKPglSuapVgkUrpUIOqxhzcL3gJmQ00yYLS61SvhlcmMsqhGSFqWJu3C+IrcUbjPYJa9ftCQ43ohzEJhRmx5gm6zR0WcYd1YDPmtsOqFVFTPiUt/3tS/m0tb5MCOI1wlKWjKlYcAi5ReoP854YzHYT75+uxRp/1lk85kMusy1yNfE8PRzRDedwhSH9Pk+FkvEwzLmANEdD6yZXunJHXuSKbXzR4htBZpvmy7xhSTjTsrnIRr3YdePtTdbPdiN7qt6Wf32roDw2EPQTUIbOW0D6WeziRjg6kaY8liW7yPimSELxfJrKip3uHV1N99UM8NCJW4lnQlJP6xKMUl8vSQ+UWXfKD4OyJznfOBwC+dovVYZzG0XZdIU4B8WRyZQAME0F/tWE26pBh3BjbTOddZi/4+WImalyvmMUavBJy06ay8js16swtc+znFweIyFqAzxtPAgJ/noviTG3Wzk05KyunR10Iq2op30Mc14x7RPwYoHtraIpj8RqIzeM50CnKtLjOuFUnuFzDRC3pFHgCf0TkYsAsbFd1kMfOvAEUKr3o6sbFzjUcOdQt4F7xMLA8kUTCTmgKgiru+t/FeyzhCEjdFlOTrBOIp93xfBaeVBH4RmZU28Ge2ZKY5jwKkoaEdOpIqj0D93i2tcstI7sByXXg+7NZ8PU/vkcbW4PpRRk1tp+HTHFZbllJakC23DWkdssvVHEDoLMHQGaRtms3BlP/byMharmuzdNZoWqMBw8SKrmK6elVe70DWDC7x6RjcUMj2kYbBopYOCJiBJp5dVzeR7GZTd4+uzxtaYNt2ij4yoUBeCtlgzbVp0ymIe5bQ0XjOrs1FBTAmY4Vs2SVrCGMmgvD7pjQxo8RCkhIFQSkAycpNzQgzq9hQ0m3sAXo1LYNjqEWfJWn/quqAVzwuWiVOsJS2YJf4MSI/okKJWrEE1T6UzEzPXyClmjzNwdk6pSIPh4OKELjkeJQm5B1fluAarcFLacCsAjtblGW335FN9IOjNUdXjis3InStQUzcatWw3EKHS+RHIV2hbd6rK0HHgS7DWsg1+OHMk9wqgXFOxDahrav94j4itqEDMsKuxFMivh5bNqLaUZ1CxrPA9JBtD+vGjygSjMnrJHlb+moS6eRGoWpFcvgoqmdN+4ZWa5mQOzY9pfMEhCcjO1icdWhXg2+UyRh0FnFt3dwESzyD3heVCRFW9C56plLlIGDsEutYEr1oymMORtB5RIdaJzF3OOsKQPBQ6W8Hgu4U7rK8505nXVFZBtB/wkIHAj6Kd11DSmFRSnY/uRxwFjNAW8XbgtHa6JjhWjFFbCpbtknMztjxSxwyVSQQn69c2iNO+SLecAM/046M0oPcs2KPioPireOIOELw6dAnHhjG7xHpG98aCYJSXsPQHPs4HhmO/s2Tf/79H5OJtT/lqX/+/f/A5Jovh/Fv490HIB8uJa66xNUyBxqKgDEaF1WNhtcRv7Pi8/AeDAFseKPT3AptjnMM2PTakqnLcWo3U4quAA21cklOxHCGk6s9j/NFWVDGJsQGNjdLRfnMDoD4DmRPqwq3cDuQzxemB4WH7oplP4VYoUyWm1Fgeyq4xrvT5GKjQRcymwGO982WDNHZ5ptfhjGeqGpsOa2QXrXkYl6CoKJEc7TPMwXe3LjmLpGS/VyiF6RWVCrQJVeJAfvlPJ2my3zzYJ3M10WogzRybndzawAVlZxNHCRxRTnAJW6y+ItHYUS21cPd/S1/pFx5FP2ia0YUH1ZQI/rh/d1+T7Gw3fcUxiB30yruuB4MUVhmiiWBmADMRt4z+2GWLrjmJf+BNwvEN+K6MKXVgxyRjiqG/gDhR2G2FJiSQl8bsTjcUnrleZSnxKYib2W3+XsRd+ITYVNxf7nIHpFy+NtmE/bCNuOhGkS4odQgIHZaTDzX2mU5z+qRdEGynthcbqIQZx4zrjk3YZFiwasMjw7QWYSu/LNZgqv1DDBhUl+wkJokaFcid1l0a91vNCBf4cty+GtKh8rMoBBPZpMLKhLhhbRS5dHgiwCiJzt+XEAR3uhPxWZrC+cz3ts87vbMHmzBOZfGwm5jb3rUaipXsRejgi7GSi0Zk8Gsk0iDWXQdB6d5ZyO3MZOeXGTxZHuMFlT2XCCGijGskOKbF4s11WBkbY+MFLoN84qz3nmg5IspySDg5H/i1TY70R+FNw2HNeGBbw+EoDq4S6n27NcAlw/ekIivQVZUCfyZhbAcR7HuDjzJNfD+2vf+6uu/1L+PG1gjD4E18mC3f3vOyPvd/lHSu38yGJwc9n3OyMPeJs7I/d7Rg6OBoo3kIXwa0kjN8fgzUC6edMwiTbqi9nfpFo8zSA4cgySm/wf8kc2NRdkkj8zjj9Mqn93YfFGEDiG1ZIaskZ4DXlVJw5JRUQrJfn/fUkj2zf+te6MDLABeKAIrs8bPj+4+QZqWRorlwANIbOn4GkTTTJEWzq+Yk4d5Va4QdiLH/qcomLwfcMz6uok6aaq8LZqKKBy5UoHHm1AuHXCjsax8YzcKJ5Tan9s3laK9nnuhhq2KTtoPfzk/++EvT4UfU6to1pFCyIbULRBWDcDbLz51NVmNob+V9+5qmRRQR7wX3k6HPEGbrU345advzrx384xT5Eri+pCsp9H6Oy803w7SzSmC6FBK21AEK5VBhrzXEcZ1LpFmBXvhMg6K9L3iord4SSj/F1oTkn5Bc9/yPrgPxL/hVebA7QZm/fLGboJUO7rhj8I5s8DhvXlT2MQe55v0SmJyxVO2/WluuSyYrbDBoTgKWV4S1I0AMdJ6MweA2znfMaNZIlWo6Y255rZN8rbP4rciuCR3Bv15N2b87xAUODP/Hwbua5DEk1mN0+kUbnNOaGbHF7NoqmKtqCxRNT86OYg41VWbdLOqbDOsfsvCx3+g8zT9guD6R/y+4sGi/WZ+ITtH4xuIWALqB61WbivFWtWVUKAYG8zmSHdoEb85tvijqDcAYPDWGP6jtjigkagSjZYLeOJVvkiEqKM+R2bPgUonbfCSUEkbapDBT/4hE8wibizRg4lzE6uDwOdw/+5QYSDzn9X4nkBjfve7Jy9ePR8+On19+rvfafgOTofdq5U/gZsPH7PzE1kWeHTINPuJUN8k38AV8sdzEFaILRdo42pF/L/xvRjtDbHH2QI0eaybzrjWEZAEmb5XQ4yeiXuDLuw5HCAmMMsDtkHppFjPRxnSRlr+cqal0VxBtE5Y2KrIuqvSKFfZjvwbidCJNLq44S/94jObX8/rDh3o5JbEkMTYFj9gq8bG1KyVYJ3QwiCDHkzM82MZDB7KzePw9gBmapOlWCEnB9Z6wi0m4DmCdrOwwy+o7BFntMuoXIKCV2XmY44joNS+bZdUlVVKCZmbOpewC214HhJCexH/uTTW1iIPIdEuN7kRCEf+nUE8U7BJNUUFClU6QgkR8R/FUVBkMqe6uD5EaOgwO9aU8hxouLXk+4YBNWibHPshxQ5dtOrQrC6J8wryY3IAPuCk4qW9DDm5mIcZQDFtBMyRLErrShA+eS796d/L/jLbmzKyauEe5J1ndGR2opSF24cN260R167ik6zhx8IKT9dGYBr5hIUgc76IZJ79mwitk2bdLFbJg2DjoiyPbhwo2E7b2SPwH03zd62aP2Es4JxQXToiGdCzVxbO7IiJQ7tHPXo2xx9VuuK7FEZFVVHKr5WK9AP+JeZfQ0e8V5GfqtYlejObhljXb9Sw6HqiqpXLdJKXvv4kUoaXIps0ykKcfV07bIq0Hmgt4a3PBVKUtIJQNZmV0YunAYxNeDfwPI6A1gUcNHpPeUN7JfwmOIMqwiVMr5AmEdRc81Z9hyH0uNkQxhmAuSqLtJ+qCqikr4lNAlIeq5r7+ZiFmQse2jUWpKEpCYxeMwSE+uyAQuKPtKaEMD+hek8e3g57KlWFVhoRscS484VZrSsRcWxiRPqIHHsxyElBy/F2qW1U8AhbusE6hfUtGrfAYkzeYEMwL2KdoiEc6QdmPnUg24oLIupXD/wQ5dICC1J1Hzj9KnHaFmpYejSbRyCSw9adZZOUq24zx+eckh04jOSZGbWX39wne8/pKsAwCB18kRTWz98gCqlRWlTMDzESioWSbZynzeqatampCfZIF4UuJ087wS89b5MBULXYZirgqat0DsW3I7bpNDhwLS9PFGrVKltYMlVM/mfGfjxk0TPe1KQ+w3Gtuvly8q5Q6lXRg8YOJYYbQpVG6ray56NctvTiU4Xbi9ghQuHjLIUa2yXRptgbfMMMe93wdeK1LRNR8ctUJTLClsu3wirB5XTbrhq/K/9tcMTuTQAQGJKr4mpQxWFbNPgEi5pCCoW7O7lMCj9LqcgCn8TbeJxCmuPIK/XMuew7qrwPmOMUTUslKOkovcFzRCYWR0MAnGLeukyh5oCDgwSZykx7ZW64S0ynF1A8GL+g6Kks2c0l2OoXvW+yW3uksLJuiXYv6aUgiTGxwDPPREQ0mN4tA7Bq8GQCIW2q6cbRbnQpk6ojiAf+M6USR6u8oSRgrR42leNj0lbwQeJLi/Lja74Yd6W3+26TCyOoMCy5z+waxNddz1FnIz1QKF0YLk1zbR3OmhOlUeTGe2aPklA9S6VrsdmJtC6n2Nt6gawqsrxwCuf7c66ut8nREDJPWR0/nYEMQ7518ZwzppkySWs1u2vrhfPlIh++/zfwUYDY5oxI1I7MxVIxlgnzlcI63LGpKwtOGwehRTKV5G30vo205GNj7GaLyHGLUJTX4rJ2a3TAb9+0UikivfAFx6T6WPCd+Yphp29rJhGPlzBbKucvpSoH72fT3UWUxrDCu0d1Y9UaABGdknLC5Vj6VTZtjhcHMyvxvEPgHpKuOaReXabYOkTW42NqjHPZ7WGLiYq/bOqyA63LPUUmKM9N4XyM+Cic+LaN6JE7qLACQSRTDKfjQKDkgCgZS/mXuDFX4sWUSFjNQIh7j9The0W4xaLKhSEM4aqoZJ6aXTEB8umKq1UwDhpoii2N/npBae0bbwFEDcgb4BohaN+revEfENcd4jP/uW1lYpyxl6SM1Hc2Lc6kZF4DiQrohERXTYZ06413mk8IuMy6dEbxirTSheJVCp1ZoYaLxeF4Apv9ij5ukYq9qmb8rUN4Hg6F2U83rgdP9Swb0hIOOWw1NHNzATfzEIs2YN2X6ZR0pif5zOq89Kt2z7R/HyslQAfZ0JdPxoK69U1HZ4/M0QYS8yohogL46HbbGxfU+iu1WGXVQA8LTtMLdGlGMeSxmeRoqFEciVYHCvUaFc8d2ryw22LTqlBbji9AaWlquqIxFk/K1ZAzvtquKuBBLpWZezFXnRRhPROSut8toDjxVcbkH0xlgnDiSFBvC281FglQgvzlH15GX6NeMZDblhtO6Q2iN5cFbWvzTrPJEC74ofMIk8EyhMMzNHrRZSrEC/hJzf0ROzOUKk2yBvOYz8XwEcYPLBhpNCLaaEEVL/1Ls7mhbARcYVb7g4xhrLeW3P13f7Rf39s4NkkN9BQ/MV3pqh7aukc8dxl/Usts8PcNLb3eOUSDo2bDTgTl+rJP4CXagmab6aRv9pXIeNH8yokmfbOgdyhHeUUoXrhCtPNLSQAiEPQLDtCKZhrYhI2OE+0yJq/hTtJ5WpJz2KU0wVW7u9vZsVmPaEXg0M5bilT7FaMnE+brlRQjcyDw3OMqgqbFwhkEooZ1QMIEq/a4jmR1Q2XOrfYwjvUh6S9P+Ep2BP4jZprxtlK6MEryUraqpzFOPDTC5tcmh7EuEpF61zOdCCQUJP7k5widjzXcOUcuGHHvdsx+tPWk9b5hD558ZVnqiGZb9Cg3N3BIcD/h7ODWBUYRn3nGTwNvhFHV49DYj/KfAW+JcLw4FhVK1Ce3OgOtwiBMfCS1RL1X4lmznQuVC4DgbPKuLlrjvFGkmgdDjqXJtThQodC3JaClcFodl2H1ppZ0HFwTljnW60S4F56aUI8uSyo0PoYCZF5F4MQ7+sCvpLhjoDMSUU7rplD0P//+j263yw6GVWL+DUkxlJnjNy2kKkE4keYX9xWdbqiGlRoTR9zpBaFjFY08iC4A9Talnln6Jmesj24YiCQuWdZSeeXF8cXrTKTrVNAZMPhOFifNW5mEDrk0MMCdSTktirm4oLdNhLvW+CiuCwrTgUnCjECalddbdG4dm6qXVW5sLxrPvJxQSEeX0nFxAsk3Ur4Wv69puhKvSuh3Vx0qjRJzCqH/nxgb9jybGY1uCTgPd4xO15CrM8vNQQclM9yM0TQjoA02y9RRogEcnxggJOG9w1LOcXlRsosKNsLEwx5dUKWm6dS2O+8EEdrmoQCMk3hVNLtLqXxXCJytkR0wlXpexOe6UX2HZhWhrkod3jxWPA6+1ZFPieTdTQqBTJ5ylJVkfQZyV3j8Ol93cOq0dtKUbsQ+X4zvI4cf5Rmq4pkOnoi5JcIBUUJyE/7A06ib3+2howhHtkLHlUAFoEkcX+e+pvaHl+KyUKrzdSon2PLGxxfJ5SaRG87SA4IDdOnvOw+/MVPIqeZMLR/363lhKGfRbPm6xr/NXNVsMmL15Lp3/ea3pcMf392puyWZEZBGTqfSOa1pasXH0dyXJIx4mFfyzyJDnWzR87cQ1mvKk3kQg9eEwaELi1w1x36B2xIkkaNe5dQmdiIiVydYAHlUASMe25i5VwnPldxiB5BZETUXYQa+u9XeslGM+h5rp4l5QyxahCgaIxMCqBIEmoWanuZtn5iPTq8zfLWDLzeqs0iuwvEdH/DqoczLQmlWGxv1ykRzrOQ3tUruhawap3gDMAsGYzCleCo21XLMmhuOB3lglq/AeKSgrIKXWUaa4Eq9hf1CwRv2sa1nE2UPWYDVVDaM3i2+qfO6RuOx7Qvad4B7f4smYDaM9nhGajbWmHkIyjToj4ty0V0v8BXGs7LyokReWbHUG8lvWRimneS9xcW2gTosmzj9bsulifXlCpGwyCSxKMzO0zQnipmpCvTljp2GWUdjp7oBBxcYINx/BEi2LNHBSHvFe61HFDmcAium4DxEXHLQ2s/tdmJPVFzAVgE8Ig/9KWJpMPPcT7YauJfir9OfaU7q5/E9mhUXmWV9wMuNeSmtm8qBEMlg5iqidUf1b+/UedBhitl/jTc9qUuzZZZObphq2+v7uV/aFwBxAAaXmlc1vG1eqag/x3MoEAWauo0NoK/ZvBIVzPbZhRul0TJ7mC7M71IeS5ZxQuwIndiYhQm4KEg1lmNAnncjvIbihrt7j5KMkpxxw7fzcuPUdo0EKVdcsMz878ahKxAHTQ8SZ5DfCbqbrKHgFl6WlKPL1S+VM5XNqcoGNjaDpF1UpkoAnQh74KdsWSIuCbsRjgtHjFKJOHdoBfL6WWOTFbFKHApbzaCMSxjE2C6vhTiIXRTwxEo7c2w/k3KOcZkF0jESeH5j/B7wKGguO1oYlQw4yjE6UXm3u8VU3QKWoCAbHgChxMFPqO6KioBUW3gugy5Ck1j1Yg39KfJjOJ6WMjzSsf0pVYUDlYq3gk3Vw/1k3hJ00TbEQaxKmnCFQn03Pz3QEQBQfApdCvgb7/IUdwIqT9U4K4DXOB6WiaC3m04lhEg23TI57wwunYz0XO2vDDl8ECFUNL25aCLSlzAZo68XxfM1ITnVLozgz+KSkWOdNhKGVa7KwAaf3SivL+IuthJcdDOq5DTIDag4scMDFNuIvtQSxALlNJjfOhJUNZVSHqB7VOatC20iPkBnGpovEd6FbOtwI8Wj0AGrqBCYhx5dt6qMQ5FU3KfL9Ao2EGnNwSq20rxBuqGl7njo70YFjazvosLxCELeWnaNOdh4o9RtFurruUsfcMWWYY1hwF2FhTQfU3LmS6/8UYtvhrq0ZnwlDhOq+CU6L57kPxXl+C1Qlf1Y8XNbOBrWyxkF75ybThtqMEXuFG3r6WGOVXKmCRWpKKSPndR9BDvRbHfzZukWzjHl4nexHRnlBfJRNuUN+wk1G7rgTenz4fDRQ4oZFd7ZwhMauIUw+ieBcYqW0LXA90E9p76FegckeTihRpc1tzvsWuGpckzcXpYLXXot/uS6axLq8qWz6/Smktry+F1vx9KWexycW+wVJYgwPAf6p233Ds7PnTC8tLnVmo/9ckkljWoecinNBNYwOsHpqt5i4GwJ/hlSthD/sMVxtpsBpIMqHsFzyoWXtji2DmYhKUFoCLm683SpKN8FA2ZDVbOdsKbpJuMT1gU/m3Aw2pT8Zazp40YPH8GBdJTARex2opU+oCNVMDOOr/EdtRlnBaYBHszmlt6qiXzW/kPxOSvV24HmmvyR87xAKILU4vQOUPttjjYshHHI+WrRyBxDZu4biR9bPdfhVD1ncXRrHIU3LBUw/boDrsjudQbg65PkoNf7feebr6oFhoqavn9lLKSxS6ty0E0/JaPOLFB5Y6WhQl/f6BF7peV/wyg9FzTKJgmJam5VSidFBQbT1R0VvKXa1SP8rYOKbQFgTtfORbbYcdbOHuGyM2SHqv94Zy0NguQfZ7yRA2HUWAA8OVjzR56pjWLAAhR1Ws3HXjsr2Jj/bEcI/h2cJ7eZ4CkGPZia+BMNTnoZfWMLjKCL86u90cY2VQK8lHEHPgOGs1pGWNJ+fPT9jsPYOIHwz7//AwnEEd8L/ufgIvAcsB97ckjIztJRVmNRVLk3NfeR2FmfYIRu6RTEc5t1YzvVeRNIO8FWNFpCrReayIV3fBQDqBsJlvvpiT9CQnVbbSbx5KWge6KL5hPMIW4pdPC6vWx3Zl2lJlJIgbm5NDajtTgVWdNcIqQDOFwzSCkGD+/H2RlmmSBhNi0yyEngEdTjHxpnGYTSPvyYoMRrk9Lzsde18QLw8xrIg0qZHZ4d+THuyJirXGF1lBbExSrFwLXFlD/VqCK3A4ANzewQlvR4qwPN0+r0VB/sJeoq2+A7EAZx/S2zh6cvXz/8w+lWXX10obHtNJ1+f/roNDnc7X251bjRstpRjMboOTNC6HmOJqytLVvTglspKf2SC/5BQIAdBREihop3UJUt6hI7zIrOpfJobEANEPlYy9io2vaRpmuvREWXFwLzgoW3MVsuNgJJeIKIxvMtARDEshP+qbX5pcLLRAD9DXbWe4yYSGEckbKzsTEKAvjx8hpgSEvElN40dUG4bRIW6CjzuPZyletRtTZBlSD3yN3hs2q7W5GBAN61gllTejfUKUePgXIUaSlvTTra73V7B0m/dzLYPxncD0hH+5tIR/sPHuwfKc5RO4ZPwzrazAz6cwU8ox00I7tUSDjOPXrguEdP+TGPfLTeSpx01MznK8hVp6csqjhgu3NBRGfl0o3Ezq0o+2i/d3zQE/bRw0ix2xiG1e6pAJXhsnklDok59vZxTRSN3Cc1l8Z+k2AU1zpt+L+8OD1/HdZck6oi3DUlykYcNX5TfzI/hThEWQn9lJx4lP+Ue8JnVJD7TanBQdMKbWqvVD3g1BY7stX/9Ng3Y8Uwhuj4TGtEnXHeqkZXmoQDASzhNTa1JQ4jxYrU2h3Ub5FXeKCs659SQQKmUiRUfnMWa2/g7QfakXg7NzPKjCCdTCeFUWgHKxjjuLFkMDyFHSOtOHGesrpTFn7WtGS5M12HR9RDd3SoGQevcFB7hTcuDZ68WSplkfAT3imiUzdzpVbAW0R1EyN1hmQz+mW3NGF50nkICR+zjuh0WAKEHO6iuxKErImhn16snhyi1ybCwuNqhfunFF/KXai2JiRRdxVdqTRkndrtE77fJMWKUI2Bs6k2umO2kQWPpHcUkvhmc8kVBQAuocyhahnLtVGCUKTJSVpdZszJgugwqMAJpjuDuqub+cgcGFwnLjmSLmhu86BUrKcckHONUk28vUQTG5509mnFJ7UBJ6dWeM31TzNX0Sq3JbMlqRGhNlVU0Pkd1tbQp79JWbXhubY9wkQjyXww0fFfmyWkerYTb0YJYI+u8I0xH3vpzbKrbOY3g6E8dRXGliq1ebPeoq0kYKRT8kGNPXu0Ef/OTkuVpRImj8PXsVmvn5xNtpCA9xRdrjXQEfggOWHC/6C1k2XGWgyCZ0+FzqJmCL63m9RVN7fjE2XESH1MrqD6H0Ed8hpdmGA4PszQ5Ey64W2+uz9Mz6nNlZ0sjYgHbVtSmNx78pJ9sE6x5cC0QccClQA2Mh+BeewudvcA7xxCfEnBEwvZ4Lq+llw/+Q7qOQZtXsMikziFShJC+kvnQXG+Y36sX28z/bDbMVSoMGUK7pIOvkNHbpOOscqu8gsy0ITh7sMMQR3K2n6r7XjYlne4kqOVHLhqUEfKFbhpu4obzQl/Zpy48hM7o3rHe0oo4WxceRJIrziijO8+D+XRTvIHkBYwDeF1mVbC5Pim4lKocrLdJg616XvvtZx+8UhXDda/brD4XT5bdYEvEe4bqVZ2BfrehIoE5q0+t9uPREoE1TU6N9E7Srow96bbjB9W5Fv2SGdrkqTwyttdeQXAZKy64qfUbi0o2dLWDBcNJg11mA/3CkW2Au8qFUSkOqulFEW8vsyNeIMLTK4upFstAZ6Iz/v+OPFMWY6PDzNKRqzGhAhK08o8haTq1odHCIFff5F8iaZY6XsNJ2r6k6UkpoRPPqNEAMH1P6AS4rwAyuZCxnmuDiUYcVZdXUJvQFn1wabHpvtImZqIqLWUiBQBg73OT+hl/vDKGV1HwAFjUwY4r9uxNyhHx2/cNiH/XF5ocizfn3+O5cbNpjJreFEubyyBh93qVsY4PS7/CBvKp76ynD7W5CEqHFBvkOu/jMfZb9sbRSFrSuGOh67J3hG6Rp0nm6D8cecB3ALPIMyrS+3xtefI6D+cInvr6ahs2pG+irQ5h+18qPnwRR+HAryqfbljS/ggq0C0GFwKl+ulzTLGhogvMeD/ICAcUxCrBJEPtDyEq3QVfsHTqHr8cB2RLGqy2X6LehXtztqLgQ3RiGdTCvNgSy086vUl+Hak0Q8IO6z5kbIlhANrjh0opqtcKS4bQeAqHziETOPiYbiMCHQn79Rc95zZ5WhWRPHg7IiPMbZUq+Eb/aucpU2FreOD/RiDJHJ6LR3NYEZUvJE1D1hINxjQcz/KSJbptQJZCs0K6VxDDq45OBFGJD7CmrlqoWbXWpJkvLW0Lce+D6MUic4PXgwoz2Regaov+z7gDz9UnZCDktUq83woP2ynnof2NLIcSB1N902Osj0o/P6BJ+H241EQfM3I5Zjn2e91KoXuPwbmJTpSTk8Vm5Yrw9tHROMHDCQM9MOOCzgzUxI5TBEh8YyiTKBGIVzaZtQLpk0WbnZwPX2MGSK6koD+qRbftBE2XbfSlpgvkzvfldZyWheTO2wefOwBQ87WRLhVxBTAhKOnqrBDmnzBk/xfMh6sGW8NFf8q4lqJ7IeVwLeZ4SuK036Uq6gGmbGiLPTpUQBloz33saY1sHBmEQsHtUxebEstSH9/glGFHlOiiZ9jmWyiT4SiLTMsZ/6Rx6O2Hgs9coo6ej3WOIagYQw5MoB5lRlj22vH/qPAEIMp9N16NZ1+mV/BiUDZ8pFPgbLOuLj1jtThJiQEswwS4w49AoGz8dubkBvRpR1jl9LeR5lPbfZisgoFgXzwUyzghDOPCuYHHtcjYSfAGXUlV9Vp0YVsxaxqMNkaC+1F0KwRZqzjCNKnduf5CEv21xJXKQoeYtnzUFqtqYfXi/GUlejhhdGn2ckxxJg11ZNXRrKDbk8yjHCmsdzHfUdBRn+pf+vPqYrYD9kM+E3hbmZxYL7ZSU7/dCp/P12mBfaDmNd9V4Ad+vNgXI2M0zh6pJYcSXjH+ngFOMYJ1oRYpWCiMX5Syi4BF4DD3e5zbufry5wSohl5Dywg5oJEfmTTQ4blRC0QTSyCaQ6pI5vHG+rt7K64gOnA65qmB4rXIQqVV51HeMwYrH9LzPzUq0VaqNUM2wAJcLg7cD8+UD+O+hicyvX5Z/SYaAp2u3iVpeUpHskuq9PjtKAkbuAyYLdPOxhDveFADdKVwIh7Q7SvJxzTdtA51y/taHu4CQbmsxdDrE+7PUoNPZ5ILfTPP6NGVKZpVRuId/FUdazvIWB9D3f7t0b69h509+8n/f7JwcHJwaGP9D14sBHp279/rJG+PIJPg/Otg3AB32vm5bosx0z230XVNAry3T90IF9dzvR7+oWP9423Ggf9mtGeAvqrIpZSzFXuyk+5Lg9LftcrCCbsOYr1ffDg6L5AfY/rmd8OtaebDGQLye1Ls83Bp5Ne7EAdLLNFB2Y7+2U5GwGaaG2xopYXDM/c3z1S4ZeGch3Ctzgpx2s4KgojQWHenAnp3DaL3CnH9dotjYXoRgDURRsm/h7ngBFDN0TyEqsorZLzS2K/gLWIfK04xY0ZHhaGqjtewvU4X4/sRqmoCa+FxwXyzdUXkuaZnYBY5kMQpDIyI8xXzptpBDrmsCPGASPNXDh+y5vyL2UKRQ0evwixtLB5MOAH+6cC14l5Cwf12K51TLhhipL8p+xs9a8u3WpDO4HiYhbGJzmjrRJFd7c082eGPnqVnATpDRkhfGawXmdZrrJ2fLeEGCG718iMLi9Sl5KBhEXYI/shciWPFoi6aOwgTWzyMJSd1w4h6cAyrMguEcvdpjjVj1cDhD/AItW3pjt6RK+EkC/wIIujP+JbS8WVBvSuy5voeDRkCIbzXXZ9kvgnzawWFab4wclnCuuxlCWpqvYWNuIXNea2yim0srpcz0cV4YhJaIqitJvcNUr64W4v+TK5p1q0oPCTBFkJ4weY3ekUg+R7HrSOLhhru/HmHpVQfVASvEm9Lxd2q6rpLEUeI8j8ZnVJpmBqf030uKhDzvK3WSSGsNUYWvqXGAZQHIXpEf5I0DyVqAZtz+16r9cdjw8laBvPrn39UQaJw1Ck8Q6pbJrW29t/g7qRcZK8KKoUcpLTyQXm3jvapJwK32WTLheZAIABnz5/8s1oF+qce53uxzb9i0IA2mjvYQViIC8AKTqJbDbxo7hrAbGYcwRkrALloHY6TG+89dnEQ/QEUAWAXTRGljTzv+M1XTSzmTcC3ug+uZVt+1zXUeXJQUAUCWde6R0kzITEwlU5fmvXH5xoWmuSUN2Pa8Txv0VB6pB89snaKFhDc/WXG4aje7NcQ6L71G6sk+SHh9/7728BFWtWgYh5FvuB3P1IGzH9C2q9IDOp+S0ou+zKfnLG3tr3HgvizSDbBEB1HlutaunhDGuko7PGLItqWMqkSwfA8n8NnrByyTFhzpNxAJ5I+68o658q9EJUFIwfMpClRBy7d6n6EDxM+xySayMNPnE5b1UMWc8LgBfEt5lZhmXyrV9SwDuScc5Gr5tkQoTltXOjOP+yJXRkqWYvgbphDCmyWM0cfU0OgBBoGOZqyWnZueweJGiITFEsMD88tPvfaTHmLC0oAcmc31jb3xLBnJQUHN9oRi/fqAAOvfPzWCN6Nvy0NC98C9kqpAtB4xdIcBVllZeTgPJrFVPkzcsOdnu7A7ysf6wSKkJodP+MXKGtjb+cpeMMyGfB54MDqvKfWIxTwxHS9xOsh2gsU/Nsxu3fyJKUbkliv7TW5stTrjD3raXTHd3whs5BbSmQy66E3eJZECA3R3yA/oUbrwlVkZWkzdgfw6RMxkuj23gsMm4TLTTFu7f9ezUYiq6U9ZU5tmVx8c31GM3iodnqQ7l2hhwJpCeYq8WJgCDBJ7bC6g1wKF/GE3GaFPza7wWXowtNyDJG1q8N+04yxG4dlaHCFckWXmHUzU1dGTWCi1bN66VvJb0MpUxT7mDNuwtnj4+/i59wa3blYqaKWzW7Xf759/9dcWObO3cvY4xEKDE/TpeWioJfLNqKUFuhkoaBu9hYR+ubbrnsUs5lZKxt45QeaCv88HDIJv7JCczv0BXCE3ctervNDUI82EbOjjMxCmhXRY7Nkceh+i9NCuQfzznpbYnuy9oBkKuaLa1GZuaQk1WHGL4tSxSHXySnC3S1IMSOKFM5gbOuKek3idMYniQ//k80vbjaAgofKK391zW4p80giI1TIxfAJwr7wAkeX+5g6wHJbTRC4o3vuAZqQ73h686iLxEg/qDqd6z851P3UFDCfCt5BocUMZoQtVvtN6yBNoP4jsRnxX8dxrR7cY+1mtZuW0rKpl+9g5KtAEBbxWWp2GeBA0AG4e8f3havyRjuJlIJRWbgDCZredPku2nbjsxuaeHGpLhYsCtnDMPZChrXs1rLV8d3eI2Ob/Ld1ydALuD93f6XLQoWuSVVXS9rRtrViI1o0Oys8PyT5p0v1gJyjXuJgJKN4pJmlvIlhkitD7fa4wm85GNd+kYRNhFdEEpusDok6/CsPo5KwKJjIt+cl8XdO4FfSu+amZHzWGJN1ckYrTGN102cb7dFF9RLYE20GaMcO3QomHY+VL8ASnlprA/JivQtb4ZP5lOh7hcr8aEzku1toft/ZathKE8/dHJOx09OxMLqtPVjLD1EOzizYSv9Q62enlkdo9KGaiVwE1FCspXfQU03bNlNXO+JmOds7UE3njsqADIkvvkhuDGG4BEaGsNxfgd35CceAa7BkCdkWK3n83R50ziSM+AbnVPJAvIs5MUVMn3cWHYWZaD7W4cCcRN7hwmtFqyBvfKkHGV0j/tBa+eIsGKAjFTyV8lnfBV1V+mo4poieDQxXHKRAZCjdipfKV54PYXSPgBY8dRHO1H2QSV9UUecQxaMVnOdBm6WNvFXNnTPRgwXrZROMOFnVPcnkdFhGjGbAJS6HHl2rsgBbdMImCvcdAxFJBBlAt2Yu/KGq6GRxcrAN3vARhC9MM1WNUfdkWcWxU0xuXDWBYlIwK+AQ4T2YL2c0hZ3mG/QHO4efnn7Fpw6dRhTJF/pXPpHSua9NrsG68Knc+cHnKdvzfxGHPzrIix4p8mClHYceHyfkGdJteQnJmUpXZ1yDqi4ulPEYl7k92gTxFqTU5pR6QzvKFD9YsbdmhOuKQ4g9WTFJw6vMZvVaHq8ObuvVMlDpfU29IehIVR8EaGCg4abE2vxlkXX96ZD2IAlHXnrxU+FLv+Ym17vFyOe+ZE7ZuXh0qrnWaxtGSiH9UojwQdR0G2tSUbqio+Nyv4Q5FkdoYYIETm+GdK656CuEWXby8uzOZ3p+C3ounvywZnQsn+AjtGf9pv7vZ3IYB+Kt7PISPk3NfM/UC3KSmhNUldFgCo0cZUpqzt9/pn8/DXd51RvJr+4hGAP8ANyERS6s0Xl3V1cLuxBlLJO4/KiwGfTKrkjCpYNJkEa8u4d1eFLVZSeLQ3eKtMc8jW8GQfSXajq4X5+FjO8H5VvjVghI/mEKMEXWtPLXflbI+HUWNSuLaGWHhbpVkmFTFsDP4nYXTURfcAZP3oND3b7xoLhpYsHbU+SpzOIFIFcuV6mVPBXt/KQc5qhrE+NzCybzWI3A9fFW2XvVmsI3utb3VqnLT8UqzpUS6z2hMT6rqS3Udatm80cT6iI5ApIuaCpu/n8uTRiLpy3+186qE3SN9NYa0Lub6Tt1Oqez87oa32kXrDzvlGDrI/X6Ue6ILH0dJKAWoNKNs/cMK+GnNWcZ9Xde7UGn+fItgWDJufMm7PQPmQ/M5UHcgtIaUsNI1TRdqn1l1PVSDs9sClqbp3Qu0Rod9Pgy+A6+WgdPmYwWa1HT/yRN6GbeEdHIHuusZqDVgVe2H+xm3z+mW3nkSuep1QPqRRGC8HeGPhV0xZ2wmAgAw6dDTJwsoiHZTFUfn1x+0YL8NAo4I6PXMHndnsoSIwI2Cwc3f7uwNHOIsR1g5Op/nu9OA0wx+Pd49vDHA8R5tg76e2f9I5vDXM8GhweKpgjj+ATwRzj2MOfqREkOpXv4jjHvsM5Isb9oX3axzjWW4viG4F01JxRs61u0IobQwndsRTpkp6o3EMMy7g/gAkX8uJaZPVVxlU5U6l6dfryDM159sVfYGwF3g4FbT8Azdvfz9mbl3LRhzFnEzgkpmhd03LZVrKGApouGiHVNlREALU4kdo1AdOv+8/F4bvMkPl1LCDnKMvmbZCUbQDKfh05eqv4GdIzRiZI57trkmKAtnGg8yVvFii3sa6Sh7ghIgM8aiIrjChoOFdQU/Eg0s42QDYjdub5ek6VXl5lF4SvxuqVs/I6shlcgfKTJHlTCP+gUUmydyeERzKStwtOu8ivt4xRHmogIuZjsMI6NPIKlRf80NyA+EEtLrXvjHo1IY3kvFute99FuDYjhO269HGyLGU18XPVHWP9CDOYGR0dstGabY7Oa7Nn3iZ/KdfJcyIE7GiVMg+LfD3HbAz4uX8RuV7rPJXBkZeCl345IM7ElXik2crLi8xKQ56FHKXK3MYlf/0lWmwjHFIteShiG/D03kf+EvCj5gDDWoaU9vDU6Y/pO6+EpOqKztm3Mj8nUuSpLZaMmRNS2pn9tNgZ71AiCoF16hpdqFY2XvVOrjPvOjLv1CPUduVZ0DyzVZj3tmuObDpJvkqTSyNFv+7s/fvi68HR8dHBfucbnqND30Xz1V5qFgLaRPCtzvPBMEdOoE9mZjAa2eefYXaPRHrTWWWrUlWkwnkkwAgQ9iaTbyTyxX4FetQ3f/sbFn4G5O9bM0lDnqSff/5qD7/XJlUxYaeNDyuHc6DmlxRhyWFulM9uf8i60A/N7Z4ukBKg9tNW8TDWLoX9cIl75vyTEI5sYZhlnzHL5SL59BYHlJzADZIQO8/Mw2AlUQ5V0XY5DOxvD2S/TzHZB7eF/ZJEgYBaUNlAZz8QfoHijD40dOFWiJGT3+2r3wWkyWxf+bhIKipc+lT+WJ25smPpc2LRkjirYZxRRRi1p9srwvD/kv4haL29+7dVhO8/OHzg8n3sCD6RIlxXUIXQn4snztNFA6H/vtOBn+NDdTp/r42o5ntofvI4rXJXIhhdpujGNJIwkCdPyxKig9BdVA8+Ojg8Fj34sKmIR8DZ5BfEJQD0CEezQt8X3C6IrU6vzJ5KHT3ughLmYREjmon9vYPDfkGtM2QjHY/LNVcrWNpwqJFNeEh9oK9qV1Vh1LdGWyHGbILUmJXQ7foj43ThC3XbN6ZIVCkXNABPd/aOnQQyZirg8RNgTY194Ss3ECYaAUpqyunxb85I2OwkGfsxsawzyJ2RpAS5BqPq+tvshjyPEt3yXosiDwjF7YK5k16nQTsWI7dwrs4T3Atz9JP+8RznBsgKOZk1pgR6Oo/fLs0F7FY0uISeyCgYVqaqHQ3PnKhmzGfaQ6BuxRPv8jLnyZiLZn2y2dedcjrt7EBAYsnpCmZKO9DST6UxhVCwQ6R22QGnAkHQs9XYm5bn6fItIBikD863JGOvCJR4ehgI8oGAwXxrf/aH10ax4fyYUNElXvv1qrTFKgm/5i2g5bNR+8pn8YcN5J89V2RC+OyZjhUfbdpQT9ZmQwarI5ua28ldkLsxM8qakQ7xqwOWFDTF7MBeYF6TMgN7BPY0LhA62sF85YA+iZu7kmdx7TJsyHbeh5/jCtyLNu7f56gLweDIU/UVZBzuJndd9iEy9IOcyefGmuOK87a6Cm5XI6ev05t7rULDkzdW+iHD87McymjBP19Mp2Z+OiKFw0wAT3syGhcsCxqUMDugkJIcN5eMXW2HF6VgKWwvZCAg3n1RN9PCl7fR491APlDX5OuKCjKMSiFJPlUMeFBCHpbGrnYB19Ayv8ohc6BcL8dRVf+wAT3fPioIxLNz0rvhsAjGRX6FVC95sRKEpiIjwKbwhwubsbmNRVwP3p2vjMqyiqwwP+oKGJbmIFRwIj1phh5W5DFHqL/kSrikfUg0gYRuZAGuoQGY/lp2IkBsquQu81Zjpr+5pm1SijmH8+qenqztTHb/yJvzntw1t8sNIHbhv37dlHuRNpUIKV2peNB60TE1WqbLG0qqBGEwps81WkS/7IJDIPtmnvi+EVGjwvOCygTsGkfuzSY1e3G8wsd5hoVOuTspgZYBVPe1pVMOuzaqZj72lj9VlZnsxdhNnufmEqvK6UrdmfG7Oo3dHrCDwZBcZkaEF8kIRCI8hBW94u2g4iBJSv56EesHZEpNaZc8Cm6mfCpPSi3Vekkh25G7kvyN0g2vKBJWFQgyrwJr/YIJlAZrTAuESZI7iHRkhy8/1ByjScRVcGPYwtdK9RF9CmPrnCOHomsxy2G7oGphLKsimsob3u2krvo2/TnhzMiCJgmkKs6DvjeH0PW6yH194iHpJ0xo5OjdhPzrxzRIxbe5hVRp2l07kgN+RaSWdK/khNoAADPEeYKXm0kMiKLsXNRGQZx9lNcrY3xeGa29ynm8nhCF/4QzB9eaua4qBJVKcdZ//v0fT0qjlyXoAlxAlWHrrZ7KF5gwjczkIgkVVwcWIPN2rfYdRiqfsKyDU0OXWLgJTdeX2QyZgC+KssrcMijfq7ILIoVoOA8PjYTUoqWo6mBm+U7lJjXNzTkzGOhElsSX5zPTkZACmV6Zoc/TqqH3iAy/JFLOb8tyBT7sBRpsy4l1qGjgXORdYpgRnHjMXBPLzfHBwa53pTBQBjCUuKUTqmr5bgH5WAUzXoxrkHOuZ60PoN+mOvF4PTFBEUpPugcQjLQuigzWBw6pEVrkA7ZO2zNE5RhRcOIyR4ygBKca3dRQikHe1o/7o+m92DGqLjoWyaarT2KJrS0TS9JI/sW76CNcg/J+z4pC86x2TsRSVjBFBLHeUwmL8eWSRZ+v61ZeojJcSF17I2GWhhfLDi8RauW61FHsu0BZLqEzXE35gCu43GsfmHVo2BNP4A+CUqi6l1ZInKdTM/yoDNhc4goNlZogAL2Cin0iB7tvwJDrRDI5A5cDddB5Y/bA/Ma+Qyes4VnS8fBTLlQD5wujIy6nN1ZRIjmfztAltuKCVyXnCGFK4AzWcSKiZTd5aJ53uEVbvx0OkyfPWTJnsWFIpqZA6HGCAOJuhjCb5UTzCKY51iEpqdRi5O5c3Rj1FCupwnQDzR8Ki+gti/XpeAeZFd5h/xRuJfAAdCGiQBhrspAY9i9YD3YmRNoOV1mpBVzpTNSOigSxnMS7g91Bb/fdvdgea7hlWAImF1lpN7KY511eJGC4onkVAnlW/N3ev8twCildvlzPMP3M3OKVVAhg7iA6oufnz4ID5sYTajBd4AuhfcSfwIjlbuIEu0YTq6kXIpdxFhu8n3nPu+BM6SLh3uSeK+F7mc8yO1l5FXqp2hv/9Rc0HeROXUiFT4jG2zq/8LM/nr/4jr+MtezyrNX+dJ5JuqfBYjCC0XKvxQUsuwZhw0vCCbPHxTZ+u2xGcYypWy6ib04wnLcvEvZanT3aFMO8jZkH/HzHtzIPxd0ygNhd+Dt4CX3oyFRl1qAnTA1gYXfe0RfHGZ7oEeXgx960gVmxcuHastCaWEzS4w1n06w9arqdJL0qc2Rk+ZHr92EhahAUxmrkaBxQHtNfdGfuCF8uqDlVky6Fw4G6FmYqltk1bBuzug8v8yKNvqgjSOwr4kSooqy/6YfUiVqHAGgjESh+u1wbJWFW2WgmchUS4ZZERwMFZIe9TjAJNoC+QzaZCpDSklYcjdM+310d5PUiWVwgHXyY0qM3YTSh+JC4T8lYw6sLFSDiz9ptiIkd7h68V7Xr/aS/D5/0e0G1696mmNjg8Oj4cKCCYjyET0iC54WufqYe7IflxFwlXaJBXsYRYjo6ho8LViOIk7W1G42Y9Y+PLRkelhFA34rjmE6oFZdnTd7OOu/iDvsfd3RNYA9qhrWB/dFzaooxijHNx7qgzCDzi8JVc3JeHuI/iVbbftB3qLVIEMVmlQFsDZ0y5qAEF4LYNzzKZ6TxxTxqHraLaj/NS2c2gwThmYv8OvTv0kxjC0GIQyPDtqw14rjBPYha5K7lwL6RnrPM3gULSQBxbCz0InRlRJqhl8agWW1WZuAAwRfj5rkt1qUbVAvMRkN3tTNhY79rw83FrkuszGvafr7/3HeaxmBuDxrUyY2VG6KgufvxtYsS1ITMNAEBDV+MCsruqGsi2syJzdsG/5vcqrhJxLUEZiEDhYBgHOME8CQDNKON/vHcI+9hyOV8YYSsWXhdeY0DsgHTOi+o51NNHMb8e7jawbrSz0nl0AVXHE27P5krtPu/dpJe98FOcnmzMEOhCyoopBnr4hnU9ct/YoMgrymk/jjyqQyFjyj5a0kFEah24Sb2p3xBWTCx3VBjQ3COmEBAQuE2ShoyXZjrshg3UbPw6LJZpsBYWK39BrUiW/gaz7HlIs9X5IUVAYw17LL2PqAFbE63YhrIKVBupsLYgNMbdRO0UNnpeTlS2lMDEYNO3tQopcosdTYR/vHZ+6QE6pEceCMZeH/te3+xlodmoKMzBNSzBro2f09vKc54jB+gAYCjIMiAIKUOdrWlSz+pVOCVBqfMjjeO9sM90dfaaayRgQ+6Omc1jzhEWmFQEZ1vgDio92I9PkiMmnXw4GSwH6CgjjdpfMeDB/0DjYL6dOpemwb2M72gfK0wa3FM1AOVF6Cf9aFRTS1G9b39IzPzp4W6TlgLQA6Wb9ANd0meCc9NxkhHSfWp2BoXZzxubPEkVDdmlec7jH7iJEnGRRcEDBT7RK73yGs65D0qfbvM3xJb+OtFl4XyHjkbqj2ziY72en15zCzaFFoB7v1yeNz783Fvd1FcxPTI+/tHpEb2lBDomdNXIwua0k34piAakFJYdmxeTx6Q91mWUq0ScivPGP6EYVAii+C7euJSlECJUs6QWDuAgL/2EkNtbBLxvzYZC/KyyMXm5U8FQ1Rw0UFdreY+jWCe3TAQFjbJJQImXLLis6BsMJSSzEjHh7CgWnNRVXjF60M49pbkyPvL3Rd2bKwYm21+RRe5hXNy/NgcZsiQ552rQ3N+Bi60f+C1/8Rvnw4GqINmh60x7U2WKpvo5EKvSZKwhJOlQSDLIh4gOXM+2Bq1qWk+Fi5ZIg4rGQiPB8x8SbaY9LKvp0n9mz4/0wjphgRlWReh+8Gf0wWQF/kK1LtGJKyRnb33sPl7R93eMSaE9c0lENwA9zfdAPv3D48OH9groPdJLf4miQyG/8B9J6XDu2R1xy3/gbsDTqXU+GN+3jf921qO3wUPzMDR4ySUmOQHgCBb1BcAyZXlOqwbyanJwsP2+WcgtMYQB8ksN5zT//78/Bm3TA6BxXq5KMnD1VTjCuJyn3+GvGW5WNflNaNLJIzHk9QdIbcWZ76DP2GcgQ0KPsh3KXJsL1zsD8TM559JhbZL9k1SvUepKWNub0pljl0Wh31xOdSoRs78c+FJswY1qXfLQ3Lc7fVBTdrvwervh8UhNh6Sg+PDQb+v9KTeJ9ST2rYrHBTzunNKle4Kg0GXa5FvrBYh4opyrSUtUp2V1sajh2Vw0OfDsl559RoIPQZYBcz44OxuqZqOEEH4Assms4Kvq4XuiGcEjBlJ87eZypBMhaZukeGudmeVg+EUB09xJ/uiGrD/7xJk0FnOckZmw0+QOQw0Fd7sumyyCngHIh+80zqTx4x9mE++Puz194/7nW/+YA4khGb49RF9yHPQXWTL7oJJ04I3hISf2Lk66N1vPFhPSHkknlfHcoN6n63BIKqG7UdFIOO/pwWqrDyTFSTYLHjDjOibqYrT9MAWLVNkuPLjWHPZmtEGHgL2qYL6H+BJBIEKGwAcjygFrfMf+BZmrF15fZpdB+sKcRv27DAcJtrdC0SpVxkT9XsFlwnX83Gk16DbP0p6D076h+ZI3lZ6ma13NDj6L5JerQKE7bwpZPWDKtmVIFaDd//YSa4n8hvHJROx96ItR8XWsVmDZ9mKLnWZusQCS3wbiy50bpKQUEYW0E1J7sVomkxvn9NkBi4A1uKkRlgJyivuiT1wkGPFW09zJTYiy/zi23EHrf9+fodCrY7pYgGvQpCpYnTcIQYy2CbW9d5B0wejy8iNaX6x9g51Y3viJeSzCzIHkWVEWlF/054/rwkR5iXRwiMWDRPbTCTOyMNUYyuSdjnGIfWvkpURLUujyzDrHQc0xWgiEA7Nb+XitZH1qolyR67y9I2D8iWRkA08k1s3JCHKCBqPezXYx/WmHMrtmoi3rVXM2QgTiyyCd2w0g/d3+zbNUW9ycp94HUkAjQZb340Bwxl50D1nCFh0lmC1gb22bzMno+5ky9mqB9PwVvV06w3v47T663xykUWZW/o2CdPfu7HQ1ZtXzyrxpQuQERV5RWfv/z5GG8XkT+SEzqP1AvrWfekPqi5AtIsZw/asfokHKz4sJmBRUFXeqLTh/F0AuLnzZ3A/r5CNnD4WwPA4W4Jo4CjbdSYY8QkU1Mi4gM8fXr9+Cb3Af2szRVQOEw7vePKQ6x0o1BW2gGMpx+VMF0OMvZVcKLJzG46hP/PNuyxFxwtQO/jMbG0lMHTT96OLGttp1SIvCvPmEFlTEjmXqhXx5rfaMUIHLrtmmzk59stvhrFe7ktCHwrRQk4buD1DUCOfVqNz5kVk10VHEenzLCJzuWUqdsOugGDPKZ8SCFuECXAZlFjPR9GL3IkXoGVT0BIZAkx3xHu4m6jrl5qyCpO9nzBajadKwGFECWfXRw0vLHIVFxPEDNMw3zXmpkh1gUpB//OiW90UY7DfvM2fOXKcjc1yZVZbB3zm3QWBNxLes34BxN/UK7kjL8xSSy4cvkFjGL+wuecZJPNCID6ctQ3NCdAPIFBB3SEWnh5xvBKfPMmshrW2istq9zyXj6Di19AGfi04dC/Nuhmi4QgIeSBcETiQRHWp9+svOAIQ1tCv39MGGgf2Q1UuL/MASvhCvu49rx175DWk3WqsaBZCIhf4/9DiLzDBmdEALekFdGWRxKLXnqeTLEJeiVPK5mwmjkCrGIQ+km3HDn+8stvrHK9eyDjDK9DydBfdxXo0y8fJo+/O5bvW7UGJN2AuAhu495JTVWaNQyAWXtEwbFuWyQxb1dMABM2QQ1jV0DxhVJAhGIQuOYJJtJAZA4HmzARgRgVvjqoVaephOSotAZrSU4nlB6s0USlwNlw4RAX7SzceQvET3Yx5NWH+ktMNufHq57EJ1ziZMlL87DcVcWqTTkbAXWWNFga9HucyBFvWa+ox4CuepMX4BiAdPHxVMxoT1EA6kZ9nSowM3HM0HLYfWNvx9XKbP6VrmPhhgVLGPPhjBany4FNaV+jlJn0yerZaqHPDQ+m7usIwaKgA++yMG/oM3Fx43KzatldjNN5RfESoONn68dsJlHaLQAzj4BaM7V9OKBQ54etrSHTOS6KNIFgeq81sXgWnMEFp2MbNVXuLjYNnzR+SQgshNwVOC3u/klPUZvRjBjQHhEc35s5IKJ8rQ76v2E4ONCzhGZAhPjYaArb2ko10mQCztyJejkgPvTrjHDhZiVMbctHw2LEdC5BQ1Mlo/K/PEX7zAMlGH3rB6fok4fniDDCWA61F/EI2lrDGEDdL9NpeQoKWNQhjtdMF9m0ddmqPjrd9HI8/6QUqBNAm8x4Jq10i99NIchuHGZAIZnxPpbOhNInkrKNZbPJAJdJ4Qi22anO3MW0eA38WYBDKG3hrFyXxZ0olL2LtXhpHF67VCnmwuSYBBiwBq2FER0MmPziar9PlZM/8BP4ru6E2VH0WzXhUkmig9XrbucaOECLjmtXqyGAjN5aTJo/EbyyoRBCk36VXpotinbwqIbjhFwJqOBbqwgCp/MbJYuQBcQOurXKMghs00QFUeUi+3PisLlDlVRYS4J3zkGJCKDj7kf+Bty7a5IMvYwtRI4ZqHuvh7iCp1zWSydczpc3aVgyRNxQy4KKJF+ibfy8Y3hGAMOD/hRGabVh5D+7bAI0dwadDYUTjJD//TJOAFPgQkMF4TP/QxaPNoyBRMVhzGILyVJj5XJ6LwysOzeucEo0BnhjIzFyqHHAPq2CllWS+Q8DmIsMEyKWF2PmcfNVuQrhS8GBcl9hBdeIuCT+Emzj+lbtFieXemMpDF/QSd/g9m+cDer71rJzxo2hMz2aKeVIZ3ywLooHdI4uYOPCAtAde6pSG31rkHf913/vr2PvryPvr0PvLb3Pg/bXv/aVHop+jp5DUEhOkkNpzJsS6MAu4licB5WWv33twv/NNsGtOklMAzhn1JcuoHgMs/VP7QLDirPhAwFzjdzcP5tdfPu5oGtK8zOrKuWBKkqHIjT7JDSM2jFKzXN10+wdHg153tOru7xptBKFD5tijMME2IPo5NgMrqstyJc0cHcXFTxwMerjXO9gLzrcgQE1DRjW6LJfS2YvCzMDT8hxheJV8itM5ROBD8CbhEC6NHhd2ttcYBD8ECE/v/sng4GQ/wLltzm3bf/DgweBYBcFl3j+NjI0JTT9aHXviZ+I8Dz7vji/z2SQqdDVDOm9YyM6cTZqy2gYoemf5xeXqOoP/TbBxlrWapEeaw292Jb2gQtyFuJvRJqbkKHR+UCuY35RBhXrE93ANBKR4wZD5dRrHjA0APKDP8FeX+0adXmH8m04vPW+Ol/nm888ogANCnghqxXZEhZRJaT//zCgIGVDgmNNm/sqnyV0iRR0is2J1N7nzw8tHj4Ysye8k95Iv0vni9/g/yb9Km+5pzvQbTiaz4cz96F7yN1A6qJexhJbKoGcm3r6LfT4bfv/41fnZi+/u/PY+NSaoAYfS/7hy57gBmLK93KGN3qUd9/+ADDrq9g66/X7SPwI17yDMttgIxBkMDvbvaxjhJ9TyWuSIj/hrfu5nUAj7pt1qPbJSpBrOzYlEYYTaYQ9GtMgFTYgf5yeHRw/uWwVyHyb2phoy+l6NGYQgfyps0vZb6t38dqbS0aCD3snf+BukgQCraThe+N9I4AgSi3FQAxzJgcIMYwxIdpj9Q9PtginGswXkvbJ2XvZo+H0da8mHUYEo25OWYZCRDGXVMYJUWJ9Yyb/1A99TnQ184Er+vf+An+7S2+7pP0Bg1uZi0GfWYmE66BIXzB6rsfwb9frwZvyie5OJ4L7kQX8WBoOmfKCg/YYJOeDXMfJlPJVn9YzAzjayBydgT/6hHnTVSExTl2l1SXfQSec4PTy4nx1mg9Fx77i3fzgaHUzS/mAy7u1n04PeiPcm3FbmHlkN4SAiJ0qeVUOKcsOB7R8OHvQe9B8c7P+eSqEMjdwd9oe9IVo+5snO70cn/d///H8BivNI3g==', 'yes');
INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(604, 'widget_woof_widget', 'a:3:{i:2;a:10:{s:5:\"title\";s:27:\"WooCommerce Products Filter\";s:22:\"additional_text_before\";s:0:\"\";s:8:\"redirect\";s:0:\"\";s:24:\"woof_start_filtering_btn\";s:1:\"0\";s:11:\"ajax_redraw\";s:1:\"1\";s:12:\"btn_position\";s:1:\"b\";s:15:\"dynamic_recount\";s:2:\"-1\";s:10:\"autosubmit\";s:2:\"-1\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:21:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"4\";s:5:\"state\";s:2:\"ok\";}}i:3;a:10:{s:5:\"title\";s:27:\"WooCommerce Products Filter\";s:22:\"additional_text_before\";s:0:\"\";s:8:\"redirect\";s:0:\"\";s:24:\"woof_start_filtering_btn\";s:1:\"0\";s:11:\"ajax_redraw\";s:1:\"0\";s:12:\"btn_position\";s:1:\"b\";s:15:\"dynamic_recount\";s:2:\"-1\";s:10:\"autosubmit\";s:2:\"-1\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:21:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"5\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(605, 'woof_first_init', '1', 'yes'),
(606, 'woof_set_automatically', '0', 'yes'),
(607, 'woof_autosubmit', '1', 'yes'),
(608, 'woof_show_count', '1', 'yes'),
(609, 'woof_show_count_dynamic', '0', 'yes'),
(610, 'woof_hide_dynamic_empty_pos', '0', 'yes'),
(611, 'woof_try_ajax', '0', 'yes'),
(612, 'woof_checkboxes_slide', '1', 'yes'),
(613, 'woof_hide_red_top_panel', '0', 'yes'),
(614, 'woof_sort_terms_checked', '0', 'yes'),
(615, 'woof_filter_btn_txt', '', 'yes'),
(616, 'woof_reset_btn_txt', '', 'yes'),
(617, 'woof_settings', 'a:41:{s:11:\"items_order\";s:0:\"\";s:8:\"by_price\";a:7:{s:4:\"show\";s:1:\"4\";s:11:\"show_button\";s:1:\"0\";s:10:\"title_text\";s:0:\"\";s:6:\"ranges\";s:0:\"\";s:17:\"first_option_text\";s:0:\"\";s:15:\"ion_slider_step\";s:1:\"0\";s:9:\"price_tax\";s:1:\"0\";}s:8:\"tax_type\";a:6:{s:18:\"product_visibility\";s:5:\"radio\";s:11:\"product_cat\";s:8:\"checkbox\";s:11:\"product_tag\";s:5:\"radio\";s:7:\"pa_make\";s:6:\"select\";s:8:\"pa_model\";s:6:\"select\";s:8:\"pa_years\";s:6:\"select\";}s:14:\"excluded_terms\";a:6:{s:18:\"product_visibility\";s:0:\"\";s:11:\"product_cat\";s:0:\"\";s:11:\"product_tag\";s:0:\"\";s:7:\"pa_make\";s:0:\"\";s:8:\"pa_model\";s:0:\"\";s:8:\"pa_years\";s:0:\"\";}s:16:\"tax_block_height\";a:6:{s:18:\"product_visibility\";s:1:\"0\";s:11:\"product_cat\";s:1:\"0\";s:11:\"product_tag\";s:1:\"0\";s:7:\"pa_make\";s:1:\"0\";s:8:\"pa_model\";s:1:\"0\";s:8:\"pa_years\";s:1:\"0\";}s:16:\"show_title_label\";a:6:{s:18:\"product_visibility\";s:1:\"0\";s:11:\"product_cat\";s:1:\"0\";s:11:\"product_tag\";s:1:\"0\";s:7:\"pa_make\";s:1:\"0\";s:8:\"pa_model\";s:1:\"0\";s:8:\"pa_years\";s:1:\"0\";}s:18:\"show_toggle_button\";a:6:{s:18:\"product_visibility\";s:1:\"0\";s:11:\"product_cat\";s:1:\"0\";s:11:\"product_tag\";s:1:\"0\";s:7:\"pa_make\";s:1:\"0\";s:8:\"pa_model\";s:1:\"0\";s:8:\"pa_years\";s:1:\"0\";}s:13:\"dispay_in_row\";a:6:{s:18:\"product_visibility\";s:1:\"0\";s:11:\"product_cat\";s:1:\"0\";s:11:\"product_tag\";s:1:\"0\";s:7:\"pa_make\";s:1:\"0\";s:8:\"pa_model\";s:1:\"0\";s:8:\"pa_years\";s:1:\"0\";}s:16:\"custom_tax_label\";a:6:{s:18:\"product_visibility\";s:0:\"\";s:11:\"product_cat\";s:0:\"\";s:11:\"product_tag\";s:0:\"\";s:7:\"pa_make\";s:0:\"\";s:8:\"pa_model\";s:0:\"\";s:8:\"pa_years\";s:0:\"\";}s:23:\"not_toggled_terms_count\";a:6:{s:18:\"product_visibility\";s:0:\"\";s:11:\"product_cat\";s:0:\"\";s:11:\"product_tag\";s:0:\"\";s:7:\"pa_make\";s:0:\"\";s:8:\"pa_model\";s:0:\"\";s:8:\"pa_years\";s:0:\"\";}s:3:\"tax\";a:4:{s:11:\"product_cat\";s:1:\"1\";s:7:\"pa_make\";s:1:\"1\";s:8:\"pa_model\";s:1:\"1\";s:8:\"pa_years\";s:1:\"1\";}s:11:\"icheck_skin\";s:4:\"none\";s:12:\"overlay_skin\";s:7:\"default\";s:19:\"overlay_skin_bg_img\";s:0:\"\";s:18:\"plainoverlay_color\";s:0:\"\";s:25:\"default_overlay_skin_word\";s:0:\"\";s:10:\"use_chosen\";s:1:\"1\";s:17:\"use_beauty_scroll\";s:1:\"0\";s:15:\"ion_slider_skin\";s:8:\"skinNice\";s:25:\"woof_auto_hide_button_img\";s:0:\"\";s:25:\"woof_auto_hide_button_txt\";s:0:\"\";s:26:\"woof_auto_subcats_plus_img\";s:0:\"\";s:27:\"woof_auto_subcats_minus_img\";s:0:\"\";s:11:\"toggle_type\";s:4:\"text\";s:18:\"toggle_opened_text\";s:0:\"\";s:18:\"toggle_closed_text\";s:0:\"\";s:19:\"toggle_opened_image\";s:0:\"\";s:19:\"toggle_closed_image\";s:0:\"\";s:16:\"custom_front_css\";s:0:\"\";s:15:\"custom_css_code\";s:0:\"\";s:18:\"js_after_ajax_done\";s:0:\"\";s:12:\"init_only_on\";s:0:\"\";s:8:\"per_page\";s:2:\"-1\";s:17:\"optimize_js_files\";s:1:\"0\";s:25:\"listen_catalog_visibility\";s:1:\"0\";s:23:\"disable_swoof_influence\";s:1:\"0\";s:16:\"cache_count_data\";s:1:\"0\";s:11:\"cache_terms\";s:1:\"0\";s:19:\"show_woof_edit_view\";s:1:\"1\";s:22:\"custom_extensions_path\";s:0:\"\";s:20:\"activated_extensions\";s:0:\"\";}', 'yes'),
(618, 'woof_version', '1.2.1', 'yes'),
(619, 'woof_alert', 'a:2:{s:29:\"woocommerce_currency_switcher\";i:1;s:23:\"woocommerce_bulk_editor\";i:1;}', 'no'),
(620, '_transient_timeout_wc_order_115_needs_processing', '1539178376', 'no'),
(621, '_transient_wc_order_115_needs_processing', '1', 'no'),
(642, 'br_filters_version', '1.2.7', 'yes'),
(643, 'berocket_admin_notices', 'a:1:{i:20;a:1:{i:0;a:1:{i:0;a:1:{s:9:\"subscribe\";a:15:{s:5:\"start\";i:0;s:3:\"end\";i:0;s:4:\"name\";s:9:\"subscribe\";s:4:\"html\";s:136:\"Subscribe to get latest BeRocket news and updates, plugin recommendations and configuration help, promotional email with discount codes.\";s:9:\"righthtml\";s:43:\"<a class=\"berocket_no_thanks\">No thanks</a>\";s:10:\"rightwidth\";i:80;s:13:\"nothankswidth\";i:60;s:12:\"contentwidth\";i:400;s:9:\"subscribe\";b:1;s:6:\"closed\";i:2;s:8:\"priority\";i:20;s:6:\"height\";i:50;s:6:\"repeat\";b:0;s:11:\"repeatcount\";i:1;s:5:\"image\";a:4:{s:5:\"local\";s:125:\"http://localhost/wordpress-project/jess/wp-content/plugins/woocommerce-ajax-filters/includes/../images/ad_white_on_orange.png\";s:5:\"width\";i:239;s:6:\"height\";i:118;s:5:\"scale\";d:0.423728813559322;}}}}}}', 'yes'),
(645, 'widget_berocket_aapf_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(646, 'berocket_current_displayed_notice', '', 'yes'),
(647, 'berocket_updater_registered_plugins', '1', 'yes'),
(650, 'berocket_admin_notices_rate_stars', 'a:1:{i:1;a:2:{s:4:\"time\";i:1539698336;s:5:\"count\";i:0;}}', 'yes'),
(651, 'berocket_last_close_notices_time', '1539093551', 'yes'),
(660, '_transient_wc_attribute_taxonomies', 'a:3:{i:0;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"1\";s:14:\"attribute_name\";s:4:\"make\";s:15:\"attribute_label\";s:4:\"Make\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"1\";}i:1;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"3\";s:14:\"attribute_name\";s:5:\"model\";s:15:\"attribute_label\";s:5:\"Model\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"1\";}i:2;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"2\";s:14:\"attribute_name\";s:5:\"years\";s:15:\"attribute_label\";s:5:\"Years\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:10:\"menu_order\";s:16:\"attribute_public\";s:1:\"1\";}}', 'yes'),
(672, '_transient_timeout_wc_order_118_needs_processing', '1539183731', 'no'),
(673, '_transient_wc_order_118_needs_processing', '1', 'no'),
(684, 'breadcrumb_text', '', 'yes'),
(685, 'breadcrumb_separator', '>', 'yes'),
(686, 'breadcrumb_word_char', 'word', 'yes'),
(687, 'breadcrumb_word_char_count', '', 'yes'),
(688, 'breadcrumb_word_char_end', '', 'yes'),
(689, 'breadcrumb_font_size', '18px', 'yes'),
(690, 'breadcrumb_link_color', '#ffffff', 'yes'),
(691, 'breadcrumb_separator_color', '#ffffff', 'yes'),
(692, 'breadcrumb_bg_color', '', 'yes'),
(693, 'breadcrumb_themes', 'theme1', 'yes'),
(694, 'breadcrumb_display_home', 'yes', 'yes'),
(709, '_transient_timeout_wc_order_143_needs_processing', '1539188796', 'no'),
(710, '_transient_wc_order_143_needs_processing', '1', 'no'),
(722, '_transient_timeout_wc_low_stock_count', '1541741248', 'no'),
(723, '_transient_wc_low_stock_count', '0', 'no'),
(724, '_transient_timeout_wc_outofstock_count', '1541741248', 'no'),
(725, '_transient_wc_outofstock_count', '0', 'no'),
(726, '_site_transient_timeout_community-events-d41d8cd98f00b204e9800998ecf8427e', '1539216466', 'no'),
(727, '_site_transient_community-events-d41d8cd98f00b204e9800998ecf8427e', 'a:2:{s:8:\"location\";a:1:{s:2:\"ip\";b:0;}s:6:\"events\";a:0:{}}', 'no'),
(736, '_transient_timeout_wc_order_144_needs_processing', '1539242962', 'no'),
(737, '_transient_wc_order_144_needs_processing', '1', 'no'),
(743, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1539170029', 'no'),
(744, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4511;}s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:3051;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2593;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2449;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:1885;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1682;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1674;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1462;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1406;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1404;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1403;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1336;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1285;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1251;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1115;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1077;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1049;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1040;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:930;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:895;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:836;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:818;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:809;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:737;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:707;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:697;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:690;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:689;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:686;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:669;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:662;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:660;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:649;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:645;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:620;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:619;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:616;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:607;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:601;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:600;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:573;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:556;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:548;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:544;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:533;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:527;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:524;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:518;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:515;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:512;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:506;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:499;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:492;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:490;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:483;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:481;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:460;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:460;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:455;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:452;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:445;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:437;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:425;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:423;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:422;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:420;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:417;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:417;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:410;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:392;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:388;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:378;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:375;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:372;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:367;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:366;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:366;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:362;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:354;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:352;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:349;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:349;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:343;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:340;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:339;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:337;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:332;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:329;}s:11:\"advertising\";a:3:{s:4:\"name\";s:11:\"advertising\";s:4:\"slug\";s:11:\"advertising\";s:5:\"count\";i:313;}s:6:\"simple\";a:3:{s:4:\"name\";s:6:\"simple\";s:4:\"slug\";s:6:\"simple\";s:5:\"count\";i:310;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:310;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:309;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:305;}s:3:\"tag\";a:3:{s:4:\"name\";s:3:\"tag\";s:4:\"slug\";s:3:\"tag\";s:5:\"count\";i:304;}s:7:\"adsense\";a:3:{s:4:\"name\";s:7:\"adsense\";s:4:\"slug\";s:7:\"adsense\";s:5:\"count\";i:303;}s:4:\"html\";a:3:{s:4:\"name\";s:4:\"html\";s:4:\"slug\";s:4:\"html\";s:5:\"count\";i:302;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:301;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:301;}s:6:\"author\";a:3:{s:4:\"name\";s:6:\"author\";s:4:\"slug\";s:6:\"author\";s:5:\"count\";i:299;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:293;}}', 'no'),
(750, '_transient_timeout__woocommerce_helper_updates', '1539202450', 'no'),
(751, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1539159251;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(758, 'phoe_Ajax_Product_Filter_value', 'a:4:{s:11:\"enable_ajax\";s:1:\"1\";s:16:\"content_Selector\";s:9:\".products\";s:13:\"next_Selector\";s:5:\".next\";s:13:\"item_Selector\";s:8:\".product\";}', 'yes'),
(759, 'widget_ajax_product_filter', 'a:2:{i:2;a:19:{s:5:\"title\";s:0:\"\";s:9:\"attribute\";s:4:\"make\";s:10:\"query_type\";s:3:\"AND\";s:9:\"disp_type\";s:8:\"Checkbox\";s:9:\"Chevrolet\";s:0:\"\";s:5:\"Dodge\";s:0:\"\";s:4:\"Ford\";s:0:\"\";s:3:\"GMC\";s:0:\"\";s:16:\"F-350 Super Duty\";s:0:\"\";s:8:\"Ram 2500\";s:0:\"\";s:8:\"Ram 3500\";s:0:\"\";s:14:\"Sierra 2500 HD\";s:0:\"\";s:14:\"Sierra 3500 HD\";s:0:\"\";s:17:\"Silverado 3500 HD\";s:0:\"\";i:2016;s:0:\"\";i:2017;s:0:\"\";i:2018;s:0:\"\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:23:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:14:\"pt-latest_news\";a:0:{}s:16:\"pt-image_gallery\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:2:\"10\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(760, 'widget_ajax_product_average_rating', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(761, 'widget_ajax_product_category', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:9:\"prod_desc\";s:0:\"\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:23:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:14:\"pt-latest_news\";a:0:{}s:16:\"pt-image_gallery\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"8\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(762, 'widget_ajax_product_price', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(764, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1539173331;s:7:\"checked\";a:12:{s:30:\"advanced-custom-fields/acf.php\";s:5:\"5.7.7\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:5:\"1.2.0\";s:19:\"akismet/akismet.php\";s:5:\"4.0.8\";s:25:\"breadcrumb/breadcrumb.php\";s:5:\"1.5.0\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.0.3\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:5:\"1.5.8\";s:34:\"custom-sidebars/customsidebars.php\";s:5:\"3.2.1\";s:9:\"hello.php\";s:3:\"1.7\";s:33:\"smart-slider-3/smart-slider-3.php\";s:5:\"3.3.7\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.4.5\";s:20:\"wp-fancybox/main.php\";s:5:\"1.0.1\";s:31:\"wp-migrate-db/wp-migrate-db.php\";s:5:\"1.0.4\";}s:8:\"response\";a:1:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":13:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.0.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.0.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}s:14:\"upgrade_notice\";s:228:\"<p>This is a security and maintenance release and we strongly encourage you to update to it immediately. For more information, refer to the <a href=\"https://contactform7.com/category/releases/\">release announcement post</a>.</p>\";s:6:\"tested\";s:5:\"4.9.8\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:11:{s:30:\"advanced-custom-fields/acf.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:36:\"w.org/plugins/advanced-custom-fields\";s:4:\"slug\";s:22:\"advanced-custom-fields\";s:6:\"plugin\";s:30:\"advanced-custom-fields/acf.php\";s:11:\"new_version\";s:5:\"5.7.7\";s:3:\"url\";s:53:\"https://wordpress.org/plugins/advanced-custom-fields/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/advanced-custom-fields.5.7.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png?rev=1082746\";s:2:\"1x\";s:75:\"https://ps.w.org/advanced-custom-fields/assets/icon-128x128.png?rev=1082746\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";s:2:\"1x\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";}s:11:\"banners_rtl\";a:0:{}}s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/ajax-search-for-woocommerce\";s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:6:\"plugin\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:11:\"new_version\";s:5:\"1.2.0\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/ajax-search-for-woocommerce/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/ajax-search-for-woocommerce.1.2.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/ajax-search-for-woocommerce/assets/icon-256x256.png?rev=1419088\";s:2:\"1x\";s:80:\"https://ps.w.org/ajax-search-for-woocommerce/assets/icon-128x128.png?rev=1419088\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:82:\"https://ps.w.org/ajax-search-for-woocommerce/assets/banner-772x250.png?rev=1419088\";}s:11:\"banners_rtl\";a:0:{}}s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.0.8\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.0.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"breadcrumb/breadcrumb.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/breadcrumb\";s:4:\"slug\";s:10:\"breadcrumb\";s:6:\"plugin\";s:25:\"breadcrumb/breadcrumb.php\";s:11:\"new_version\";s:5:\"1.5.0\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/breadcrumb/\";s:7:\"package\";s:53:\"https://downloads.wordpress.org/plugin/breadcrumb.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/breadcrumb/assets/icon-128x128.png?rev=1363440\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/breadcrumb/assets/banner-772x250.png?rev=1363440\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"custom-post-type-ui/custom-post-type-ui.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:33:\"w.org/plugins/custom-post-type-ui\";s:4:\"slug\";s:19:\"custom-post-type-ui\";s:6:\"plugin\";s:43:\"custom-post-type-ui/custom-post-type-ui.php\";s:11:\"new_version\";s:5:\"1.5.8\";s:3:\"url\";s:50:\"https://wordpress.org/plugins/custom-post-type-ui/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/plugin/custom-post-type-ui.1.5.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-256x256.png?rev=1069557\";s:2:\"1x\";s:72:\"https://ps.w.org/custom-post-type-ui/assets/icon-128x128.png?rev=1069557\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-post-type-ui/assets/banner-1544x500.png?rev=1069557\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-post-type-ui/assets/banner-772x250.png?rev=1069557\";}s:11:\"banners_rtl\";a:0:{}}s:34:\"custom-sidebars/customsidebars.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/custom-sidebars\";s:4:\"slug\";s:15:\"custom-sidebars\";s:6:\"plugin\";s:34:\"custom-sidebars/customsidebars.php\";s:11:\"new_version\";s:5:\"3.2.1\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/custom-sidebars/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/custom-sidebars.3.2.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/custom-sidebars/assets/icon-256x256.png?rev=1414065\";s:2:\"1x\";s:68:\"https://ps.w.org/custom-sidebars/assets/icon-128x128.png?rev=1414065\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/custom-sidebars/assets/banner-1544x500.png?rev=1414065\";s:2:\"1x\";s:70:\"https://ps.w.org/custom-sidebars/assets/banner-772x250.png?rev=1414065\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:75:\"https://ps.w.org/custom-sidebars/assets/banner-1544x500-rtl.png?rev=1562672\";s:2:\"1x\";s:74:\"https://ps.w.org/custom-sidebars/assets/banner-772x250-rtl.png?rev=1562672\";}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=969907\";s:2:\"1x\";s:63:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=969907\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/hello-dolly/assets/banner-772x250.png?rev=478342\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"smart-slider-3/smart-slider-3.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/smart-slider-3\";s:4:\"slug\";s:14:\"smart-slider-3\";s:6:\"plugin\";s:33:\"smart-slider-3/smart-slider-3.php\";s:11:\"new_version\";s:5:\"3.3.7\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/smart-slider-3/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/smart-slider-3.3.3.7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/smart-slider-3/assets/icon-256x256.png?rev=1284893\";s:2:\"1x\";s:67:\"https://ps.w.org/smart-slider-3/assets/icon-128x128.png?rev=1284893\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/smart-slider-3/assets/banner-1544x500.png?rev=1902662\";s:2:\"1x\";s:69:\"https://ps.w.org/smart-slider-3/assets/banner-772x250.png?rev=1902662\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.4.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.4.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=1440831\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=1440831\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=1629184\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=1629184\";}s:11:\"banners_rtl\";a:0:{}}s:20:\"wp-fancybox/main.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/wp-fancybox\";s:4:\"slug\";s:11:\"wp-fancybox\";s:6:\"plugin\";s:20:\"wp-fancybox/main.php\";s:11:\"new_version\";s:5:\"1.0.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/wp-fancybox/\";s:7:\"package\";s:54:\"https://downloads.wordpress.org/plugin/wp-fancybox.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:64:\"https://ps.w.org/wp-fancybox/assets/icon-128x128.png?rev=1656914\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/wp-fancybox/assets/banner-772x250.png?rev=1656914\";}s:11:\"banners_rtl\";a:0:{}}s:31:\"wp-migrate-db/wp-migrate-db.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wp-migrate-db\";s:4:\"slug\";s:13:\"wp-migrate-db\";s:6:\"plugin\";s:31:\"wp-migrate-db/wp-migrate-db.php\";s:11:\"new_version\";s:5:\"1.0.4\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wp-migrate-db/\";s:7:\"package\";s:62:\"https://downloads.wordpress.org/plugin/wp-migrate-db.1.0.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-migrate-db/assets/icon-256x256.jpg?rev=1809889\";s:2:\"1x\";s:66:\"https://ps.w.org/wp-migrate-db/assets/icon-128x128.jpg?rev=1809889\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wp-migrate-db/assets/banner-1544x500.jpg?rev=1809889\";s:2:\"1x\";s:68:\"https://ps.w.org/wp-migrate-db/assets/banner-772x250.jpg?rev=1809889\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(765, 'fs_active_plugins', 'O:8:\"stdClass\":3:{s:7:\"plugins\";a:1:{s:34:\"ajax-search-for-woocommerce/fs/lib\";O:8:\"stdClass\":4:{s:7:\"version\";s:5:\"2.1.2\";s:4:\"type\";s:6:\"plugin\";s:9:\"timestamp\";i:1539175095;s:11:\"plugin_path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}}s:7:\"abspath\";s:39:\"E:\\xampp\\htdocs\\wordpress-project\\jess/\";s:6:\"newest\";O:8:\"stdClass\":5:{s:11:\"plugin_path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:8:\"sdk_path\";s:34:\"ajax-search-for-woocommerce/fs/lib\";s:7:\"version\";s:5:\"2.1.2\";s:13:\"in_activation\";b:0;s:9:\"timestamp\";i:1539175095;}}', 'yes'),
(766, 'fs_debug_mode', '', 'yes'),
(767, 'fs_accounts', 'a:6:{s:21:\"id_slug_type_path_map\";a:1:{i:700;a:3:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:4:\"type\";s:6:\"plugin\";s:4:\"path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}}s:11:\"plugin_data\";a:1:{s:27:\"ajax-search-for-woocommerce\";a:14:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:4:\"path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}s:17:\"install_timestamp\";i:1539159858;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.1.2\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:5:\"1.2.0\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:9:\"localhost\";s:9:\"server_ip\";s:3:\"::1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1539159858;s:7:\"version\";s:5:\"1.2.0\";}s:17:\"was_plugin_loaded\";b:1;s:15:\"prev_is_premium\";b:0;}}s:13:\"file_slug_map\";a:1:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:27:\"ajax-search-for-woocommerce\";}s:7:\"plugins\";a:1:{s:27:\"ajax-search-for-woocommerce\";O:9:\"FS_Plugin\":18:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:4:\"type\";s:6:\"plugin\";s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;s:4:\"file\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:7:\"version\";s:5:\"1.2.0\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_f4f2a51dbe0aee43de0692db77a3e\";s:10:\"secret_key\";N;s:2:\"id\";s:3:\"700\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:9:\"unique_id\";s:32:\"fb110241f9bef19f93be917053324c0c\";s:13:\"admin_notices\";a:1:{s:27:\"ajax-search-for-woocommerce\";a:0:{}}}', 'yes'),
(768, 'fs_api_cache', 'a:0:{}', 'yes'),
(769, 'fs_gdpr', 'a:1:{s:2:\"u1\";a:1:{s:8:\"required\";b:0;}}', 'yes'),
(772, 'widget_dgwt_wcas_ajax_search', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:14:\"csb_visibility\";a:3:{s:6:\"action\";s:4:\"show\";s:10:\"conditions\";a:23:{s:5:\"guest\";a:0:{}s:4:\"date\";a:0:{}s:5:\"roles\";a:0:{}s:9:\"pagetypes\";a:0:{}s:9:\"posttypes\";a:0:{}s:10:\"membership\";a:0:{}s:11:\"membership2\";a:0:{}s:7:\"prosite\";a:0:{}s:7:\"pt-post\";a:0:{}s:7:\"pt-page\";a:0:{}s:10:\"pt-product\";a:0:{}s:11:\"pt-partners\";a:0:{}s:14:\"pt-latest_news\";a:0:{}s:16:\"pt-image_gallery\";a:0:{}s:12:\"tax-category\";a:0:{}s:12:\"tax-post_tag\";a:0:{}s:15:\"tax-post_format\";a:0:{}s:15:\"tax-product_cat\";a:0:{}s:15:\"tax-product_tag\";a:0:{}s:26:\"tax-product_shipping_class\";a:0:{}s:11:\"tax-pa_make\";a:0:{}s:12:\"tax-pa_model\";a:0:{}s:12:\"tax-pa_years\";a:0:{}}s:6:\"always\";b:1;}s:9:\"csb_clone\";a:2:{s:5:\"group\";s:1:\"7\";s:5:\"state\";s:2:\"ok\";}}s:12:\"_multiwidget\";i:1;}', 'yes'),
(773, 'dgwt_wcas_activation_date', '1539159862', 'yes'),
(774, 'dgwt_wcas_settings', 'a:41:{s:10:\"how_to_use\";s:0:\"\";s:17:\"suggestions_limit\";i:10;s:9:\"min_chars\";i:3;s:14:\"max_form_width\";i:600;s:18:\"show_submit_button\";s:3:\"off\";s:18:\"search_submit_text\";s:6:\"Search\";s:18:\"search_placeholder\";s:22:\"Search for products...\";s:23:\"product_suggestion_head\";s:0:\"\";s:18:\"show_product_image\";s:3:\"off\";s:18:\"show_product_price\";s:3:\"off\";s:17:\"show_product_desc\";s:3:\"off\";s:16:\"show_product_sku\";s:3:\"off\";s:24:\"show_matching_categories\";s:2:\"on\";s:18:\"show_matching_tags\";s:3:\"off\";s:9:\"preloader\";s:0:\"\";s:14:\"show_preloader\";s:2:\"on\";s:13:\"preloader_url\";s:0:\"\";s:16:\"details_box_head\";s:0:\"\";s:16:\"show_details_box\";s:3:\"off\";s:12:\"show_for_tax\";s:2:\"on\";s:15:\"orderby_for_tax\";s:2:\"on\";s:13:\"order_for_tax\";s:4:\"desc\";s:11:\"search_form\";s:0:\"\";s:14:\"bg_input_color\";s:0:\"\";s:16:\"text_input_color\";s:0:\"\";s:18:\"border_input_color\";s:0:\"\";s:15:\"bg_submit_color\";s:0:\"\";s:17:\"text_submit_color\";s:0:\"\";s:22:\"syggestions_style_head\";s:0:\"\";s:12:\"sug_bg_color\";s:0:\"\";s:15:\"sug_hover_color\";s:0:\"\";s:14:\"sug_text_color\";s:0:\"\";s:19:\"sug_highlight_color\";s:0:\"\";s:16:\"sug_border_color\";s:0:\"\";s:21:\"search_engine_nw_head\";s:0:\"\";s:25:\"search_in_product_content\";s:3:\"off\";s:25:\"search_in_product_excerpt\";s:3:\"off\";s:21:\"search_in_product_sku\";s:3:\"off\";s:20:\"exclude_out_of_stock\";s:3:\"off\";s:18:\"search_engine_head\";s:0:\"\";s:13:\"search_engine\";s:6:\"native\";}', 'yes'),
(775, 'dgwt_wcas_version', '1.2.0', 'yes'),
(779, '_transient_timeout_wc_order_145_needs_processing', '1539248324', 'no'),
(780, '_transient_wc_order_145_needs_processing', '1', 'no'),
(782, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(796, 'product_cat_children', 'a:4:{i:31;a:6:{i:0;i:32;i:1;i:33;i:2;i:34;i:3;i:35;i:4;i:36;i:5;i:37;}i:38;a:6:{i:0;i:39;i:1;i:40;i:2;i:41;i:3;i:42;i:4;i:43;i:5;i:44;}i:45;a:7:{i:0;i:46;i:1;i:47;i:2;i:48;i:3;i:49;i:4;i:50;i:5;i:51;i:6;i:52;}i:57;a:2:{i:0;i:56;i:1;i:58;}}', 'yes'),
(803, '_transient_timeout_wc_report_sales_by_date', '1539259662', 'no'),
(804, '_transient_wc_report_sales_by_date', 'a:16:{s:32:\"8e0d670a65030cf18b0d79e0d8397eb3\";a:2:{i:0;O:8:\"stdClass\":2:{s:5:\"count\";s:2:\"13\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":2:{s:5:\"count\";s:1:\"2\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"0c608077f97952639f2d1ccbe251438f\";a:0:{}s:32:\"6fc48469fdfc218ded956141e1707e9b\";a:2:{i:0;O:8:\"stdClass\":2:{s:16:\"order_item_count\";s:2:\"54\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":2:{s:16:\"order_item_count\";s:1:\"2\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"30f4b14d473441fa58a6af3c3d4bfb79\";N;s:32:\"0c92859b78d2353ef079470f109028f2\";a:2:{i:0;O:8:\"stdClass\":5:{s:11:\"total_sales\";s:4:\"7380\";s:14:\"total_shipping\";s:1:\"0\";s:9:\"total_tax\";s:1:\"0\";s:18:\"total_shipping_tax\";s:1:\"0\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":5:{s:11:\"total_sales\";s:3:\"350\";s:14:\"total_shipping\";s:1:\"0\";s:9:\"total_tax\";s:1:\"0\";s:18:\"total_shipping_tax\";s:1:\"0\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"6e2084c69994f178b46b7c34fbed72b0\";a:0:{}s:32:\"077f8ff353d1bed7336f5a34a18dbbf6\";a:0:{}s:32:\"52c02bcc5dc94ba7807e48bcf08dd7f4\";a:0:{}s:32:\"bd0e85246a33d4aa909f73ac71e739a3\";a:2:{i:0;O:8:\"stdClass\":2:{s:5:\"count\";s:2:\"13\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":2:{s:5:\"count\";s:1:\"2\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"65faf82a16f4418b28d12694b373d282\";a:0:{}s:32:\"1f11320bd2e5553c632fe7d37bf706ac\";a:2:{i:0;O:8:\"stdClass\":2:{s:16:\"order_item_count\";s:2:\"54\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":2:{s:16:\"order_item_count\";s:1:\"2\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"bb48c7921918f502581f8ec60a692064\";N;s:32:\"4fa52eaac9a8c7a82fc9d2a40407cb95\";a:2:{i:0;O:8:\"stdClass\":5:{s:11:\"total_sales\";s:4:\"7380\";s:14:\"total_shipping\";s:1:\"0\";s:9:\"total_tax\";s:1:\"0\";s:18:\"total_shipping_tax\";s:1:\"0\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":5:{s:11:\"total_sales\";s:3:\"350\";s:14:\"total_shipping\";s:1:\"0\";s:9:\"total_tax\";s:1:\"0\";s:18:\"total_shipping_tax\";s:1:\"0\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"edddaa1fb7c179459c9b5790fe259fc1\";a:0:{}s:32:\"b9ddd8cabd41329b39f56b32448580bc\";a:0:{}s:32:\"e4ca0979fe06698f4b6067f6048931ae\";a:0:{}}', 'no'),
(805, '_transient_timeout_wc_admin_report', '1539256441', 'no'),
(806, '_transient_wc_admin_report', 'a:2:{s:32:\"0d865ae31619110d5a4c6083f86d4796\";a:2:{i:0;O:8:\"stdClass\":2:{s:15:\"sparkline_value\";s:4:\"7380\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":2:{s:15:\"sparkline_value\";s:3:\"350\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}s:32:\"7d630ff0d99e2f0d0724d660b01fc15f\";a:2:{i:0;O:8:\"stdClass\":3:{s:10:\"product_id\";s:2:\"33\";s:15:\"sparkline_value\";s:2:\"30\";s:9:\"post_date\";s:19:\"2018-10-09 06:16:55\";}i:1;O:8:\"stdClass\":3:{s:10:\"product_id\";s:2:\"33\";s:15:\"sparkline_value\";s:1:\"1\";s:9:\"post_date\";s:19:\"2018-10-10 07:29:19\";}}}', 'no'),
(811, '_site_transient_timeout_theme_roots', '1539175102', 'no'),
(812, '_site_transient_theme_roots', 'a:4:{s:4:\"Jess\";s:7:\"/themes\";s:13:\"twentyfifteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}', 'no'),
(813, '_transient_is_multi_author', '0', 'yes'),
(816, 'wpmdb_state_timeout_5bbded398f234', '1539260105', 'no'),
(817, 'wpmdb_state_5bbded398f234', 'a:22:{s:6:\"action\";s:19:\"wpmdb_migrate_table\";s:6:\"intent\";s:8:\"savefile\";s:3:\"url\";s:0:\"\";s:9:\"form_data\";s:321:\"action=savefile&save_computer=1&connection_info=&replace_old%5B%5D=&replace_new%5B%5D=&replace_old%5B%5D=%2F%2Flocalhost%2Fwordpress-project%2Fjess&replace_new%5B%5D=%2F%2F18.191.53.95%2Fdev%2Fwordpress%2Fjess%2F&replace_guids=1&exclude_transients=1&save_migration_profile_option=new&create_new_profile=&remote_json_data=\";s:5:\"stage\";s:7:\"migrate\";s:5:\"nonce\";s:10:\"4763f968bb\";s:12:\"site_details\";a:1:{s:5:\"local\";a:10:{s:12:\"is_multisite\";s:5:\"false\";s:8:\"site_url\";s:39:\"http://localhost/wordpress-project/jess\";s:8:\"home_url\";s:39:\"http://localhost/wordpress-project/jess\";s:6:\"prefix\";s:3:\"wp_\";s:15:\"uploads_baseurl\";s:59:\"http://localhost/wordpress-project/jess/wp-content/uploads/\";s:7:\"uploads\";a:6:{s:4:\"path\";s:65:\"E:\\xampp\\htdocs\\wordpress-project\\jess/wp-content/uploads/2018/10\";s:3:\"url\";s:66:\"http://localhost/wordpress-project/jess/wp-content/uploads/2018/10\";s:6:\"subdir\";s:8:\"/2018/10\";s:7:\"basedir\";s:57:\"E:\\xampp\\htdocs\\wordpress-project\\jess/wp-content/uploads\";s:7:\"baseurl\";s:58:\"http://localhost/wordpress-project/jess/wp-content/uploads\";s:5:\"error\";b:0;}s:11:\"uploads_dir\";s:33:\"wp-content/uploads/wp-migrate-db/\";s:8:\"subsites\";a:0:{}s:13:\"subsites_info\";a:0:{}s:20:\"is_subdomain_install\";s:5:\"false\";}}s:4:\"code\";i:200;s:7:\"message\";s:2:\"OK\";s:4:\"body\";s:11:\"{\"error\":0}\";s:9:\"dump_path\";s:110:\"E:\\xampp\\htdocs\\wordpress-project\\jess\\wp-content\\uploads\\wp-migrate-db\\js_08-migrate-20181010121449-jx6qf.sql\";s:13:\"dump_filename\";s:34:\"js_08-migrate-20181010121449-jx6qf\";s:8:\"dump_url\";s:111:\"http://localhost/wordpress-project/jess/wp-content/uploads/wp-migrate-db/js_08-migrate-20181010121449-jx6qf.sql\";s:10:\"db_version\";s:5:\"5.5.5\";s:8:\"site_url\";s:39:\"http://localhost/wordpress-project/jess\";s:18:\"find_replace_pairs\";a:2:{s:11:\"replace_old\";a:1:{i:1;s:34:\"//localhost/wordpress-project/jess\";}s:11:\"replace_new\";a:1:{i:1;s:34:\"//18.191.53.95/dev/wordpress/jess/\";}}s:18:\"migration_state_id\";s:13:\"5bbded398f234\";s:5:\"table\";s:24:\"wp_woocommerce_tax_rates\";s:11:\"current_row\";s:0:\"\";s:10:\"last_table\";s:1:\"1\";s:12:\"primary_keys\";s:0:\"\";s:4:\"gzip\";s:1:\"0\";}', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', '2018/09/logo.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1695;s:6:\"height\";i:788;s:4:\"file\";s:16:\"2018/09/logo.png\";s:5:\"sizes\";a:5:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:16:\"logo-300x139.png\";s:5:\"width\";i:300;s:6:\"height\";i:139;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:16:\"logo-768x357.png\";s:5:\"width\";i:768;s:6:\"height\";i:357;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:17:\"logo-1024x476.png\";s:5:\"width\";i:1024;s:6:\"height\";i:476;s:9:\"mime-type\";s:9:\"image/png\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 16, '_edit_last', '1'),
(38, 16, '_edit_lock', '1538119390:1'),
(39, 19, '_edit_last', '1'),
(40, 19, '_edit_lock', '1539094389:1'),
(41, 21, '_edit_last', '1'),
(42, 21, '_edit_lock', '1539062828:1'),
(43, 23, '_edit_last', '1'),
(44, 23, '_edit_lock', '1539062873:1'),
(45, 25, '_edit_last', '1'),
(46, 25, '_edit_lock', '1539063420:1'),
(47, 29, '_wc_review_count', '0'),
(48, 29, '_wc_rating_count', 'a:0:{}'),
(49, 29, '_wc_average_rating', '0'),
(50, 29, '_edit_last', '1'),
(51, 29, '_edit_lock', '1539070720:1'),
(52, 30, '_wp_attached_file', '2018/10/YTP70.jpg'),
(53, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:1400;s:4:\"file\";s:17:\"2018/10/YTP70.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"YTP70-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"YTP70-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"YTP70-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"YTP70-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:19:\"YTP70-1400x1200.jpg\";s:5:\"width\";i:1400;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:17:\"YTP70-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"YTP70-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"YTP70-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"YTP70-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"YTP70-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"YTP70-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"YTP70-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:27:\"Canon EOS DIGITAL REBEL XSi\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1284989489\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"18\";s:3:\"iso\";s:3:\"400\";s:13:\"shutter_speed\";s:5:\"0.005\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(54, 29, '_thumbnail_id', '30'),
(55, 29, '_sku', ''),
(56, 29, '_regular_price', '60'),
(57, 29, '_sale_price', ''),
(58, 29, '_sale_price_dates_from', ''),
(59, 29, '_sale_price_dates_to', ''),
(60, 29, 'total_sales', '8'),
(61, 29, '_tax_status', 'taxable'),
(62, 29, '_tax_class', ''),
(63, 29, '_manage_stock', 'no'),
(64, 29, '_backorders', 'no'),
(65, 29, '_sold_individually', 'no'),
(66, 29, '_weight', ''),
(67, 29, '_length', ''),
(68, 29, '_width', ''),
(69, 29, '_height', ''),
(70, 29, '_upsell_ids', 'a:0:{}'),
(71, 29, '_crosssell_ids', 'a:0:{}'),
(72, 29, '_purchase_note', ''),
(73, 29, '_default_attributes', 'a:0:{}'),
(74, 29, '_virtual', 'no'),
(75, 29, '_downloadable', 'no'),
(76, 29, '_product_image_gallery', ''),
(77, 29, '_download_limit', '-1'),
(78, 29, '_download_expiry', '-1'),
(79, 29, '_stock', NULL),
(80, 29, '_stock_status', 'instock'),
(81, 29, '_product_version', '3.4.5'),
(82, 29, '_price', '60'),
(83, 31, '_wc_review_count', '0'),
(84, 31, '_wc_rating_count', 'a:0:{}'),
(85, 31, '_wc_average_rating', '0'),
(86, 31, '_edit_last', '1'),
(87, 31, '_edit_lock', '1539070684:1'),
(88, 32, '_wp_attached_file', '2018/10/YTP09.jpg'),
(89, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1400;s:6:\"height\";i:1400;s:4:\"file\";s:17:\"2018/10/YTP09.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"YTP09-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"YTP09-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:17:\"YTP09-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"YTP09-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:19:\"YTP09-1400x1200.jpg\";s:5:\"width\";i:1400;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:17:\"YTP09-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"YTP09-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"YTP09-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"YTP09-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"YTP09-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"YTP09-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"YTP09-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(90, 31, '_thumbnail_id', '32'),
(91, 31, '_sku', ''),
(92, 31, '_regular_price', '100'),
(93, 31, '_sale_price', ''),
(94, 31, '_sale_price_dates_from', ''),
(95, 31, '_sale_price_dates_to', ''),
(96, 31, 'total_sales', '8'),
(97, 31, '_tax_status', 'taxable'),
(98, 31, '_tax_class', ''),
(99, 31, '_manage_stock', 'no'),
(100, 31, '_backorders', 'no'),
(101, 31, '_sold_individually', 'no'),
(102, 31, '_weight', ''),
(103, 31, '_length', ''),
(104, 31, '_width', ''),
(105, 31, '_height', ''),
(106, 31, '_upsell_ids', 'a:0:{}'),
(107, 31, '_crosssell_ids', 'a:0:{}'),
(108, 31, '_purchase_note', ''),
(109, 31, '_default_attributes', 'a:0:{}'),
(110, 31, '_virtual', 'no'),
(111, 31, '_downloadable', 'no'),
(112, 31, '_product_image_gallery', ''),
(113, 31, '_download_limit', '-1'),
(114, 31, '_download_expiry', '-1'),
(115, 31, '_stock', NULL),
(116, 31, '_stock_status', 'instock'),
(117, 31, '_product_version', '3.4.5'),
(118, 31, '_price', '100'),
(119, 33, '_wc_review_count', '0'),
(120, 33, '_wc_rating_count', 'a:0:{}'),
(121, 33, '_wc_average_rating', '0'),
(122, 33, '_edit_last', '1'),
(123, 33, '_edit_lock', '1539089322:1'),
(124, 34, '_wp_attached_file', '2018/10/851062_1.jpg'),
(125, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1500;s:6:\"height\";i:1500;s:4:\"file\";s:20:\"2018/10/851062_1.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"851062_1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"851062_1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"851062_1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"851062_1-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:22:\"851062_1-1500x1200.jpg\";s:5:\"width\";i:1500;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"851062_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"851062_1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"851062_1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"851062_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"851062_1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"851062_1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"851062_1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(126, 35, '_wp_attached_file', '2018/10/851063.jpg'),
(127, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1500;s:6:\"height\";i:1500;s:4:\"file\";s:18:\"2018/10/851063.jpg\";s:5:\"sizes\";a:12:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"851063-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:18:\"851063-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"851063-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:20:\"851063-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:30:\"twentyseventeen-featured-image\";a:4:{s:4:\"file\";s:20:\"851063-1500x1200.jpg\";s:5:\"width\";i:1500;s:6:\"height\";i:1200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:18:\"851063-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"851063-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:18:\"851063-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"851063-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"851063-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:18:\"851063-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"851063-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(128, 33, '_thumbnail_id', '35'),
(129, 33, '_sku', ''),
(130, 33, '_regular_price', '150'),
(131, 33, '_sale_price', ''),
(132, 33, '_sale_price_dates_from', ''),
(133, 33, '_sale_price_dates_to', ''),
(134, 33, 'total_sales', '31'),
(135, 33, '_tax_status', 'taxable'),
(136, 33, '_tax_class', ''),
(137, 33, '_manage_stock', 'no'),
(138, 33, '_backorders', 'no'),
(139, 33, '_sold_individually', 'no'),
(140, 33, '_weight', ''),
(141, 33, '_length', ''),
(142, 33, '_width', ''),
(143, 33, '_height', ''),
(144, 33, '_upsell_ids', 'a:0:{}'),
(145, 33, '_crosssell_ids', 'a:0:{}'),
(146, 33, '_purchase_note', ''),
(147, 33, '_default_attributes', 'a:0:{}'),
(148, 33, '_virtual', 'no'),
(149, 33, '_downloadable', 'no'),
(150, 33, '_product_image_gallery', ''),
(151, 33, '_download_limit', '-1'),
(152, 33, '_download_expiry', '-1'),
(153, 33, '_stock', NULL),
(154, 33, '_stock_status', 'instock'),
(155, 33, '_product_version', '3.4.5'),
(156, 33, '_price', '150'),
(157, 36, '_wc_review_count', '0'),
(158, 36, '_wc_rating_count', 'a:0:{}'),
(159, 36, '_wc_average_rating', '0'),
(160, 36, '_edit_last', '1'),
(161, 36, '_edit_lock', '1539088086:1'),
(162, 36, '_thumbnail_id', '34'),
(163, 36, '_sku', ''),
(164, 36, '_regular_price', '200'),
(165, 36, '_sale_price', ''),
(166, 36, '_sale_price_dates_from', ''),
(167, 36, '_sale_price_dates_to', ''),
(168, 36, 'total_sales', '9'),
(169, 36, '_tax_status', 'taxable'),
(170, 36, '_tax_class', ''),
(171, 36, '_manage_stock', 'no'),
(172, 36, '_backorders', 'no'),
(173, 36, '_sold_individually', 'no'),
(174, 36, '_weight', ''),
(175, 36, '_length', ''),
(176, 36, '_width', ''),
(177, 36, '_height', ''),
(178, 36, '_upsell_ids', 'a:0:{}'),
(179, 36, '_crosssell_ids', 'a:0:{}'),
(180, 36, '_purchase_note', ''),
(181, 36, '_default_attributes', 'a:0:{}'),
(182, 36, '_virtual', 'no'),
(183, 36, '_downloadable', 'no'),
(184, 36, '_product_image_gallery', ''),
(185, 36, '_download_limit', '-1'),
(186, 36, '_download_expiry', '-1'),
(187, 36, '_stock', NULL),
(188, 36, '_stock_status', 'instock'),
(189, 36, '_product_version', '3.4.5'),
(190, 36, '_price', '200'),
(191, 37, '_order_key', 'wc_order_5bbc47d77013f'),
(192, 37, '_customer_user', '1'),
(193, 37, '_payment_method', 'cod'),
(194, 37, '_payment_method_title', 'Cash on delivery'),
(195, 37, '_transaction_id', ''),
(196, 37, '_customer_ip_address', '::1'),
(197, 37, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(198, 37, '_created_via', 'checkout'),
(199, 37, '_date_completed', ''),
(200, 37, '_completed_date', ''),
(201, 37, '_date_paid', ''),
(202, 37, '_paid_date', ''),
(203, 37, '_cart_hash', 'a2bd953d34e0cdafefa1565721b7198f'),
(204, 37, '_billing_first_name', 'Martina'),
(205, 37, '_billing_last_name', 'Erickson'),
(206, 37, '_billing_company', 'Valentine and Sloan LLC'),
(207, 37, '_billing_address_1', '606 Rocky Second Street'),
(208, 37, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(209, 37, '_billing_city', 'ahmedab'),
(210, 37, '_billing_state', 'AP'),
(211, 37, '_billing_postcode', '78965-411'),
(212, 37, '_billing_country', 'BR'),
(213, 37, '_billing_email', 'qogybyzud@mailinator.net'),
(214, 37, '_billing_phone', '+442-68-9152410'),
(215, 37, '_shipping_first_name', ''),
(216, 37, '_shipping_last_name', ''),
(217, 37, '_shipping_company', ''),
(218, 37, '_shipping_address_1', ''),
(219, 37, '_shipping_address_2', ''),
(220, 37, '_shipping_city', ''),
(221, 37, '_shipping_state', ''),
(222, 37, '_shipping_postcode', ''),
(223, 37, '_shipping_country', ''),
(224, 37, '_order_currency', 'GBP'),
(225, 37, '_cart_discount', '0'),
(226, 37, '_cart_discount_tax', '0'),
(227, 37, '_order_shipping', '0.00'),
(228, 37, '_order_shipping_tax', '0'),
(229, 37, '_order_tax', '0'),
(230, 37, '_order_total', '1550.00'),
(231, 37, '_order_version', '3.4.5'),
(232, 37, '_prices_include_tax', 'no'),
(233, 37, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(234, 37, '_shipping_address_index', '        '),
(235, 37, '_download_permissions_granted', 'yes'),
(236, 37, '_recorded_sales', 'yes'),
(237, 37, '_recorded_coupon_usage_counts', 'yes'),
(238, 37, '_order_stock_reduced', 'yes'),
(239, 38, '_order_key', 'wc_order_5bbc4b2cef652'),
(240, 38, '_customer_user', '1'),
(241, 38, '_payment_method', 'cod'),
(242, 38, '_payment_method_title', 'Cash on delivery'),
(243, 38, '_transaction_id', ''),
(244, 38, '_customer_ip_address', '::1'),
(245, 38, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(246, 38, '_created_via', 'checkout'),
(247, 38, '_date_completed', ''),
(248, 38, '_completed_date', ''),
(249, 38, '_date_paid', ''),
(250, 38, '_paid_date', ''),
(251, 38, '_cart_hash', '19d46925d376e9f40538627a613844cd'),
(252, 38, '_billing_first_name', 'Martina'),
(253, 38, '_billing_last_name', 'Erickson'),
(254, 38, '_billing_company', 'Valentine and Sloan LLC'),
(255, 38, '_billing_address_1', '606 Rocky Second Street'),
(256, 38, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(257, 38, '_billing_city', 'ahmedab'),
(258, 38, '_billing_state', 'AP'),
(259, 38, '_billing_postcode', '78965-411'),
(260, 38, '_billing_country', 'BR'),
(261, 38, '_billing_email', 'qogybyzud@mailinator.net'),
(262, 38, '_billing_phone', '+442-68-9152410'),
(263, 38, '_shipping_first_name', ''),
(264, 38, '_shipping_last_name', ''),
(265, 38, '_shipping_company', ''),
(266, 38, '_shipping_address_1', ''),
(267, 38, '_shipping_address_2', ''),
(268, 38, '_shipping_city', ''),
(269, 38, '_shipping_state', ''),
(270, 38, '_shipping_postcode', ''),
(271, 38, '_shipping_country', ''),
(272, 38, '_order_currency', 'GBP'),
(273, 38, '_cart_discount', '0'),
(274, 38, '_cart_discount_tax', '0'),
(275, 38, '_order_shipping', '0.00'),
(276, 38, '_order_shipping_tax', '0'),
(277, 38, '_order_tax', '0'),
(278, 38, '_order_total', '360.00'),
(279, 38, '_order_version', '3.4.5'),
(280, 38, '_prices_include_tax', 'no'),
(281, 38, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(282, 38, '_shipping_address_index', '        '),
(283, 38, '_download_permissions_granted', 'yes'),
(284, 38, '_recorded_sales', 'yes'),
(285, 38, '_recorded_coupon_usage_counts', 'yes'),
(286, 38, '_order_stock_reduced', 'yes'),
(287, 39, '_order_key', 'wc_order_5bbc4cda38600'),
(288, 39, '_customer_user', '1'),
(289, 39, '_payment_method', 'cod'),
(290, 39, '_payment_method_title', 'Cash on delivery'),
(291, 39, '_transaction_id', ''),
(292, 39, '_customer_ip_address', '::1'),
(293, 39, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(294, 39, '_created_via', 'checkout'),
(295, 39, '_date_completed', ''),
(296, 39, '_completed_date', ''),
(297, 39, '_date_paid', ''),
(298, 39, '_paid_date', ''),
(299, 39, '_cart_hash', 'd9582bad22100d5e0372a452b09d6626'),
(300, 39, '_billing_first_name', 'Martina'),
(301, 39, '_billing_last_name', 'Erickson'),
(302, 39, '_billing_company', 'Valentine and Sloan LLC'),
(303, 39, '_billing_address_1', '606 Rocky Second Street'),
(304, 39, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(305, 39, '_billing_city', 'ahmedab'),
(306, 39, '_billing_state', 'AP'),
(307, 39, '_billing_postcode', '78965-411'),
(308, 39, '_billing_country', 'BR'),
(309, 39, '_billing_email', 'qogybyzud@mailinator.net'),
(310, 39, '_billing_phone', '+442-68-9152410'),
(311, 39, '_shipping_first_name', ''),
(312, 39, '_shipping_last_name', ''),
(313, 39, '_shipping_company', ''),
(314, 39, '_shipping_address_1', ''),
(315, 39, '_shipping_address_2', ''),
(316, 39, '_shipping_city', ''),
(317, 39, '_shipping_state', ''),
(318, 39, '_shipping_postcode', ''),
(319, 39, '_shipping_country', ''),
(320, 39, '_order_currency', 'GBP'),
(321, 39, '_cart_discount', '0'),
(322, 39, '_cart_discount_tax', '0'),
(323, 39, '_order_shipping', '0.00'),
(324, 39, '_order_shipping_tax', '0'),
(325, 39, '_order_tax', '0'),
(326, 39, '_order_total', '1050.00'),
(327, 39, '_order_version', '3.4.5'),
(328, 39, '_prices_include_tax', 'no'),
(329, 39, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(330, 39, '_shipping_address_index', '        '),
(331, 39, '_download_permissions_granted', 'yes'),
(332, 39, '_recorded_sales', 'yes'),
(333, 39, '_recorded_coupon_usage_counts', 'yes'),
(334, 39, '_order_stock_reduced', 'yes'),
(335, 40, '_order_key', 'wc_order_5bbc51e65f0ec'),
(336, 40, '_customer_user', '1'),
(337, 40, '_payment_method', 'cod'),
(338, 40, '_payment_method_title', 'Cash on delivery'),
(339, 40, '_transaction_id', ''),
(340, 40, '_customer_ip_address', '::1'),
(341, 40, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(342, 40, '_created_via', 'checkout'),
(343, 40, '_date_completed', ''),
(344, 40, '_completed_date', ''),
(345, 40, '_date_paid', ''),
(346, 40, '_paid_date', ''),
(347, 40, '_cart_hash', '25b6169071563e9ca410648a73a111e6'),
(348, 40, '_billing_first_name', 'Martina'),
(349, 40, '_billing_last_name', 'Erickson'),
(350, 40, '_billing_company', 'Valentine and Sloan LLC'),
(351, 40, '_billing_address_1', '606 Rocky Second Street'),
(352, 40, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(353, 40, '_billing_city', 'ahmedab'),
(354, 40, '_billing_state', 'AP'),
(355, 40, '_billing_postcode', '78965-411'),
(356, 40, '_billing_country', 'BR'),
(357, 40, '_billing_email', 'qogybyzud@mailinator.net'),
(358, 40, '_billing_phone', '+442-68-9152410'),
(359, 40, '_shipping_first_name', ''),
(360, 40, '_shipping_last_name', ''),
(361, 40, '_shipping_company', ''),
(362, 40, '_shipping_address_1', ''),
(363, 40, '_shipping_address_2', ''),
(364, 40, '_shipping_city', ''),
(365, 40, '_shipping_state', ''),
(366, 40, '_shipping_postcode', ''),
(367, 40, '_shipping_country', ''),
(368, 40, '_order_currency', 'GBP'),
(369, 40, '_cart_discount', '0'),
(370, 40, '_cart_discount_tax', '0'),
(371, 40, '_order_shipping', '0.00'),
(372, 40, '_order_shipping_tax', '0'),
(373, 40, '_order_tax', '0'),
(374, 40, '_order_total', '600.00'),
(375, 40, '_order_version', '3.4.5'),
(376, 40, '_prices_include_tax', 'no'),
(377, 40, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(378, 40, '_shipping_address_index', '        '),
(379, 40, '_download_permissions_granted', 'yes'),
(380, 40, '_recorded_sales', 'yes'),
(381, 40, '_recorded_coupon_usage_counts', 'yes'),
(382, 40, '_order_stock_reduced', 'yes'),
(383, 41, '_order_key', 'wc_order_5bbc5758ba8d9'),
(384, 41, '_customer_user', '1'),
(385, 41, '_payment_method', 'cod'),
(386, 41, '_payment_method_title', 'Cash on delivery'),
(387, 41, '_transaction_id', ''),
(388, 41, '_customer_ip_address', '::1'),
(389, 41, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(390, 41, '_created_via', 'checkout'),
(391, 41, '_date_completed', ''),
(392, 41, '_completed_date', ''),
(393, 41, '_date_paid', ''),
(394, 41, '_paid_date', ''),
(395, 41, '_cart_hash', '162545566f8d4fb615b0bacdc35e9032'),
(396, 41, '_billing_first_name', 'Martina'),
(397, 41, '_billing_last_name', 'Erickson'),
(398, 41, '_billing_company', 'Valentine and Sloan LLC'),
(399, 41, '_billing_address_1', '606 Rocky Second Street'),
(400, 41, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(401, 41, '_billing_city', 'ahmedab'),
(402, 41, '_billing_state', 'AP'),
(403, 41, '_billing_postcode', '78965-411'),
(404, 41, '_billing_country', 'BR'),
(405, 41, '_billing_email', 'qogybyzud@mailinator.net'),
(406, 41, '_billing_phone', '+442-68-9152410'),
(407, 41, '_shipping_first_name', ''),
(408, 41, '_shipping_last_name', ''),
(409, 41, '_shipping_company', ''),
(410, 41, '_shipping_address_1', ''),
(411, 41, '_shipping_address_2', ''),
(412, 41, '_shipping_city', ''),
(413, 41, '_shipping_state', ''),
(414, 41, '_shipping_postcode', ''),
(415, 41, '_shipping_country', ''),
(416, 41, '_order_currency', 'GBP'),
(417, 41, '_cart_discount', '0'),
(418, 41, '_cart_discount_tax', '0'),
(419, 41, '_order_shipping', '0.00'),
(420, 41, '_order_shipping_tax', '0'),
(421, 41, '_order_tax', '0'),
(422, 41, '_order_total', '900.00'),
(423, 41, '_order_version', '3.4.5'),
(424, 41, '_prices_include_tax', 'no'),
(425, 41, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(426, 41, '_shipping_address_index', '        '),
(427, 41, '_download_permissions_granted', 'yes'),
(428, 41, '_recorded_sales', 'yes'),
(429, 41, '_recorded_coupon_usage_counts', 'yes'),
(430, 41, '_order_stock_reduced', 'yes'),
(431, 36, '_product_attributes', 'a:3:{s:7:\"pa_make\";a:6:{s:4:\"name\";s:7:\"pa_make\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_model\";a:6:{s:4:\"name\";s:8:\"pa_model\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_years\";a:6:{s:4:\"name\";s:8:\"pa_years\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:2;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}'),
(432, 33, '_product_attributes', 'a:3:{s:7:\"pa_make\";a:6:{s:4:\"name\";s:7:\"pa_make\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_model\";a:6:{s:4:\"name\";s:8:\"pa_model\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_years\";a:6:{s:4:\"name\";s:8:\"pa_years\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:2;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}'),
(433, 31, '_product_attributes', 'a:3:{s:7:\"pa_make\";a:6:{s:4:\"name\";s:7:\"pa_make\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_model\";a:6:{s:4:\"name\";s:8:\"pa_model\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_years\";a:6:{s:4:\"name\";s:8:\"pa_years\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:2;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}'),
(434, 29, '_product_attributes', 'a:3:{s:7:\"pa_make\";a:6:{s:4:\"name\";s:7:\"pa_make\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_model\";a:6:{s:4:\"name\";s:8:\"pa_model\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:1;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}s:8:\"pa_years\";a:6:{s:4:\"name\";s:8:\"pa_years\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:2;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:0;s:11:\"is_taxonomy\";i:1;}}'),
(435, 42, '_order_key', 'wc_order_5bbc5b22b82c9'),
(436, 42, '_customer_user', '1'),
(437, 42, '_payment_method', 'cod'),
(438, 42, '_payment_method_title', 'Cash on delivery'),
(439, 42, '_transaction_id', ''),
(440, 42, '_customer_ip_address', '::1'),
(441, 42, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(442, 42, '_created_via', 'checkout'),
(443, 42, '_date_completed', ''),
(444, 42, '_completed_date', ''),
(445, 42, '_date_paid', ''),
(446, 42, '_paid_date', ''),
(447, 42, '_cart_hash', '56b57f2410d437e0c7fde20f7db3fd1b'),
(448, 42, '_billing_first_name', 'Martina'),
(449, 42, '_billing_last_name', 'Erickson'),
(450, 42, '_billing_company', 'Valentine and Sloan LLC'),
(451, 42, '_billing_address_1', '606 Rocky Second Street'),
(452, 42, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(453, 42, '_billing_city', 'ahmedab'),
(454, 42, '_billing_state', 'AP'),
(455, 42, '_billing_postcode', '78965-411'),
(456, 42, '_billing_country', 'BR'),
(457, 42, '_billing_email', 'qogybyzud@mailinator.net'),
(458, 42, '_billing_phone', '+442-68-9152410'),
(459, 42, '_shipping_first_name', ''),
(460, 42, '_shipping_last_name', ''),
(461, 42, '_shipping_company', ''),
(462, 42, '_shipping_address_1', ''),
(463, 42, '_shipping_address_2', ''),
(464, 42, '_shipping_city', ''),
(465, 42, '_shipping_state', ''),
(466, 42, '_shipping_postcode', ''),
(467, 42, '_shipping_country', ''),
(468, 42, '_order_currency', 'GBP'),
(469, 42, '_cart_discount', '0'),
(470, 42, '_cart_discount_tax', '0'),
(471, 42, '_order_shipping', '0.00'),
(472, 42, '_order_shipping_tax', '0'),
(473, 42, '_order_tax', '0'),
(474, 42, '_order_total', '600.00'),
(475, 42, '_order_version', '3.4.5'),
(476, 42, '_prices_include_tax', 'no'),
(477, 42, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(478, 42, '_shipping_address_index', '        '),
(479, 42, '_download_permissions_granted', 'yes'),
(480, 42, '_recorded_sales', 'yes'),
(481, 42, '_recorded_coupon_usage_counts', 'yes'),
(482, 42, '_order_stock_reduced', 'yes'),
(483, 43, '_order_key', 'wc_order_5bbc787aaf027'),
(484, 43, '_customer_user', '2'),
(485, 43, '_payment_method', 'cod'),
(486, 43, '_payment_method_title', 'Cash on delivery'),
(487, 43, '_transaction_id', ''),
(488, 43, '_customer_ip_address', '::1'),
(489, 43, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(490, 43, '_created_via', 'checkout'),
(491, 43, '_date_completed', ''),
(492, 43, '_completed_date', ''),
(493, 43, '_date_paid', ''),
(494, 43, '_paid_date', ''),
(495, 43, '_cart_hash', '2135a1cb06b61653e122f02d4d6c367e'),
(496, 43, '_billing_first_name', 'Snehal'),
(497, 43, '_billing_last_name', 'Gohel'),
(498, 43, '_billing_company', 'citrusbug'),
(499, 43, '_billing_address_1', 'B - 102'),
(500, 43, '_billing_address_2', 'Panchgini'),
(501, 43, '_billing_city', 'ahmedab'),
(502, 43, '_billing_state', 'GJ'),
(503, 43, '_billing_postcode', '382345'),
(504, 43, '_billing_country', 'IN'),
(505, 43, '_billing_email', 'snehal.citrusbug@gmail.com'),
(506, 43, '_billing_phone', '8899665588'),
(507, 43, '_shipping_first_name', ''),
(508, 43, '_shipping_last_name', ''),
(509, 43, '_shipping_company', ''),
(510, 43, '_shipping_address_1', ''),
(511, 43, '_shipping_address_2', ''),
(512, 43, '_shipping_city', ''),
(513, 43, '_shipping_state', ''),
(514, 43, '_shipping_postcode', ''),
(515, 43, '_shipping_country', ''),
(516, 43, '_order_currency', 'GBP'),
(517, 43, '_cart_discount', '0'),
(518, 43, '_cart_discount_tax', '0'),
(519, 43, '_order_shipping', '0.00'),
(520, 43, '_order_shipping_tax', '0'),
(521, 43, '_order_tax', '0'),
(522, 43, '_order_total', '410.00'),
(523, 43, '_order_version', '3.4.5'),
(524, 43, '_prices_include_tax', 'no'),
(525, 43, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(526, 43, '_shipping_address_index', '        '),
(527, 43, '_download_permissions_granted', 'yes'),
(528, 43, '_recorded_sales', 'yes'),
(529, 43, '_recorded_coupon_usage_counts', 'yes'),
(530, 43, '_order_stock_reduced', 'yes'),
(531, 44, '_edit_lock', '1539087494:1'),
(532, 44, '_edit_last', '1'),
(533, 46, '_edit_lock', '1539084109:1'),
(534, 46, '_edit_last', '1'),
(535, 46, '_wp_page_template', 'contactus.php'),
(536, 44, '_wp_page_template', 'about-us.php'),
(537, 48, '_order_key', 'wc_order_5bbc7dc678342'),
(538, 48, '_customer_user', '2'),
(539, 48, '_payment_method', 'cod'),
(540, 48, '_payment_method_title', 'Cash on delivery'),
(541, 48, '_transaction_id', ''),
(542, 48, '_customer_ip_address', '::1'),
(543, 48, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(544, 48, '_created_via', 'checkout'),
(545, 48, '_date_completed', ''),
(546, 48, '_completed_date', ''),
(547, 48, '_date_paid', ''),
(548, 48, '_paid_date', ''),
(549, 48, '_cart_hash', '67ff4065d376c3619efaab199a5fd7af'),
(550, 48, '_billing_first_name', 'Snehal'),
(551, 48, '_billing_last_name', 'Gohel'),
(552, 48, '_billing_company', 'citrusbug'),
(553, 48, '_billing_address_1', 'B - 102'),
(554, 48, '_billing_address_2', 'Panchgini'),
(555, 48, '_billing_city', 'ahmedab'),
(556, 48, '_billing_state', 'GJ'),
(557, 48, '_billing_postcode', '382345'),
(558, 48, '_billing_country', 'IN'),
(559, 48, '_billing_email', 'snehal.citrusbug@gmail.com'),
(560, 48, '_billing_phone', '8899665588'),
(561, 48, '_shipping_first_name', ''),
(562, 48, '_shipping_last_name', ''),
(563, 48, '_shipping_company', ''),
(564, 48, '_shipping_address_1', ''),
(565, 48, '_shipping_address_2', ''),
(566, 48, '_shipping_city', ''),
(567, 48, '_shipping_state', ''),
(568, 48, '_shipping_postcode', ''),
(569, 48, '_shipping_country', ''),
(570, 48, '_order_currency', 'GBP'),
(571, 48, '_cart_discount', '0'),
(572, 48, '_cart_discount_tax', '0'),
(573, 48, '_order_shipping', '0.00'),
(574, 48, '_order_shipping_tax', '0'),
(575, 48, '_order_tax', '0'),
(576, 48, '_order_total', '60.00'),
(577, 48, '_order_version', '3.4.5'),
(578, 48, '_prices_include_tax', 'no'),
(579, 48, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(580, 48, '_shipping_address_index', '        '),
(581, 48, '_download_permissions_granted', 'yes'),
(582, 48, '_recorded_sales', 'yes'),
(583, 48, '_recorded_coupon_usage_counts', 'yes'),
(584, 48, '_order_stock_reduced', 'yes'),
(585, 49, '_form', '<form class=\"clearfix\">\n    <div class=\"row\">\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>First Name<sup class=\"error\">*</sup>:</label>\n                [text* first class:form-control  ]\n            </div>\n        </div> \n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>Last Name<sup class=\"error\">*</sup>:</label>\n                [text* last class:form-control  ]\n            </div>\n        </div>        \n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>Email<sup class=\"error\">*</sup>:</label>\n                [email* email class:form-control ]\n            </div>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>Phone<sup class=\"error\">*</sup>:</label>\n                [text* phone class:form-control  ]\n            </div>\n        </div>\n        <div class=\"col-md-12 col-sm-12\">\n            <div class=\"form-group\">\n                <label>Address:</label>\n                [textarea address class:form-control rows:5]\n            </div>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>City<sup class=\"error\">*</sup>:</label>\n                [text* city class:form-control  ]\n            </div>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>State/Province<sup class=\"error\">*</sup>:</label>\n                [text* state class:form-control  ]\n            </div>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n            <div class=\"form-group\">\n                <label>Zip/Postal<sup class=\"error\">*</sup>:</label>\n                [text* zip class:form-control  ]\n            </div>\n        </div>\n        <div class=\"col-md-12 col-sm-12\">\n            <div class=\"form-group\">\n                <label>Questions Or Comments:</label>\n                [textarea comments class:form-control rows:5]\n            </div>\n        </div>\n        <div class=\"col-md-12 col-sm-12\">\n            <div class=\"slide-Link\">\n                [submit  class:btn-more-red-link \"submit\" ]\n            </div>\n        </div>\n    </div>\n</form>'),
(586, 49, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:20:\"Jess Contact Inquiry\";s:6:\"sender\";s:24:\"[first] [last] <[email]>\";s:9:\"recipient\";s:24:\"user.citrusbug@gmail.com\";s:4:\"body\";s:138:\"From: [first] [last] <[email]>\nPhone No. : [phone]\nAddress : [address]\nCity : [city]\nState : [state]\nZip : [zip]\n\nMessage Body:\n[comments]\";s:18:\"additional_headers\";s:17:\"Reply-To: [email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:1;s:13:\"exclude_blank\";b:0;}'),
(587, 49, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:21:\"Jess \"[your-subject]\"\";s:6:\"sender\";s:31:\"Jess <user.citrusbug@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:124:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Jess (http://localhost/wordpress-project/jess)\";s:18:\"additional_headers\";s:34:\"Reply-To: user.citrusbug@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(588, 49, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(589, 49, '_additional_settings', ''),
(590, 49, '_locale', 'en_US'),
(591, 50, '_order_key', 'wc_order_5bbc7ee0ca150'),
(592, 50, '_customer_user', '2'),
(593, 50, '_payment_method', 'cod'),
(594, 50, '_payment_method_title', 'Cash on delivery'),
(595, 50, '_transaction_id', ''),
(596, 50, '_customer_ip_address', '::1'),
(597, 50, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(598, 50, '_created_via', 'checkout'),
(599, 50, '_date_completed', ''),
(600, 50, '_completed_date', ''),
(601, 50, '_date_paid', ''),
(602, 50, '_paid_date', ''),
(603, 50, '_cart_hash', 'c3e3e9d54bfc02f8b0e1ed2d8cb8d43e'),
(604, 50, '_billing_first_name', 'Snehal'),
(605, 50, '_billing_last_name', 'Gohel'),
(606, 50, '_billing_company', 'citrusbug'),
(607, 50, '_billing_address_1', 'B - 102'),
(608, 50, '_billing_address_2', 'Panchgini'),
(609, 50, '_billing_city', 'ahmedab'),
(610, 50, '_billing_state', 'GJ'),
(611, 50, '_billing_postcode', '382345'),
(612, 50, '_billing_country', 'IN'),
(613, 50, '_billing_email', 'snehal.citrusbug@gmail.com'),
(614, 50, '_billing_phone', '8899665588'),
(615, 50, '_shipping_first_name', ''),
(616, 50, '_shipping_last_name', ''),
(617, 50, '_shipping_company', ''),
(618, 50, '_shipping_address_1', ''),
(619, 50, '_shipping_address_2', ''),
(620, 50, '_shipping_city', ''),
(621, 50, '_shipping_state', ''),
(622, 50, '_shipping_postcode', ''),
(623, 50, '_shipping_country', ''),
(624, 50, '_order_currency', 'GBP'),
(625, 50, '_cart_discount', '0'),
(626, 50, '_cart_discount_tax', '0'),
(627, 50, '_order_shipping', '0.00'),
(628, 50, '_order_shipping_tax', '0'),
(629, 50, '_order_tax', '0'),
(630, 50, '_order_total', '100.00'),
(631, 50, '_order_version', '3.4.5'),
(632, 50, '_prices_include_tax', 'no'),
(633, 50, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(634, 50, '_shipping_address_index', '        '),
(635, 50, '_download_permissions_granted', 'yes'),
(636, 50, '_recorded_sales', 'yes'),
(637, 50, '_recorded_coupon_usage_counts', 'yes'),
(638, 50, '_order_stock_reduced', 'yes'),
(639, 51, '_menu_item_type', 'taxonomy'),
(640, 51, '_menu_item_menu_item_parent', '0'),
(641, 51, '_menu_item_object_id', '31'),
(642, 51, '_menu_item_object', 'product_cat'),
(643, 51, '_menu_item_target', ''),
(644, 51, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(645, 51, '_menu_item_xfn', ''),
(646, 51, '_menu_item_url', ''),
(648, 52, '_menu_item_type', 'taxonomy'),
(649, 52, '_menu_item_menu_item_parent', '51'),
(650, 52, '_menu_item_object_id', '37'),
(651, 52, '_menu_item_object', 'product_cat'),
(652, 52, '_menu_item_target', ''),
(653, 52, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(654, 52, '_menu_item_xfn', ''),
(655, 52, '_menu_item_url', ''),
(657, 53, '_menu_item_type', 'taxonomy'),
(658, 53, '_menu_item_menu_item_parent', '51'),
(659, 53, '_menu_item_object_id', '36'),
(660, 53, '_menu_item_object', 'product_cat'),
(661, 53, '_menu_item_target', ''),
(662, 53, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(663, 53, '_menu_item_xfn', ''),
(664, 53, '_menu_item_url', ''),
(666, 54, '_menu_item_type', 'taxonomy'),
(667, 54, '_menu_item_menu_item_parent', '51'),
(668, 54, '_menu_item_object_id', '35'),
(669, 54, '_menu_item_object', 'product_cat'),
(670, 54, '_menu_item_target', ''),
(671, 54, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(672, 54, '_menu_item_xfn', ''),
(673, 54, '_menu_item_url', ''),
(675, 55, '_menu_item_type', 'taxonomy'),
(676, 55, '_menu_item_menu_item_parent', '51'),
(677, 55, '_menu_item_object_id', '34'),
(678, 55, '_menu_item_object', 'product_cat'),
(679, 55, '_menu_item_target', ''),
(680, 55, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(681, 55, '_menu_item_xfn', ''),
(682, 55, '_menu_item_url', ''),
(684, 56, '_menu_item_type', 'taxonomy'),
(685, 56, '_menu_item_menu_item_parent', '51'),
(686, 56, '_menu_item_object_id', '33'),
(687, 56, '_menu_item_object', 'product_cat'),
(688, 56, '_menu_item_target', ''),
(689, 56, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(690, 56, '_menu_item_xfn', ''),
(691, 56, '_menu_item_url', ''),
(693, 57, '_menu_item_type', 'taxonomy'),
(694, 57, '_menu_item_menu_item_parent', '51'),
(695, 57, '_menu_item_object_id', '32'),
(696, 57, '_menu_item_object', 'product_cat'),
(697, 57, '_menu_item_target', ''),
(698, 57, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(699, 57, '_menu_item_xfn', ''),
(700, 57, '_menu_item_url', ''),
(702, 58, '_menu_item_type', 'taxonomy'),
(703, 58, '_menu_item_menu_item_parent', '0'),
(704, 58, '_menu_item_object_id', '45'),
(705, 58, '_menu_item_object', 'product_cat'),
(706, 58, '_menu_item_target', ''),
(707, 58, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(708, 58, '_menu_item_xfn', ''),
(709, 58, '_menu_item_url', ''),
(711, 59, '_menu_item_type', 'taxonomy'),
(712, 59, '_menu_item_menu_item_parent', '58'),
(713, 59, '_menu_item_object_id', '52'),
(714, 59, '_menu_item_object', 'product_cat'),
(715, 59, '_menu_item_target', ''),
(716, 59, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(717, 59, '_menu_item_xfn', ''),
(718, 59, '_menu_item_url', ''),
(720, 60, '_menu_item_type', 'taxonomy'),
(721, 60, '_menu_item_menu_item_parent', '58'),
(722, 60, '_menu_item_object_id', '51'),
(723, 60, '_menu_item_object', 'product_cat'),
(724, 60, '_menu_item_target', ''),
(725, 60, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(726, 60, '_menu_item_xfn', ''),
(727, 60, '_menu_item_url', ''),
(729, 61, '_menu_item_type', 'taxonomy'),
(730, 61, '_menu_item_menu_item_parent', '58'),
(731, 61, '_menu_item_object_id', '50'),
(732, 61, '_menu_item_object', 'product_cat'),
(733, 61, '_menu_item_target', ''),
(734, 61, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(735, 61, '_menu_item_xfn', ''),
(736, 61, '_menu_item_url', ''),
(738, 62, '_menu_item_type', 'taxonomy'),
(739, 62, '_menu_item_menu_item_parent', '58'),
(740, 62, '_menu_item_object_id', '49'),
(741, 62, '_menu_item_object', 'product_cat'),
(742, 62, '_menu_item_target', ''),
(743, 62, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(744, 62, '_menu_item_xfn', ''),
(745, 62, '_menu_item_url', ''),
(747, 63, '_menu_item_type', 'taxonomy'),
(748, 63, '_menu_item_menu_item_parent', '58'),
(749, 63, '_menu_item_object_id', '48'),
(750, 63, '_menu_item_object', 'product_cat'),
(751, 63, '_menu_item_target', ''),
(752, 63, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(753, 63, '_menu_item_xfn', ''),
(754, 63, '_menu_item_url', ''),
(756, 64, '_menu_item_type', 'taxonomy'),
(757, 64, '_menu_item_menu_item_parent', '58'),
(758, 64, '_menu_item_object_id', '47'),
(759, 64, '_menu_item_object', 'product_cat'),
(760, 64, '_menu_item_target', ''),
(761, 64, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(762, 64, '_menu_item_xfn', ''),
(763, 64, '_menu_item_url', ''),
(765, 65, '_menu_item_type', 'taxonomy'),
(766, 65, '_menu_item_menu_item_parent', '58'),
(767, 65, '_menu_item_object_id', '46'),
(768, 65, '_menu_item_object', 'product_cat'),
(769, 65, '_menu_item_target', ''),
(770, 65, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(771, 65, '_menu_item_xfn', ''),
(772, 65, '_menu_item_url', ''),
(774, 66, '_menu_item_type', 'taxonomy'),
(775, 66, '_menu_item_menu_item_parent', '0'),
(776, 66, '_menu_item_object_id', '38'),
(777, 66, '_menu_item_object', 'product_cat'),
(778, 66, '_menu_item_target', ''),
(779, 66, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(780, 66, '_menu_item_xfn', ''),
(781, 66, '_menu_item_url', ''),
(783, 67, '_menu_item_type', 'taxonomy'),
(784, 67, '_menu_item_menu_item_parent', '66'),
(785, 67, '_menu_item_object_id', '44'),
(786, 67, '_menu_item_object', 'product_cat'),
(787, 67, '_menu_item_target', ''),
(788, 67, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(789, 67, '_menu_item_xfn', ''),
(790, 67, '_menu_item_url', ''),
(792, 68, '_menu_item_type', 'taxonomy'),
(793, 68, '_menu_item_menu_item_parent', '66'),
(794, 68, '_menu_item_object_id', '43'),
(795, 68, '_menu_item_object', 'product_cat'),
(796, 68, '_menu_item_target', ''),
(797, 68, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(798, 68, '_menu_item_xfn', ''),
(799, 68, '_menu_item_url', ''),
(801, 69, '_menu_item_type', 'taxonomy'),
(802, 69, '_menu_item_menu_item_parent', '66'),
(803, 69, '_menu_item_object_id', '41'),
(804, 69, '_menu_item_object', 'product_cat'),
(805, 69, '_menu_item_target', ''),
(806, 69, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(807, 69, '_menu_item_xfn', ''),
(808, 69, '_menu_item_url', ''),
(810, 70, '_menu_item_type', 'taxonomy'),
(811, 70, '_menu_item_menu_item_parent', '66'),
(812, 70, '_menu_item_object_id', '42'),
(813, 70, '_menu_item_object', 'product_cat'),
(814, 70, '_menu_item_target', ''),
(815, 70, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(816, 70, '_menu_item_xfn', ''),
(817, 70, '_menu_item_url', ''),
(819, 71, '_menu_item_type', 'taxonomy'),
(820, 71, '_menu_item_menu_item_parent', '66'),
(821, 71, '_menu_item_object_id', '40'),
(822, 71, '_menu_item_object', 'product_cat'),
(823, 71, '_menu_item_target', ''),
(824, 71, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(825, 71, '_menu_item_xfn', ''),
(826, 71, '_menu_item_url', ''),
(828, 72, '_menu_item_type', 'taxonomy');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(829, 72, '_menu_item_menu_item_parent', '66'),
(830, 72, '_menu_item_object_id', '39'),
(831, 72, '_menu_item_object', 'product_cat'),
(832, 72, '_menu_item_target', ''),
(833, 72, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(834, 72, '_menu_item_xfn', ''),
(835, 72, '_menu_item_url', ''),
(846, 74, '_menu_item_type', 'custom'),
(847, 74, '_menu_item_menu_item_parent', '0'),
(848, 74, '_menu_item_object_id', '74'),
(849, 74, '_menu_item_object', 'custom'),
(850, 74, '_menu_item_target', ''),
(851, 74, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(852, 74, '_menu_item_xfn', ''),
(853, 74, '_menu_item_url', '#'),
(855, 75, '_menu_item_type', 'post_type'),
(856, 75, '_menu_item_menu_item_parent', '0'),
(857, 75, '_menu_item_object_id', '46'),
(858, 75, '_menu_item_object', 'page'),
(859, 75, '_menu_item_target', ''),
(860, 75, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(861, 75, '_menu_item_xfn', ''),
(862, 75, '_menu_item_url', ''),
(864, 76, '_menu_item_type', 'post_type'),
(865, 76, '_menu_item_menu_item_parent', '0'),
(866, 76, '_menu_item_object_id', '44'),
(867, 76, '_menu_item_object', 'page'),
(868, 76, '_menu_item_target', ''),
(869, 76, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(870, 76, '_menu_item_xfn', ''),
(871, 76, '_menu_item_url', ''),
(873, 3, '_edit_lock', '1539081032:1'),
(874, 3, '_edit_last', '1'),
(875, 3, '_wp_trash_meta_status', 'draft'),
(876, 3, '_wp_trash_meta_time', '1539081180'),
(877, 3, '_wp_desired_post_slug', 'privacy-policy'),
(878, 78, '_edit_lock', '1539081145:1'),
(879, 78, '_edit_last', '1'),
(880, 78, '_wp_page_template', 'default'),
(881, 80, '_menu_item_type', 'post_type'),
(882, 80, '_menu_item_menu_item_parent', '0'),
(883, 80, '_menu_item_object_id', '78'),
(884, 80, '_menu_item_object', 'page'),
(885, 80, '_menu_item_target', ''),
(886, 80, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(887, 80, '_menu_item_xfn', ''),
(888, 80, '_menu_item_url', ''),
(889, 80, '_menu_item_orphaned', '1539081212'),
(890, 81, '_edit_lock', '1539081171:1'),
(891, 81, '_edit_last', '1'),
(892, 81, '_wp_page_template', 'default'),
(893, 83, '_edit_last', '1'),
(894, 83, '_wp_page_template', 'default'),
(895, 83, '_edit_lock', '1539096946:1'),
(896, 85, '_menu_item_type', 'post_type'),
(897, 85, '_menu_item_menu_item_parent', '0'),
(898, 85, '_menu_item_object_id', '83'),
(899, 85, '_menu_item_object', 'page'),
(900, 85, '_menu_item_target', ''),
(901, 85, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(902, 85, '_menu_item_xfn', ''),
(903, 85, '_menu_item_url', ''),
(905, 86, '_menu_item_type', 'post_type'),
(906, 86, '_menu_item_menu_item_parent', '0'),
(907, 86, '_menu_item_object_id', '81'),
(908, 86, '_menu_item_object', 'page'),
(909, 86, '_menu_item_target', ''),
(910, 86, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(911, 86, '_menu_item_xfn', ''),
(912, 86, '_menu_item_url', ''),
(914, 87, '_menu_item_type', 'post_type'),
(915, 87, '_menu_item_menu_item_parent', '0'),
(916, 87, '_menu_item_object_id', '78'),
(917, 87, '_menu_item_object', 'page'),
(918, 87, '_menu_item_target', ''),
(919, 87, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(920, 87, '_menu_item_xfn', ''),
(921, 87, '_menu_item_url', ''),
(923, 88, '_menu_item_type', 'post_type'),
(924, 88, '_menu_item_menu_item_parent', '0'),
(925, 88, '_menu_item_object_id', '46'),
(926, 88, '_menu_item_object', 'page'),
(927, 88, '_menu_item_target', ''),
(928, 88, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(929, 88, '_menu_item_xfn', ''),
(930, 88, '_menu_item_url', ''),
(932, 89, '_menu_item_type', 'custom'),
(933, 89, '_menu_item_menu_item_parent', '0'),
(934, 89, '_menu_item_object_id', '89'),
(935, 89, '_menu_item_object', 'custom'),
(936, 89, '_menu_item_target', ''),
(937, 89, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(938, 89, '_menu_item_xfn', ''),
(939, 89, '_menu_item_url', 'http://localhost/wordpress-project/jess'),
(941, 90, '_menu_item_type', 'custom'),
(942, 90, '_menu_item_menu_item_parent', '0'),
(943, 90, '_menu_item_object_id', '90'),
(944, 90, '_menu_item_object', 'custom'),
(945, 90, '_menu_item_target', ''),
(946, 90, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(947, 90, '_menu_item_xfn', ''),
(948, 90, '_menu_item_url', '#'),
(950, 91, '_menu_item_type', 'custom'),
(951, 91, '_menu_item_menu_item_parent', '0'),
(952, 91, '_menu_item_object_id', '91'),
(953, 91, '_menu_item_object', 'custom'),
(954, 91, '_menu_item_target', ''),
(955, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(956, 91, '_menu_item_xfn', ''),
(957, 91, '_menu_item_url', '#'),
(959, 92, '_menu_item_type', 'custom'),
(960, 92, '_menu_item_menu_item_parent', '0'),
(961, 92, '_menu_item_object_id', '92'),
(962, 92, '_menu_item_object', 'custom'),
(963, 92, '_menu_item_target', ''),
(964, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(965, 92, '_menu_item_xfn', ''),
(966, 92, '_menu_item_url', '#'),
(968, 93, '_menu_item_type', 'custom'),
(969, 93, '_menu_item_menu_item_parent', '0'),
(970, 93, '_menu_item_object_id', '93'),
(971, 93, '_menu_item_object', 'custom'),
(972, 93, '_menu_item_target', ''),
(973, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(974, 93, '_menu_item_xfn', ''),
(975, 93, '_menu_item_url', '#'),
(977, 94, '_form', '[email* email class:form-control class:form-control-10 placeholder \"Email address\"]\n[submit class:btn class:btn-danger class:btn-scbscribe \"subscribe\"]'),
(978, 94, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:18:\"Jess New Subscribe\";s:6:\"sender\";s:26:\"<user.citrusbug@gmail.com>\";s:9:\"recipient\";s:24:\"user.citrusbug@gmail.com\";s:4:\"body\";s:54:\"From: [email] \n\nMessage Body:\nNew Subscription [email]\";s:18:\"additional_headers\";s:17:\"Reply-To: [email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:1;s:13:\"exclude_blank\";b:0;}'),
(979, 94, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:21:\"Jess \"[your-subject]\"\";s:6:\"sender\";s:31:\"Jess <user.citrusbug@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:124:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Jess (http://localhost/wordpress-project/jess)\";s:18:\"additional_headers\";s:34:\"Reply-To: user.citrusbug@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(980, 94, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(981, 94, '_additional_settings', ''),
(982, 94, '_locale', 'en_US'),
(983, 95, '_edit_lock', '1539083691:1'),
(984, 95, '_edit_last', '1'),
(985, 97, '_edit_lock', '1539084098:1'),
(986, 97, '_edit_last', '1'),
(987, 100, '_wp_attached_file', '2018/10/about-us.jpg'),
(988, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:472;s:6:\"height\";i:386;s:4:\"file\";s:20:\"2018/10/about-us.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"about-us-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"about-us-300x245.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:245;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"about-us-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"about-us-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"about-us-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"about-us-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"about-us-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(989, 44, '_thumbnail_id', '100'),
(990, 44, 'home_section_content', '<h3>WE HAVE THE SECTION OF JESS PERFORMANCE IS PLACE FOR ALL YOUR DIESEL POWER &amp; PERFORMANCE NEEDS, LOCATED IN NORTHERN INDIANA NEAR SHIPSHEWANA!</h3>\r\n<p class=\"p-one\">Whether you want power and performance or need service work done on your Ford Powerstroke, Dodge Cummins or Chevrolet/GMC Duramax Jess Performance is the place. We take pride in the excellent diesel products and services we offer for Dodge Cummins, Chevrolet/GMC Duramax and Ford Powerstroke. Whether you have questions or already know what you want for your diesel truck we are here for YOU! Call us at 574-349-4354. Located in Shipshewana, IN we are conveniently located next to Elkhart County. We offer fast free shipping on most orders over $50</p>\r\n<p class=\"p-two\">Want parts for Cummins, Duramax or Powerstroke....we are here for you!</p>'),
(991, 44, '_home_section_content', 'field_5bbc8e24351e8'),
(992, 101, 'home_section_content', '<h3>WE HAVE THE SECTION OF JESS PERFORMANCE IS PLACE FOR ALL YOUR DIESEL POWER &amp; PERFORMANCE NEEDS, LOCATED IN NORTHERN INDIANA NEAR SHIPSHEWANA!</h3>\r\n<p class=\"p-one\">Whether you want power and performance or need service work done on your Ford Powerstroke, Dodge Cummins or Chevrolet/GMC Duramax Jess Performance is the place. We take pride in the excellent diesel products and services we offer for Dodge Cummins, Chevrolet/GMC Duramax and Ford Powerstroke. Whether you have questions or already know what you want for your diesel truck we are here for YOU! Call us at 574-349-4354. Located in Shipshewana, IN we are conveniently located next to Elkhart County. We offer fast free shipping on most orders over $50</p>\r\n<p class=\"p-two\">Want parts for Cummins, Duramax or Powerstroke....we are here for you!</p>'),
(993, 101, '_home_section_content', 'field_5bbc8e24351e8'),
(994, 46, 'latitude', '41.6852346'),
(995, 46, '_latitude', 'field_5bbc8e3f16fbf'),
(996, 46, 'longitude', '-85.578095'),
(997, 46, '_longitude', 'field_5bbc8e4816fc0'),
(998, 102, 'latitude', '41.6852346'),
(999, 102, '_latitude', 'field_5bbc8e3f16fbf'),
(1000, 102, 'longitude', '-85.578095'),
(1001, 102, '_longitude', 'field_5bbc8e4816fc0'),
(1002, 103, '_wp_attached_file', '2018/10/banner-1.jpg'),
(1003, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1366;s:6:\"height\";i:500;s:4:\"file\";s:20:\"2018/10/banner-1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"banner-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"banner-1-300x110.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:110;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"banner-1-768x281.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:281;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"banner-1-1024x375.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:375;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"banner-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"banner-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"banner-1-600x220.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"banner-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"banner-1-600x220.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:220;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"banner-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1004, 104, '_wp_attached_file', '2018/10/banner-1-1.jpg'),
(1005, 104, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1318;s:6:\"height\";i:379;s:4:\"file\";s:22:\"2018/10/banner-1-1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"banner-1-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"banner-1-1-300x86.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:86;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"banner-1-1-768x221.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:221;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"banner-1-1-1024x294.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:294;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:22:\"banner-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"banner-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"banner-1-1-600x173.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:173;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"banner-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"banner-1-1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"banner-1-1-600x173.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:173;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"banner-1-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1537028332\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(1006, 105, '_edit_lock', '1539087283:1'),
(1007, 105, '_edit_last', '1'),
(1008, 106, '_wp_attached_file', '2018/10/img-001.png'),
(1009, 106, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:181;s:6:\"height\";i:69;s:4:\"file\";s:19:\"2018/10/img-001.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"img-001-150x69.png\";s:5:\"width\";i:150;s:6:\"height\";i:69;s:9:\"mime-type\";s:9:\"image/png\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:18:\"img-001-100x69.png\";s:5:\"width\";i:100;s:6:\"height\";i:69;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"img-001-100x69.png\";s:5:\"width\";i:100;s:6:\"height\";i:69;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"img-001-100x69.png\";s:5:\"width\";i:100;s:6:\"height\";i:69;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1010, 105, '_thumbnail_id', '106'),
(1011, 107, '_edit_lock', '1539087295:1'),
(1012, 107, '_edit_last', '1'),
(1013, 107, '_thumbnail_id', '106'),
(1014, 108, '_edit_lock', '1539087309:1'),
(1015, 108, '_edit_last', '1'),
(1016, 108, '_thumbnail_id', '106'),
(1017, 109, '_edit_lock', '1539087323:1'),
(1018, 109, '_edit_last', '1'),
(1019, 109, '_thumbnail_id', '106'),
(1020, 110, '_edit_last', '1'),
(1021, 110, '_edit_lock', '1539087450:1'),
(1022, 110, '_thumbnail_id', '106'),
(1023, 111, '_wp_attached_file', '2018/10/bg-img-2-Copy.jpg'),
(1024, 111, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:575;s:4:\"file\";s:25:\"2018/10/bg-img-2-Copy.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"bg-img-2-Copy-300x90.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-768x230.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:26:\"bg-img-2-Copy-1024x307.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:307;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:25:\"bg-img-2-Copy-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-600x180.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-600x180.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:25:\"bg-img-2-Copy-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1025, 112, '_wp_attached_file', '2018/10/bg-img-2.jpg'),
(1026, 112, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:575;s:4:\"file\";s:20:\"2018/10/bg-img-2.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"bg-img-2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"bg-img-2-300x90.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"bg-img-2-768x230.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:230;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"bg-img-2-1024x307.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:307;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"bg-img-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"bg-img-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"bg-img-2-600x180.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"bg-img-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"bg-img-2-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"bg-img-2-600x180.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:180;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"bg-img-2-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1027, 113, '_wp_attached_file', '2018/10/duramax.jpg'),
(1028, 113, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:480;s:6:\"height\";i:477;s:4:\"file\";s:19:\"2018/10/duramax.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"duramax-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:19:\"duramax-300x298.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:19:\"duramax-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:19:\"duramax-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"duramax-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:19:\"duramax-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"duramax-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1029, 114, '_order_key', 'wc_order_5bbc9eaa5eafa'),
(1030, 114, '_customer_user', '2'),
(1031, 114, '_payment_method', 'cod'),
(1032, 114, '_payment_method_title', 'Cash on delivery'),
(1033, 114, '_transaction_id', ''),
(1034, 114, '_customer_ip_address', '::1'),
(1035, 114, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(1036, 114, '_created_via', 'checkout'),
(1037, 114, '_date_completed', ''),
(1038, 114, '_completed_date', ''),
(1039, 114, '_date_paid', ''),
(1040, 114, '_paid_date', ''),
(1041, 114, '_cart_hash', '1d4030f2f07b61114ce2aa310363b775'),
(1042, 114, '_billing_first_name', 'Snehal'),
(1043, 114, '_billing_last_name', 'Gohel'),
(1044, 114, '_billing_company', 'citrusbug'),
(1045, 114, '_billing_address_1', 'B - 102'),
(1046, 114, '_billing_address_2', 'Panchgini'),
(1047, 114, '_billing_city', 'ahmedab'),
(1048, 114, '_billing_state', 'GJ'),
(1049, 114, '_billing_postcode', '382345'),
(1050, 114, '_billing_country', 'IN'),
(1051, 114, '_billing_email', 'snehal.citrusbug@gmail.com'),
(1052, 114, '_billing_phone', '8899665588'),
(1053, 114, '_shipping_first_name', ''),
(1054, 114, '_shipping_last_name', ''),
(1055, 114, '_shipping_company', ''),
(1056, 114, '_shipping_address_1', ''),
(1057, 114, '_shipping_address_2', ''),
(1058, 114, '_shipping_city', ''),
(1059, 114, '_shipping_state', ''),
(1060, 114, '_shipping_postcode', ''),
(1061, 114, '_shipping_country', ''),
(1062, 114, '_order_currency', 'GBP'),
(1063, 114, '_cart_discount', '0'),
(1064, 114, '_cart_discount_tax', '0'),
(1065, 114, '_order_shipping', '0.00'),
(1066, 114, '_order_shipping_tax', '0'),
(1067, 114, '_order_tax', '0'),
(1068, 114, '_order_total', '750.00'),
(1069, 114, '_order_version', '3.4.5'),
(1070, 114, '_prices_include_tax', 'no'),
(1071, 114, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(1072, 114, '_shipping_address_index', '        '),
(1073, 114, '_download_permissions_granted', 'yes'),
(1074, 114, '_recorded_sales', 'yes'),
(1075, 114, '_recorded_coupon_usage_counts', 'yes'),
(1076, 114, '_order_stock_reduced', 'yes'),
(1077, 115, '_order_key', 'wc_order_5bbcae0517ed4'),
(1078, 115, '_customer_user', '2'),
(1079, 115, '_payment_method', 'cod'),
(1080, 115, '_payment_method_title', 'Cash on delivery'),
(1081, 115, '_transaction_id', ''),
(1082, 115, '_customer_ip_address', '::1'),
(1083, 115, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(1084, 115, '_created_via', 'checkout'),
(1085, 115, '_date_completed', ''),
(1086, 115, '_completed_date', ''),
(1087, 115, '_date_paid', ''),
(1088, 115, '_paid_date', ''),
(1089, 115, '_cart_hash', '56b57f2410d437e0c7fde20f7db3fd1b'),
(1090, 115, '_billing_first_name', 'Snehal'),
(1091, 115, '_billing_last_name', 'Gohel'),
(1092, 115, '_billing_company', 'citrusbug'),
(1093, 115, '_billing_address_1', 'B - 102'),
(1094, 115, '_billing_address_2', 'Panchgini'),
(1095, 115, '_billing_city', 'ahmedab'),
(1096, 115, '_billing_state', 'GJ'),
(1097, 115, '_billing_postcode', '382345'),
(1098, 115, '_billing_country', 'IN'),
(1099, 115, '_billing_email', 'snehal.citrusbug@gmail.com'),
(1100, 115, '_billing_phone', '8899665588'),
(1101, 115, '_shipping_first_name', ''),
(1102, 115, '_shipping_last_name', ''),
(1103, 115, '_shipping_company', ''),
(1104, 115, '_shipping_address_1', ''),
(1105, 115, '_shipping_address_2', ''),
(1106, 115, '_shipping_city', ''),
(1107, 115, '_shipping_state', ''),
(1108, 115, '_shipping_postcode', ''),
(1109, 115, '_shipping_country', ''),
(1110, 115, '_order_currency', 'GBP'),
(1111, 115, '_cart_discount', '0'),
(1112, 115, '_cart_discount_tax', '0'),
(1113, 115, '_order_shipping', '0.00'),
(1114, 115, '_order_shipping_tax', '0'),
(1115, 115, '_order_tax', '0'),
(1116, 115, '_order_total', '600.00'),
(1117, 115, '_order_version', '3.4.5'),
(1118, 115, '_prices_include_tax', 'no'),
(1119, 115, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(1120, 115, '_shipping_address_index', '        '),
(1121, 115, '_download_permissions_granted', 'yes'),
(1122, 115, '_recorded_sales', 'yes'),
(1123, 115, '_recorded_coupon_usage_counts', 'yes'),
(1124, 115, '_order_stock_reduced', 'yes'),
(1125, 118, '_order_key', 'wc_order_5bbcc2f06b9a9'),
(1126, 118, '_customer_user', '2'),
(1127, 118, '_payment_method', 'cod'),
(1128, 118, '_payment_method_title', 'Cash on delivery'),
(1129, 118, '_transaction_id', ''),
(1130, 118, '_customer_ip_address', '::1'),
(1131, 118, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(1132, 118, '_created_via', 'checkout'),
(1133, 118, '_date_completed', ''),
(1134, 118, '_completed_date', ''),
(1135, 118, '_date_paid', ''),
(1136, 118, '_paid_date', ''),
(1137, 118, '_cart_hash', 'c3e3e9d54bfc02f8b0e1ed2d8cb8d43e'),
(1138, 118, '_billing_first_name', 'Snehal'),
(1139, 118, '_billing_last_name', 'Gohel'),
(1140, 118, '_billing_company', 'citrusbug'),
(1141, 118, '_billing_address_1', 'B - 102'),
(1142, 118, '_billing_address_2', 'Panchgini'),
(1143, 118, '_billing_city', 'ahmedab'),
(1144, 118, '_billing_state', 'GJ'),
(1145, 118, '_billing_postcode', '382345'),
(1146, 118, '_billing_country', 'IN'),
(1147, 118, '_billing_email', 'snehal.citrusbug@gmail.com'),
(1148, 118, '_billing_phone', '8899665588'),
(1149, 118, '_shipping_first_name', ''),
(1150, 118, '_shipping_last_name', ''),
(1151, 118, '_shipping_company', ''),
(1152, 118, '_shipping_address_1', ''),
(1153, 118, '_shipping_address_2', ''),
(1154, 118, '_shipping_city', ''),
(1155, 118, '_shipping_state', ''),
(1156, 118, '_shipping_postcode', ''),
(1157, 118, '_shipping_country', ''),
(1158, 118, '_order_currency', 'GBP'),
(1159, 118, '_cart_discount', '0'),
(1160, 118, '_cart_discount_tax', '0'),
(1161, 118, '_order_shipping', '0.00'),
(1162, 118, '_order_shipping_tax', '0'),
(1163, 118, '_order_tax', '0'),
(1164, 118, '_order_total', '100.00'),
(1165, 118, '_order_version', '3.4.5'),
(1166, 118, '_prices_include_tax', 'no'),
(1167, 118, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(1168, 118, '_shipping_address_index', '        '),
(1169, 118, '_download_permissions_granted', 'yes'),
(1170, 118, '_recorded_sales', 'yes'),
(1171, 118, '_recorded_coupon_usage_counts', 'yes'),
(1172, 118, '_order_stock_reduced', 'yes'),
(1173, 119, '_edit_lock', '1539098249:1'),
(1174, 119, '_edit_last', '1'),
(1175, 120, '_edit_lock', '1539097951:1'),
(1176, 120, '_edit_last', '1'),
(1177, 119, '_wp_page_template', 'video.php'),
(1178, 119, 'youtube_iframe_link', 'https://www.youtube.com/embed/3y8pZvr3ngU'),
(1179, 119, '_youtube_iframe_link', 'field_5bbcc370bc030'),
(1180, 123, 'youtube_iframe_link', 'https://www.youtube.com/embed/3y8pZvr3ngU'),
(1181, 123, '_youtube_iframe_link', 'field_5bbcc370bc030'),
(1182, 124, 'youtube_iframe_link', 'https://www.youtube.com/embed/3y8pZvr3ngU'),
(1183, 124, '_youtube_iframe_link', 'field_5bbcc370bc030'),
(1184, 125, '_edit_lock', '1539100396:1'),
(1185, 125, '_edit_last', '1'),
(1186, 126, '_edit_lock', '1539100998:1'),
(1187, 126, '_edit_last', '1'),
(1188, 127, '_edit_lock', '1539100411:1'),
(1189, 127, '_edit_last', '1'),
(1190, 126, '_wp_old_slug', 'new-2'),
(1191, 125, '_wp_old_slug', 'new-1'),
(1192, 128, '_edit_lock', '1539099295:1'),
(1193, 128, '_edit_last', '1'),
(1194, 128, '_wp_page_template', 'news.php'),
(1195, 131, '_edit_lock', '1539100383:1'),
(1196, 131, '_edit_last', '1'),
(1197, 131, '_wp_page_template', 'gallery.php'),
(1198, 133, '_edit_lock', '1539099613:1'),
(1199, 133, '_edit_last', '1'),
(1200, 134, '_wp_attached_file', '2018/10/1221_jim1.jpg'),
(1201, 134, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:21:\"2018/10/1221_jim1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"1221_jim1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"1221_jim1-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"1221_jim1-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:22:\"1221_jim1-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:21:\"1221_jim1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"1221_jim1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"1221_jim1-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"1221_jim1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"1221_jim1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"1221_jim1-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"1221_jim1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1202, 135, '_wp_attached_file', '2018/10/1223_megacab.jpg'),
(1203, 135, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1440;s:6:\"height\";i:1080;s:4:\"file\";s:24:\"2018/10/1223_megacab.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"1223_megacab-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"1223_megacab-300x225.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:24:\"1223_megacab-768x576.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:25:\"1223_megacab-1024x768.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:24:\"1223_megacab-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"1223_megacab-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:24:\"1223_megacab-600x450.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"1223_megacab-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:24:\"1223_megacab-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:24:\"1223_megacab-600x450.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:450;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"1223_megacab-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1204, 136, '_wp_attached_file', '2018/10/1225_dan.jpg'),
(1205, 136, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:20:\"2018/10/1225_dan.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"1225_dan-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"1225_dan-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"1225_dan-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"1225_dan-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:20:\"1225_dan-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"1225_dan-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"1225_dan-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"1225_dan-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"1225_dan-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"1225_dan-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"1225_dan-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1206, 137, '_wp_attached_file', '2018/10/1229_Pulling-Truck1.jpg'),
(1207, 137, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1740;s:6:\"height\";i:1080;s:4:\"file\";s:31:\"2018/10/1229_Pulling-Truck1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-300x186.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:186;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-768x477.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:477;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"1229_Pulling-Truck1-1024x636.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:636;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-600x372.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:372;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-600x372.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:372;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"1229_Pulling-Truck1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1208, 138, '_wp_attached_file', '2018/10/1253_20160603_100904.jpg'),
(1209, 138, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:32:\"2018/10/1253_20160603_100904.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:33:\"1253_20160603_100904-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:32:\"twentyseventeen-thumbnail-avatar\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:32:\"1253_20160603_100904-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:1;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-600x338.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:32:\"1253_20160603_100904-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1210, 133, '_thumbnail_id', '137'),
(1211, 139, '_edit_lock', '1539099483:1'),
(1212, 139, '_edit_last', '1'),
(1213, 139, '_thumbnail_id', '138'),
(1214, 140, '_edit_lock', '1539099645:1'),
(1215, 140, '_edit_last', '1'),
(1216, 140, '_thumbnail_id', '136'),
(1217, 141, '_edit_lock', '1539099515:1'),
(1218, 141, '_edit_last', '1'),
(1219, 141, '_thumbnail_id', '135'),
(1220, 142, '_edit_lock', '1539099528:1'),
(1221, 142, '_edit_last', '1'),
(1222, 142, '_thumbnail_id', '134'),
(1223, 125, '_thumbnail_id', '136'),
(1224, 127, '_thumbnail_id', '137'),
(1225, 126, '_thumbnail_id', '135'),
(1226, 143, '_order_key', 'wc_order_5bbcd6ba780e5'),
(1227, 143, '_customer_user', '2'),
(1228, 143, '_payment_method', 'cod'),
(1229, 143, '_payment_method_title', 'Cash on delivery'),
(1230, 143, '_transaction_id', ''),
(1231, 143, '_customer_ip_address', '::1'),
(1232, 143, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(1233, 143, '_created_via', 'checkout'),
(1234, 143, '_date_completed', ''),
(1235, 143, '_completed_date', ''),
(1236, 143, '_date_paid', ''),
(1237, 143, '_paid_date', ''),
(1238, 143, '_cart_hash', 'b77452a2eb0e4c4ea641d94625e8a979'),
(1239, 143, '_billing_first_name', 'Snehal'),
(1240, 143, '_billing_last_name', 'Gohel'),
(1241, 143, '_billing_company', 'citrusbug'),
(1242, 143, '_billing_address_1', 'B - 102'),
(1243, 143, '_billing_address_2', 'Panchgini'),
(1244, 143, '_billing_city', 'ahmedab'),
(1245, 143, '_billing_state', 'GJ'),
(1246, 143, '_billing_postcode', '382345'),
(1247, 143, '_billing_country', 'IN'),
(1248, 143, '_billing_email', 'snehal.citrusbug@gmail.com'),
(1249, 143, '_billing_phone', '8899665588'),
(1250, 143, '_shipping_first_name', ''),
(1251, 143, '_shipping_last_name', ''),
(1252, 143, '_shipping_company', ''),
(1253, 143, '_shipping_address_1', ''),
(1254, 143, '_shipping_address_2', ''),
(1255, 143, '_shipping_city', ''),
(1256, 143, '_shipping_state', ''),
(1257, 143, '_shipping_postcode', ''),
(1258, 143, '_shipping_country', ''),
(1259, 143, '_order_currency', 'GBP'),
(1260, 143, '_cart_discount', '0'),
(1261, 143, '_cart_discount_tax', '0'),
(1262, 143, '_order_shipping', '0.00'),
(1263, 143, '_order_shipping_tax', '0'),
(1264, 143, '_order_tax', '0'),
(1265, 143, '_order_total', '300.00'),
(1266, 143, '_order_version', '3.4.5'),
(1267, 143, '_prices_include_tax', 'no'),
(1268, 143, '_billing_address_index', 'Snehal Gohel citrusbug B - 102 Panchgini ahmedab GJ 382345 IN snehal.citrusbug@gmail.com 8899665588'),
(1269, 143, '_shipping_address_index', '        '),
(1270, 143, '_download_permissions_granted', 'yes'),
(1271, 143, '_recorded_sales', 'yes'),
(1272, 143, '_recorded_coupon_usage_counts', 'yes'),
(1273, 143, '_order_stock_reduced', 'yes'),
(1274, 144, '_order_key', 'wc_order_5bbdaa4f98170'),
(1275, 144, '_customer_user', '1'),
(1276, 144, '_payment_method', 'cod'),
(1277, 144, '_payment_method_title', 'Cash on delivery'),
(1278, 144, '_transaction_id', ''),
(1279, 144, '_customer_ip_address', '::1'),
(1280, 144, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(1281, 144, '_created_via', 'checkout'),
(1282, 144, '_date_completed', ''),
(1283, 144, '_completed_date', ''),
(1284, 144, '_date_paid', ''),
(1285, 144, '_paid_date', ''),
(1286, 144, '_cart_hash', 'e73e5acac99b01ea5750271b3c03c384'),
(1287, 144, '_billing_first_name', 'Martina'),
(1288, 144, '_billing_last_name', 'Erickson'),
(1289, 144, '_billing_company', 'Valentine and Sloan LLC'),
(1290, 144, '_billing_address_1', '606 Rocky Second Street'),
(1291, 144, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(1292, 144, '_billing_city', 'ahmedab'),
(1293, 144, '_billing_state', 'AP'),
(1294, 144, '_billing_postcode', '78965-411'),
(1295, 144, '_billing_country', 'BR'),
(1296, 144, '_billing_email', 'qogybyzud@mailinator.net'),
(1297, 144, '_billing_phone', '+442-68-9152410'),
(1298, 144, '_shipping_first_name', ''),
(1299, 144, '_shipping_last_name', ''),
(1300, 144, '_shipping_company', ''),
(1301, 144, '_shipping_address_1', ''),
(1302, 144, '_shipping_address_2', ''),
(1303, 144, '_shipping_city', ''),
(1304, 144, '_shipping_state', ''),
(1305, 144, '_shipping_postcode', ''),
(1306, 144, '_shipping_country', ''),
(1307, 144, '_order_currency', 'GBP'),
(1308, 144, '_cart_discount', '0'),
(1309, 144, '_cart_discount_tax', '0'),
(1310, 144, '_order_shipping', '0.00'),
(1311, 144, '_order_shipping_tax', '0'),
(1312, 144, '_order_tax', '0'),
(1313, 144, '_order_total', '150.00'),
(1314, 144, '_order_version', '3.4.5'),
(1315, 144, '_prices_include_tax', 'no'),
(1316, 144, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(1317, 144, '_shipping_address_index', '        '),
(1318, 144, '_download_permissions_granted', 'yes'),
(1319, 144, '_recorded_sales', 'yes'),
(1320, 144, '_recorded_coupon_usage_counts', 'yes'),
(1321, 144, '_order_stock_reduced', 'yes'),
(1322, 145, '_order_key', 'wc_order_5bbdbf407fad4'),
(1323, 145, '_customer_user', '1'),
(1324, 145, '_payment_method', 'cod'),
(1325, 145, '_payment_method_title', 'Cash on delivery'),
(1326, 145, '_transaction_id', ''),
(1327, 145, '_customer_ip_address', '::1'),
(1328, 145, '_customer_user_agent', 'mozilla/5.0 (windows nt 10.0; win64; x64) applewebkit/537.36 (khtml, like gecko) chrome/69.0.3497.100 safari/537.36'),
(1329, 145, '_created_via', 'checkout'),
(1330, 145, '_date_completed', ''),
(1331, 145, '_completed_date', ''),
(1332, 145, '_date_paid', ''),
(1333, 145, '_paid_date', ''),
(1334, 145, '_cart_hash', '9b98f6de682b5d55124cf4a1fe39b4f7'),
(1335, 145, '_billing_first_name', 'Martina'),
(1336, 145, '_billing_last_name', 'Erickson'),
(1337, 145, '_billing_company', 'Valentine and Sloan LLC'),
(1338, 145, '_billing_address_1', '606 Rocky Second Street'),
(1339, 145, '_billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(1340, 145, '_billing_city', 'ahmedab'),
(1341, 145, '_billing_state', 'AP');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1342, 145, '_billing_postcode', '78965-411'),
(1343, 145, '_billing_country', 'BR'),
(1344, 145, '_billing_email', 'qogybyzud@mailinator.net'),
(1345, 145, '_billing_phone', '+442-68-9152410'),
(1346, 145, '_shipping_first_name', ''),
(1347, 145, '_shipping_last_name', ''),
(1348, 145, '_shipping_company', ''),
(1349, 145, '_shipping_address_1', ''),
(1350, 145, '_shipping_address_2', ''),
(1351, 145, '_shipping_city', ''),
(1352, 145, '_shipping_state', ''),
(1353, 145, '_shipping_postcode', ''),
(1354, 145, '_shipping_country', ''),
(1355, 145, '_order_currency', 'GBP'),
(1356, 145, '_cart_discount', '0'),
(1357, 145, '_cart_discount_tax', '0'),
(1358, 145, '_order_shipping', '0.00'),
(1359, 145, '_order_shipping_tax', '0'),
(1360, 145, '_order_tax', '0'),
(1361, 145, '_order_total', '200.00'),
(1362, 145, '_order_version', '3.4.5'),
(1363, 145, '_prices_include_tax', 'no'),
(1364, 145, '_billing_address_index', 'Martina Erickson Valentine and Sloan LLC 606 Rocky Second Street Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et ahmedab AP 78965-411 BR qogybyzud@mailinator.net +442-68-9152410'),
(1365, 145, '_shipping_address_index', '        '),
(1366, 145, '_download_permissions_granted', 'yes'),
(1367, 145, '_recorded_sales', 'yes'),
(1368, 145, '_recorded_coupon_usage_counts', 'yes'),
(1369, 145, '_order_stock_reduced', 'yes'),
(1370, 146, '_edit_lock', '1539162020:1'),
(1371, 147, '_menu_item_type', 'taxonomy'),
(1372, 147, '_menu_item_menu_item_parent', '0'),
(1373, 147, '_menu_item_object_id', '57'),
(1374, 147, '_menu_item_object', 'product_cat'),
(1375, 147, '_menu_item_target', ''),
(1376, 147, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1377, 147, '_menu_item_xfn', ''),
(1378, 147, '_menu_item_url', ''),
(1379, 147, '_menu_item_orphaned', '1539164872'),
(1380, 148, '_menu_item_type', 'taxonomy'),
(1381, 148, '_menu_item_menu_item_parent', '0'),
(1382, 148, '_menu_item_object_id', '56'),
(1383, 148, '_menu_item_object', 'product_cat'),
(1384, 148, '_menu_item_target', ''),
(1385, 148, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1386, 148, '_menu_item_xfn', ''),
(1387, 148, '_menu_item_url', ''),
(1388, 148, '_menu_item_orphaned', '1539164872'),
(1389, 149, '_menu_item_type', 'taxonomy'),
(1390, 149, '_menu_item_menu_item_parent', '0'),
(1391, 149, '_menu_item_object_id', '57'),
(1392, 149, '_menu_item_object', 'product_cat'),
(1393, 149, '_menu_item_target', ''),
(1394, 149, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1395, 149, '_menu_item_xfn', ''),
(1396, 149, '_menu_item_url', ''),
(1398, 150, '_menu_item_type', 'taxonomy'),
(1399, 150, '_menu_item_menu_item_parent', '149'),
(1400, 150, '_menu_item_object_id', '56'),
(1401, 150, '_menu_item_object', 'product_cat'),
(1402, 150, '_menu_item_target', ''),
(1403, 150, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1404, 150, '_menu_item_xfn', ''),
(1405, 150, '_menu_item_url', ''),
(1407, 151, '_menu_item_type', 'taxonomy'),
(1408, 151, '_menu_item_menu_item_parent', '149'),
(1409, 151, '_menu_item_object_id', '58'),
(1410, 151, '_menu_item_object', 'product_cat'),
(1411, 151, '_menu_item_target', ''),
(1412, 151, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1413, 151, '_menu_item_xfn', ''),
(1414, 151, '_menu_item_url', ''),
(1416, 152, '_edit_lock', '1539173655:1'),
(1417, 152, '_edit_last', '1');

-- --------------------------------------------------------

--
-- Table structure for table `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2018-09-28 05:00:55', '2018-09-28 05:00:55', 'Welcome to WordPress. This is your first post. Edit or delete it, then start writing!', 'Hello world!', '', 'publish', 'open', 'open', '', 'hello-world', '', '', '2018-09-28 05:00:55', '2018-09-28 05:00:55', '', 0, 'http://localhost/wordpress-project/jess/?p=1', 0, 'post', '', 1),
(2, 1, '2018-09-28 05:00:55', '2018-09-28 05:00:55', 'This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:\n\n<blockquote>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</blockquote>\n\n...or something like this:\n\n<blockquote>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</blockquote>\n\nAs a new WordPress user, you should go to <a href=\"http://localhost/wordpress-project/jess/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!', 'Sample Page', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2018-09-28 05:00:55', '2018-09-28 05:00:55', '', 0, 'http://localhost/wordpress-project/jess/?page_id=2', 0, 'page', '', 0),
(3, 1, '2018-09-28 05:00:55', '2018-09-28 05:00:55', '<h2>Who we are</h2>\r\nOur website address is: http://localhost/wordpress-project/jess.\r\n<h2>What personal data we collect and why we collect it</h2>\r\n<h3>Comments</h3>\r\nWhen visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.\r\n\r\nAn anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.\r\n<h3>Media</h3>\r\nIf you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.\r\n<h3>Contact forms</h3>\r\n<h3>Cookies</h3>\r\nIf you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.\r\n\r\nIf you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.\r\n\r\nWhen you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select \"Remember Me\", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.\r\n\r\nIf you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.\r\n<h3>Embedded content from other websites</h3>\r\nArticles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.\r\n\r\nThese websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.\r\n<h3>Analytics</h3>\r\n<h2>Who we share your data with</h2>\r\n<h2>How long we retain your data</h2>\r\nIf you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.\r\n\r\nFor users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.\r\n<h2>What rights you have over your data</h2>\r\nIf you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.\r\n<h2>Where we send your data</h2>\r\nVisitor comments may be checked through an automated spam detection service.\r\n<h2>Your contact information</h2>\r\n<h2>Additional information</h2>\r\n<h3>How we protect your data</h3>\r\n<h3>What data breach procedures we have in place</h3>\r\n<h3>What third parties we receive data from</h3>\r\n<h3>What automated decision making and/or profiling we do with user data</h3>\r\n<h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'trash', 'closed', 'open', '', 'privacy-policy__trashed', '', '', '2018-10-09 10:33:00', '2018-10-09 10:33:00', '', 0, 'http://localhost/wordpress-project/jess/?page_id=3', 0, 'page', '', 0),
(5, 1, '2018-09-28 05:02:08', '2018-09-28 05:02:08', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2018-09-28 05:02:08', '2018-09-28 05:02:08', '', 0, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/09/logo.png', 0, 'attachment', 'image/png', 0),
(16, 1, '2018-09-28 07:24:24', '2018-09-28 07:24:24', 'Home Page', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-09-28 07:24:24', '2018-09-28 07:24:24', '', 0, 'http://localhost/wordpress-project/jess/?page_id=16', 0, 'page', '', 0),
(17, 1, '2018-09-28 07:24:24', '2018-09-28 07:24:24', 'Home Page', 'Home', '', 'inherit', 'closed', 'closed', '', '16-revision-v1', '', '', '2018-09-28 07:24:24', '2018-09-28 07:24:24', '', 16, 'http://localhost/wordpress-project/jess/2018/09/28/16-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2018-10-08 14:54:59', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2018-10-08 14:54:59', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-project/jess/?p=18', 0, 'post', '', 0),
(19, 1, '2018-10-09 05:29:17', '2018-10-09 05:29:17', '', 'Shop', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2018-10-09 14:12:40', '2018-10-09 14:12:40', '', 0, 'http://localhost/wordpress-project/jess/?page_id=19', 0, 'page', '', 0),
(20, 1, '2018-10-09 05:29:17', '2018-10-09 05:29:17', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-10-09 05:29:17', '2018-10-09 05:29:17', '', 19, 'http://localhost/wordpress-project/jess/2018/10/09/19-revision-v1/', 0, 'revision', '', 0),
(21, 1, '2018-10-09 05:29:30', '2018-10-09 05:29:30', '<pre>[woocommerce_cart]</pre>', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2018-10-09 05:29:30', '2018-10-09 05:29:30', '', 0, 'http://localhost/wordpress-project/jess/?page_id=21', 0, 'page', '', 0),
(22, 1, '2018-10-09 05:29:30', '2018-10-09 05:29:30', '<pre>[woocommerce_cart]</pre>', 'Cart', '', 'inherit', 'closed', 'closed', '', '21-revision-v1', '', '', '2018-10-09 05:29:30', '2018-10-09 05:29:30', '', 21, 'http://localhost/wordpress-project/jess/2018/10/09/21-revision-v1/', 0, 'revision', '', 0),
(23, 1, '2018-10-09 05:29:46', '2018-10-09 05:29:46', '<pre>[woocommerce_checkout]</pre>', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2018-10-09 05:29:46', '2018-10-09 05:29:46', '', 0, 'http://localhost/wordpress-project/jess/?page_id=23', 0, 'page', '', 0),
(24, 1, '2018-10-09 05:29:46', '2018-10-09 05:29:46', '<pre>[woocommerce_checkout]</pre>', 'Checkout', '', 'inherit', 'closed', 'closed', '', '23-revision-v1', '', '', '2018-10-09 05:29:46', '2018-10-09 05:29:46', '', 23, 'http://localhost/wordpress-project/jess/2018/10/09/23-revision-v1/', 0, 'revision', '', 0),
(25, 1, '2018-10-09 05:30:28', '2018-10-09 05:30:28', '<pre>[woocommerce_my_account]</pre>', 'My Account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2018-10-09 05:39:22', '2018-10-09 05:39:22', '', 0, 'http://localhost/wordpress-project/jess/?page_id=25', 0, 'page', '', 0),
(26, 1, '2018-10-09 05:30:28', '2018-10-09 05:30:28', '<pre>[woocommerce_my_account]</pre>', 'My Account', '', 'inherit', 'closed', 'closed', '', '25-revision-v1', '', '', '2018-10-09 05:30:28', '2018-10-09 05:30:28', '', 25, 'http://localhost/wordpress-project/jess/2018/10/09/25-revision-v1/', 0, 'revision', '', 0),
(27, 1, '2018-10-09 05:39:33', '2018-10-09 05:39:33', '[products]', 'Shop', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-10-09 05:39:33', '2018-10-09 05:39:33', '', 19, 'http://localhost/wordpress-project/jess/2018/10/09/19-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2018-10-09 05:40:45', '2018-10-09 05:40:45', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-10-09 05:40:45', '2018-10-09 05:40:45', '', 19, 'http://localhost/wordpress-project/jess/2018/10/09/19-revision-v1/', 0, 'revision', '', 0),
(29, 1, '2018-10-09 05:42:27', '2018-10-09 05:42:27', '', 'YUKON GEAR & AXLE AXLE BEARING PULLER TOOL **UNIVERSAL**', '', 'publish', 'open', 'closed', '', 'yukon-gear-axle-axle-bearing-puller-tool-universal', '', '', '2018-10-09 07:38:37', '2018-10-09 07:38:37', '', 0, 'http://localhost/wordpress-project/jess/?post_type=product&#038;p=29', 0, 'product', '', 0),
(30, 1, '2018-10-09 05:42:04', '2018-10-09 05:42:04', '', 'YTP70', '', 'inherit', 'open', 'closed', '', 'ytp70', '', '', '2018-10-09 05:42:04', '2018-10-09 05:42:04', '', 29, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/YTP70.jpg', 0, 'attachment', 'image/jpeg', 0),
(31, 1, '2018-10-09 05:42:50', '2018-10-09 05:42:50', '', 'YUKON GEAR & AXLE CLAMSHELL RETENTION SLEEVE CARRIER BEARING PULLER **UNIVERSAL**', '', 'publish', 'open', 'closed', '', 'yukon-gear-axle-clamshell-retention-sleeve-carrier-bearing-puller-universal', '', '', '2018-10-09 07:38:02', '2018-10-09 07:38:02', '', 0, 'http://localhost/wordpress-project/jess/?post_type=product&#038;p=31', 0, 'product', '', 0),
(32, 1, '2018-10-09 05:42:45', '2018-10-09 05:42:45', '', 'YTP09', '', 'inherit', 'open', 'closed', '', 'ytp09', '', '', '2018-10-09 05:42:45', '2018-10-09 05:42:45', '', 31, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/YTP09.jpg', 0, 'attachment', 'image/jpeg', 0),
(33, 1, '2018-10-09 05:45:00', '2018-10-09 05:45:00', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '12 Volt DC 30 Amp Relay *Universal*', '', 'publish', 'open', 'closed', '', '12-volt-dc-30-amp-relay-universal', '', '', '2018-10-09 12:30:38', '2018-10-09 12:30:38', '', 0, 'http://localhost/wordpress-project/jess/?post_type=product&#038;p=33', 0, 'product', '', 0),
(34, 1, '2018-10-09 05:44:39', '2018-10-09 05:44:39', '', '851062_1', '', 'inherit', 'open', 'closed', '', '851062_1', '', '', '2018-10-09 05:44:39', '2018-10-09 05:44:39', '', 33, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/851062_1.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2018-10-09 05:44:42', '2018-10-09 05:44:42', '', '851063', '', 'inherit', 'open', 'closed', '', '851063', '', '', '2018-10-09 05:44:42', '2018-10-09 05:44:42', '', 33, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/851063.jpg', 0, 'attachment', 'image/jpeg', 0),
(36, 1, '2018-10-09 05:47:00', '2018-10-09 05:47:00', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n\r\n&nbsp;\r\n\r\n<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '12V AUXILIARY WIRING KIT W/ ILLUMINATED SWITCH *UNIVERSAL*', '<strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'publish', 'open', 'closed', '', '12v-auxiliary-wiring-kit-w-illuminated-switch-universal', '', '', '2018-10-09 12:30:17', '2018-10-09 12:30:17', '', 0, 'http://localhost/wordpress-project/jess/?post_type=product&#038;p=36', 0, 'product', '', 0),
(37, 1, '2018-10-09 06:16:55', '2018-10-09 06:16:55', '', 'Order &ndash; October 9, 2018 @ 06:16 AM', 'Est omnis ratione ipsum consequatur Aliquam distinctio In', 'wc-processing', 'open', 'closed', 'order_5bbc47d7701d7', 'order-oct-09-2018-0616-am', '', '', '2018-10-09 06:16:58', '2018-10-09 06:16:58', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=37', 0, 'shop_order', '', 1),
(38, 1, '2018-10-09 06:31:08', '2018-10-09 06:31:08', '', 'Order &ndash; October 9, 2018 @ 06:31 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc4b2cef795', 'order-oct-09-2018-0631-am', '', '', '2018-10-09 06:31:11', '2018-10-09 06:31:11', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=38', 0, 'shop_order', '', 1),
(39, 1, '2018-10-09 06:38:18', '2018-10-09 06:38:18', '', 'Order &ndash; October 9, 2018 @ 06:38 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc4cda386cc', 'order-oct-09-2018-0638-am', '', '', '2018-10-09 06:38:21', '2018-10-09 06:38:21', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=39', 0, 'shop_order', '', 1),
(40, 1, '2018-10-09 06:59:50', '2018-10-09 06:59:50', '', 'Order &ndash; October 9, 2018 @ 06:59 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc51e65f221', 'order-oct-09-2018-0659-am', '', '', '2018-10-09 06:59:53', '2018-10-09 06:59:53', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=40', 0, 'shop_order', '', 1),
(41, 1, '2018-10-09 07:23:04', '2018-10-09 07:23:04', '', 'Order &ndash; October 9, 2018 @ 07:23 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc5758ba99c', 'order-oct-09-2018-0723-am', '', '', '2018-10-09 07:23:08', '2018-10-09 07:23:08', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=41', 0, 'shop_order', '', 1),
(42, 1, '2018-10-09 07:39:14', '2018-10-09 07:39:14', '', 'Order &ndash; October 9, 2018 @ 07:39 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc5b22b8358', 'order-oct-09-2018-0739-am', '', '', '2018-10-09 07:39:17', '2018-10-09 07:39:17', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=42', 0, 'shop_order', '', 1),
(43, 1, '2018-10-09 09:44:26', '2018-10-09 09:44:26', '', 'Order &ndash; October 9, 2018 @ 09:44 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc787aaf0c7', 'order-oct-09-2018-0944-am', '', '', '2018-10-09 09:44:30', '2018-10-09 09:44:30', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=43', 0, 'shop_order', '', 1),
(44, 1, '2018-10-09 09:51:57', '2018-10-09 09:51:57', 'Jess Performance began in 2009 in Shipshewana, IN by owner Jesse Yoder selling performance parts and evolved into a full service facility.\r\nWe have over 29 years of combined experience.\r\n\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris augue leo, consequat ut sem at, commodo sagittis tortor. In et lectus lorem. Etiam sit amet elementum orci, ut bibendum ligula. Nulla volutpat magna nulla, tempus laoreet turpis vehicula eget. Cras id ipsum vestibulum, facilisis felis eleifend, vehicula sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam tempus vel massa et laoreet. Vestibulum elementum erat ac dictum aliquam. Sed eget ligula lobortis, venenatis magna id, tristique ex. Aenean aliquet tellus ligula, efficitur molestie sapien elementum sed. Sed sagittis elementum turpis sit amet sodales. Vivamus in rhoncus eros. Curabitur eu odio nec purus vehicula consequat. Etiam feugiat, justo eu tincidunt consectetur, sapien arcu accumsan lectus, a luctus sapien ligula ac erat. Sed enim turpis, malesuada ullamcorper risus nec, bibendum pretium eros.\r\n\r\nQuisque dignissim congue odio maximus aliquam. Sed congue nunc nec posuere varius. Etiam hendrerit mi nec justo euismod blandit a non ex. Etiam ac ipsum risus. Phasellus sit amet massa a nibh imperdiet volutpat. Vestibulum vehicula, metus sit amet imperdiet pretium, erat lectus euismod orci, quis dictum sem elit id nunc. Vestibulum ac consectetur ante. Etiam et tincidunt neque, et facilisis felis. Quisque ut sem ut felis varius semper nec non nibh. Vivamus dapibus sem non tortor cursus, id condimentum tortor tempor. Curabitur scelerisque placerat arcu id lacinia. Sed faucibus fringilla lectus ut lacinia. Proin at tortor ut nibh porttitor tempus. Aliquam aliquam diam sit amet nisi congue, a fermentum metus hendrerit. Duis ut tempus elit.\r\n\r\nMaecenas lacinia gravida elementum. Morbi rhoncus interdum convallis. Morbi vel est fringilla, congue arcu sed, viverra elit. Ut dapibus suscipit viverra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam sit amet vestibulum sem. Aliquam eget nisi aliquam, imperdiet est in, blandit purus. Suspendisse vulputate viverra lectus, vitae facilisis ante lobortis eu. Suspendisse neque urna, auctor ac viverra sed, posuere eget mi. Nunc blandit turpis ac est pretium, a faucibus enim dapibus. Nam sodales nulla a rhoncus accumsan. Curabitur dui quam, tristique sed magna a, laoreet placerat purus. Nulla ut est felis. Duis molestie dolor at nibh aliquet, faucibus sodales ante efficitur.\r\n\r\nCras molestie pharetra tortor. Mauris aliquam tortor nec tristique fringilla. Phasellus sollicitudin egestas porttitor. Vestibulum luctus, metus eget ornare mollis, turpis orci blandit leo, nec consequat libero nulla a arcu. Duis pharetra sodales diam ut malesuada. Nulla vel consequat nisi. Integer ultricies augue erat. Nullam venenatis ut ipsum vel egestas.\r\n\r\nNullam lobortis, diam et tincidunt mollis, sapien nisl varius ipsum, a mollis justo est id risus. Sed gravida vestibulum mi ac dictum. Donec augue dolor, laoreet id sagittis nec, rutrum vitae eros. Nulla fermentum mi in odio dignissim, sed rhoncus mauris convallis. Donec vitae lorem tincidunt, luctus purus vitae, tincidunt turpis. Duis molestie ex et elementum faucibus. Phasellus vehicula consectetur velit eu tempor. Sed non accumsan dolor. Etiam non pulvinar leo, nec venenatis mauris.\r\n\r\n', 'About Us', '', 'publish', 'closed', 'closed', '', 'about-us', '', '', '2018-10-09 11:18:44', '2018-10-09 11:18:44', '', 0, 'http://localhost/wordpress-project/jess/?page_id=44', 0, 'page', '', 0),
(45, 1, '2018-10-09 09:51:57', '2018-10-09 09:51:57', 'Jess Performance began in 2009 in Shipshewana, IN by owner Jesse Yoder selling performance parts and evolved into a full service facility.\r\nWe have over 29 years of combined experience.', 'About Us', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2018-10-09 09:51:57', '2018-10-09 09:51:57', '', 44, 'http://localhost/wordpress-project/jess/2018/10/09/44-revision-v1/', 0, 'revision', '', 0),
(46, 1, '2018-10-09 09:55:06', '2018-10-09 09:55:06', '', 'Contact Us', '', 'publish', 'closed', 'closed', '', 'contact-us', '', '', '2018-10-09 11:19:14', '2018-10-09 11:19:14', '', 0, 'http://localhost/wordpress-project/jess/?page_id=46', 0, 'page', '', 0),
(47, 1, '2018-10-09 09:55:06', '2018-10-09 09:55:06', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-10-09 09:55:06', '2018-10-09 09:55:06', '', 46, 'http://localhost/wordpress-project/jess/2018/10/09/46-revision-v1/', 0, 'revision', '', 0),
(48, 1, '2018-10-09 10:07:02', '2018-10-09 10:07:02', '', 'Order &ndash; October 9, 2018 @ 10:07 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc7dc678409', 'order-oct-09-2018-1007-am', '', '', '2018-10-09 10:07:06', '2018-10-09 10:07:06', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=48', 0, 'shop_order', '', 1),
(49, 1, '2018-10-09 10:11:17', '2018-10-09 10:11:17', '<form class=\"clearfix\">\r\n    <div class=\"row\">\r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>First Name<sup class=\"error\">*</sup>:</label>\r\n                [text* first class:form-control  ]\r\n            </div>\r\n        </div> \r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>Last Name<sup class=\"error\">*</sup>:</label>\r\n                [text* last class:form-control  ]\r\n            </div>\r\n        </div>        \r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>Email<sup class=\"error\">*</sup>:</label>\r\n                [email* email class:form-control ]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>Phone<sup class=\"error\">*</sup>:</label>\r\n                [text* phone class:form-control  ]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>Address:</label>\r\n                [textarea address class:form-control rows:5]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>City<sup class=\"error\">*</sup>:</label>\r\n                [text* city class:form-control  ]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>State/Province<sup class=\"error\">*</sup>:</label>\r\n                [text* state class:form-control  ]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-6 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>Zip/Postal<sup class=\"error\">*</sup>:</label>\r\n                [text* zip class:form-control  ]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-sm-12\">\r\n            <div class=\"form-group\">\r\n                <label>Questions Or Comments:</label>\r\n                [textarea comments class:form-control rows:5]\r\n            </div>\r\n        </div>\r\n        <div class=\"col-md-12 col-sm-12\">\r\n            <div class=\"slide-Link\">\r\n                [submit  class:btn-more-red-link \"submit\" ]\r\n            </div>\r\n        </div>\r\n    </div>\r\n</form>\n1\nJess Contact Inquiry\n[first] [last] <[email]>\nuser.citrusbug@gmail.com\nFrom: [first] [last] <[email]>\r\nPhone No. : [phone]\r\nAddress : [address]\r\nCity : [city]\r\nState : [state]\r\nZip : [zip]\r\n\r\nMessage Body:\r\n[comments]\nReply-To: [email]\n\n1\n\n\nJess \"[your-subject]\"\nJess <user.citrusbug@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Jess (http://localhost/wordpress-project/jess)\nReply-To: user.citrusbug@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Contact form', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2018-10-09 11:11:58', '2018-10-09 11:11:58', '', 0, 'http://localhost/wordpress-project/jess/?post_type=wpcf7_contact_form&#038;p=49', 0, 'wpcf7_contact_form', '', 0),
(50, 1, '2018-10-09 10:11:44', '2018-10-09 10:11:44', '', 'Order &ndash; October 9, 2018 @ 10:11 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbc7ee0ca23f', 'order-oct-09-2018-1011-am', '', '', '2018-10-09 10:11:47', '2018-10-09 10:11:47', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=50', 0, 'shop_order', '', 1),
(51, 1, '2018-10-09 10:26:49', '2018-10-09 10:26:49', ' ', '', '', 'publish', 'closed', 'closed', '', '51', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 0, 'http://localhost/wordpress-project/jess/?p=51', 1, 'nav_menu_item', '', 0),
(52, 1, '2018-10-09 10:26:50', '2018-10-09 10:26:50', ' ', '', '', 'publish', 'closed', 'closed', '', '52', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 31, 'http://localhost/wordpress-project/jess/?p=52', 3, 'nav_menu_item', '', 0),
(53, 1, '2018-10-09 10:26:50', '2018-10-09 10:26:50', ' ', '', '', 'publish', 'closed', 'closed', '', '53', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 31, 'http://localhost/wordpress-project/jess/?p=53', 4, 'nav_menu_item', '', 0),
(54, 1, '2018-10-09 10:26:50', '2018-10-09 10:26:50', ' ', '', '', 'publish', 'closed', 'closed', '', '54', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 31, 'http://localhost/wordpress-project/jess/?p=54', 5, 'nav_menu_item', '', 0),
(55, 1, '2018-10-09 10:26:51', '2018-10-09 10:26:51', ' ', '', '', 'publish', 'closed', 'closed', '', '55', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 31, 'http://localhost/wordpress-project/jess/?p=55', 6, 'nav_menu_item', '', 0),
(56, 1, '2018-10-09 10:26:51', '2018-10-09 10:26:51', ' ', '', '', 'publish', 'closed', 'closed', '', '56', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 31, 'http://localhost/wordpress-project/jess/?p=56', 7, 'nav_menu_item', '', 0),
(57, 1, '2018-10-09 10:26:49', '2018-10-09 10:26:49', ' ', '', '', 'publish', 'closed', 'closed', '', '57', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 31, 'http://localhost/wordpress-project/jess/?p=57', 2, 'nav_menu_item', '', 0),
(58, 1, '2018-10-09 10:26:51', '2018-10-09 10:26:51', ' ', '', '', 'publish', 'closed', 'closed', '', '58', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 0, 'http://localhost/wordpress-project/jess/?p=58', 8, 'nav_menu_item', '', 0),
(59, 1, '2018-10-09 10:26:52', '2018-10-09 10:26:52', ' ', '', '', 'publish', 'closed', 'closed', '', '59', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 45, 'http://localhost/wordpress-project/jess/?p=59', 10, 'nav_menu_item', '', 0),
(60, 1, '2018-10-09 10:26:52', '2018-10-09 10:26:52', ' ', '', '', 'publish', 'closed', 'closed', '', '60', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 45, 'http://localhost/wordpress-project/jess/?p=60', 11, 'nav_menu_item', '', 0),
(61, 1, '2018-10-09 10:26:52', '2018-10-09 10:26:52', ' ', '', '', 'publish', 'closed', 'closed', '', '61', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 45, 'http://localhost/wordpress-project/jess/?p=61', 12, 'nav_menu_item', '', 0),
(62, 1, '2018-10-09 10:26:53', '2018-10-09 10:26:53', ' ', '', '', 'publish', 'closed', 'closed', '', '62', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 45, 'http://localhost/wordpress-project/jess/?p=62', 13, 'nav_menu_item', '', 0),
(63, 1, '2018-10-09 10:26:53', '2018-10-09 10:26:53', ' ', '', '', 'publish', 'closed', 'closed', '', '63', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 45, 'http://localhost/wordpress-project/jess/?p=63', 14, 'nav_menu_item', '', 0),
(64, 1, '2018-10-09 10:26:53', '2018-10-09 10:26:53', ' ', '', '', 'publish', 'closed', 'closed', '', '64', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 45, 'http://localhost/wordpress-project/jess/?p=64', 15, 'nav_menu_item', '', 0),
(65, 1, '2018-10-09 10:26:52', '2018-10-09 10:26:52', ' ', '', '', 'publish', 'closed', 'closed', '', '65', '', '', '2018-10-10 09:49:41', '2018-10-10 09:49:41', '', 45, 'http://localhost/wordpress-project/jess/?p=65', 9, 'nav_menu_item', '', 0),
(66, 1, '2018-10-09 10:26:53', '2018-10-09 10:26:53', ' ', '', '', 'publish', 'closed', 'closed', '', '66', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 0, 'http://localhost/wordpress-project/jess/?p=66', 16, 'nav_menu_item', '', 0),
(67, 1, '2018-10-09 10:26:54', '2018-10-09 10:26:54', ' ', '', '', 'publish', 'closed', 'closed', '', '67', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 38, 'http://localhost/wordpress-project/jess/?p=67', 19, 'nav_menu_item', '', 0),
(68, 1, '2018-10-09 10:26:54', '2018-10-09 10:26:54', ' ', '', '', 'publish', 'closed', 'closed', '', '68', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 38, 'http://localhost/wordpress-project/jess/?p=68', 18, 'nav_menu_item', '', 0),
(69, 1, '2018-10-09 10:26:54', '2018-10-09 10:26:54', ' ', '', '', 'publish', 'closed', 'closed', '', '69', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 38, 'http://localhost/wordpress-project/jess/?p=69', 20, 'nav_menu_item', '', 0),
(70, 1, '2018-10-09 10:26:54', '2018-10-09 10:26:54', ' ', '', '', 'publish', 'closed', 'closed', '', '70', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 38, 'http://localhost/wordpress-project/jess/?p=70', 21, 'nav_menu_item', '', 0),
(71, 1, '2018-10-09 10:26:55', '2018-10-09 10:26:55', ' ', '', '', 'publish', 'closed', 'closed', '', '71', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 38, 'http://localhost/wordpress-project/jess/?p=71', 22, 'nav_menu_item', '', 0),
(72, 1, '2018-10-09 10:26:53', '2018-10-09 10:26:53', ' ', '', '', 'publish', 'closed', 'closed', '', '72', '', '', '2018-10-10 09:49:42', '2018-10-10 09:49:42', '', 38, 'http://localhost/wordpress-project/jess/?p=72', 17, 'nav_menu_item', '', 0),
(74, 1, '2018-10-09 10:27:45', '2018-10-09 10:27:45', '', 'BRANDS', '', 'publish', 'closed', 'closed', '', 'brands', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 0, 'http://localhost/wordpress-project/jess/?p=74', 26, 'nav_menu_item', '', 0),
(75, 1, '2018-10-09 10:27:46', '2018-10-09 10:27:46', ' ', '', '', 'publish', 'closed', 'closed', '', '75', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 0, 'http://localhost/wordpress-project/jess/?p=75', 27, 'nav_menu_item', '', 0),
(76, 1, '2018-10-09 10:33:20', '2018-10-09 10:33:20', ' ', '', '', 'publish', 'closed', 'closed', '', '76', '', '', '2018-10-09 10:36:16', '2018-10-09 10:36:16', '', 0, 'http://localhost/wordpress-project/jess/?p=76', 1, 'nav_menu_item', '', 0),
(77, 1, '2018-10-09 10:32:01', '2018-10-09 10:32:01', '<h2>Who we are</h2>\r\nOur website address is: http://localhost/wordpress-project/jess.\r\n<h2>What personal data we collect and why we collect it</h2>\r\n<h3>Comments</h3>\r\nWhen visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection.\r\n\r\nAn anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.\r\n<h3>Media</h3>\r\nIf you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.\r\n<h3>Contact forms</h3>\r\n<h3>Cookies</h3>\r\nIf you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.\r\n\r\nIf you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.\r\n\r\nWhen you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select \"Remember Me\", your login will persist for two weeks. If you log out of your account, the login cookies will be removed.\r\n\r\nIf you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.\r\n<h3>Embedded content from other websites</h3>\r\nArticles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.\r\n\r\nThese websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.\r\n<h3>Analytics</h3>\r\n<h2>Who we share your data with</h2>\r\n<h2>How long we retain your data</h2>\r\nIf you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.\r\n\r\nFor users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.\r\n<h2>What rights you have over your data</h2>\r\nIf you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.\r\n<h2>Where we send your data</h2>\r\nVisitor comments may be checked through an automated spam detection service.\r\n<h2>Your contact information</h2>\r\n<h2>Additional information</h2>\r\n<h3>How we protect your data</h3>\r\n<h3>What data breach procedures we have in place</h3>\r\n<h3>What third parties we receive data from</h3>\r\n<h3>What automated decision making and/or profiling we do with user data</h3>\r\n<h3>Industry regulatory disclosure requirements</h3>', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2018-10-09 10:32:01', '2018-10-09 10:32:01', '', 3, 'http://localhost/wordpress-project/jess/2018/10/09/3-revision-v1/', 0, 'revision', '', 0),
(78, 1, '2018-10-09 10:33:13', '2018-10-09 10:33:13', '', 'Privacy Policy', '', 'publish', 'closed', 'closed', '', 'privacy-policy', '', '', '2018-10-09 10:33:13', '2018-10-09 10:33:13', '', 0, 'http://localhost/wordpress-project/jess/?page_id=78', 0, 'page', '', 0),
(79, 1, '2018-10-09 10:33:13', '2018-10-09 10:33:13', '', 'Privacy Policy', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2018-10-09 10:33:13', '2018-10-09 10:33:13', '', 78, 'http://localhost/wordpress-project/jess/2018/10/09/78-revision-v1/', 0, 'revision', '', 0),
(80, 1, '2018-10-09 10:33:32', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-10-09 10:33:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-project/jess/?p=80', 1, 'nav_menu_item', '', 0),
(81, 1, '2018-10-09 10:35:07', '2018-10-09 10:35:07', '', 'Our Blog', '', 'publish', 'closed', 'closed', '', 'our-blog', '', '', '2018-10-09 10:35:07', '2018-10-09 10:35:07', '', 0, 'http://localhost/wordpress-project/jess/?page_id=81', 0, 'page', '', 0),
(82, 1, '2018-10-09 10:35:07', '2018-10-09 10:35:07', '', 'Our Blog', '', 'inherit', 'closed', 'closed', '', '81-revision-v1', '', '', '2018-10-09 10:35:07', '2018-10-09 10:35:07', '', 81, 'http://localhost/wordpress-project/jess/2018/10/09/81-revision-v1/', 0, 'revision', '', 0),
(83, 1, '2018-10-09 10:35:28', '2018-10-09 10:35:28', '', 'Terms And Conditions', '', 'publish', 'closed', 'closed', '', 'terms-and-conditions', '', '', '2018-10-09 10:35:28', '2018-10-09 10:35:28', '', 0, 'http://localhost/wordpress-project/jess/?page_id=83', 0, 'page', '', 0),
(84, 1, '2018-10-09 10:35:28', '2018-10-09 10:35:28', '', 'Terms And Conditions', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2018-10-09 10:35:28', '2018-10-09 10:35:28', '', 83, 'http://localhost/wordpress-project/jess/2018/10/09/83-revision-v1/', 0, 'revision', '', 0),
(85, 1, '2018-10-09 10:36:17', '2018-10-09 10:36:17', ' ', '', '', 'publish', 'closed', 'closed', '', '85', '', '', '2018-10-09 10:36:17', '2018-10-09 10:36:17', '', 0, 'http://localhost/wordpress-project/jess/?p=85', 5, 'nav_menu_item', '', 0),
(86, 1, '2018-10-09 10:36:16', '2018-10-09 10:36:16', ' ', '', '', 'publish', 'closed', 'closed', '', '86', '', '', '2018-10-09 10:36:16', '2018-10-09 10:36:16', '', 0, 'http://localhost/wordpress-project/jess/?p=86', 3, 'nav_menu_item', '', 0),
(87, 1, '2018-10-09 10:36:16', '2018-10-09 10:36:16', ' ', '', '', 'publish', 'closed', 'closed', '', '87', '', '', '2018-10-09 10:36:16', '2018-10-09 10:36:16', '', 0, 'http://localhost/wordpress-project/jess/?p=87', 2, 'nav_menu_item', '', 0),
(88, 1, '2018-10-09 10:36:16', '2018-10-09 10:36:16', '', 'Contact', '', 'publish', 'closed', 'closed', '', 'contact', '', '', '2018-10-09 10:36:16', '2018-10-09 10:36:16', '', 0, 'http://localhost/wordpress-project/jess/?p=88', 4, 'nav_menu_item', '', 0),
(89, 1, '2018-10-09 10:37:51', '2018-10-09 10:37:51', '', 'Home', '', 'publish', 'closed', 'closed', '', 'home', '', '', '2018-10-09 10:37:51', '2018-10-09 10:37:51', '', 0, 'http://localhost/wordpress-project/jess/?p=89', 1, 'nav_menu_item', '', 0),
(90, 1, '2018-10-09 10:37:51', '2018-10-09 10:37:51', '', 'Our Services', '', 'publish', 'closed', 'closed', '', 'our-services', '', '', '2018-10-09 10:37:51', '2018-10-09 10:37:51', '', 0, 'http://localhost/wordpress-project/jess/?p=90', 2, 'nav_menu_item', '', 0),
(91, 1, '2018-10-09 10:37:52', '2018-10-09 10:37:52', '', 'How it Works', '', 'publish', 'closed', 'closed', '', 'how-it-works', '', '', '2018-10-09 10:37:52', '2018-10-09 10:37:52', '', 0, 'http://localhost/wordpress-project/jess/?p=91', 3, 'nav_menu_item', '', 0),
(92, 1, '2018-10-09 10:37:52', '2018-10-09 10:37:52', '', 'FAQ', '', 'publish', 'closed', 'closed', '', 'faq', '', '', '2018-10-09 10:37:52', '2018-10-09 10:37:52', '', 0, 'http://localhost/wordpress-project/jess/?p=92', 4, 'nav_menu_item', '', 0),
(93, 1, '2018-10-09 10:37:52', '2018-10-09 10:37:52', '', 'Our Portfolio', '', 'publish', 'closed', 'closed', '', 'our-portfolio', '', '', '2018-10-09 10:37:52', '2018-10-09 10:37:52', '', 0, 'http://localhost/wordpress-project/jess/?p=93', 5, 'nav_menu_item', '', 0),
(94, 1, '2018-10-09 11:12:50', '2018-10-09 11:12:50', '[email* email class:form-control class:form-control-10 placeholder \"Email address\"]\r\n[submit class:btn class:btn-danger class:btn-scbscribe \"subscribe\"]\n1\nJess New Subscribe\n<user.citrusbug@gmail.com>\nuser.citrusbug@gmail.com\nFrom: [email] \r\n\r\nMessage Body:\r\nNew Subscription [email]\nReply-To: [email]\n\n1\n\n\nJess \"[your-subject]\"\nJess <user.citrusbug@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Jess (http://localhost/wordpress-project/jess)\nReply-To: user.citrusbug@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Subscribe Form', '', 'publish', 'closed', 'closed', '', 'subscribe-form', '', '', '2018-10-09 11:13:51', '2018-10-09 11:13:51', '', 0, 'http://localhost/wordpress-project/jess/?post_type=wpcf7_contact_form&#038;p=94', 0, 'wpcf7_contact_form', '', 0),
(95, 1, '2018-10-09 11:17:12', '2018-10-09 11:17:12', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"44\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'About Us Page', 'about-us-page', 'publish', 'closed', 'closed', '', 'group_5bbc8e0f763df', '', '', '2018-10-09 11:17:12', '2018-10-09 11:17:12', '', 0, 'http://localhost/wordpress-project/jess/?post_type=acf-field-group&#038;p=95', 0, 'acf-field-group', '', 0),
(96, 1, '2018-10-09 11:17:12', '2018-10-09 11:17:12', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Home Section Content', 'home_section_content', 'publish', 'closed', 'closed', '', 'field_5bbc8e24351e8', '', '', '2018-10-09 11:17:12', '2018-10-09 11:17:12', '', 95, 'http://localhost/wordpress-project/jess/?post_type=acf-field&p=96', 0, 'acf-field', '', 0),
(97, 1, '2018-10-09 11:17:44', '2018-10-09 11:17:44', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:2:\"46\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Contact Us Page', 'contact-us-page', 'publish', 'closed', 'closed', '', 'group_5bbc8e3bf1247', '', '', '2018-10-09 11:19:04', '2018-10-09 11:19:04', '', 0, 'http://localhost/wordpress-project/jess/?post_type=acf-field-group&#038;p=97', 0, 'acf-field-group', '', 0),
(98, 1, '2018-10-09 11:17:45', '2018-10-09 11:17:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Latitude', 'latitude', 'publish', 'closed', 'closed', '', 'field_5bbc8e3f16fbf', '', '', '2018-10-09 11:17:45', '2018-10-09 11:17:45', '', 97, 'http://localhost/wordpress-project/jess/?post_type=acf-field&p=98', 0, 'acf-field', '', 0),
(99, 1, '2018-10-09 11:17:45', '2018-10-09 11:17:45', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Longitude', 'longitude', 'publish', 'closed', 'closed', '', 'field_5bbc8e4816fc0', '', '', '2018-10-09 11:17:45', '2018-10-09 11:17:45', '', 97, 'http://localhost/wordpress-project/jess/?post_type=acf-field&p=99', 1, 'acf-field', '', 0),
(100, 1, '2018-10-09 11:18:38', '2018-10-09 11:18:38', '', 'about-us', '', 'inherit', 'open', 'closed', '', 'about-us-2', '', '', '2018-10-09 11:18:38', '2018-10-09 11:18:38', '', 44, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/about-us.jpg', 0, 'attachment', 'image/jpeg', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(101, 1, '2018-10-09 11:18:44', '2018-10-09 11:18:44', 'Jess Performance began in 2009 in Shipshewana, IN by owner Jesse Yoder selling performance parts and evolved into a full service facility.\r\nWe have over 29 years of combined experience.\r\n\r\n\r\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris augue leo, consequat ut sem at, commodo sagittis tortor. In et lectus lorem. Etiam sit amet elementum orci, ut bibendum ligula. Nulla volutpat magna nulla, tempus laoreet turpis vehicula eget. Cras id ipsum vestibulum, facilisis felis eleifend, vehicula sapien. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam tempus vel massa et laoreet. Vestibulum elementum erat ac dictum aliquam. Sed eget ligula lobortis, venenatis magna id, tristique ex. Aenean aliquet tellus ligula, efficitur molestie sapien elementum sed. Sed sagittis elementum turpis sit amet sodales. Vivamus in rhoncus eros. Curabitur eu odio nec purus vehicula consequat. Etiam feugiat, justo eu tincidunt consectetur, sapien arcu accumsan lectus, a luctus sapien ligula ac erat. Sed enim turpis, malesuada ullamcorper risus nec, bibendum pretium eros.\r\n\r\nQuisque dignissim congue odio maximus aliquam. Sed congue nunc nec posuere varius. Etiam hendrerit mi nec justo euismod blandit a non ex. Etiam ac ipsum risus. Phasellus sit amet massa a nibh imperdiet volutpat. Vestibulum vehicula, metus sit amet imperdiet pretium, erat lectus euismod orci, quis dictum sem elit id nunc. Vestibulum ac consectetur ante. Etiam et tincidunt neque, et facilisis felis. Quisque ut sem ut felis varius semper nec non nibh. Vivamus dapibus sem non tortor cursus, id condimentum tortor tempor. Curabitur scelerisque placerat arcu id lacinia. Sed faucibus fringilla lectus ut lacinia. Proin at tortor ut nibh porttitor tempus. Aliquam aliquam diam sit amet nisi congue, a fermentum metus hendrerit. Duis ut tempus elit.\r\n\r\nMaecenas lacinia gravida elementum. Morbi rhoncus interdum convallis. Morbi vel est fringilla, congue arcu sed, viverra elit. Ut dapibus suscipit viverra. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nam sit amet vestibulum sem. Aliquam eget nisi aliquam, imperdiet est in, blandit purus. Suspendisse vulputate viverra lectus, vitae facilisis ante lobortis eu. Suspendisse neque urna, auctor ac viverra sed, posuere eget mi. Nunc blandit turpis ac est pretium, a faucibus enim dapibus. Nam sodales nulla a rhoncus accumsan. Curabitur dui quam, tristique sed magna a, laoreet placerat purus. Nulla ut est felis. Duis molestie dolor at nibh aliquet, faucibus sodales ante efficitur.\r\n\r\nCras molestie pharetra tortor. Mauris aliquam tortor nec tristique fringilla. Phasellus sollicitudin egestas porttitor. Vestibulum luctus, metus eget ornare mollis, turpis orci blandit leo, nec consequat libero nulla a arcu. Duis pharetra sodales diam ut malesuada. Nulla vel consequat nisi. Integer ultricies augue erat. Nullam venenatis ut ipsum vel egestas.\r\n\r\nNullam lobortis, diam et tincidunt mollis, sapien nisl varius ipsum, a mollis justo est id risus. Sed gravida vestibulum mi ac dictum. Donec augue dolor, laoreet id sagittis nec, rutrum vitae eros. Nulla fermentum mi in odio dignissim, sed rhoncus mauris convallis. Donec vitae lorem tincidunt, luctus purus vitae, tincidunt turpis. Duis molestie ex et elementum faucibus. Phasellus vehicula consectetur velit eu tempor. Sed non accumsan dolor. Etiam non pulvinar leo, nec venenatis mauris.\r\n\r\n', 'About Us', '', 'inherit', 'closed', 'closed', '', '44-revision-v1', '', '', '2018-10-09 11:18:44', '2018-10-09 11:18:44', '', 44, 'http://localhost/wordpress-project/jess/2018/10/09/44-revision-v1/', 0, 'revision', '', 0),
(102, 1, '2018-10-09 11:19:14', '2018-10-09 11:19:14', '', 'Contact Us', '', 'inherit', 'closed', 'closed', '', '46-revision-v1', '', '', '2018-10-09 11:19:14', '2018-10-09 11:19:14', '', 46, 'http://localhost/wordpress-project/jess/2018/10/09/46-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2018-10-09 11:55:36', '2018-10-09 11:55:36', '', 'banner-1', '', 'inherit', 'open', 'closed', '', 'banner-1', '', '', '2018-10-09 11:55:36', '2018-10-09 11:55:36', '', 0, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/banner-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2018-10-09 12:11:05', '2018-10-09 12:11:05', '', 'banner-1', '', 'inherit', 'open', 'closed', '', 'banner-1-2', '', '', '2018-10-09 12:11:05', '2018-10-09 12:11:05', '', 0, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/banner-1-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(105, 1, '2018-10-09 12:17:03', '2018-10-09 12:17:03', '', 'Partner 1', '', 'publish', 'closed', 'closed', '', 'partner-1', '', '', '2018-10-09 12:17:03', '2018-10-09 12:17:03', '', 0, 'http://localhost/wordpress-project/jess/?post_type=partners&#038;p=105', 0, 'partners', '', 0),
(106, 1, '2018-10-09 12:16:57', '2018-10-09 12:16:57', '', 'img-001', '', 'inherit', 'open', 'closed', '', 'img-001', '', '', '2018-10-09 12:16:57', '2018-10-09 12:16:57', '', 105, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/img-001.png', 0, 'attachment', 'image/png', 0),
(107, 1, '2018-10-09 12:17:16', '2018-10-09 12:17:16', '', 'Partner 2', '', 'publish', 'closed', 'closed', '', 'partner-2', '', '', '2018-10-09 12:17:16', '2018-10-09 12:17:16', '', 0, 'http://localhost/wordpress-project/jess/?post_type=partners&#038;p=107', 0, 'partners', '', 0),
(108, 1, '2018-10-09 12:17:29', '2018-10-09 12:17:29', '', 'Partner 3', '', 'publish', 'closed', 'closed', '', 'partner-3', '', '', '2018-10-09 12:17:29', '2018-10-09 12:17:29', '', 0, 'http://localhost/wordpress-project/jess/?post_type=partners&#038;p=108', 0, 'partners', '', 0),
(109, 1, '2018-10-09 12:17:44', '2018-10-09 12:17:44', '', 'Partner 4', '', 'publish', 'closed', 'closed', '', 'partner-4', '', '', '2018-10-09 12:17:44', '2018-10-09 12:17:44', '', 0, 'http://localhost/wordpress-project/jess/?post_type=partners&#038;p=109', 0, 'partners', '', 0),
(110, 1, '2018-10-09 12:17:51', '2018-10-09 12:17:51', '', 'Partner 5', '', 'publish', 'closed', 'closed', '', 'partner-5', '', '', '2018-10-09 12:17:58', '2018-10-09 12:17:58', '', 0, 'http://localhost/wordpress-project/jess/?post_type=partners&#038;p=110', 0, 'partners', '', 0),
(111, 1, '2018-10-09 12:21:11', '2018-10-09 12:21:11', '', 'bg-img-2', '', 'inherit', 'open', 'closed', '', 'bg-img-2-copy', '', '', '2018-10-09 12:21:21', '2018-10-09 12:21:21', '', 0, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/bg-img-2-Copy.jpg', 0, 'attachment', 'image/jpeg', 0),
(112, 1, '2018-10-09 12:24:13', '2018-10-09 12:24:13', '', 'bg-img-2', '', 'inherit', 'open', 'closed', '', 'bg-img-2', '', '', '2018-10-09 12:24:13', '2018-10-09 12:24:13', '', 0, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/bg-img-2.jpg', 0, 'attachment', 'image/jpeg', 0),
(113, 1, '2018-10-09 12:24:15', '2018-10-09 12:24:15', '', 'duramax', '', 'inherit', 'open', 'closed', '', 'duramax', '', '', '2018-10-09 12:24:15', '2018-10-09 12:24:15', '', 0, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/duramax.jpg', 0, 'attachment', 'image/jpeg', 0),
(114, 1, '2018-10-09 12:27:22', '2018-10-09 12:27:22', '', 'Order &ndash; October 9, 2018 @ 12:27 PM', '', 'wc-processing', 'open', 'closed', 'order_5bbc9eaa5ebc1', 'order-oct-09-2018-1227-pm', '', '', '2018-10-09 12:27:25', '2018-10-09 12:27:25', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=114', 0, 'shop_order', '', 1),
(115, 1, '2018-10-09 13:32:53', '2018-10-09 13:32:53', '', 'Order &ndash; October 9, 2018 @ 01:32 PM', '', 'wc-processing', 'open', 'closed', 'order_5bbcae0518030', 'order-oct-09-2018-0132-pm', '', '', '2018-10-09 13:32:56', '2018-10-09 13:32:56', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=115', 0, 'shop_order', '', 1),
(116, 1, '2018-10-09 14:12:26', '2018-10-09 14:12:26', '[woof_text_filter]', 'Shop', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-10-09 14:12:26', '2018-10-09 14:12:26', '', 19, 'http://localhost/wordpress-project/jess/2018/10/09/19-revision-v1/', 0, 'revision', '', 0),
(117, 1, '2018-10-09 14:12:40', '2018-10-09 14:12:40', '', 'Shop', '', 'inherit', 'closed', 'closed', '', '19-revision-v1', '', '', '2018-10-09 14:12:40', '2018-10-09 14:12:40', '', 19, 'http://localhost/wordpress-project/jess/2018/10/09/19-revision-v1/', 0, 'revision', '', 0),
(118, 1, '2018-10-09 15:02:08', '2018-10-09 15:02:08', '', 'Order &ndash; October 9, 2018 @ 03:02 PM', '', 'wc-processing', 'open', 'closed', 'order_5bbcc2f06ba90', 'order-oct-09-2018-0302-pm', '', '', '2018-10-09 15:02:11', '2018-10-09 15:02:11', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=118', 0, 'shop_order', '', 1),
(119, 1, '2018-10-09 15:08:10', '2018-10-09 15:08:10', 'Check out all our videos!', 'Video', '', 'publish', 'closed', 'closed', '', 'video', '', '', '2018-10-09 15:12:39', '2018-10-09 15:12:39', '', 0, 'http://localhost/wordpress-project/jess/?page_id=119', 0, 'page', '', 0),
(120, 1, '2018-10-09 15:06:01', '2018-10-09 15:06:01', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:3:\"119\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Video Page', 'video-page', 'publish', 'closed', 'closed', '', 'group_5bbcc36a6638e', '', '', '2018-10-09 15:06:57', '2018-10-09 15:06:57', '', 0, 'http://localhost/wordpress-project/jess/?post_type=acf-field-group&#038;p=120', 0, 'acf-field-group', '', 0),
(121, 1, '2018-10-09 15:06:01', '2018-10-09 15:06:01', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:1;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Youtube iframe link', 'youtube_iframe_link', 'publish', 'closed', 'closed', '', 'field_5bbcc370bc030', '', '', '2018-10-09 15:06:57', '2018-10-09 15:06:57', '', 120, 'http://localhost/wordpress-project/jess/?post_type=acf-field&#038;p=121', 0, 'acf-field', '', 0),
(122, 1, '2018-10-09 15:06:21', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-09 15:06:21', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-project/jess/?page_id=122', 0, 'page', '', 0),
(123, 1, '2018-10-09 15:08:10', '2018-10-09 15:08:10', '', 'Video', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2018-10-09 15:08:10', '2018-10-09 15:08:10', '', 119, 'http://localhost/wordpress-project/jess/2018/10/09/119-revision-v1/', 0, 'revision', '', 0),
(124, 1, '2018-10-09 15:12:39', '2018-10-09 15:12:39', 'Check out all our videos!', 'Video', '', 'inherit', 'closed', 'closed', '', '119-revision-v1', '', '', '2018-10-09 15:12:39', '2018-10-09 15:12:39', '', 119, 'http://localhost/wordpress-project/jess/2018/10/09/119-revision-v1/', 0, 'revision', '', 0),
(125, 1, '2018-10-09 15:18:49', '2018-10-09 15:18:49', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'News 1', '', 'publish', 'closed', 'closed', '', 'news-1', '', '', '2018-10-09 15:55:38', '2018-10-09 15:55:38', '', 0, 'http://localhost/wordpress-project/jess/?post_type=latest_news&#038;p=125', 0, 'latest_news', '', 0),
(126, 1, '2018-10-09 15:19:04', '2018-10-09 15:19:04', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'News 2', '', 'publish', 'closed', 'closed', '', 'news-2', '', '', '2018-10-09 15:56:05', '2018-10-09 15:56:05', '', 0, 'http://localhost/wordpress-project/jess/?post_type=latest_news&#038;p=126', 0, 'latest_news', '', 0),
(127, 1, '2018-10-09 15:19:19', '2018-10-09 15:19:19', 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 'News 3', '', 'publish', 'closed', 'closed', '', 'news-3', '', '', '2018-10-09 15:55:52', '2018-10-09 15:55:52', '', 0, 'http://localhost/wordpress-project/jess/?post_type=latest_news&#038;p=127', 0, 'latest_news', '', 0),
(128, 1, '2018-10-09 15:21:29', '2018-10-09 15:21:29', '', 'News', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2018-10-09 15:21:47', '2018-10-09 15:21:47', '', 0, 'http://localhost/wordpress-project/jess/?page_id=128', 0, 'page', '', 0),
(129, 1, '2018-10-09 15:20:24', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-09 15:20:24', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-project/jess/?page_id=129', 0, 'page', '', 0),
(130, 1, '2018-10-09 15:21:29', '2018-10-09 15:21:29', '', 'News', '', 'inherit', 'closed', 'closed', '', '128-revision-v1', '', '', '2018-10-09 15:21:29', '2018-10-09 15:21:29', '', 128, 'http://localhost/wordpress-project/jess/2018/10/09/128-revision-v1/', 0, 'revision', '', 0),
(131, 1, '2018-10-09 15:37:28', '2018-10-09 15:37:28', '', 'Gallery', '', 'publish', 'closed', 'closed', '', 'gallery', '', '', '2018-10-09 15:37:28', '2018-10-09 15:37:28', '', 0, 'http://localhost/wordpress-project/jess/?page_id=131', 0, 'page', '', 0),
(132, 1, '2018-10-09 15:37:28', '2018-10-09 15:37:28', '', 'Gallery', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2018-10-09 15:37:28', '2018-10-09 15:37:28', '', 131, 'http://localhost/wordpress-project/jess/2018/10/09/131-revision-v1/', 0, 'revision', '', 0),
(133, 1, '2018-10-09 15:40:11', '2018-10-09 15:40:11', '', 'Image 1', '', 'publish', 'closed', 'closed', '', 'image-1', '', '', '2018-10-09 15:40:11', '2018-10-09 15:40:11', '', 0, 'http://localhost/wordpress-project/jess/?post_type=image_gallery&#038;p=133', 0, 'image_gallery', '', 0),
(134, 1, '2018-10-09 15:39:56', '2018-10-09 15:39:56', '', '1221_jim1', '', 'inherit', 'open', 'closed', '', '1221_jim1', '', '', '2018-10-09 15:39:56', '2018-10-09 15:39:56', '', 133, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/1221_jim1.jpg', 0, 'attachment', 'image/jpeg', 0),
(135, 1, '2018-10-09 15:39:58', '2018-10-09 15:39:58', '', '1223_megacab', '', 'inherit', 'open', 'closed', '', '1223_megacab', '', '', '2018-10-09 15:39:58', '2018-10-09 15:39:58', '', 133, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/1223_megacab.jpg', 0, 'attachment', 'image/jpeg', 0),
(136, 1, '2018-10-09 15:40:00', '2018-10-09 15:40:00', '', '1225_dan', '', 'inherit', 'open', 'closed', '', '1225_dan', '', '', '2018-10-09 15:40:00', '2018-10-09 15:40:00', '', 133, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/1225_dan.jpg', 0, 'attachment', 'image/jpeg', 0),
(137, 1, '2018-10-09 15:40:03', '2018-10-09 15:40:03', '', '1229_Pulling-Truck1', '', 'inherit', 'open', 'closed', '', '1229_pulling-truck1', '', '', '2018-10-09 15:40:03', '2018-10-09 15:40:03', '', 133, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/1229_Pulling-Truck1.jpg', 0, 'attachment', 'image/jpeg', 0),
(138, 1, '2018-10-09 15:40:05', '2018-10-09 15:40:05', '', '1253_20160603_100904', '', 'inherit', 'open', 'closed', '', '1253_20160603_100904', '', '', '2018-10-09 15:40:05', '2018-10-09 15:40:05', '', 133, 'http://localhost/wordpress-project/jess/wp-content/uploads/2018/10/1253_20160603_100904.jpg', 0, 'attachment', 'image/jpeg', 0),
(139, 1, '2018-10-09 15:40:24', '2018-10-09 15:40:24', '', 'Image 2', '', 'publish', 'closed', 'closed', '', 'image-2', '', '', '2018-10-09 15:40:24', '2018-10-09 15:40:24', '', 0, 'http://localhost/wordpress-project/jess/?post_type=image_gallery&#038;p=139', 0, 'image_gallery', '', 0),
(140, 1, '2018-10-09 15:40:43', '2018-10-09 15:40:43', '', 'Image 3', '', 'publish', 'closed', 'closed', '', 'image-3', '', '', '2018-10-09 15:40:43', '2018-10-09 15:40:43', '', 0, 'http://localhost/wordpress-project/jess/?post_type=image_gallery&#038;p=140', 0, 'image_gallery', '', 0),
(141, 1, '2018-10-09 15:40:56', '2018-10-09 15:40:56', '', 'Image 4', '', 'publish', 'closed', 'closed', '', 'image-4', '', '', '2018-10-09 15:40:56', '2018-10-09 15:40:56', '', 0, 'http://localhost/wordpress-project/jess/?post_type=image_gallery&#038;p=141', 0, 'image_gallery', '', 0),
(142, 1, '2018-10-09 15:41:09', '2018-10-09 15:41:09', '', 'Image 5', '', 'publish', 'closed', 'closed', '', 'image-5', '', '', '2018-10-09 15:41:09', '2018-10-09 15:41:09', '', 0, 'http://localhost/wordpress-project/jess/?post_type=image_gallery&#038;p=142', 0, 'image_gallery', '', 0),
(143, 1, '2018-10-09 16:26:34', '2018-10-09 16:26:34', '', 'Order &ndash; October 9, 2018 @ 04:26 PM', '', 'wc-processing', 'open', 'closed', 'order_5bbcd6ba78145', 'order-oct-09-2018-0426-pm', '', '', '2018-10-09 16:26:36', '2018-10-09 16:26:36', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=143', 0, 'shop_order', '', 1),
(144, 1, '2018-10-10 07:29:19', '2018-10-10 07:29:19', '', 'Order &ndash; October 10, 2018 @ 07:29 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbdaa4f982c0', 'order-oct-10-2018-0729-am', '', '', '2018-10-10 07:29:22', '2018-10-10 07:29:22', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=144', 0, 'shop_order', '', 1),
(145, 1, '2018-10-10 08:58:40', '2018-10-10 08:58:40', '', 'Order &ndash; October 10, 2018 @ 08:58 AM', '', 'wc-processing', 'open', 'closed', 'order_5bbdbf407fb92', 'order-oct-10-2018-0858-am', '', '', '2018-10-10 08:58:44', '2018-10-10 08:58:44', '', 0, 'http://localhost/wordpress-project/jess/?post_type=shop_order&#038;p=145', 0, 'shop_order', '', 1),
(146, 1, '2018-10-10 08:59:41', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2018-10-10 08:59:41', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-project/jess/?post_type=services&p=146', 0, 'services', '', 0),
(147, 1, '2018-10-10 09:47:51', '0000-00-00 00:00:00', ' ', '', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-10-10 09:47:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/wordpress-project/jess/?p=147', 1, 'nav_menu_item', '', 0),
(148, 1, '2018-10-10 09:47:52', '0000-00-00 00:00:00', '', 'Hat\'s', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-10-10 09:47:52', '0000-00-00 00:00:00', '', 57, 'http://localhost/wordpress-project/jess/?p=148', 1, 'nav_menu_item', '', 0),
(149, 1, '2018-10-10 09:48:44', '2018-10-10 09:48:44', ' ', '', '', 'publish', 'closed', 'closed', '', '149', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 0, 'http://localhost/wordpress-project/jess/?p=149', 23, 'nav_menu_item', '', 0),
(150, 1, '2018-10-10 09:48:44', '2018-10-10 09:48:44', '', 'Hat\'s', '', 'publish', 'closed', 'closed', '', 'hats', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 57, 'http://localhost/wordpress-project/jess/?p=150', 24, 'nav_menu_item', '', 0),
(151, 1, '2018-10-10 09:48:44', '2018-10-10 09:48:44', ' ', '', '', 'publish', 'closed', 'closed', '', '151', '', '', '2018-10-10 09:49:43', '2018-10-10 09:49:43', '', 57, 'http://localhost/wordpress-project/jess/?p=151', 25, 'nav_menu_item', '', 0),
(152, 1, '2018-10-10 12:10:49', '0000-00-00 00:00:00', '', 'Partner 6', '', 'draft', 'closed', 'closed', '', '', '', '', '2018-10-10 12:10:49', '2018-10-10 12:10:49', '', 0, 'http://localhost/wordpress-project/jess/?post_type=partners&#038;p=152', 0, 'partners', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_termmeta`
--

INSERT INTO `wp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 16, 'order', '0'),
(2, 16, 'product_count_product_cat', '2'),
(3, 17, 'order', '0'),
(4, 17, 'product_count_product_cat', '2'),
(5, 18, 'order_pa_make', '0'),
(6, 19, 'order_pa_make', '0'),
(7, 20, 'order_pa_make', '0'),
(8, 21, 'order_pa_make', '0'),
(9, 22, 'order_pa_model', '0'),
(10, 23, 'order_pa_model', '0'),
(11, 24, 'order_pa_model', '0'),
(12, 25, 'order_pa_model', '0'),
(13, 26, 'order_pa_model', '0'),
(14, 27, 'order_pa_model', '0'),
(15, 28, 'order_pa_years', '0'),
(16, 29, 'order_pa_years', '0'),
(17, 30, 'order_pa_years', '0'),
(18, 31, 'order', '0'),
(19, 31, 'display_type', ''),
(20, 31, 'thumbnail_id', '0'),
(21, 32, 'order', '0'),
(22, 32, 'display_type', ''),
(23, 32, 'thumbnail_id', '0'),
(24, 33, 'order', '0'),
(25, 33, 'display_type', ''),
(26, 33, 'thumbnail_id', '0'),
(27, 34, 'order', '0'),
(28, 34, 'display_type', ''),
(29, 34, 'thumbnail_id', '0'),
(30, 35, 'order', '0'),
(31, 35, 'display_type', ''),
(32, 35, 'thumbnail_id', '0'),
(33, 36, 'order', '0'),
(34, 36, 'display_type', ''),
(35, 36, 'thumbnail_id', '0'),
(36, 37, 'order', '0'),
(37, 37, 'display_type', ''),
(38, 37, 'thumbnail_id', '0'),
(39, 38, 'order', '0'),
(40, 38, 'display_type', ''),
(41, 38, 'thumbnail_id', '0'),
(42, 39, 'order', '0'),
(43, 39, 'display_type', ''),
(44, 39, 'thumbnail_id', '0'),
(45, 40, 'order', '0'),
(46, 40, 'display_type', ''),
(47, 40, 'thumbnail_id', '0'),
(48, 41, 'order', '0'),
(49, 41, 'display_type', ''),
(50, 41, 'thumbnail_id', '0'),
(51, 42, 'order', '0'),
(52, 42, 'display_type', ''),
(53, 42, 'thumbnail_id', '0'),
(54, 43, 'order', '0'),
(55, 43, 'display_type', ''),
(56, 43, 'thumbnail_id', '0'),
(57, 44, 'order', '0'),
(58, 44, 'display_type', ''),
(59, 44, 'thumbnail_id', '0'),
(60, 45, 'order', '0'),
(61, 45, 'display_type', ''),
(62, 45, 'thumbnail_id', '0'),
(63, 46, 'order', '0'),
(64, 46, 'display_type', ''),
(65, 46, 'thumbnail_id', '0'),
(66, 47, 'order', '0'),
(67, 47, 'display_type', ''),
(68, 47, 'thumbnail_id', '0'),
(69, 48, 'order', '0'),
(70, 48, 'display_type', ''),
(71, 48, 'thumbnail_id', '0'),
(72, 49, 'order', '0'),
(73, 49, 'display_type', ''),
(74, 49, 'thumbnail_id', '0'),
(75, 50, 'order', '0'),
(76, 50, 'display_type', ''),
(77, 50, 'thumbnail_id', '0'),
(78, 51, 'order', '0'),
(79, 51, 'display_type', ''),
(80, 51, 'thumbnail_id', '0'),
(81, 52, 'order', '0'),
(82, 52, 'display_type', ''),
(83, 52, 'thumbnail_id', '0'),
(84, 56, 'order', '0'),
(85, 56, 'display_type', ''),
(86, 56, 'thumbnail_id', '0'),
(87, 57, 'order', '0'),
(88, 57, 'display_type', ''),
(89, 57, 'thumbnail_id', '0'),
(90, 58, 'order', '0'),
(91, 58, 'display_type', ''),
(92, 58, 'thumbnail_id', '0');

-- --------------------------------------------------------

--
-- Table structure for table `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Tools', 'tools', 0),
(17, 'Lighting', 'lighting', 0),
(18, 'Chevrolet', 'chevrolet', 0),
(19, 'Dodge', 'dodge', 0),
(20, 'Ford', 'ford', 0),
(21, 'GMC', 'gmc', 0),
(22, 'F-350 Super Duty', 'f-350-super-duty', 0),
(23, 'Ram 2500', 'ram-2500', 0),
(24, 'Ram 3500', 'ram-3500', 0),
(25, 'Sierra 2500 HD', 'sierra-2500-hd', 0),
(26, 'Sierra 3500 HD', 'sierra-3500-hd', 0),
(27, 'Silverado 3500 HD', 'silverado-3500-hd', 0),
(28, '2016', '2016', 0),
(29, '2017', '2017', 0),
(30, '2018', '2018', 0),
(31, 'Powerstroke', 'powerstroke', 0),
(32, 'Ford Super Duty', 'ford-super-duty', 0),
(33, '2011-2016 6.7L Powerstroke', '2011-2016-6-7l-powerstroke', 0),
(34, '2008-2010 6.4L Powerstroke', '2008-2010-6-4l-powerstroke', 0),
(35, '2003-2007 6.0L Powerstroke', '2003-2007-6-0l-powerstroke', 0),
(36, '1999-2003 7.3L Powerstroke', '1999-2003-7-3l-powerstroke', 0),
(37, '1994-1997 7.3L Powerstroke', '1994-1997-7-3l-powerstroke', 0),
(38, 'Cummins', 'cummins', 0),
(39, 'Dodge / Ram', 'dodge-ram', 0),
(40, '2007.5-2018 6.7L 24V Cummins', '2007-5-2018-6-7l-24v-cummins', 0),
(41, '2003-2004 5.9L 24V Cummins', '2003-2004-5-9l-24v-cummins', 0),
(42, '2003-2004 5.9L 24V Cummins', '1998-5-2002-5-9l-24v-cummins', 0),
(43, '1994-1998 5.9L 12V Cummins', '1994-1998-5-9l-12v-cummins', 0),
(44, '1989-1993 5.9L 12V Cummins', '1989-1993-5-9l-12v-cummins', 0),
(45, 'Duramax', 'duramax', 0),
(46, 'Chevrolet Silverado / GMC Sierra', 'chevrolet-silverado-gmc-sierra', 0),
(47, '2017-2018 6.6L L5P Duramax', '2017-2018-6-6l-l5p-duramax', 0),
(48, '2011-2016 6.6L LML Duramax', '2011-2016-6-6l-lml-duramax', 0),
(49, '2007.5-2010 6.6L LMM Duramax', '2007-5-2010-6-6l-lmm-duramax', 0),
(50, '2006-2007 6.6L LLY/LBZ Duramax', '2006-2007-6-6l-lly-lbz-duramax', 0),
(51, '2004.5-2005 6.6L LLY Duramax', '2004-5-2005-6-6l-lly-duramax', 0),
(52, '2001-2004 6.6L LB7 Duramax', '2001-2004-6-6l-lb7-duramax', 0),
(53, 'Top Menu', 'top-menu', 0),
(54, 'Footer Menu 1', 'footer-menu-1', 0),
(55, 'Footer Menu 2', 'footer-menu-2', 0),
(56, 'Hat\'s', 'hats', 0),
(57, 'MERCHANDISE', 'merchandise', 0),
(58, 'T-Shift', 't-shift', 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(29, 2, 0),
(29, 16, 0),
(29, 21, 0),
(29, 25, 0),
(29, 28, 0),
(29, 29, 0),
(29, 30, 0),
(31, 2, 0),
(31, 16, 0),
(31, 20, 0),
(31, 24, 0),
(31, 30, 0),
(33, 2, 0),
(33, 17, 0),
(33, 19, 0),
(33, 23, 0),
(33, 29, 0),
(36, 2, 0),
(36, 17, 0),
(36, 18, 0),
(36, 22, 0),
(36, 28, 0),
(51, 53, 0),
(52, 53, 0),
(53, 53, 0),
(54, 53, 0),
(55, 53, 0),
(56, 53, 0),
(57, 53, 0),
(58, 53, 0),
(59, 53, 0),
(60, 53, 0),
(61, 53, 0),
(62, 53, 0),
(63, 53, 0),
(64, 53, 0),
(65, 53, 0),
(66, 53, 0),
(67, 53, 0),
(68, 53, 0),
(69, 53, 0),
(70, 53, 0),
(71, 53, 0),
(72, 53, 0),
(74, 53, 0),
(75, 53, 0),
(76, 54, 0),
(85, 54, 0),
(86, 54, 0),
(87, 54, 0),
(88, 54, 0),
(89, 55, 0),
(90, 55, 0),
(91, 55, 0),
(92, 55, 0),
(93, 55, 0),
(149, 53, 0),
(150, 53, 0),
(151, 53, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 4),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 0),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'product_cat', '', 0, 2),
(17, 17, 'product_cat', '', 0, 2),
(18, 18, 'pa_make', '', 0, 1),
(19, 19, 'pa_make', '', 0, 1),
(20, 20, 'pa_make', '', 0, 1),
(21, 21, 'pa_make', '', 0, 1),
(22, 22, 'pa_model', '', 0, 1),
(23, 23, 'pa_model', '', 0, 1),
(24, 24, 'pa_model', '', 0, 1),
(25, 25, 'pa_model', '', 0, 1),
(26, 26, 'pa_model', '', 0, 0),
(27, 27, 'pa_model', '', 0, 0),
(28, 28, 'pa_years', '', 0, 2),
(29, 29, 'pa_years', '', 0, 2),
(30, 30, 'pa_years', '', 0, 2),
(31, 31, 'product_cat', '', 0, 0),
(32, 32, 'product_cat', '', 31, 0),
(33, 33, 'product_cat', '', 31, 0),
(34, 34, 'product_cat', '', 31, 0),
(35, 35, 'product_cat', '', 31, 0),
(36, 36, 'product_cat', '', 31, 0),
(37, 37, 'product_cat', '', 31, 0),
(38, 38, 'product_cat', '', 0, 0),
(39, 39, 'product_cat', '', 38, 0),
(40, 40, 'product_cat', '', 38, 0),
(41, 41, 'product_cat', '', 38, 0),
(42, 42, 'product_cat', '', 38, 0),
(43, 43, 'product_cat', '', 38, 0),
(44, 44, 'product_cat', '', 38, 0),
(45, 45, 'product_cat', '', 0, 0),
(46, 46, 'product_cat', '', 45, 0),
(47, 47, 'product_cat', '', 45, 0),
(48, 48, 'product_cat', '', 45, 0),
(49, 49, 'product_cat', '', 45, 0),
(50, 50, 'product_cat', '', 45, 0),
(51, 51, 'product_cat', '', 45, 0),
(52, 52, 'product_cat', '', 45, 0),
(53, 53, 'nav_menu', '', 0, 27),
(54, 54, 'nav_menu', '', 0, 5),
(55, 55, 'nav_menu', '', 0, 5),
(56, 56, 'product_cat', '', 57, 0),
(57, 57, 'product_cat', '', 0, 0),
(58, 58, 'product_cat', '', 57, 0);

-- --------------------------------------------------------

--
-- Table structure for table `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', 'Martina'),
(3, 1, 'last_name', 'Erickson'),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', 'wp496_privacy,wpmudcs1'),
(15, 1, 'show_welcome_panel', '0'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '18'),
(18, 1, 'session_tokens', 'a:6:{s:64:\"66c8ad525672c7a64c1cdd5b8f2c7fdef200a1964d48ae66cb4ce6813af67452\";a:4:{s:10:\"expiration\";i:1539183297;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539010497;}s:64:\"4bff936cbffa105fd1a4df264a0f4fe698c6d664c5bb04d13769c23c4af295aa\";a:4:{s:10:\"expiration\";i:1539183297;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539010497;}s:64:\"86c05f584d3fba6205d1e308f366e6fa0ab522677e5d0befb67028dd78dc79ba\";a:4:{s:10:\"expiration\";i:1539251083;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539078283;}s:64:\"e4db167a1c70265e08abfccf04747b7f0f3dab0f92209e17ce16bb2ddc7e01a0\";a:4:{s:10:\"expiration\";i:1539255954;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539083154;}s:64:\"e84e2c0b6dec7000b10eae2e41364d8aa1f1a35e8b54eabfc402f942a877bb3e\";a:4:{s:10:\"expiration\";i:1539257264;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539084464;}s:64:\"9b35a94da2d4ae964609ace5c8cf02ae23c06d0d7893b44874f6e6a376d5b90e\";a:4:{s:10:\"expiration\";i:1539346060;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539173260;}}'),
(19, 1, 'show_try_gutenberg_panel', '0'),
(20, 1, 'closedpostboxes_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(21, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(22, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(23, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-post_format\";}'),
(25, 1, 'wc_last_active', '1539129600'),
(26, 1, 'dismissed_install_notice', '1'),
(27, 1, 'wp_user-settings', 'libraryContent=browse&editor=html'),
(28, 1, 'wp_user-settings-time', '1539094343'),
(29, 1, 'last_update', '1539161920'),
(30, 1, 'billing_first_name', 'Martina'),
(31, 1, 'billing_last_name', 'Erickson'),
(32, 1, 'billing_company', 'Valentine and Sloan LLC'),
(33, 1, 'billing_address_1', '606 Rocky Second Street'),
(34, 1, 'billing_address_2', 'Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et'),
(35, 1, 'billing_city', 'ahmedab'),
(36, 1, 'billing_state', 'AP'),
(37, 1, 'billing_postcode', '78965-411'),
(38, 1, 'billing_country', 'BR'),
(39, 1, 'billing_email', 'qogybyzud@mailinator.net'),
(40, 1, 'billing_phone', '+442-68-9152410'),
(56, 1, 'wp_custom-sidebars-editor-advance', 'a:5:{s:4:\"cs-1\";b:0;s:4:\"cs-2\";b:0;s:4:\"cs-3\";b:0;s:4:\"cs-4\";b:0;s:4:\"cs-5\";b:0;}'),
(60, 2, 'nickname', 'snehal.citrusbug'),
(61, 2, 'first_name', 'Snehal'),
(62, 2, 'last_name', 'Gohel'),
(63, 2, 'description', ''),
(64, 2, 'rich_editing', 'true'),
(65, 2, 'syntax_highlighting', 'true'),
(66, 2, 'comment_shortcuts', 'false'),
(67, 2, 'admin_color', 'fresh'),
(68, 2, 'use_ssl', '0'),
(69, 2, 'show_admin_bar_front', 'true'),
(70, 2, 'locale', ''),
(71, 2, 'wp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(72, 2, 'wp_user_level', '0'),
(73, 2, 'session_tokens', 'a:3:{s:64:\"64e7d4a86438fe9897162f7ded57793d7d633ede549e8cd3264691f31fde857d\";a:4:{s:10:\"expiration\";i:1540280952;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539071352;}s:64:\"d45bf04ebfe89e737fb6063ec1a5d7299a2d30962a1af3a0acc6daacca6cf2d0\";a:4:{s:10:\"expiration\";i:1540281029;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539071429;}s:64:\"ac8eb33ac8b04b44d774d7a2571a4155b154e2896dbd41a3ec2b70046f3aebe1\";a:4:{s:10:\"expiration\";i:1539260833;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36\";s:5:\"login\";i:1539088033;}}'),
(75, 2, 'wc_last_active', '1539129600'),
(76, 2, 'last_update', '1539102394'),
(77, 2, 'billing_first_name', 'Snehal'),
(78, 2, 'billing_last_name', 'Gohel'),
(79, 2, 'billing_company', 'citrusbug'),
(80, 2, 'billing_address_1', 'B - 102'),
(81, 2, 'billing_address_2', 'Panchgini'),
(82, 2, 'billing_city', 'ahmedab'),
(83, 2, 'billing_state', 'GJ'),
(84, 2, 'billing_postcode', '382345'),
(85, 2, 'billing_country', 'IN'),
(86, 2, 'billing_email', 'snehal.citrusbug@gmail.com'),
(87, 2, 'billing_phone', '8899665588'),
(97, 1, 'closedpostboxes_page', 'a:1:{i:0;s:17:\"customsidebars-mb\";}'),
(98, 1, 'metaboxhidden_page', 'a:4:{i:0;s:10:\"postcustom\";i:1;s:16:\"commentstatusdiv\";i:2;s:7:\"slugdiv\";i:3;s:9:\"authordiv\";}'),
(108, 2, 'shipping_method', ''),
(110, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:0:{}}'),
(114, 1, 'shipping_method', ''),
(116, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"182be0c5cdcd5072bb1864cdee4d3d6e\";a:11:{s:3:\"key\";s:32:\"182be0c5cdcd5072bb1864cdee4d3d6e\";s:10:\"product_id\";i:33;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:150;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:150;s:8:\"line_tax\";i:0;}}}'),
(117, 1, 'nav_menu_recently_edited', '53');

-- --------------------------------------------------------

--
-- Table structure for table `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$ByOHhtBGzjUJNnghhJQpWSsc7JoLpY.', 'admin', 'user.citrusbug@gmail.com', '', '2018-09-28 05:00:54', '', 0, 'admin'),
(2, 'snehal.citrusbug', '$P$B47OGDsLim1FD.YmLBg3zZ9Tc/kmDF0', 'snehal-citrusbug', 'snehal.citrusbug@gmail.com', '', '2018-10-09 07:49:07', '', 0, 'snehal.citrusbug');

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_download_log`
--

CREATE TABLE `wp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_wc_webhooks`
--

CREATE TABLE `wp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_api_keys`
--

CREATE TABLE `wp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_attribute_taxonomies`
--

CREATE TABLE `wp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_attribute_taxonomies`
--

INSERT INTO `wp_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'make', 'Make', 'select', 'menu_order', 1),
(2, 'years', 'Years', 'select', 'menu_order', 1),
(3, 'model', 'Model', 'select', 'menu_order', 1);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `wp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_log`
--

CREATE TABLE `wp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_itemmeta`
--

CREATE TABLE `wp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_itemmeta`
--

INSERT INTO `wp_woocommerce_order_itemmeta` (`meta_id`, `order_item_id`, `meta_key`, `meta_value`) VALUES
(1, 1, '_product_id', '33'),
(2, 1, '_variation_id', '0'),
(3, 1, '_qty', '1'),
(4, 1, '_tax_class', ''),
(5, 1, '_line_subtotal', '150'),
(6, 1, '_line_subtotal_tax', '0'),
(7, 1, '_line_total', '150'),
(8, 1, '_line_tax', '0'),
(9, 1, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(10, 2, '_product_id', '36'),
(11, 2, '_variation_id', '0'),
(12, 2, '_qty', '7'),
(13, 2, '_tax_class', ''),
(14, 2, '_line_subtotal', '1400'),
(15, 2, '_line_subtotal_tax', '0'),
(16, 2, '_line_total', '1400'),
(17, 2, '_line_tax', '0'),
(18, 2, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(19, 3, '_product_id', '29'),
(20, 3, '_variation_id', '0'),
(21, 3, '_qty', '6'),
(22, 3, '_tax_class', ''),
(23, 3, '_line_subtotal', '360'),
(24, 3, '_line_subtotal_tax', '0'),
(25, 3, '_line_total', '360'),
(26, 3, '_line_tax', '0'),
(27, 3, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(28, 4, '_product_id', '33'),
(29, 4, '_variation_id', '0'),
(30, 4, '_qty', '7'),
(31, 4, '_tax_class', ''),
(32, 4, '_line_subtotal', '1050'),
(33, 4, '_line_subtotal_tax', '0'),
(34, 4, '_line_total', '1050'),
(35, 4, '_line_tax', '0'),
(36, 4, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(37, 5, '_product_id', '31'),
(38, 5, '_variation_id', '0'),
(39, 5, '_qty', '6'),
(40, 5, '_tax_class', ''),
(41, 5, '_line_subtotal', '600'),
(42, 5, '_line_subtotal_tax', '0'),
(43, 5, '_line_total', '600'),
(44, 5, '_line_tax', '0'),
(45, 5, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(46, 6, '_product_id', '33'),
(47, 6, '_variation_id', '0'),
(48, 6, '_qty', '6'),
(49, 6, '_tax_class', ''),
(50, 6, '_line_subtotal', '900'),
(51, 6, '_line_subtotal_tax', '0'),
(52, 6, '_line_total', '900'),
(53, 6, '_line_tax', '0'),
(54, 6, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(55, 7, '_product_id', '33'),
(56, 7, '_variation_id', '0'),
(57, 7, '_qty', '4'),
(58, 7, '_tax_class', ''),
(59, 7, '_line_subtotal', '600'),
(60, 7, '_line_subtotal_tax', '0'),
(61, 7, '_line_total', '600'),
(62, 7, '_line_tax', '0'),
(63, 7, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(64, 8, '_product_id', '33'),
(65, 8, '_variation_id', '0'),
(66, 8, '_qty', '1'),
(67, 8, '_tax_class', ''),
(68, 8, '_line_subtotal', '150'),
(69, 8, '_line_subtotal_tax', '0'),
(70, 8, '_line_total', '150'),
(71, 8, '_line_tax', '0'),
(72, 8, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(73, 9, '_product_id', '36'),
(74, 9, '_variation_id', '0'),
(75, 9, '_qty', '1'),
(76, 9, '_tax_class', ''),
(77, 9, '_line_subtotal', '200'),
(78, 9, '_line_subtotal_tax', '0'),
(79, 9, '_line_total', '200'),
(80, 9, '_line_tax', '0'),
(81, 9, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(82, 10, '_product_id', '29'),
(83, 10, '_variation_id', '0'),
(84, 10, '_qty', '1'),
(85, 10, '_tax_class', ''),
(86, 10, '_line_subtotal', '60'),
(87, 10, '_line_subtotal_tax', '0'),
(88, 10, '_line_total', '60'),
(89, 10, '_line_tax', '0'),
(90, 10, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(91, 11, '_product_id', '29'),
(92, 11, '_variation_id', '0'),
(93, 11, '_qty', '1'),
(94, 11, '_tax_class', ''),
(95, 11, '_line_subtotal', '60'),
(96, 11, '_line_subtotal_tax', '0'),
(97, 11, '_line_total', '60'),
(98, 11, '_line_tax', '0'),
(99, 11, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(100, 12, '_product_id', '31'),
(101, 12, '_variation_id', '0'),
(102, 12, '_qty', '1'),
(103, 12, '_tax_class', ''),
(104, 12, '_line_subtotal', '100'),
(105, 12, '_line_subtotal_tax', '0'),
(106, 12, '_line_total', '100'),
(107, 12, '_line_tax', '0'),
(108, 12, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(109, 13, '_product_id', '33'),
(110, 13, '_variation_id', '0'),
(111, 13, '_qty', '5'),
(112, 13, '_tax_class', ''),
(113, 13, '_line_subtotal', '750'),
(114, 13, '_line_subtotal_tax', '0'),
(115, 13, '_line_total', '750'),
(116, 13, '_line_tax', '0'),
(117, 13, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(118, 14, '_product_id', '33'),
(119, 14, '_variation_id', '0'),
(120, 14, '_qty', '4'),
(121, 14, '_tax_class', ''),
(122, 14, '_line_subtotal', '600'),
(123, 14, '_line_subtotal_tax', '0'),
(124, 14, '_line_total', '600'),
(125, 14, '_line_tax', '0'),
(126, 14, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(127, 15, '_product_id', '31'),
(128, 15, '_variation_id', '0'),
(129, 15, '_qty', '1'),
(130, 15, '_tax_class', ''),
(131, 15, '_line_subtotal', '100'),
(132, 15, '_line_subtotal_tax', '0'),
(133, 15, '_line_total', '100'),
(134, 15, '_line_tax', '0'),
(135, 15, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(136, 16, '_product_id', '33'),
(137, 16, '_variation_id', '0'),
(138, 16, '_qty', '2'),
(139, 16, '_tax_class', ''),
(140, 16, '_line_subtotal', '300'),
(141, 16, '_line_subtotal_tax', '0'),
(142, 16, '_line_total', '300'),
(143, 16, '_line_tax', '0'),
(144, 16, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(145, 17, '_product_id', '33'),
(146, 17, '_variation_id', '0'),
(147, 17, '_qty', '1'),
(148, 17, '_tax_class', ''),
(149, 17, '_line_subtotal', '150'),
(150, 17, '_line_subtotal_tax', '0'),
(151, 17, '_line_total', '150'),
(152, 17, '_line_tax', '0'),
(153, 17, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}'),
(154, 18, '_product_id', '36'),
(155, 18, '_variation_id', '0'),
(156, 18, '_qty', '1'),
(157, 18, '_tax_class', ''),
(158, 18, '_line_subtotal', '200'),
(159, 18, '_line_subtotal_tax', '0'),
(160, 18, '_line_total', '200'),
(161, 18, '_line_tax', '0'),
(162, 18, '_line_tax_data', 'a:2:{s:5:\"total\";a:0:{}s:8:\"subtotal\";a:0:{}}');

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_order_items`
--

CREATE TABLE `wp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_order_items`
--

INSERT INTO `wp_woocommerce_order_items` (`order_item_id`, `order_item_name`, `order_item_type`, `order_id`) VALUES
(1, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 37),
(2, '12V AUXILIARY WIRING KIT W/ ILLUMINATED SWITCH *UNIVERSAL*', 'line_item', 37),
(3, 'YUKON GEAR & AXLE AXLE BEARING PULLER TOOL **UNIVERSAL**', 'line_item', 38),
(4, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 39),
(5, 'YUKON GEAR & AXLE CLAMSHELL RETENTION SLEEVE CARRIER BEARING PULLER **UNIVERSAL**', 'line_item', 40),
(6, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 41),
(7, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 42),
(8, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 43),
(9, '12V AUXILIARY WIRING KIT W/ ILLUMINATED SWITCH *UNIVERSAL*', 'line_item', 43),
(10, 'YUKON GEAR & AXLE AXLE BEARING PULLER TOOL **UNIVERSAL**', 'line_item', 43),
(11, 'YUKON GEAR & AXLE AXLE BEARING PULLER TOOL **UNIVERSAL**', 'line_item', 48),
(12, 'YUKON GEAR & AXLE CLAMSHELL RETENTION SLEEVE CARRIER BEARING PULLER **UNIVERSAL**', 'line_item', 50),
(13, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 114),
(14, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 115),
(15, 'YUKON GEAR & AXLE CLAMSHELL RETENTION SLEEVE CARRIER BEARING PULLER **UNIVERSAL**', 'line_item', 118),
(16, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 143),
(17, '12 Volt DC 30 Amp Relay *Universal*', 'line_item', 144),
(18, '12V AUXILIARY WIRING KIT W/ ILLUMINATED SWITCH *UNIVERSAL*', 'line_item', 145);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokenmeta`
--

CREATE TABLE `wp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_payment_tokens`
--

CREATE TABLE `wp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_sessions`
--

CREATE TABLE `wp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `wp_woocommerce_sessions`
--

INSERT INTO `wp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(194, '1', 'a:1:{s:8:\"customer\";s:943:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2018-10-10T08:58:40+00:00\";s:8:\"postcode\";s:9:\"78965-411\";s:4:\"city\";s:7:\"ahmedab\";s:9:\"address_1\";s:23:\"606 Rocky Second Street\";s:7:\"address\";s:23:\"606 Rocky Second Street\";s:9:\"address_2\";s:81:\"Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et\";s:5:\"state\";s:2:\"AP\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"AP\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:7:\"Martina\";s:9:\"last_name\";s:8:\"Erickson\";s:7:\"company\";s:23:\"Valentine and Sloan LLC\";s:5:\"phone\";s:15:\"+442-68-9152410\";s:5:\"email\";s:24:\"qogybyzud@mailinator.net\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1539342947),
(192, '2', 'a:1:{s:8:\"customer\";s:811:\"a:26:{s:2:\"id\";s:1:\"2\";s:13:\"date_modified\";s:25:\"2018-10-09T16:26:34+00:00\";s:8:\"postcode\";s:6:\"382345\";s:4:\"city\";s:7:\"ahmedab\";s:9:\"address_1\";s:7:\"B - 102\";s:7:\"address\";s:7:\"B - 102\";s:9:\"address_2\";s:9:\"Panchgini\";s:5:\"state\";s:2:\"GJ\";s:7:\"country\";s:2:\"IN\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"GJ\";s:16:\"shipping_country\";s:2:\"IN\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:6:\"Snehal\";s:9:\"last_name\";s:5:\"Gohel\";s:7:\"company\";s:9:\"citrusbug\";s:5:\"phone\";s:10:\"8899665588\";s:5:\"email\";s:26:\"snehal.citrusbug@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1539342829),
(86, '5941d7d4433f9c348bec7fa0fe53a570', 'a:7:{s:4:\"cart\";s:6:\"a:0:{}\";s:11:\"cart_totals\";s:367:\"a:15:{s:8:\"subtotal\";i:0;s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";i:0;s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";i:0;s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";i:0;s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";i:0;s:9:\"total_tax\";i:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:8:\"customer\";s:687:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"IN\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"IN\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1539244147),
(196, 'e29c0f94bb6503cadc700e51621ac561', 'a:8:{s:4:\"cart\";s:412:\"a:1:{s:32:\"182be0c5cdcd5072bb1864cdee4d3d6e\";a:11:{s:3:\"key\";s:32:\"182be0c5cdcd5072bb1864cdee4d3d6e\";s:10:\"product_id\";i:33;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:150;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:150;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:408:\"a:15:{s:8:\"subtotal\";s:6:\"150.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:4:\"0.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"150.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"150.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:10:\"wc_notices\";N;s:8:\"customer\";s:943:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2018-10-10T08:58:40+00:00\";s:8:\"postcode\";s:9:\"78965-411\";s:4:\"city\";s:7:\"ahmedab\";s:9:\"address_1\";s:23:\"606 Rocky Second Street\";s:7:\"address\";s:23:\"606 Rocky Second Street\";s:9:\"address_2\";s:81:\"Omnis est aperiam at aliquip officia officia labore quos iusto est perferendis et\";s:5:\"state\";s:2:\"AP\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"AP\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:7:\"Martina\";s:9:\"last_name\";s:8:\"Erickson\";s:7:\"company\";s:23:\"Valentine and Sloan LLC\";s:5:\"phone\";s:15:\"+442-68-9152410\";s:5:\"email\";s:24:\"qogybyzud@mailinator.net\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1539343003);

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zones`
--

CREATE TABLE `wp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_locations`
--

CREATE TABLE `wp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_shipping_zone_methods`
--

CREATE TABLE `wp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rates`
--

CREATE TABLE `wp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wp_woocommerce_tax_rate_locations`
--

CREATE TABLE `wp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `wp_nextend2_image_storage`
--
ALTER TABLE `wp_nextend2_image_storage`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `hash` (`hash`);

--
-- Indexes for table `wp_nextend2_section_storage`
--
ALTER TABLE `wp_nextend2_section_storage`
  ADD PRIMARY KEY (`id`),
  ADD KEY `application` (`application`,`section`,`referencekey`),
  ADD KEY `application_2` (`application`,`section`);

--
-- Indexes for table `wp_nextend2_smartslider3_generators`
--
ALTER TABLE `wp_nextend2_smartslider3_generators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nextend2_smartslider3_sliders`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_nextend2_smartslider3_sliders_xref`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders_xref`
  ADD PRIMARY KEY (`group_id`,`slider_id`);

--
-- Indexes for table `wp_nextend2_smartslider3_slides`
--
ALTER TABLE `wp_nextend2_smartslider3_slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`);

--
-- Indexes for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  ADD PRIMARY KEY (`session_key`),
  ADD UNIQUE KEY `session_id` (`session_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_nextend2_image_storage`
--
ALTER TABLE `wp_nextend2_image_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `wp_nextend2_section_storage`
--
ALTER TABLE `wp_nextend2_section_storage`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10021;

--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_generators`
--
ALTER TABLE `wp_nextend2_smartslider3_generators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_sliders`
--
ALTER TABLE `wp_nextend2_smartslider3_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_nextend2_smartslider3_slides`
--
ALTER TABLE `wp_nextend2_smartslider3_slides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=822;

--
-- AUTO_INCREMENT for table `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1418;

--
-- AUTO_INCREMENT for table `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=153;

--
-- AUTO_INCREMENT for table `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=118;

--
-- AUTO_INCREMENT for table `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_wc_webhooks`
--
ALTER TABLE `wp_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_api_keys`
--
ALTER TABLE `wp_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_attribute_taxonomies`
--
ALTER TABLE `wp_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `wp_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `wp_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_log`
--
ALTER TABLE `wp_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_itemmeta`
--
ALTER TABLE `wp_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=163;

--
-- AUTO_INCREMENT for table `wp_woocommerce_order_items`
--
ALTER TABLE `wp_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokenmeta`
--
ALTER TABLE `wp_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_payment_tokens`
--
ALTER TABLE `wp_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_sessions`
--
ALTER TABLE `wp_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=197;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zones`
--
ALTER TABLE `wp_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_locations`
--
ALTER TABLE `wp_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_shipping_zone_methods`
--
ALTER TABLE `wp_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rates`
--
ALTER TABLE `wp_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wp_woocommerce_tax_rate_locations`
--
ALTER TABLE `wp_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `wp_wc_download_log`
--
ALTER TABLE `wp_wc_download_log`
  ADD CONSTRAINT `fk_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `wp_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
